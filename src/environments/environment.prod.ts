export const environment = {
  production: true,
  baseURL: 'https://api.eleveglobal.com/api',
  baseURLCommon: 'https://api.eleveglobal.com'
};
