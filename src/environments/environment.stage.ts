export const environment = {
    production: true,
    baseURL: 'https://demo-api.eleveglobal.com/api',
    baseURLCommon: 'https://demo-api.eleveglobal.com'
  };