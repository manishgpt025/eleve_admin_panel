import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Options } from 'ng5-slider';

import { AdminService, AuthService, ToastsService } from 'src/app/services';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
})
export class UpdatePasswordComponent implements OnInit {
  updatePasswordForm: FormGroup;
  message: string;
  data: any;

  constructor(
    private auth: AuthService, 
    private router: Router,
    private fb: FormBuilder, 
    private apiService: AdminService, 
    private el: ElementRef,
    private toastsService: ToastsService
  ) {}

  ngOnInit() {
    this.updateFormVal();
  }
  updateFormVal() {
    this.updatePasswordForm = new FormGroup({
      newpassword: new FormControl('', Validators.compose([Validators.required, Validators.minLength(8)])),
      confirmnewpassword: new FormControl('', Validators.compose([Validators.required])),
    });
  }
  onSubmitupdatePasswordForm(data) {
    if (this.updatePasswordForm.invalid) {
      return;
    }
    let dataid = this.auth.getData();

    let reqData = {
      'id' : dataid._id,
      'password' : data.newpassword
    }
    this.apiService.show();
    this.apiService.postApiCommon('/campaign/admin/password_update',reqData).subscribe((response: any) => {
      if (response.status == 200) {
        this.apiService.hide();
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.toastsService.success(this.data.responseMessage);
          this.router.navigate(['admin-profile']);
            // console.log(this.data.responseMessage);

        } else if(this.data.responseCode == 409) {
          this.message = this.data.responseMessage;
          this.toastsService.error(this.data.responseMessage);
          this.apiService.hide();
          // console.log(this.data.responseMessage);
        } else {
          this.message = this.data.responseMessage;
          // console.log(this.data.responseMessage)
        }
      }
    }, error => {
      this.apiService.hide();
      this.message = "Something went wrong! Please try again.";
    });
  }
}
