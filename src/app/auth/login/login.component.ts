import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, FormControl, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Options } from 'ng5-slider';

import { AdminService, AuthService, ToastsService } from 'src/app/services';
import { validEmailPattern } from "../../constants";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  showEye: boolean = false;
  loading: boolean = false;

  constructor(
    private router: Router,
    private apiService: AdminService,
    private authService: AuthService,
    private toastsService: ToastsService
  ) { }

  ngOnInit() {
    this.createLoginForm();
  }

  createLoginForm = (): void => {
    this.loginForm = new FormGroup({
      email: new FormControl({ value: "", disabled: false }, Validators.compose(
        [
          Validators.required,
          Validators.pattern(validEmailPattern)
        ]
      )),
      password: new FormControl({ value: "", disabled: false }, Validators.compose([Validators.required]))
    });
  };

  toggleFormDisablity = (action): void => {
    this.loading = action === "disable";
    this.loginForm.controls["email"][action]();
    this.loginForm.controls["password"][action]();
  };

  //submit login form
  onSubmitLoginForm = (formData): void => {
    if (this.loginForm.invalid)return;
    const params = {
      email: formData.email,
      password: formData.password
    };
    this.toggleFormDisablity("disable");
    this.apiService.postApiLogin("/campaign/adminlogin", params)
      .subscribe((response: any) => {
        this.toggleFormDisablity("enable");
        const { status } = response;
        if (status === 200) {
          const { body: {
              token,
              responseCode,
              result,
              responseMessage
            },
          } = response;
          if (responseCode === 200) {
            this.authService.sendToken(token, result);
            const { first_login: firstLogin } = result;
            if (firstLogin === "1") {
              this.router.navigate(["update-password"]);
            } else {
              this.router.navigate(["admin-profile"]);
            }
          } else if (responseCode === 505) {
            this.router.navigate(["403"]);
          } else {
            this.toastsService.error(responseMessage);
          }
        }
      }, error => {
        this.toggleFormDisablity("enable");
        this.toastsService.error("Something went wrong! Please try again.")
      }
    );
  };

 showPassword = (): void => {
   this.showEye = !this.showEye;
 };

}
