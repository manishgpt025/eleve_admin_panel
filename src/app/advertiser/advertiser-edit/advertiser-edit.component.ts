import { Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { MatSelect, MatSelectChange } from '@angular/material';
import { Validators, FormControl, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Ng2ImgMaxService } from 'node_modules/ng2-img-max';

import { AdminService, AuthService } from 'src/app/services';

@Component({
  selector: 'app-advertiser-edit',
  templateUrl: './advertiser-edit.component.html',
})
export class AdvertiserEditComponent implements OnInit {
  @ViewChild('s',{static:true}) myorgansiationForm
  show_model:boolean=false;
  advertiserForm: FormGroup;
  countrylist: string[] = [];
  statelist: string[] = [];
  citylist: string[] = [];
  organisationlist: object[] = [];
  asscosiativeListData: object[] = [];
  isdlist: string[] = [];
  advertiserAsscosiativeList: object[] = [];
  data:any;
  message:string;
  country: any;
  city: any;
  state: any;
  image: HTMLImageElement;
  imageurl: string = "assets/images/user.jpg";
  imageurlUpload: string;
  getAdvertiserData: any;
  advertiserId: any;

  show_modal:boolean=false;
  show_step1:boolean=true;
  show_brand:boolean=false;
  show_brand_yes:boolean=false;
  show_brand_agency:boolean=false;

  organsiationForm: FormGroup;
  organisationTypeValue: any = "";
  brandExists: any;
  error_note: string  ="";
  error_fb: string  ="";
  error_instagram: string  ="";
  error_twitter: string  ="";
  error_blog: string  ="";
  error_pinterest: string  ="";
  error_linkedin: string  ="";
  error_youtube: string  ="";
  error_snapchat: string ="";
  socialaccount: string = "0";
  error_social: boolean = false ;
  i: number;
  facebookArray: any =[];
  instagramArray: any =[];
  twitterArray: any=[];
  blogArray: any=[];
  pinterestArray: any=[];
  youtubeArray: any=[];
  linkedinArray: any=[];
  snapchatArray: any=[];
  associativebrandMark: string= '0'
  listNameBrand: any;
  BrandValueAndNameList: any;
  showSelect: boolean = false;
  mobile_code: any = "";
  hidden_code: any;
  managelistData: any[];
  getOrganitionAddressData: any[];
  address_id: string = "";
  old_address_id: string = "";
  show_modal_associated_brand: boolean = false;
  userId: string = "";

  constructor(
      private route: ActivatedRoute,
      private fb: FormBuilder,
      private _service: AdminService,
      private ng2ImgMaxService: Ng2ImgMaxService,
      private auth: AuthService,
      private router:Router
  ){}

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.advertiserId=params.id;
    });
    this.editAdvertiserFormVal();
    this.getAdvertiserDetail();
    this.getOrganisationLists();
    this.asscosiativeList();
    this.getCountryList();
    this.getISDList();
    this.managerlist();
    this.organisationFormVal();

    this.show_step1 = true;
    this.show_brand = false;
    this.show_brand_yes = false;
    this.show_brand_agency = false;
    this.setDefaultValues();

  }

  getLocalhostData = (): any => {
    if(!this.userId){
      return this.userId = this.auth.getData()._id;
    }
    return this.userId;
  }

  managerlist = ():void => {
      this.managelistData = [];
      this._service.getApiCommon('/campaign/userlist')
      .subscribe((response: any) => {
        if (response.status == 200) {
          this.data = response.body;
          if (this.data.responseCode == 200) {
            this.managelistData = this.data.result;
          }
        }
      }, error => {
        this._service.error("Something went wrong! Please try again.");
      });
  }

  getAdvertiserDetail = ():any => {
    let reqData ={"user_id": this.advertiserId}
    this._service.postApiCommon('/campaign/get-advertiser',reqData)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.getAdvertiserData = this.data.result;
          this.advertiserForm.get('first_name').setValue(this.getAdvertiserData.name);
          this.advertiserForm.get('last_name').setValue(this.getAdvertiserData.last_name);
          this.advertiserForm.get('designation').setValue(this.getAdvertiserData.designation);
          this.advertiserForm.get('email').setValue(this.getAdvertiserData.email);
          this.advertiserForm.get('isd_code').setValue(this.getAdvertiserData.isd);
          this.advertiserForm.get('mobile').setValue(this.getAdvertiserData.phone);
          this.advertiserForm.get('organisation').setValue(this.getAdvertiserData.organisation_id);
          this.advertiserForm.get('manager').setValue(this.getAdvertiserData.manager);
          this.advertiserForm.get('note').setValue(this.getAdvertiserData.notes);
          this.advertiserForm.get('bill_address').setValue(this.getAdvertiserData.address_id);
          if(this.getAdvertiserData.isd){
            this.mobile_code = this.getAdvertiserData.isd
          }else this.mobile_code = '';
          if(this.getAdvertiserData.brand_details){
            const creds = <FormArray>this.advertiserForm.controls['asscosiativeBrandList'];
            const credsName = <FormArray>this.advertiserForm.controls['asscosiativeBrandListValue'];
            for (let i = 0; i < this.getAdvertiserData.brand_details.length; i++) {
              creds.push(new FormControl(this.getAdvertiserData.brand_details[i]._id));
              credsName.push(new FormControl(this.getAdvertiserData.brand_details[i].organisation_name));
            }
          }
          if(this.getAdvertiserData.profile_image){
            this.imageurl = this.getAdvertiserData.profile_image;
          }
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  //Advertiser add form validation
  editAdvertiserFormVal = ():any => {
      this.advertiserForm = new FormGroup({
        first_name: new FormControl('', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(20), Validators.pattern('^[a-zA-Z0-9 ]*$')])),
        last_name: new FormControl('', Validators.compose([Validators.minLength(1), Validators.maxLength(20), Validators.pattern('^[a-zA-Z0-9 ]*$')])),
        country: new FormControl({value: '', disabled: true}),
        state: new FormControl({value: '', disabled: true}),
        city: new FormControl({value: '', disabled: true}),
        bill_address: new FormControl('', Validators.compose([Validators.required])),
        address: new FormControl({value: '', disabled: true}),
        organisation: new FormControl('', Validators.compose([Validators.required])),
        designation: new FormControl(''),
        manager : new FormControl(''),
        note : new FormControl(''),
        email: new FormControl({ value: '', disabled: true }),
        isd_code: new FormControl(''),
        mobile: new FormControl('', Validators.compose([Validators.minLength(4), Validators.maxLength(14), Validators.pattern('[0-9]*')])),
        asscosiativeBrand: new FormControl(''),
        asscosiativeBrandList: this.fb.array([]),
        asscosiativeBrandListValue: this.fb.array([])
      });
  }

  // change state list behalf country select
  countrySelectChange = selectCountryName => {
    if(selectCountryName){
      this.advertiserForm.get('state').setValue('');
      this.advertiserForm.get('city').setValue('');
      this.country = selectCountryName;
      this.statelist = [];
      this.citylist = [];
      this.getStateList();
    }
  }

  // state change behalf country
  stateSelectChange = selectStateName => {
      if (selectStateName){
          this.advertiserForm.get('city').setValue('');
          this.state = selectStateName;
          this.citylist = [];
          this.getCityList();
      }
  }

  // Get country list from database
  getCountryList = ():void => {
    this.countrylist = [];
    this._service.getApiCommon('/api/countries')
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.countrylist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.countrylist.push(this.data.result[i].name);
          }
        } else {
          this.countrylist = [];
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });

  }

  // Get state list from database
  getStateList = ():void => {
  let data = {
    'country': this.country
  }
  this.statelist = [];
    this._service.postApiCommon('/api/statelist', data)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.statelist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.statelist.push(this.data.result[i].name);
          }
        } else {
          this.statelist = [];
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });

  }

  // Get city List from database
  getCityList = ():void => {
    let data = {
      'state': this.state
    }
    this.citylist = [];
    this._service.postApiCommon('/api/cities', data)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.citylist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.citylist.push(this.data.result[i].name);
          }
        } else {
          this.citylist = [];
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  // Get organisation list from database
  getOrganisationLists = ():void => {
    this.organisationlist = [];
    let adminId = this.getLocalhostData();
    let reqData =
      {
        "user_id":adminId,
        "type":"1"
      }
    this._service.postApiCommon('/campaign/admin/search-organisation',reqData)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.organisationlist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.organisationlist.push({id: this.data.result[i]._id, name: this.data.result[i].organisation_name})
          }
        } else {
          this.organisationlist = [];
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  // get ISD List for mobile numbers
  getISDList = ():void => {
    this._service.getApiCommon('/api/country/isd')
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.isdlist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.isdlist.push(this.data.result[i]);
          }
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

// get asscosiative brand List
  asscosiativeList = ():void => {
    this.asscosiativeListData = [];
    let adminId = this.getLocalhostData();
    let reqData =
      {
        "user_id":adminId,
        "type":"3"
      }
    this._service.postApiCommon('/campaign/admin/search-organisation',reqData)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.BrandValueAndNameList = this.data.result;
          this.asscosiativeListData = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.asscosiativeListData.push({id: this.data.result[i]._id, name: this.data.result[i].organisation_name})
          }
        } else {
          this.asscosiativeListData = [];
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  //Add Aarry in form data
  addBrandFormArray = (event) => {
    if(this.BrandValueAndNameList){
      this.listNameBrand =this.BrandValueAndNameList.filter(list => list._id == event);
    }
    const creds = <FormArray>this.advertiserForm.controls['asscosiativeBrandList'];
    const credsName = <FormArray>this.advertiserForm.controls['asscosiativeBrandListValue'];
    const duplicate = this.advertiserForm.get('asscosiativeBrandList').value;
    if(duplicate.filter(x => x == event) == ""){
      creds.push(new FormControl(this.advertiserForm.get('asscosiativeBrand').value))
      if(this.listNameBrand.length > 0)
        credsName.push(new FormControl(this.listNameBrand[0].organisation_name));
    }
  }

//Remove Aarry in form data
  removeBrandFormArray = index => {
    const creds = <FormArray>this.advertiserForm.controls['asscosiativeBrandList'];
    const credsName = <FormArray>this.advertiserForm.controls['asscosiativeBrandListValue'];
    creds.removeAt(index);
    credsName.removeAt(index);
  }

 //Show dynamically profile image change and convert , image validation
  onImageChange = (event):any => {
    if (event.target.files && event.target.files[0]) {
    let image = event.target.files[0];
      this.ng2ImgMaxService.resizeImage(image, 400, 300).subscribe(result => {
        var reader = new FileReader();
        reader.readAsDataURL(result); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.image = new Image();
          this.image.src = event.target.result;
          this.imageurl = this.image.src;
          this.imageurlUpload = this.image.src;
        }
      }, error => {
        this._service.error(error.reason)
      })
    }
  }

  //Get organization detail Address from Id
  getOrganisationAddressDetail = event => {
    this.getOrganitionAddressData = [];
    let reqData =
    {
      "id":event
    }
  this._service.postApiCommon('/api/organisation/details',reqData)
  .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.getOrganitionAddressData = this.data.result.organisationInfo.addresses;
          if(this.data.result.organisationInfo.type == 'brand'){
            this.show_modal_associated_brand = false;
          }else{
            this.show_modal_associated_brand = true;
          }
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  //Location disabled or enabled
  changeAddressFunction = (event):void => {
    if(event && (event == 'location')){
      this.advertiserForm.controls['country'].enable();
      this.advertiserForm.controls['state'].enable();
      this.advertiserForm.controls['city'].enable();
      this.advertiserForm.controls['address'].enable();
    }else{
      this.advertiserForm.get('country').setValue('');
      this.advertiserForm.get('state').setValue('');
      this.advertiserForm.get('city').setValue('');
      this.advertiserForm.get('address').setValue('');
      this.advertiserForm.controls['country'].disable();
      this.advertiserForm.controls['state'].disable();
      this.advertiserForm.controls['city'].disable();
      this.advertiserForm.controls['address'].disable();
    }
  }

  // Submit for advertiser creation
  onSubmitAdvertiserForm = (resData):any => {
    if(resData.bill_address == 'location'){
      this.advertiserForm.get('country').setValidators(Validators.compose([Validators.required]));
      this.advertiserForm.get('country').updateValueAndValidity();
      this.advertiserForm.get('state').setValidators(Validators.compose([Validators.required]));
      this.advertiserForm.get('state').updateValueAndValidity();
      this.advertiserForm.get('address').setValidators(Validators.compose([Validators.required]));
      this.advertiserForm.get('address').updateValueAndValidity();
      this.old_address_id = this.getAdvertiserData.address_id;
    }else{
      this.address_id = resData.bill_address;
      this.advertiserForm.get('country').setValidators(null);
      this.advertiserForm.get('country').updateValueAndValidity();
      this.advertiserForm.get('state').setValidators(null);
      this.advertiserForm.get('state').updateValueAndValidity();
      this.advertiserForm.get('city').setValidators(null);
      this.advertiserForm.get('city').updateValueAndValidity();
      this.advertiserForm.get('address').setValidators(null);
      this.advertiserForm.get('address').updateValueAndValidity();
    }

    if (this.advertiserForm.invalid) return;
    let reqData = {
      "user_id":this.advertiserId,
      "first_name": resData.first_name,
      "last_name": resData.last_name,
      "email": this.getAdvertiserData.email,
      "isd": resData.isd_code,
      "mobile": resData.mobile,
      "status": 1,
      "organisation_id": resData.organisation,
      "manager": resData.manager,
      "address": resData.address,
      "country": resData.country,
      "state": resData.state,
      "city": resData.city,
      "associated_brands": resData.asscosiativeBrandList,
      "profilePic": this.imageurlUpload,
      "notes": resData.note,
      "designation": resData.designation,
      "address_id": this.address_id,
      "old_address_id":this.old_address_id || ""
    }
    this._service.postApiCommon('/api/advertiseredit',reqData)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this._service.success(this.data.responseMessage);
          this.router.navigate(['advertiser']);
        } else {
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

// **************** All Functionality for Organisation modal and direct , associated brand*************//

  organisationFormVal = ():any =>{
    this.organsiationForm = new FormGroup({
      organisation_name: new FormControl('', Validators.compose([Validators.required])),
      orgtype: new FormControl(),
      brandexists: new FormControl(),
      facebook: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      instagram: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      twitter: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      blog: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      pinterest: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      youtube: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      linkedin: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      snapchat: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
    });
  }

  showModel = ():void => {
    this.show_modal = true;
  }

  setDefaultValues = ():void => {
    this.organsiationForm.patchValue({ orgtype: 'agency' });
  }

  // Get organisation list from database
  getOrganisationList = ():void => {
    this.organisationlist = [];
    let adminId = this.getLocalhostData();
    let reqData =
    {
      "user_id": adminId,
      "type": "3"
    }
    this._service.postApiCommon('/campaign/admin/search-organisation', reqData)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.organisationlist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.organisationlist.push(this.data.result[i].organisation_name);
          }
        } else {
          this.organisationlist = [];
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });

  }

  handleChange = (evt) => {
    let target = evt.target;
    if (target.checked) {
      this.show_step1 = false;
      this.organisationTypeValue = target.value;
      if (this.organisationTypeValue == "agency") {
        this.show_brand_agency = true;
        this.show_brand = false;
      } else {
        this.show_brand_agency = false;
        this.show_brand = true;
      }
    }
  }

  handleChangeBrand = (evt) => {
    let target = evt.target;
    if (target.checked) {
      this.show_brand = false;
      this.brandExists = target.value;
      if (this.brandExists == "yes") {
        this.show_brand_agency = false;
        this.show_brand_yes = true;
        this.getOrganisationList();
      } else {
        this.show_brand_yes = false;
        this.show_brand_agency = true;
      }
    }
  }

  // facebook rows
  get facebookRows() {
    return this.organsiationForm.get('facebook') as FormArray;
  }

  addfacebookRows = ():void => {
    this.facebookRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // twitter rows
  get twitterRows() {
    return this.organsiationForm.get('twitter') as FormArray;
  }

  addtwitterRows = ():void => {
    this.twitterRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // instagram rows
  get instagramRows() {
    return this.organsiationForm.get('instagram') as FormArray;
  }

  addinstagramRows = ():void => {
    this.instagramRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // blog rows
  get blogRows() {
    return this.organsiationForm.get('blog') as FormArray;
  }

  addblogRows = ():void => {
    this.blogRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // pinterestRows
  get pinterestRows() {
    return this.organsiationForm.get('pinterest') as FormArray;
  }

  addpinterestRows = ():void => {
    this.pinterestRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // youtubeRows
  get youtubeRows() {
    return this.organsiationForm.get('youtube') as FormArray;
  }

  addyoutubeRows = ():void => {
    this.youtubeRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // linkedinRows
  get linkedinRows() {
    return this.organsiationForm.get('linkedin') as FormArray;
  }

  addlinkedinRows = ():void => {
    this.linkedinRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // snapchatRows
  get snapchatRows() {
    return this.organsiationForm.get('snapchat') as FormArray;
  }

  addsnapchatRows = ():void => {
    this.snapchatRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  onOrganisationFormSubmit = (data):any => {
    if (this.organsiationForm.invalid)return;
    if(this.associativebrandMark == "1"){
      this.organsiationForm.get('orgtype').setValue('brand');
      data.orgtype = 'Brand';
    }
    if (this.organsiationForm.get('orgtype').value == "brand") {
      if (this.socialaccount == "0") {
        this.error_social = true;
        return;
      } else {
        this.error_social = false;
      }
    }
    if (this.error_note !== "") return;
    if (data.facebook.length > 0) {
      for (this.i = 0; this.i < data.facebook.length; this.i++) {
        if (data.facebook[this.i].handle !== "" && data.facebook[this.i].url !== "") {
          this.facebookArray.push({ "handle": data.facebook[this.i].handle, "url": data.facebook[this.i].url });
        }
      }
    }

    if (data.instagram.length > 0) {
      for (this.i = 0; this.i < data.instagram.length; this.i++) {
        if (data.instagram[this.i].handle !== "" && data.instagram[this.i].url !== "") {
          this.instagramArray.push({ "handle": data.instagram[this.i].handle, "url": data.instagram[this.i].url });
        }
      }
    }

    if (data.twitter.length > 0) {
      for (this.i = 0; this.i < data.twitter.length; this.i++) {
        if (data.twitter[this.i].handle !== "" && data.twitter[this.i].url !== "") {
          this.twitterArray.push({ "handle": data.twitter[this.i].handle, "url": data.twitter[this.i].url });
        }
      }
    }

    if (data.blog.length > 0) {
      for (this.i = 0; this.i < data.blog.length; this.i++) {
        if (data.blog[this.i].handle !== "" && data.blog[this.i].url !== "") {
          this.blogArray.push({ "handle": data.blog[this.i].handle, "url": data.blog[this.i].url });
        }
      }
    }

    if (data.pinterest.length > 0) {
      for (this.i = 0; this.i < data.pinterest.length; this.i++) {
        if (data.pinterest[this.i].handle !== "" && data.pinterest[this.i].url !== "") {
          this.pinterestArray.push({ "handle": data.pinterest[this.i].handle, "url": data.pinterest[this.i].url });
        }
      }
    }

    if (data.youtube.length > 0) {
      for (this.i = 0; this.i < data.youtube.length; this.i++) {
        if (data.youtube[this.i].handle !== "" && data.youtube[this.i].url !== "") {
          this.youtubeArray.push({ "handle": data.youtube[this.i].handle, "url": data.youtube[this.i].url });
        }
      }
    }

    if (data.linkedin.length > 0) {
      for (this.i = 0; this.i < data.linkedin.length; this.i++) {
        if (data.linkedin[this.i].handle !== "" && data.linkedin[this.i].url !== "") {
          this.linkedinArray.push({ "handle": data.linkedin[this.i].handle, "url": data.linkedin[this.i].url });
        }
      }
    }

    if (data.snapchat.length > 0) {
      for (this.i = 0; this.i < data.snapchat.length; this.i++) {
        if (data.snapchat[this.i].handle !== "" && data.snapchat[this.i].url !== "") {
          this.snapchatArray.push({ "handle": data.snapchat[this.i].handle, "url": data.snapchat[this.i].url });
        }
      }
    }
    let arrayData = {
      "organisation_name": data.organisation_name,
      "type": data.orgtype,
      "social_media": [{
        "facebook": this.facebookArray,
        "instagram": this.instagramArray,
        "twitter": this.twitterArray,
        "blog": this.blogArray,
        "pinterest": this.pinterestArray,
        "youtube": this.youtubeArray,
        "linkedin": this.linkedinArray,
        "snapchat": this.snapchatArray,
      }],
      "associated_brands": [],
      "is_asscoiated": this.associativebrandMark
    }
    this._service.postApiCommon('/campaign/admin/add-organisation', arrayData)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          if(this.associativebrandMark == '1'){
            this.asscosiativeList();
            this._service.success("Associated brand added successfully.");
          }else{
            this.getOrganisationLists();
            this._service.success("Organisation created successfully.");
          }
          this.hideModel();
        } else if (this.data.responseCode == 409) {
          this._service.error(this.data.responseMessage);
        } else {
          this._service.error("Organisation not Created");
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  checkValidHandle = (handle, index, platform) => {
    let format = /[ !#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
    let res = format.test(handle);
    if (res == true) {
      this.error_note = "A valid handle must not have spaces";
    } else {
      this.error_note = "";
    }
    switch (platform) {
      case 'instagram': {
        this.error_instagram = this.error_note;
        break;
      }
      case 'twitter': {
        this.error_twitter = this.error_note;
        break;
      }
      default: {
        break;
      }
    }
  }

  checkUrl = (url, platform, socialarray, index) => {
    let handle = socialarray.value[index].handle;
    if (url !== "") {
      let res = url.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
      if (res == null) {
        this.error_note = "Provide a valid URL";
      } else {
        if (url.indexOf(platform) !== -1) {
          if (platform == 'instagram' || platform == 'twitter') {
            this.checkValidHandle(handle, index, platform);
            if (this.error_note == "") {
              if (url.indexOf(handle) !== -1) {
                this.error_note = "";
                this.socialaccount = "1";
              } else {
                this.error_note = "URL does not belong to the same habndle.";
              }
            }
          } else {
            this.error_note = "";
            this.socialaccount = "1";
          }
        } else {
          this.error_note = "Match URL domain with the platform";
        }
      }
    }

    switch (platform) {
      case 'facebook': {
        this.error_fb = this.error_note;
        break;
      }
      case 'instagram': {
        this.error_instagram = this.error_note;
        break;
      }
      case 'twitter': {
        this.error_twitter = this.error_note;
        break;
      }
      case 'blog': {
        this.error_blog = this.error_note;
        break;
      }
      case 'pinterest': {
        this.error_pinterest = this.error_note;
        break;
      }
      case 'youtube': {
        this.error_youtube = this.error_note;
        break;
      }
      case 'linkedin': {
        this.error_linkedin = this.error_note;
        break;
      }
      case 'snapchat': {
        this.error_snapchat = this.error_note;
        break;
      }

      default: {
        //statements;
        break;
      }
    }
  }

  cancelForm = ():void => {
    this.myorgansiationForm.resetForm();
  }

  hideModel = () => {
    this.show_modal = false;
    this.show_step1 = true;
    this.show_brand = false;
    this.show_brand_yes = false;
    this.show_brand_agency = false;
    this.myorgansiationForm.resetForm();
    this.associativebrandMark = '0';
  }

  onSelectChange = () => {
    let organisation_name = this.organsiationForm.get('organisation_name').value;
    let arrayData = {
      "organisation_name": organisation_name
    }
    this._service.postApiCommon('/campaign/admin/get-organisation', arrayData)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          if (this.data.result.social_media !== undefined) {
            if (this.data.result.social_media[0].facebook !== undefined) {
              let len = this.data.result.social_media[0].facebook.length;
              for (this.i = 0; this.i < len; this.i++) {
                this.facebookRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].facebook[this.i].handle, url: this.data.result.social_media[0].facebook[this.i].url }));
              }
              this.facebookRows.removeAt(0);
            }
            if (this.data.result.social_media[0].instagram !== undefined) {
              let len = this.data.result.social_media[0].instagram.length;
              for (this.i = 0; this.i < len; this.i++) {
                this.instagramRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].instagram[this.i].handle, url: this.data.result.social_media[0].instagram[this.i].url }));
              }
              this.instagramRows.removeAt(0);
            }
            if (this.data.result.social_media[0].twitter !== undefined) {
              let len = this.data.result.social_media[0].twitter.length;
              for (this.i = 0; this.i < len; this.i++) {
                this.twitterRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].twitter[this.i].handle, url: this.data.result.social_media[0].twitter[this.i].url }));
              }
              this.twitterRows.removeAt(0);
            }
            if (this.data.result.social_media[0].blog !== undefined) {
              let len = this.data.result.social_media[0].blog.length;
              for (this.i = 0; this.i < len; this.i++) {
                this.blogRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].blog[this.i].handle, url: this.data.result.social_media[0].blog[this.i].url }));
              }
              this.blogRows.removeAt(0);
            }
            if (this.data.result.social_media[0].pinterest !== undefined) {
              let len = this.data.result.social_media[0].pinterest.length;
              for (this.i = 0; this.i < len; this.i++) {
                this.pinterestRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].pinterest[this.i].handle, url: this.data.result.social_media[0].pinterest[this.i].url }));
              }
              this.pinterestRows.removeAt(0);
            }
            if (this.data.result.social_media[0].youtube !== undefined) {
              let len = this.data.result.social_media[0].youtube.length;
              for (this.i = 0; this.i < len; this.i++) {
                this.youtubeRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].youtube[this.i].handle, url: this.data.result.social_media[0].youtube[this.i].url }));
              }
              this.youtubeRows.removeAt(0);
            }
            if (this.data.result.social_media[0].linkedin !== undefined) {
              let len = this.data.result.social_media[0].linkedin.length;
              for (this.i = 0; this.i < len; this.i++) {
                this.linkedinRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].linkedin[this.i].handle, url: this.data.result.social_media[0].linkedin[this.i].url }));
              }
              this.linkedinRows.removeAt(0);
            }
            if (this.data.result.social_media[0].snapchat !== undefined) {
              let len = this.data.result.social_media[0].snapchat.length;
              for (this.i = 0; this.i < len; this.i++) {
                this.snapchatRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].snapchat[this.i].handle, url: this.data.result.social_media[0].snapchat[this.i].url }));
              }
              this.snapchatRows.removeAt(0);
            }
          }
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

// *********************************************open new brand create************************************************
  openAssociativeBrandModal = ():void => {
    this.show_modal = true;
    this.show_step1 = false;
    this.show_brand = false;
    this.show_brand_yes = false;
    this.show_brand_agency = true;
    this.associativebrandMark = '1';
    this.organisationTypeValue = "brand";
  }

  onFirstThingSelect = (event: MatSelectChange) => {
    if(event.value==this.mobile_code)return;
    this.mobile_code = event.value;
    this.hidden_code = event.value;
    this.showSelect = false;
  }

  onNoChangeSelect = (event: MatSelect):void => {
    this.mobile_code = this.hidden_code;
    this.showSelect = false;
  }

  showSelectOption = ():void => {
    this.showSelect = true;
  }

  openedChange = (opened: boolean):void => {
    if(!opened){
    this.showSelect = false;
    }
  }
}
