import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateAdvertiserComponent } from './create-advertiser/create-advertiser.component';
import { LayoutComponent } from './layout/layout.component';
import { AdvertiserListComponent } from './advertiser-list/advertiser-list.component';
import { AdvertiserDetailsComponent } from './advertiser-details/advertiser-details.component';
import {AdvertiserEditComponent} from './advertiser-edit/advertiser-edit.component';
import { OrganisationDetailsComponent } from './organisation-details/organisation-details.component';

const routes: Routes = [
  {
    path: "", component: LayoutComponent,
    children: [
      { path: "", component: AdvertiserListComponent },
      { path: "create", component: CreateAdvertiserComponent },
      { path: "advertiser-details/:id", component: AdvertiserDetailsComponent },
      { path: "advertiser-edit/:id", component: AdvertiserEditComponent },
      { path: "organisation-details/:id", component: OrganisationDetailsComponent },
      { path: "**", component: AdvertiserListComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdvertiserRoutingModule { }
