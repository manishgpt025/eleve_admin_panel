import { Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

import { AuthService, AdminService } from 'src/app/services';

@Component({
  selector: 'app-advertiser-list',
  templateUrl: './advertiser-list.component.html',
})
export class AdvertiserListComponent implements OnInit {
  @ViewChild('s',{static:true}) myorgansiationForm
  advertiserlist: any;
  data: any;
  organisationlistData: any = [];

  show_active_model: boolean = false;
  advertiserPageRecord: any;
  organisationPageRecord: any;
  statusActiveInactive: any;
  userId: any;

  show_modal: boolean = false;
  show_step1: boolean = true;
  show_brand: boolean = false;
  show_brand_yes: boolean = false;
  show_brand_agency: boolean = false;

  pageNumber: number = 1;

  organsiationForm: FormGroup;
  organisationTypeValue: any;
  brandExists: any;
  error_note: string = "";
  error_fb: string = "";
  error_instagram: string = "";
  error_twitter: string = "";
  error_blog: string = "";
  error_pinterest: string = "";
  error_linkedin: string = "";
  error_youtube: string = "";
  error_snapchat: string = "";
  socialaccount: string = "0";
  error_social: boolean = false;
  i: number;
  facebookArray: any = [];
  instagramArray: any = [];
  twitterArray: any = [];
  blogArray: any = [];
  pinterestArray: any = [];
  youtubeArray: any = [];
  linkedinArray: any = [];
  snapchatArray: any = [];
  organisationlist: string[] = [];

  throttle = 50;
  scrollDistance = 2;
  scrollUpDistance = 2;

  associativebrandMark: string = '0';
  oraganisationNameEdit: any;
  organisation_name_data: any;
  urlData: string = '/campaign/admin/add-organisation';
  oraganisationIdEdit: any = '';
  addClass: string;
  sortAdvertiser: string = "";
  searchData: string = "";
  statusFilter: string = "";
  filterOrganisation: string = "";
  sortOrganisation: string = "";
  TabChangeValue: number = 0;
  searchDataValue: string;
  searchSymbolValue: boolean = true;
  respAdvertiser: any = [];
  respOrg: any;
  pageNumberOrg: number = 1;
  showActiveLoader : boolean = true;

  constructor(
      private auth: AuthService,
      private fb: FormBuilder,
      private _service: AdminService
  ){ }
  // call oninit when page load
  ngOnInit() {
    this.getAdvertiserList();
    this.getOrganisationLists();
    this.organisationFormVal();
    this.show_step1 = true;
    this.show_brand = false;
    this.show_brand_yes = false;
    this.show_brand_agency = false;
    this.setDefaultValues();
  }
  // Search Functionality for advertiser
  searchDataAdvertiser = (event):void => {
    this.searchData = event.target.value;
    this.searchDataValue = event.target.value;
    if (this.searchData.length > 1) {
      this.searchSymbolValue = false;
      if (this.TabChangeValue == 1){
        this.organisationlist = [];
        this.pageNumberOrg = 1
        this.getOrganisationLists();
      }  else{
        this.pageNumber = 1;
        this.organisationlistData = [];
        this.getAdvertiserList();
      }
    }

    if(this.searchData.length === 0){
      this.searchSymbolValue = true;
    }

  }
  // Clear search for advertiser
  clearCrosValue = ():void => {
    this.searchData = "";
    this.searchDataValue = "";
    this.searchSymbolValue = true;
    this.getAdvertiserList();
    this.getOrganisationLists();
  }
  //Get advertiser list
  getAdvertiserList = ():void => {
    let data = this.auth.getData();
    //this.organisationlistData = [];
    let reqData =
    {
      "user_id": data._id,
      "keyword": this.searchData,
      "pageNumber": this.pageNumber,
      "filter": this.statusFilter,
      "sort": this.sortAdvertiser
    }
    this._service.postApiCommon('/campaign/advertisers', reqData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        this.showActiveLoader = false;
        if (this.data.responseCode == 200) {
          this.respAdvertiser = this.data.result.docs ? this.data.result.docs : [];
          if(this.respAdvertiser.length){
            this.organisationlistData =  this.organisationlistData.concat(this.respAdvertiser);
          }
          this.advertiserPageRecord = this.data.result;
        } else {
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }
  // Change status list for Advertiser filter
  changeStatusList = (statusChange: string):void => {
    this.statusFilter = statusChange;
    this.pageNumber = 1;
    this.organisationlistData = [];
    this.getAdvertiserList()
  }
  // Change sorting for organisation list
  changeSortingData = ():void => {
    if (this.sortAdvertiser == "0")
      this.sortAdvertiser = '1';
    else if (this.sortAdvertiser == "1")
      this.sortAdvertiser = '0';
    else
      this.sortAdvertiser = '1';
      this.organisationlistData = [];
    this.getAdvertiserList()
  }
  // Refresh list and reset data for advertiser list
  refreshAllFilter = ():void => {
    this.searchData = "";
    this.pageNumber = 1;
    this.pageNumberOrg = 1;
    this.organisationlistData = [];
    this.organisationlist = [];
    this.statusFilter = "";
    this.sortAdvertiser = "";
    this.filterOrganisation = "";
    this.sortOrganisation = "";
    this.searchDataValue = "";
    this.getAdvertiserList();
    this.getOrganisationLists();
  }
// Load more function call scroll down
  onScrollDown = ():void => {
    if(this.respAdvertiser.length === 10 && this.TabChangeValue == 0){
      this.pageNumber=this.pageNumber+1;
      this.getAdvertiserList();
    }
    if(this.respOrg.length === 10 && this.TabChangeValue == 1){
      this.pageNumberOrg=this.pageNumberOrg+1;
      this.getOrganisationLists();
    }
  }
  // Get organisation list data
  getOrganisationLists = ():void => {
    let data = this.auth.getData();
    let reqData =
    {
      "user_id": data._id,
      "keyword": this.searchData,
      "pageNumber": this.pageNumberOrg,
      "filter": this.filterOrganisation,
      "sort": this.sortOrganisation
    }
    this._service.postApiCommon('/campaign/organisations', reqData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.respOrg = this.data.result.docs ? this.data.result.docs : [];
          if(this.respOrg.length){
            this.organisationlist =  this.organisationlist.concat(this.respOrg);
          }
          this.organisationPageRecord = this.data.result;
        } else {
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }
  // Change status list for organisation data
  changeOrgStatusList = (statusChange: string):void => {
    this.filterOrganisation = statusChange;
    this.pageNumberOrg = 1;
    this.organisationlist = [];
    this.getOrganisationLists()
  }
  // Change sorting data for organisation list
  changeOrgSortingData = ():void => {
    if (this.sortOrganisation == "0")
      this.sortOrganisation = '1';
    else if (this.sortOrganisation == "1")
      this.sortOrganisation = '0';
    else
      this.sortOrganisation = '1';
    this.organisationlist = [];
    this.getOrganisationLists()
  }

  // Advertiser open modal status change
  advertiserStatus = (status, user_id):void => {
    this.statusActiveInactive = status.target.value;
    this.userId = user_id;
    this.show_active_model = true;
  }
  // Advertiser close modal status change
  advertiserStatusClose = ():void => {
    this.getAdvertiserList();
    this.show_active_model = false;
  }
  // Advertiser status change
  advertiserStatusChange = ():void => {
    let reqData =
    {
      "user_id": this.userId,
      "status": this.statusActiveInactive
    }
    this._service.postApiCommon('/api/advertiser/status', reqData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.getAdvertiserList();
          this.show_active_model = false;
          this._service.success(this.data.responseMessage);
        } else {
          this.show_active_model = false;
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  // **************** All Functionality for Organisation modal and direct , associated brand*************//
  organisationFormVal = ():any => {
    this.organsiationForm = new FormGroup({
      organisation_name: new FormControl('', Validators.compose([Validators.required])),
      orgtype: new FormControl(),
      brandexists: new FormControl(),
      facebook: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      instagram: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      twitter: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      blog: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      pinterest: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      youtube: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      linkedin: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
      snapchat: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
    });
  }
  // Open intial modal
  showModel = ():void => {
    this.show_modal = true;
  }

  setDefaultValues = ():void => {
    this.organsiationForm.patchValue({ orgtype: 'agency' });
  }

  // Get organisation list from database
  getOrganisationList = ():void => {
    this.organisationlist = [];
    let reqData =
    {
      "user_id": "sssss",
      "type": "3"
    }
    this._service.postApiCommon('/campaign/admin/search-organisation', reqData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.organisationlist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.organisationlist.push(this.data.result[i].organisation_name);
          }
        } else {
          this.organisationlist = [];
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });

  }

  handleChange = (evt):void => {
    let target = evt.target;
    if (target.checked) {
      this.show_step1 = false;
      // alert(target.value);
      this.organisationTypeValue = target.value;

      if (this.organisationTypeValue == "agency") {
        this.show_brand_agency = true;
        this.show_brand = false;
      } else {
        this.show_brand_agency = false;
        this.show_brand = true;
      }
    }

  }
  // Change moodal for direct brand
  handleChangeBrand = (evt):void => {
    let target = evt.target;
    if (target.checked) {
      this.show_brand = false;
      // alert(target.value);
      this.brandExists = target.value;

      if (this.brandExists == "yes") {
        this.show_brand_agency = false;
        this.show_brand_yes = true;
        this.getOrganisationList();
      } else {
        this.show_brand_yes = false;
        this.show_brand_agency = true;
      }
    }

  }

  // facebook rows
  get facebookRows(){
    return this.organsiationForm.get('facebook') as FormArray;
  }

  addfacebookRows = ():void => {
    this.facebookRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // twitter rows
  get twitterRows() {
    return this.organsiationForm.get('twitter') as FormArray;
  }

  addtwitterRows = ():void => {
    this.twitterRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // instagram rows
  get instagramRows() {
    return this.organsiationForm.get('instagram') as FormArray;
  }

  addinstagramRows = ():void => {
    this.instagramRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // blog rows
  get blogRows() {
    return this.organsiationForm.get('blog') as FormArray;
  }

  addblogRows = ():void => {
    this.blogRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // pinterestRows
  get pinterestRows() {
    return this.organsiationForm.get('pinterest') as FormArray;
  }

  addpinterestRows = ():void => {
    this.pinterestRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // youtubeRows
  get youtubeRows() {
    return this.organsiationForm.get('youtube') as FormArray;
  }

  addyoutubeRows = ():void => {
    this.youtubeRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // linkedinRows
  get linkedinRows() {
    return this.organsiationForm.get('linkedin') as FormArray;
  }

  addlinkedinRows = ():void => {
    this.linkedinRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // snapchatRows
  get snapchatRows() {
    return this.organsiationForm.get('snapchat') as FormArray;
  }

  addsnapchatRows = ():void => {
    this.snapchatRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // Organisation or direct brand new edit submit function
  onOrganisationFormSubmit = (data):any => {
    if (this.organsiationForm.invalid) return;
    if (this.associativebrandMark == '1') {
      data.orgtype = 'agency';
      this.urlData = '/campaign/admin/update-organisation';
      this.organsiationForm.get('orgtype').setValue('agency');
    }
    if (this.associativebrandMark == '2') {
      data.orgtype = 'brand';
      this.urlData = '/campaign/admin/update-organisation';
      this.organsiationForm.get('orgtype').setValue('brand');
    }
    if (this.organsiationForm.get('orgtype').value == "brand") {
      if (this.socialaccount == "0") {
        this.error_social = true;
        return false;
      } else {
        this.error_social = false;
      }
    }
    if (this.error_note !== "") {
      return false;
    }
    if (data.facebook.length > 0) {
      for (this.i = 0; this.i < data.facebook.length; this.i++) {
        if (data.facebook[this.i].handle !== "" && data.facebook[this.i].url !== "") {
          this.facebookArray.push({ "handle": data.facebook[this.i].handle, "url": data.facebook[this.i].url });
        }
      }
    }
    if (data.instagram.length > 0) {
      for (this.i = 0; this.i < data.instagram.length; this.i++) {
        if (data.instagram[this.i].handle !== "" && data.instagram[this.i].url !== "") {
          this.instagramArray.push({ "handle": data.instagram[this.i].handle, "url": data.instagram[this.i].url });
        }
      }
    }
    if (data.twitter.length > 0) {
      for (this.i = 0; this.i < data.twitter.length; this.i++) {
        if (data.twitter[this.i].handle !== "" && data.twitter[this.i].url !== "") {
          this.twitterArray.push({ "handle": data.twitter[this.i].handle, "url": data.twitter[this.i].url });
        }
      }
    }
    if (data.blog.length > 0) {
      for (this.i = 0; this.i < data.blog.length; this.i++) {
        if (data.blog[this.i].handle !== "" && data.blog[this.i].url !== "") {
          this.blogArray.push({ "handle": data.blog[this.i].handle, "url": data.blog[this.i].url });
        }
      }
    }
    if (data.pinterest.length > 0) {
      for (this.i = 0; this.i < data.pinterest.length; this.i++) {
        if (data.pinterest[this.i].handle !== "" && data.pinterest[this.i].url !== "") {
          this.pinterestArray.push({ "handle": data.pinterest[this.i].handle, "url": data.pinterest[this.i].url });
        }
      }
    }
    if (data.youtube.length > 0) {
      for (this.i = 0; this.i < data.youtube.length; this.i++) {
        if (data.youtube[this.i].handle !== "" && data.youtube[this.i].url !== "") {
          this.youtubeArray.push({ "handle": data.youtube[this.i].handle, "url": data.youtube[this.i].url });
        }
      }
    }
    if (data.linkedin.length > 0) {
      for (this.i = 0; this.i < data.linkedin.length; this.i++) {
        if (data.linkedin[this.i].handle !== "" && data.linkedin[this.i].url !== "") {
          this.linkedinArray.push({ "handle": data.linkedin[this.i].handle, "url": data.linkedin[this.i].url });
        }
      }
    }
    if (data.snapchat.length > 0) {
      for (this.i = 0; this.i < data.snapchat.length; this.i++) {
        if (data.snapchat[this.i].handle !== "" && data.snapchat[this.i].url !== "") {
          this.snapchatArray.push({ "handle": data.snapchat[this.i].handle, "url": data.snapchat[this.i].url });
        }
      }
    }
    let arrayData = {
      "organisation_name": data.organisation_name,
      "type": data.orgtype,
      "social_media": [{
        "facebook": this.facebookArray,
        "instagram": this.instagramArray,
        "twitter": this.twitterArray,
        "blog": this.blogArray,
        "pinterest": this.pinterestArray,
        "youtube": this.youtubeArray,
        "linkedin": this.linkedinArray,
        "snapchat": this.snapchatArray,
      }],
      "associated_brands": [],
      "is_asscoiated": "0",
      "id": this.oraganisationIdEdit
    }
    this._service.postApiCommon(this.urlData, arrayData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this._service.success("Organisation created successfully.");
          this.getOrganisationLists();
          this.hideModel();
        } else if (this.data.responseCode == 409) {
          this._service.error(this.data.responseMessage);
        } else {
          this._service.error("Organisation not created, Please try again");
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  //Check social handle names
  checkValidHandle = (handle, index, platform):any => {
    let format = /[ !#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
    let res = format.test(handle);
    if (res == true) {
      this.error_note = "A valid handle must not have spaces";
    } else {
      this.error_note = "";
    }
    switch (platform) {
      case 'instagram': {
        this.error_instagram = this.error_note;
        break;
      }
      case 'twitter': {
        this.error_twitter = this.error_note;
        break;
      }
      default: {
        //statements;
        break;
      }
    }

  }

  // Check social Urls
  checkUrl = (url, platform, socialarray, index):any => {
    let handle = socialarray.value[index].handle;
    if (url !== "") {
      let res = url.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
      if (res == null) {
        // console.log("not valid");
        this.error_note = "Provide a valid URL";
      } else {
        if (url.indexOf(platform) !== -1) {
          if (platform == 'instagram' || platform == 'twitter') {
            this.checkValidHandle(handle, index, platform);
            if (this.error_note == "") {
              if (url.indexOf(handle) !== -1) {
                this.error_note = "";
                this.socialaccount = "1";
              } else {
                this.error_note = "URL does not belong to the same handle.";
              }
            }
          } else {
            this.error_note = "";
            this.socialaccount = "1";
          }
        } else {
          this.error_note = "Match URL domain with the platform";
        }
      }
    }
    switch (platform) {
      case 'facebook': {
        this.error_fb = this.error_note;
        break;
      }
      case 'instagram': {
        this.error_instagram = this.error_note;
        break;
      }
      case 'twitter': {
        this.error_twitter = this.error_note;
        break;
      }
      case 'blog': {
        this.error_blog = this.error_note;
        break;
      }
      case 'pinterest': {
        this.error_pinterest = this.error_note;
        break;
      }
      case 'youtube': {
        this.error_youtube = this.error_note;
        break;
      }
      case 'linkedin': {
        this.error_linkedin = this.error_note;
        break;
      }
      case 'snapchat': {
        this.error_snapchat = this.error_note;
        break;
      }
      default: {
        //statements;
        break;
      }
    }
  }

  // Close modals
  cancelForm = ():void => {
   // this.organsiationForm.reset();
    this.myorgansiationForm.resetForm();
    this.hideModel();
  }
  // Hide rest modals
  hideModel = ():void => {
    this.show_modal = false;
    this.show_step1 = true;
    this.show_brand = false;
    this.show_brand_yes = false;
    this.show_brand_agency = false;
    this.oraganisationIdEdit = "";
    //this.organsiationForm.reset();
    this.myorgansiationForm.resetForm();
  }
  // Get data of social references of organisation or Direct brand
  onSelectChange = ():any => {
    this.organisation_name_data = this.organsiationForm.get('organisation_name').value;
    if (this.organsiationForm.get('organisation_name').value == "" || this.organsiationForm.get('organisation_name').value == null) {
      this.organisation_name_data = this.oraganisationNameEdit;
      this.organsiationForm.get('organisation_name').setValue(this.organisation_name_data);
    }
    let arrayData = {
      "organisation_name": this.organisation_name_data
    }
    this._service.postApiCommon('/campaign/admin/get-organisation', arrayData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          if (this.data.result.social_media !== undefined) {
            if (this.data.result.social_media[0].facebook !== undefined) {
              let len = this.data.result.social_media[0].facebook.length;
              while (this.facebookRows.length !== 0) {
                this.facebookRows.removeAt(0);
              }
              for (this.i = 0; this.i < len; this.i++) {
                if(this.data.result.social_media[0].facebook[this.i].handle && this.data.result.social_media[0].facebook[this.i].url)
                  this.facebookRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].facebook[this.i].handle, url: this.data.result.social_media[0].facebook[this.i].url }));
              }
              if(this.facebookRows.length === 0)
                this.addfacebookRows();
              //this.facebookRows.removeAt(0);
            }
            if (this.data.result.social_media[0].instagram !== undefined) {
              let len = this.data.result.social_media[0].instagram.length;
              while (this.instagramRows.length !== 0) {
                this.instagramRows.removeAt(0);
              }
              for (this.i = 0; this.i < len; this.i++) {
                if(this.data.result.social_media[0].instagram[this.i].handle && this.data.result.social_media[0].instagram[this.i].url)
                  this.instagramRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].instagram[this.i].handle, url: this.data.result.social_media[0].instagram[this.i].url }));
              }
              if(this.instagramRows.length === 0)
                this.addinstagramRows();
              //this.instagramRows.removeAt(0);
            }
            if (this.data.result.social_media[0].twitter !== undefined) {
              let len = this.data.result.social_media[0].twitter.length;
              while (this.twitterRows.length !== 0) {
                this.twitterRows.removeAt(0);
              }
              for (this.i = 0; this.i < len; this.i++) {
                if(this.data.result.social_media[0].twitter[this.i].handle && this.data.result.social_media[0].twitter[this.i].url)
                this.twitterRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].twitter[this.i].handle, url: this.data.result.social_media[0].twitter[this.i].url }));
              }
              if(this.twitterRows.length === 0)
                this.addtwitterRows();
              //this.twitterRows.removeAt(0);
            }
            if (this.data.result.social_media[0].blog !== undefined) {
              let len = this.data.result.social_media[0].blog.length;
              while (this.blogRows.length !== 0) {
                this.blogRows.removeAt(0);
              }
              for (this.i = 0; this.i < len; this.i++) {
                if(this.data.result.social_media[0].blog[this.i].handle && this.data.result.social_media[0].blog[this.i].url)
                this.blogRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].blog[this.i].handle, url: this.data.result.social_media[0].blog[this.i].url }));
              }
              if(this.blogRows.length === 0)
                this.addblogRows();
              //this.blogRows.removeAt(0);
            }
            if (this.data.result.social_media[0].pinterest !== undefined) {
              let len = this.data.result.social_media[0].pinterest.length;
              while (this.pinterestRows.length !== 0) {
                this.pinterestRows.removeAt(0);
              }
              for (this.i = 0; this.i < len; this.i++) {
                if(this.data.result.social_media[0].pinterest[this.i].handle && this.data.result.social_media[0].pinterest[this.i].url)
                this.pinterestRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].pinterest[this.i].handle, url: this.data.result.social_media[0].pinterest[this.i].url }));
              }
              if(this.pinterestRows.length === 0)
                this.addpinterestRows();
             // this.pinterestRows.removeAt(0);
            }
            if (this.data.result.social_media[0].youtube !== undefined) {
              let len = this.data.result.social_media[0].youtube.length;
              while (this.youtubeRows.length !== 0) {
                this.youtubeRows.removeAt(0);
              }
              for (this.i = 0; this.i < len; this.i++) {
                if(this.data.result.social_media[0].youtube[this.i].handle && this.data.result.social_media[0].youtube[this.i].url)
                this.youtubeRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].youtube[this.i].handle, url: this.data.result.social_media[0].youtube[this.i].url }));
              }
              if(this.youtubeRows.length === 0)
                this.addyoutubeRows();
              //this.youtubeRows.removeAt(0);
            }
            if (this.data.result.social_media[0].linkedin !== undefined) {
              let len = this.data.result.social_media[0].linkedin.length;
              while (this.linkedinRows.length !== 0) {
                this.linkedinRows.removeAt(0);
              }
              for (this.i = 0; this.i < len; this.i++) {
                if(this.data.result.social_media[0].linkedin[this.i].handle && this.data.result.social_media[0].linkedin[this.i].url)
                this.linkedinRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].linkedin[this.i].handle, url: this.data.result.social_media[0].linkedin[this.i].url }));
              }
              if(this.linkedinRows.length === 0)
                this.addlinkedinRows();
              //this.linkedinRows.removeAt(0);
            }
            if (this.data.result.social_media[0].snapchat !== undefined) {
              let len = this.data.result.social_media[0].snapchat.length;
              while (this.snapchatRows.length !== 0) {
                this.snapchatRows.removeAt(0);
              }
              for (this.i = 0; this.i < len; this.i++) {
                if(this.data.result.social_media[0].snapchat[this.i].handle && this.data.result.social_media[0].snapchat[this.i].url)
                this.snapchatRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].snapchat[this.i].handle, url: this.data.result.social_media[0].snapchat[this.i].url }));
              }
              if(this.snapchatRows.length === 0)
                this.addsnapchatRows();
             // this.snapchatRows.removeAt(0);
            }


          }
        } else {
          //this._service.error("Organisation not Created");
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }
  // Open modal edit organisation
  getOpenModalOrganisationEdit = (orgId, orgName, type):any => {
    if (type == 'agency') {
      this.associativebrandMark = '1';
    } else {
      this.associativebrandMark = '2';
    }
    this.organsiationForm.reset();
    this.oraganisationNameEdit = orgName;
    this.oraganisationIdEdit = orgId;
    this.show_modal = true;
    this.show_step1 = false;
    this.show_brand = false;
    this.show_brand_yes = false;
    this.show_brand_agency = true;
    this.onSelectChange();
  }

  //Change tab value
  ActiveTagValue = event => {
    // this.refreshAllFilter();
    this.TabChangeValue = event.index;
  }

}
