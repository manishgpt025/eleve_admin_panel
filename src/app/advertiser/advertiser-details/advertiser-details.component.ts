import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Ng2ImgMaxService } from 'node_modules/ng2-img-max';

import { AdminService } from 'src/app/services';

@Component({
  selector: 'app-advertiser-details',
  templateUrl: './advertiser-details.component.html',
})
export class AdvertiserDetailsComponent implements OnInit {
  data: any;
  getAdvertiserData: any;
  lastChar: any;
  firstChar: any;
  imageurl: any;
  message: any;
  AssciatedBrandData: any;
  advertiserId: any;
  show_active_model:boolean=false;
  advertiserStatusKey: any;
  user_id: any;

  constructor(
      private route: ActivatedRoute,
      private _service: AdminService
  ) {}

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.advertiserId=params.id;
    });
    this.getAdvertiserDetail();
  }
//Get Advertiser detail by id
  getAdvertiserDetail = ():void => {
  let reqData = { "user_id":this.advertiserId }
  this._service.postApiCommon('/campaign/get-advertiser',reqData)
  .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.getAdvertiserData = this.data.result;
          if(this.getAdvertiserData.brand_details)
            this.AssciatedBrandData = this.getAdvertiserData.brand_details;
          else
            this.AssciatedBrandData = [];
          if(this.getAdvertiserData.profile_image){
            this.imageurl = this.getAdvertiserData.profile_image;
          }else{
            if(this.getAdvertiserData.name){
              this.firstChar = this.getAdvertiserData.name[0].toUpperCase();
                if(this.getAdvertiserData.last_name)
                  this.lastChar = this.getAdvertiserData.last_name[0].toUpperCase();
                else
                  this.lastChar = '';
            }else{
              this.imageurl = "";
            }
          }
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }
  //Open confirmation modal for change status
  advertiserStatus = (statusValue, user_id):void => {
    this.advertiserStatusKey = statusValue.target.value;
    this.user_id = user_id;
    this.show_active_model = true;
  }
//submit change status for advertiser
  advertiserStatusChange = ():void => {
    let reqData =
      {
        "user_id":this.user_id,
        "status": this.advertiserStatusKey
      }
    this._service.postApiCommon('/api/advertiser/status',reqData)
    .subscribe((responseData: any) => {
      if (responseData.status == 200) {
        this.data = responseData.body;
        if (this.data.responseCode == 200) {
          this.getAdvertiserDetail();
          this.show_active_model = false;
          this.advertiserStatusKey = "";
          this.user_id = "";
          this._service.success(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }
//close modal for status
  closeActiveInactiveModel = ():void => {
    this.getAdvertiserDetail();
    this.show_active_model = false;
    this.advertiserStatusKey = "";
    this.user_id = "";
  }
}
