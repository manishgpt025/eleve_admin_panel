import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

import { AdminService } from 'src/app/services';

@Component({
  selector: 'app-organisation-details',
  templateUrl: './organisation-details.component.html',
})

export class OrganisationDetailsComponent implements OnInit {
  orgId: any;
  data: any;
  message: any;
  getOrganitionData: any;

  show_modal: boolean = false;
  show_step1: boolean = true;
  show_brand: boolean = false;
  show_brand_yes: boolean = false;
  show_brand_agency: boolean = false;

  organsiationForm: FormGroup;
  organisationTypeValue: any;
  brandExists: any;
  error_note: string = "";
  error_fb: string = "";
  error_instagram: string = "";
  error_twitter: string = "";
  error_blog: string = "";
  error_pinterest: string = "";
  error_linkedin: string = "";
  error_youtube: string = "";
  error_snapchat: string = "";
  socialaccount: string = "0";
  error_social: boolean = false;
  i: number;
  facebookArray: any = [];
  instagramArray: any = [];
  twitterArray: any = [];
  blogArray: any = [];
  pinterestArray: any = [];
  youtubeArray: any = [];
  linkedinArray: any = [];
  snapchatArray: any = [];
  organisationlist: string[] = [];
  associativebrandMark: string = '0';
  oraganisationNameEdit: any;
  organisation_name_data: any;
  oraganisationIdEdit: any = '';
  AssoBrandCount: string[] = [];
  filteredAssociated: number = 0;
  constructor(private fb: FormBuilder, private route: ActivatedRoute,private router: Router, private _service: AdminService) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.orgId=params.id;
    });
    this.getAdvertiserDetail();


    this.organisationFormVal();
    this.show_step1 = true;
    this.show_brand = false;
    this.show_brand_yes = false;
    this.show_brand_agency = false;
    this.setDefaultValues();


  }

  getAdvertiserDetail(){
    let reqData =
    {
      "id":this.orgId
    }
  this._service.postApiCommon('/api/organisation/details',reqData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.getOrganitionData = this.data.result;

          if(this.getOrganitionData.advertiserInfo.length > 0 ){
            var advertiserBrandDeatil = this.getOrganitionData.advertiserInfo;

            for(let brandData of advertiserBrandDeatil){
             // console.log('kkkkkkkkkkkkkkkk---->',brandData);
              for(let brandDataAsso of brandData.associated_brands)
              //this.AssoBrandCount = brandDataAsso;
              this.AssoBrandCount.push(brandDataAsso);
            }

          }
          if(this.AssoBrandCount)
            var value = this.remove_duplicates(this.AssoBrandCount);
            this.filteredAssociated = value.length;

          //console.log('kkkkkkkkkkkkkkkk---->',this.filteredAssociated.length);
        } else {
          this.message = this.data.responseMessage;
        }
      }
    }, error => {
      this.message = "Something went wrong! Please try again.";
    });
  }

// Remove duplicate
  remove_duplicates(arr) {
    let obj = {};
    for (let i = 0; i < arr.length; i++) {
        obj[arr[i]] = true;
    }
    arr = [];
    for (let key in obj) {
        arr.push(key);
    }
    return arr;
}



  // **************** All Functionality for Organisation modal and direct , associated brand*************//
 organisationFormVal(){
  this.organsiationForm = new FormGroup({
    organisation_name: new FormControl('', Validators.compose([Validators.required])),
    orgtype: new FormControl(),
    brandexists: new FormControl(),
    facebook: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
    instagram: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
    twitter: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
    blog: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
    pinterest: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
    youtube: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
    linkedin: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
    snapchat: this.fb.array([this.fb.group({ platform: '', handle: '', url: '' })]),
  });
}

  showModel() {
    this.show_modal = true;
  }


  setDefaultValues() {
    this.organsiationForm.patchValue({ orgtype: 'agency' });
  }

  // Get organisation list from database
  getOrganisationList() {
   // let data = this.auth.getData()
    this.organisationlist = [];
    let reqData =
    {
      "user_id": "sssss",
      "type": "3"
    }
    this._service.postApiCommon('/campaign/admin/search-organisation', reqData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.organisationlist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.organisationlist.push(this.data.result[i].organisation_name);
            console.log('organisationlist-----', this.organisationlist)
          }
        } else {
          this.organisationlist = [];
          this.message = this.data.responseMessage;
        }
      }
    }, error => {
      this.message = "Something went wrong! Please try again.";
    });

  }

  handleChange(evt) {

    let target = evt.target;
    if (target.checked) {
      this.show_step1 = false;
      // alert(target.value);
      this.organisationTypeValue = target.value;

      if (this.organisationTypeValue == "agency") {
        this.show_brand_agency = true;
        this.show_brand = false;
      } else {
        this.show_brand_agency = false;
        this.show_brand = true;
      }
    }

  }

  handleChangeBrand(evt) {

    let target = evt.target;
    if (target.checked) {
      this.show_brand = false;
      // alert(target.value);
      this.brandExists = target.value;

      if (this.brandExists == "yes") {
        this.show_brand_agency = false;
        this.show_brand_yes = true;
        this.getOrganisationList();
      } else {
        this.show_brand_yes = false;
        this.show_brand_agency = true;
      }
    }

  }

  // facebook rows
  get facebookRows() {
    return this.organsiationForm.get('facebook') as FormArray;
  }

  addfacebookRows() {
    this.facebookRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // twitter rows
  get twitterRows() {
    return this.organsiationForm.get('twitter') as FormArray;
  }

  addtwitterRows() {
    this.twitterRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // instagram rows
  get instagramRows() {
    return this.organsiationForm.get('instagram') as FormArray;
  }

  addinstagramRows() {
    this.instagramRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // blog rows
  get blogRows() {
    return this.organsiationForm.get('blog') as FormArray;
  }

  addblogRows() {
    this.blogRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // pinterestRows
  get pinterestRows() {
    return this.organsiationForm.get('pinterest') as FormArray;
  }

  addpinterestRows() {
    this.pinterestRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // youtubeRows
  get youtubeRows() {
    return this.organsiationForm.get('youtube') as FormArray;
  }

  addyoutubeRows() {
    this.youtubeRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // linkedinRows
  get linkedinRows() {
    return this.organsiationForm.get('linkedin') as FormArray;
  }

  addlinkedinRows() {
    this.linkedinRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  // snapchatRows
  get snapchatRows() {
    return this.organsiationForm.get('snapchat') as FormArray;
  }

  addsnapchatRows() {
    this.snapchatRows.push(this.fb.group({ platform: '', handle: '', url: '' }));
  }

  onOrganisationFormSubmit(data) {
    if (this.organsiationForm.invalid) {
      console.log("invalid");
      return false;
    }
    if (this.organsiationForm.get('orgtype').value == "brand") {
      if (this.socialaccount == "0") {
        this.error_social = true;
        console.log("invalid no account connected");
        return false;
      } else {
        this.error_social = false;
      }
    }

    if (this.error_note !== "") {
      console.log("invalid handle or url");
      return false;
    }

    console.log(data);

    if (data.facebook.length > 0) {
      this.facebookArray = [];
      for (this.i = 0; this.i < data.facebook.length; this.i++) {
        if (data.facebook[this.i].handle !== "" && data.facebook[this.i].url !== "") {
          this.facebookArray.push({ "handle": data.facebook[this.i].handle, "url": data.facebook[this.i].url });
        }
      }
    }

    if (data.instagram.length > 0) {
      this.instagramArray = [];
      for (this.i = 0; this.i < data.instagram.length; this.i++) {
        if (data.instagram[this.i].handle !== "" && data.instagram[this.i].url !== "") {
          this.instagramArray.push({ "handle": data.instagram[this.i].handle, "url": data.instagram[this.i].url });
        }
      }
    }

    if (data.twitter.length > 0) {
      this.twitterArray = [];
      for (this.i = 0; this.i < data.twitter.length; this.i++) {
        if (data.twitter[this.i].handle !== "" && data.twitter[this.i].url !== "") {
          this.twitterArray.push({ "handle": data.twitter[this.i].handle, "url": data.twitter[this.i].url });
        }
      }
    }

    if (data.blog.length > 0) {
      this.blogArray = [];
      for (this.i = 0; this.i < data.blog.length; this.i++) {
        if (data.blog[this.i].handle !== "" && data.blog[this.i].url !== "") {
          this.blogArray.push({ "handle": data.blog[this.i].handle, "url": data.blog[this.i].url });
        }
      }
    }

    if (data.pinterest.length > 0) {
      this.pinterestArray = [];
      for (this.i = 0; this.i < data.pinterest.length; this.i++) {
        if (data.pinterest[this.i].handle !== "" && data.pinterest[this.i].url !== "") {
          this.pinterestArray.push({ "handle": data.pinterest[this.i].handle, "url": data.pinterest[this.i].url });
        }
      }
    }

    if (data.youtube.length > 0) {
      this.youtubeArray = [];
      for (this.i = 0; this.i < data.youtube.length; this.i++) {
        if (data.youtube[this.i].handle !== "" && data.youtube[this.i].url !== "") {
          this.youtubeArray.push({ "handle": data.youtube[this.i].handle, "url": data.youtube[this.i].url });
        }
      }
    }

    if (data.linkedin.length > 0) {
      this.linkedinArray = [];
      for (this.i = 0; this.i < data.linkedin.length; this.i++) {
        if (data.linkedin[this.i].handle !== "" && data.linkedin[this.i].url !== "") {
          this.linkedinArray.push({ "handle": data.linkedin[this.i].handle, "url": data.linkedin[this.i].url });
        }
      }
    }

    if (data.snapchat.length > 0) {
      this.snapchatArray = [];
      for (this.i = 0; this.i < data.snapchat.length; this.i++) {
        if (data.snapchat[this.i].handle !== "" && data.snapchat[this.i].url !== "") {
          this.snapchatArray.push({ "handle": data.snapchat[this.i].handle, "url": data.snapchat[this.i].url });
        }
      }
    }


    let arrayData = {
      "organisation_name": data.organisation_name,
      "type": data.orgtype,
      "social_media": [{
        "facebook": this.facebookArray,
        "instagram": this.instagramArray,
        "twitter": this.twitterArray,
        "blog": this.blogArray,
        "pinterest": this.pinterestArray,
        "youtube": this.youtubeArray,
        "linkedin": this.linkedinArray,
        "snapchat": this.snapchatArray,
      }],
      "associated_brands": [],
      "is_asscoiated": "0",
      "id" : this.oraganisationIdEdit
    }

   // console.log('qqqqqqqqqqqq--------->',arrayData);return;
    this._service.postApiCommon('/campaign/admin/update-organisation', arrayData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this._service.success("Organisation Updated successfully.");
          this.getAdvertiserDetail();
          this.hideModel();
        } else if (this.data.responseCode == 409) {
          this._service.error(this.data.responseMessage);
        } else {
          this._service.error("Organisation not updated");
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  checkValidHandle(handle, index, platform) {
    let format = /[ !#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
    let res = format.test(handle);
    console.log(index);

    if (res == true) {
      this.error_note = "A valid handle must not have spaces";
    } else {
      this.error_note = "";
    }
    switch (platform) {

      case 'instagram': {
        this.error_instagram = this.error_note;
        break;
      }
      case 'twitter': {
        this.error_twitter = this.error_note;
        break;
      }
      default: {
        //statements;
        break;
      }
    }

  }

  checkUrl(url, platform, socialarray, index) {
    console.log(url);
    console.log(platform);
    console.log(socialarray.value[index].handle);
    let handle = socialarray.value[index].handle;
    if (url !== "") {
      let res = url.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
      if (res == null) {
        // console.log("not valid");
        this.error_note = "Provide a valid URL";
      } else {
        if (url.indexOf(platform) !== -1) {
          if (platform == 'instagram' || platform == 'twitter') {
            this.checkValidHandle(handle, index, platform);
            if (this.error_note == "") {
              if (url.indexOf(handle) !== -1) {
                this.error_note = "";
                this.socialaccount = "1";
              } else {
                this.error_note = "URL does not belong to the same habndle.";
              }
            }
          } else {
            this.error_note = "";
            this.socialaccount = "1";
          }
        } else {
          this.error_note = "Match URL domain with the platform";
        }
      }
    }

    switch (platform) {
      case 'facebook': {
        this.error_fb = this.error_note;
        break;
      }
      case 'instagram': {
        this.error_instagram = this.error_note;
        break;
      }
      case 'twitter': {
        this.error_twitter = this.error_note;
        break;
      }
      case 'blog': {
        this.error_blog = this.error_note;
        break;
      }
      case 'pinterest': {
        this.error_pinterest = this.error_note;
        break;
      }
      case 'youtube': {
        this.error_youtube = this.error_note;
        break;
      }
      case 'linkedin': {
        this.error_linkedin = this.error_note;
        break;
      }
      case 'snapchat': {
        this.error_snapchat = this.error_note;
        break;
      }

      default: {
        //statements;
        break;
      }
    }
  }

  cancelForm() {
    this.organsiationForm.reset();
  }

  hideModel() {
    this.show_modal = false;
    this.show_step1 = true;
    this.show_brand = false;
    this.show_brand_yes = false;
    this.show_brand_agency = false;
    this.organsiationForm.reset();
  }

  onSelectChange(OrId, OrName, Type) {
    this.organisation_name_data = OrName;
    this.organsiationForm.get('organisation_name').setValue(OrName);
    let arrayData = {
      "organisation_name": this.organisation_name_data
    }
    console.log('arrayDataarrayData---->', arrayData);
    this._service.postApiCommon('/campaign/admin/get-organisation', arrayData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          console.log(this.data.result);
          if (this.data.result.social_media !== undefined) {
            if (this.data.result.social_media[0].facebook !== undefined) {
              let len = this.data.result.social_media[0].facebook.length;
              // this.organsiationForm.get('facebook').setValue(this.fb.group({platform:'',handle:this.data.result.social_media[0].facebook[0].handle,url:this.data.result.social_media[0].facebook[0].url }));
              for (this.i = 0; this.i < len; this.i++) {
                this.facebookRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].facebook[this.i].handle, url: this.data.result.social_media[0].facebook[this.i].url }));
              }
              this.facebookRows.removeAt(0);
            }
            if (this.data.result.social_media[0].instagram !== undefined ) {
              let len = this.data.result.social_media[0].instagram.length;
              // this.organsiationForm.get('facebook').setValue(this.fb.group({platform:'',handle:this.data.result.social_media[0].facebook[0].handle,url:this.data.result.social_media[0].facebook[0].url }));
              for (this.i = 0; this.i < len; this.i++) {
                this.instagramRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].instagram[this.i].handle, url: this.data.result.social_media[0].instagram[this.i].url }));
              }
              this.instagramRows.removeAt(0);
            }
            if (this.data.result.social_media[0].twitter !== undefined) {
              let len = this.data.result.social_media[0].twitter.length;
              // this.organsiationForm.get('facebook').setValue(this.fb.group({platform:'',handle:this.data.result.social_media[0].facebook[0].handle,url:this.data.result.social_media[0].facebook[0].url }));
              for (this.i = 0; this.i < len; this.i++) {
                this.twitterRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].twitter[this.i].handle, url: this.data.result.social_media[0].twitter[this.i].url }));
              }
              this.twitterRows.removeAt(0);
            }
            if (this.data.result.social_media[0].blog !== undefined ) {
              let len = this.data.result.social_media[0].blog.length;
              // this.organsiationForm.get('facebook').setValue(this.fb.group({platform:'',handle:this.data.result.social_media[0].facebook[0].handle,url:this.data.result.social_media[0].facebook[0].url }));
              for (this.i = 0; this.i < len; this.i++) {
                this.blogRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].blog[this.i].handle, url: this.data.result.social_media[0].blog[this.i].url }));
              }
              this.blogRows.removeAt(0);
            }
            if (this.data.result.social_media[0].pinterest !== undefined ) {
              let len = this.data.result.social_media[0].pinterest.length;
              // this.organsiationForm.get('facebook').setValue(this.fb.group({platform:'',handle:this.data.result.social_media[0].facebook[0].handle,url:this.data.result.social_media[0].facebook[0].url }));
              for (this.i = 0; this.i < len; this.i++) {
                this.pinterestRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].pinterest[this.i].handle, url: this.data.result.social_media[0].pinterest[this.i].url }));
              }
              this.pinterestRows.removeAt(0);
            }
            if (this.data.result.social_media[0].youtube !== undefined ) {
              let len = this.data.result.social_media[0].youtube.length;
              // this.organsiationForm.get('facebook').setValue(this.fb.group({platform:'',handle:this.data.result.social_media[0].facebook[0].handle,url:this.data.result.social_media[0].facebook[0].url }));
              for (this.i = 0; this.i < len; this.i++) {
                this.youtubeRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].youtube[this.i].handle, url: this.data.result.social_media[0].youtube[this.i].url }));
              }
              this.youtubeRows.removeAt(0);
            }
            if (this.data.result.social_media[0].linkedin !== undefined ) {
              let len = this.data.result.social_media[0].linkedin.length;
              // this.organsiationForm.get('facebook').setValue(this.fb.group({platform:'',handle:this.data.result.social_media[0].facebook[0].handle,url:this.data.result.social_media[0].facebook[0].url }));
              for (this.i = 0; this.i < len; this.i++) {
                this.linkedinRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].linkedin[this.i].handle, url: this.data.result.social_media[0].linkedin[this.i].url }));
              }
              this.linkedinRows.removeAt(0);
            }
            if (this.data.result.social_media[0].snapchat !== undefined ) {
              let len = this.data.result.social_media[0].snapchat.length;
              // this.organsiationForm.get('facebook').setValue(this.fb.group({platform:'',handle:this.data.result.social_media[0].facebook[0].handle,url:this.data.result.social_media[0].facebook[0].url }));
              for (this.i = 0; this.i < len; this.i++) {
                this.snapchatRows.push(this.fb.group({ platform: '', handle: this.data.result.social_media[0].snapchat[this.i].handle, url: this.data.result.social_media[0].snapchat[this.i].url }));
              }
              this.snapchatRows.removeAt(0);
            }
          }
        } else {
          console.log(this.data);
          //this._service.error("Organisation not Created");
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  getOpenModalOrganisationEdit(orgId, orgName, type){
    if(type == 'agency'){
      this.associativebrandMark = '1';
    }else{
      this.associativebrandMark = '2';
    }
    this.oraganisationNameEdit = orgName;
    this.oraganisationIdEdit = orgId;
    this.show_modal = true;
    this.show_step1 = false;
    this.show_brand = false;
    this.show_brand_yes = false;
    this.show_brand_agency = true;
    this.onSelectChange(orgId,orgName,type );
  }

}
