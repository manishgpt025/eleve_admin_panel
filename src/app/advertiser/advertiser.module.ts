import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {
    MatButtonModule, 
    MatCheckboxModule, 
    MatInputModule, 
    MatMenuModule, 
    MatIconModule, 
    MatSelectModule, 
    MatRadioModule, 
    MatExpansionModule, 
    MatTabsModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { Ng5SliderModule } from 'ng5-slider';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import {MatNativeDateModule} from '@angular/material/core';
import { NgSelectModule } from '@ng-select/ng-select';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AdvertiserListComponent } from './advertiser-list/advertiser-list.component';
import { AdvertiserDetailsComponent } from './advertiser-details/advertiser-details.component';
import { OrganisationDetailsComponent } from './organisation-details/organisation-details.component';
import { AdvertiserEditComponent } from './advertiser-edit/advertiser-edit.component';
import { CommonFileModule } from '../common-file/common-file.module';
import { AdvertiserRoutingModule } from './advertiser-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { CreateAdvertiserComponent } from './create-advertiser/create-advertiser.component';


@NgModule({
  declarations: [
    CreateAdvertiserComponent, 
    LayoutComponent, 
    AdvertiserListComponent, 
    AdvertiserDetailsComponent, 
    OrganisationDetailsComponent,
    AdvertiserEditComponent
  ],
  imports: [
    CommonModule,
    AdvertiserRoutingModule,
    CommonFileModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    Ng2ImgMaxModule,
    MatNativeDateModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatSelectModule,
    MatCheckboxModule,
    MatRadioModule,
    Ng5SliderModule,
    MatExpansionModule,
    MatTabsModule,
    Ng5SliderModule,
    NgSelectModule,
    InfiniteScrollModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AdvertiserModule { }
