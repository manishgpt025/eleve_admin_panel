import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatMenuModule,
  MatIconModule,
  MatSelectModule,
  MatRadioModule,
  MatExpansionModule,
  MatTabsModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MatNativeDateModule } from "@angular/material/core";
import { AngularEditorModule } from "@kolkov/angular-editor";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { NgxFileDropModule } from "ngx-file-drop";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { Ng2ImgMaxModule } from "ng2-img-max";
import { Ng5SliderModule } from "ng5-slider";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgxDaterangepickerMd } from "ngx-daterangepicker-material";

import { CommonFileModule } from "../common-file/common-file.module";

import { ContentRoutingModule } from './content-routing.module';
import { YoutubeComponent } from './youtube/youtube.component';
import { LayoutComponent } from './layout/layout.component';

@NgModule({
  declarations: [YoutubeComponent, LayoutComponent],
  imports: [
    CommonModule,
    CommonFileModule,
    ContentRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2ImgMaxModule,
    MatNativeDateModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatSelectModule,
    MatRadioModule,
    Ng5SliderModule,
    MatExpansionModule,
    MatTabsModule,
    NgSelectModule,
    NgxDaterangepickerMd.forRoot(),
    InfiniteScrollModule,
    Ng2SearchPipeModule,
    AngularEditorModule,
    NgxFileDropModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ContentModule { }
