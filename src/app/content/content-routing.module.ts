import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { YoutubeComponent } from './youtube/youtube.component';
const routes: Routes = [
  {
    path: "", component: LayoutComponent,
    children: [
      { path: "", component: YoutubeComponent },
    ],
    runGuardsAndResolvers: "always"
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContentRoutingModule { }
