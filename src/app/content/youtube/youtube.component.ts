import { Component, OnInit } from '@angular/core';
import { AngularCsv } from 'angular7-csv';
import { AdminService, AuthService } from 'src/app/services';
@Component({
  selector: 'app-youtube',
  templateUrl: './youtube.component.html'
})
export class YoutubeComponent implements OnInit {
  exts = ['.csv'];
  uploadVideoSuccess: boolean = false;
  video_document_csv: File;
  videoFileUpload: string = "";

  uploadContentSuccess: boolean = false;
  content_document_csv: File;
  contentFileUpload: string = "";
  csvData: any = [];

  // constructor for dependencies injection
  constructor(
        private auth: AuthService,
        private _service: AdminService,
    ) { }

  ngOnInit() {
  }

    //Upload Video csv
uploadYoutubeVideoCSV = ($event):any=> {
    if($event.target.files.length == 0)
      return this._service.error("Must upload a CSV first.");

   this.video_document_csv = <File>$event.target.files[0];
    let name = this.video_document_csv.name;
    let type = this.video_document_csv.type;
    let size = this.video_document_csv.size;
    if (size > 10242880) {
      this._service.error("File is too large. Upload file less than 10 MB");
      this.video_document_csv = null;
      this.videoFileUpload = "";
      return;
    }
    let validCSV = (new RegExp('(' + this.exts.join('|').replace(/\./g, '\\.') + ')$')).test(name);
    if (!validCSV) {
      this._service.error("Upload only CSV format.");
      this.video_document_csv = null;
      this.videoFileUpload = "";
      return;
    } else {
      this.uploadVideoSuccess = true;
    }
}

   // youtube video upload submit
uploadYoutubeVideoSubmit = ():any => {
    if(this.video_document_csv == null || this.uploadVideoSuccess != true)
      return this._service.error("Must upload a CSV first.");
    const form = new FormData();
    form.append('file_data', this.video_document_csv, this.video_document_csv.name);
    this._service.postFormApi('/content/youtube_upload_video', form).subscribe((response: any) => {
      if (response.status == 200) {
          if(response.body.responseCode === 200){
            this.video_document_csv = null;
            this.videoFileUpload = "";
            this.uploadVideoSuccess = false;
            let data = response.body;
            this._service.success(data.responseMessage);
          }else{
            this.video_document_csv = null;
            this.videoFileUpload = "";
            this.uploadVideoSuccess = false;
            this._service.error('Something went wrong. Please try again');
          }

      }
    });
}

//Upload Content csv
uploadYoutubeContentCSV = ($event):any=> {
  if($event.target.files.length == 0)
    return this._service.error("Must upload a CSV first.");

  this.content_document_csv = <File>$event.target.files[0];
  let name = this.content_document_csv.name;
  let type = this.content_document_csv.type;
  let size = this.content_document_csv.size;
  if (size > 10242880) {
    this._service.error("File is too large. Upload file less than 10 MB");
    this.content_document_csv = null;
    this.contentFileUpload = "";
    return;
  }
  let validCSV = (new RegExp('(' + this.exts.join('|').replace(/\./g, '\\.') + ')$')).test(name);
  if (!validCSV) {
    this._service.error("Upload only CSV format.");
    this.content_document_csv = null;
    this.contentFileUpload = "";
    return;
  } else {
    this.uploadContentSuccess = true;
  }
}

// youtube channel upload submit
uploadYoutubeContentSubmit = ():any => {
    if(this.content_document_csv == null || this.uploadContentSuccess != true)
      return this._service.error("Must upload a CSV first.");
    const form = new FormData();
    form.append('file_data', this.content_document_csv, this.content_document_csv.name);
    this._service.postFormApi('/content/youtube_upload_channel', form).subscribe((response: any) => {
      if (response.status == 200) {
          if(response.body.responseCode === 200){
            this.content_document_csv = null;
            this.contentFileUpload = "";
            this.uploadContentSuccess = false;
            let data = response.body;
            this._service.success(data.responseMessage);
          }else{
            this.content_document_csv = null;
            this.contentFileUpload = "";
            this.uploadContentSuccess = false;
            this._service.error('Something went wrong. Please try again');
          }

      }
    });
}

// Download youtube channel Data
downloadYoutubeChannel = (): void => {
  this._service.getApiCommon('/content/get_channel_data').subscribe((response: any) => {
    if (response.status == 200) {
        if(response.body.responseCode === 200){
          let data = response.body.result;
          if(data.length){
            for (let item of data) {
              this.csvData.push({
                  'ChannelId': item.channelId,
                  'Description': (item.snippet && item.snippet.description) ? item.snippet.description : '',
                  'ChannelTitle': (item.snippet && item.snippet.title) ? item.snippet.title : '',
                  'Views': (item.statistics && item.statistics.viewCount) ? item.statistics.viewCount : '',
                  'Subscribers': (item.statistics && item.statistics.subscriberCount) ? item.statistics.subscriberCount : '',
                  'Videos': (item.statistics && item.statistics.videoCount) ? item.statistics.videoCount : '',
                  'Comments': (item.statistics && item.statistics.commentCount) ? item.statistics.commentCount : '',
                });
            }

            let options = {
              showLabels: true,
              headers: ["ChannelId", "Description","ChannelTitle"," Views","Subscribers","Videos","Comments"]
            };
            new AngularCsv(this.csvData, 'ChannelCsv',options);
          }else{
            this._service.error('Channel not found')
          }
          this._service.success(response.body.responseMessage);
        }else{
          this._service.error('Something went wrong. Please try again');
        }

    }
  });
}

}
