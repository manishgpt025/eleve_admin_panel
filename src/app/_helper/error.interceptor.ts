import { Injectable } from "@angular/core";
import { 
  HttpRequest, 
  HttpHandler, 
  HttpEvent, 
  HttpInterceptor, 
  HttpResponse, 
  HttpErrorResponse 
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";

import { AuthService, AdminService } from "../services";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService, private service: AdminService) { }
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          if (event.status === 200) {
            if(event.body.responseCode == 401){
              this.auth.logout();
            }
          } else {
            this.auth.logout();
          }
        }
      return event;
    }),
    catchError((error: HttpErrorResponse) => {
      let data = {};
      data = {
        reason: error && error.error.reason ? error.error.reason : "",
        status: error.status
      };
      this.auth.logout();
      return throwError(error);
    }));
  }
}
