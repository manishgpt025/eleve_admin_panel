import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSelectChange } from '@angular/material';
import { Validators, FormControl, FormGroup} from '@angular/forms';
import { Ng2ImgMaxService } from 'node_modules/ng2-img-max';
import { AdminService, AuthService } from '../services';

@Component({
  selector: 'app-admin-profile',
  templateUrl: './admin-profile.component.html',
})

export class AdminProfileComponent implements OnInit {
  @ViewChild('pf', { static: true }) myChangePasswordForm;
  adminProfileForm: FormGroup;
  data: any;
  isdlist: string[] = [];
  message: any;
  adminData: any;
  show_password_model: boolean = false;
  adminChangePasswordForm: FormGroup;
  image: HTMLImageElement;
  imageurl: string ="";
  imageurlUpload: string;
  mobile_code: any;
  showSelect: boolean = false;
  hidden_code: any;
  minDate: Date = null;
  maxDate: Date = null;
  skelLoging: boolean = true;
  userId: string = "";

  constructor(
    private auth: AuthService,
    private _service: AdminService,
    private ng2ImgMaxService: Ng2ImgMaxService
  ) { }


  ngOnInit() {
    this.getAdminProfileData();
    this.profileForm();
    this.getISDList();
    this.adminPasswordForm();
    this.imageurl = "";
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 100, 0, 1);
    this.maxDate = new Date(currentYear - 18, 0, 1);
  }

  getLocalhostData = (): any => {
    if(!this.userId){
      return this.userId = this.auth.getData().admin_id;
    }
    return this.userId;
  }

  // Get Profle data
  getAdminProfileData = ():void => {
    this.adminData = [];
    this.skelLoging = true;
    this._service.getApiCommon('/campaign/admin/user_data').subscribe((response: any) => {
      if (response.status == 200) {
        this.skelLoging = false;
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.adminData = this.data.result;
          this.adminProfileForm.get('first_name').setValue(this.adminData.name);
          this.adminProfileForm.get('last_name').setValue(this.adminData.last_name);
          this.adminProfileForm.get('email').setValue(this.adminData.email);
          this.adminProfileForm.get('dob').setValue(this.adminData.dob);
          this.adminProfileForm.get('isd_code').setValue(this.adminData.isd);
          this.adminProfileForm.get('mobile').setValue(this.adminData.mobile);
          this.adminProfileForm.get('phone_ext').setValue(this.adminData.extension);
          this.adminProfileForm.get('team').setValue(this.adminData.team);
          this.adminProfileForm.get('department').setValue(this.adminData.team);
          this.adminProfileForm.get('location').setValue(this.adminData.location);
          if(this.adminData.isd){
            this.mobile_code = this.adminData.isd;
          } else {this.mobile_code = '';}
          if(this.adminData.profile_image){this.imageurl = this.adminData.profile_image;}
        }else return;
      }
    }, error => {
      this.skelLoging = false;
      this.message = this._service.error("Something went wrong! Please try again.");
    });
  }

  // Profile form
  profileForm = ():void => {
    this.adminProfileForm = new FormGroup({
      first_name: new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z]*$')])),
      last_name: new FormControl('', Validators.compose([Validators.pattern('^[a-zA-Z]*$')])),
      dob: new FormControl(''),
      email: new FormControl({ value: '', disabled: true }),
      isd_code: new FormControl(''),
      mobile: new FormControl('', Validators.compose([Validators.minLength(4), Validators.maxLength(14), Validators.pattern('[0-9]*')])),
      phone_ext: new FormControl('', Validators.compose([Validators.pattern('[0-9]*')])),
      team: new FormControl({ value: '',disabled: true }),
      department: new FormControl(''),
      location: new FormControl('', Validators.compose([Validators.required]))
    });
  }

  // get ISD List for mobile numbers
  getISDList = ():void => {
    this._service.getApi('/country/isd').subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.isdlist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.isdlist.push(this.data.result[i]);
          }
        } else {
          this.message = this.data.responseMessage;
        }
      }
    }, error => {
      this.message = "Something went wrong! Please try again.";
    });

  }

  onSubmitProfile = (editData) => {
    if (this.adminProfileForm.invalid)return;
    let adminId = this.getLocalhostData();
    let token = this.auth.getToken();
    let reqData = {
      //"name": editData.first_name.slice(0, 1).toUpperCase() + editData.first_name.slice(1),
      "name": editData.first_name ? editData.first_name.toLowerCase() : "",
      "last_name": editData.last_name || "",
      "mobile": editData.mobile || "",
      "isd": editData.isd_code || "",
      "team": editData.team || editData.department,
      "status": this.adminData.status || "",
      "location": editData.location || "",
      "dob": editData.dob || "",
      "extension": editData.phone_ext || "",
      "role": this.auth.getData().role,
      "id": adminId
    }
    this._service.postApiCommon('/campaign/admin/update', reqData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.getAdminProfileData();
          this.auth.sendToken(token, this.data.result)
          this._service.success("Profile updated successfully");
        } else {
          this.message = this.data.responseMessage;
        }
      }
    }, error => {
      this.message = "Something went wrong! Please try again.";
    });

  }

  openPasswordMdal = ():void => {
    this.adminChangePasswordForm.reset();
    this.show_password_model = true;
  }

  closePasswordModal = ():void => {
    this.myChangePasswordForm.resetForm();
    this.show_password_model = false;
  }

  adminPasswordForm = ():void => {
    this.adminChangePasswordForm = new FormGroup({
      current_password: new FormControl('', Validators.compose([Validators.required])),
      new_password: new FormControl('', Validators.compose([Validators.required,Validators.minLength(8)])),
      re_password: new FormControl('', Validators.compose([Validators.required])),
    });
  }

  onSubmitPassword = (reqData) => {
    if(
      this.adminChangePasswordForm.invalid
      ||
      (reqData.new_password !== reqData.re_password)
      )return;
    if(
      reqData.current_password
      ===
      reqData.new_password
      ) return;
    let req =
    {
      "current_password": reqData.current_password,
      "password": reqData.new_password
    }
    this._service.postApiCommon('/campaign/admin/change_password', req).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this._service.success("Password updated successfully");
          this.closePasswordModal();
          this.getAdminProfileData();
        } else {
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  //Show dynamically profile image change and convert , image validation
  onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let image = event.target.files[0];
      this.ng2ImgMaxService.resizeImage(image, 400, 300).subscribe(result => {
        var reader = new FileReader();
        reader.readAsDataURL(result); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.image = new Image();
          this.image.src = event.target.result;
          this.imageurl = this.image.src;
          this.imageurlUpload = this.image.src;

          if (this.imageurlUpload && this.imageurlUpload != 'undefind') {
            let adminId = this.getLocalhostData();
            let data = {
              "id": adminId,
              "profilePic": this.imageurlUpload
            }
            this._service.postApiCommon('/campaign/admin/user_image_upload', data).subscribe((response: any) => {
              if (response.status == 200) {
                this.data = response.body;
                if (this.data.responseCode == 200) {
                  let token = this.auth.getToken();
                  this.auth.sendToken(token, this.data.result)
                  this._service.success(this.data.responseMessage);
                } else {
                  this._service.error(this.data.responseMessage);
                }
              }
            }, error => {
              this.message = "Something went wrong! Please try again.";
            })
          }

        }
      }, error => {
        this._service.error(error.reason)
      }
      )
    }
  }

  onFirstThingSelect = (event: MatSelectChange) => {
    if(event.value==this.mobile_code){
      return;
    }
    this.mobile_code = event.value;
    this.hidden_code = event.value;
    this.showSelect = false;
  }

  onNoChangeSelect = ():void => {
    this.mobile_code = this.hidden_code;
    this.showSelect = false;
  }

  showSelectOption =(): void => {
    this.showSelect = true;
  }

  openedChange = (opened: boolean) => {
    if(!opened){
      this.showSelect = false;
    }
  }
}
