import { Injectable } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { debounce } from "lodash";

import { ifString } from "../handlers";

@Injectable({
  providedIn: "root"
})
export class ToastsService {
  debounceEvent = debounce(
    (msg, type) => {
      this.showToast(msg, type)
    }, 500
  )

  constructor(private toastrService: ToastrService) { }

  showToast = (message, messageType): void => {
    this.toastrService[messageType](message);
  };

  success = (message = ""): void => {
    if (ifString(message)) {
      this.debounceEvent(message, "success")
    }
  };

  error = (message = ""): void => {
    if (ifString(message)) {
      this.debounceEvent(message, "error");
    }      
  };
}
