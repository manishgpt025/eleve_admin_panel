import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { AuthService } from './auth.service';
import _ from 'lodash';
import * as CryptoJS from 'crypto-js';
import { environment } from '../../environments/environment';

import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  baseURL = environment.baseURL;
  baseURLCommon = environment.baseURLCommon;

  stepType: string;
  constructor(@Inject(DOCUMENT) private document: Document, private auth:AuthService, private http: HttpClient, private toastr: ToastrService, private spinner: NgxSpinnerService) { }
  showToast = (msg, type) => this.toastr[type](msg);
  debounceEvent = _.debounce((msg, type)=> {this.showToast(msg, type)}, 500)

  // For toastr msg
  success(msg) {
    if(typeof msg !== 'string') return;
    this.debounceEvent(msg, "success")
  }

  error(msg) {
    if(typeof msg !== 'string') return;
      this.debounceEvent(msg, "error");
  }

  // For spinner
  show() {
    this.spinner.show();
  }
  hide() {
    this.spinner.hide();
  }

  //post Api
  postApi(url, data): Observable<any> {
    let userData = this.auth.getData();
    let userToken = this.auth.getToken();
    let httpOptions;
    httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json",'user_id':userData._id,'authorization' : 'Bearer '+ userToken }),
      observe: 'response'
    }
    return this.http.post((this.baseURL + url), data, httpOptions);
  }

  //post Api
  postApiCommon(url, data): Observable<any> {
    let userData = this.auth.getData();
    let userToken = this.auth.getToken();
    let httpOptions;
    httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json",'user_id':userData._id ,'authorization' : 'Bearer '+ userToken }),
      observe: 'response'
    }
    return this.http.post((this.baseURLCommon + url), data, httpOptions)
  }

  //post Api
   postApiLogin(url, data): Observable<any> {
    let httpOptions;
    httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json" }),
      observe: 'response'
    }

    return this.http.post((this.baseURLCommon + url), data, httpOptions)
  }

  // post form api
  postFormApi(url, data): Observable<any> {
    let userData = this.auth.getData();
    let userToken = this.auth.getToken();
    let httpOptions;
    httpOptions = {
        headers: new HttpHeaders({ 'user_id':userData._id ,'authorization' : 'Bearer '+ userToken }),
      observe: 'response'
    }
    return this.http.post((this.baseURLCommon + url), data, httpOptions)
  }

  postFormApiWithHeader(url, data): Observable<any> {
    let userData = this.auth.getData();
    let userToken = this.auth.getToken();
    this.stepType = '1';
    let httpOptions;
    httpOptions = {
        headers: new HttpHeaders({"stepType": this.stepType,"user_id":userData._id,'authorization' : 'Bearer '+ userToken }),
      observe: 'response'
    }
    return this.http.post((this.baseURLCommon + url), data, httpOptions)
  }

  postApiWithHeader(url, data, type:string): Observable<any> {
    let userData = this.auth.getData();
    let userToken = this.auth.getToken();
    if(type == '2B'){
      this.stepType = '2B';
    }else if(type == '2C'){
      this.stepType = '2C';
    }else{
      this.stepType = '3';
    }
    let httpOptions;
    httpOptions = {
        headers: new HttpHeaders({"Content-Type": "application/json", "stepType": this.stepType,"user_id":userData._id,'authorization' : 'Bearer '+ userToken }),
      observe: 'response'
    }
    return this.http.post((this.baseURLCommon + url), data, httpOptions)
  }

  getApi(url): Observable<any> {
    let userData = this.auth.getData();
    let userToken = this.auth.getToken();
    let httpOptions;
    httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json",'user_id':userData._id ,'authorization' : 'Bearer '+ userToken }),
      observe: 'response'
    }
    return this.http.get((this.baseURL + url), httpOptions);
  }

  getApiCommon(url): Observable<any> {
    let userData = this.auth.getData();
    let userToken = this.auth.getToken();
    let httpOptions;
    httpOptions = {
      headers: new HttpHeaders({ "Content-Type": "application/json", 'user_id':userData._id ,'authorization' : 'Bearer '+ userToken }),
      observe: 'response'
    }
    return this.http.get((this.baseURLCommon + url), httpOptions);
  }

  public generateThumbnail(videoFile: Blob): Promise<string> {
    const video: HTMLVideoElement = this.document.createElement('video');
    const canvas: HTMLCanvasElement = this.document.createElement('canvas');
    const context: CanvasRenderingContext2D = canvas.getContext('2d');
    return new Promise<string>((resolve, reject) => {
      canvas.addEventListener('error',  reject);
      video.addEventListener('error',  reject);
      video.addEventListener('canplay', event => {
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        context.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
        resolve(canvas.toDataURL());
      });
      if (videoFile.type) {
        video.setAttribute('type', videoFile.type);
      }

      video.preload = 'auto';
      video.src = window.URL.createObjectURL(videoFile);
      video.load();
    });
  }
}
