import { Injectable, EventEmitter } from "@angular/core";
import { Router,NavigationEnd } from "@angular/router";
import * as CryptoJS from "crypto-js";


@Injectable()
export class AuthService {
  userDataEnr: any;
  loggingUserData: any = "";
  private previousUrl: string;
  private currentUrl: string;
  updatedData: EventEmitter<number> = new EventEmitter();
  constructor(
    private myRoute: Router
  ){
    this.currentUrl = this.myRoute.url;
    myRoute.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;
      };
    });
  }

  public getPreviousUrl() {
    return this.previousUrl;
  }

  sendToken = (token: string, userData: any): void => {
    this.updatedData.emit(userData);
    this.userDataEnr = CryptoJS.AES.encrypt(JSON.stringify(userData), "admin");
    localStorage.setItem("LoggedInUser", token);
    localStorage.setItem("userData", this.userDataEnr);
  };

  getData() {
    let data = localStorage.getItem("userData");
    if (data != null) {
      return JSON.parse(CryptoJS.AES.decrypt(data, 'admin').toString(CryptoJS.enc.Utf8));
    } else {
      return data;
    }
  }

  getToken() {
    return localStorage.getItem("LoggedInUser");
  }

  isLoggedIn() {
    return this.getToken() !== null;
  }

  logout() {
    localStorage.removeItem("LoggedInUser");
    localStorage.removeItem("userData");
    this.myRoute.navigate(["/"]);
  }

  logout403() {
    localStorage.removeItem("LoggedInUser");
    localStorage.removeItem("userData");
    this.myRoute.navigate(["403"]);
  }

  getSideChangeEmitter() {
    return this.updatedData;
  }
}