import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';

import { AuthService } from '../../services';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  currentRoute: string = "";
  adminPermission: string = "";
  hideShowMenu: boolean = true;
  constructor(
     private auth:AuthService,
     private router:Router
  ){
    router.events.subscribe((event: Event) => {
      if(event instanceof NavigationEnd) {
        this.currentRoute = event.url.split('/')[1];
      }
    })
  }

  ngOnInit() {
    this.adminPermission = this.auth.getData().role;
  }

  onClickRoute = (route): void => {
    this.router.navigate([route]);
  };


}
