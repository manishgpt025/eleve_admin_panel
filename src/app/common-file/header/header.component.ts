import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
  userName: any;
  subscription: any;
  adminPermission: any;
  userPic: any;

  constructor(private auth:AuthService, private router: Router) { }

  ngOnInit() {
    this.userPic ="assets/images/user.jpg";
    this.getUserData();
    this.subscription = this.auth.getSideChangeEmitter()
    .subscribe(item => this.selectedUpdatedData(item));
  }

  logout(){
    this.auth.logout();
  }

  getUserData(){
    let data = this.auth.getData();
    this.userName= data.name;
    if(data.profile_image)
      this.userPic= data.profile_image;
    this.adminPermission= data.role;
  }

  selectedUpdatedData(response:any) {
    if(response.name){
      this.userName= response.name;
      if(response.profile_image)
        this.userPic= response.profile_image;
    }
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
