import { Component, OnInit, ElementRef, HostListener, ViewChild } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import _ from "lodash";
import { Router } from "@angular/router";
import { MatSelect, MatSelectChange } from "@angular/material";
import { Ng2ImgMaxService } from "node_modules/ng2-img-max";

import { AdminService, AuthService, ToastsService } from "src/app/services";
import { ifStatusOkay, getErrorMessage, findIndexAndItem } from "../../handlers";

@Component({
  selector: "app-admin-list",
  templateUrl: "./admin-list.component.html",
})

export class AdminListComponent implements OnInit {
  @ViewChild('pf', { static: true }) myResetForm;
  @ViewChild('f', { static: true }) myCreateForm;
  @ViewChild('g', { static: true }) myEditForm;
  loading: boolean = true;
  createForm: FormGroup;
  data: any;
  message: string;
  isdlist: any[];
  show_create_admin: boolean = false;
  isd_code: any;
  hidden_code: any;
  showSelect: boolean = false;
  mobile_number: any;
  managelistData: any = [];
  show_edit_model: boolean = false;
  userId: any;
  adminData: any;
  imageurl: any;
  adminProfileForm: FormGroup;
  image: HTMLImageElement;
  imageurlUpload: string;

  filters: any = {
    keyword: "",
    filter: "",
    sort: "",
    loc: ""
  };
  emptyLoadingArray: Array<number> = [1, 2, 3, 4, 5,];
  pageDetails: any = {
    limit: 10,
    page: 1,
    pages: 1,
  };
  showFilters: boolean = true;
  minDate: Date = null;
  maxDate: Date = null;
  searchKeyInput: string = "";

  statusActiveInactive: any;
  show_active_model: boolean = false;
  mobile_code: any;
  showCreateSelect: boolean = false;
  create_mobile_code: any;
  statusShowEdit: any;
  filterAdmin: string = "";
  sortAdmin: string = "";
  searchDataValue: string;
  searchSymbolValue: boolean = true;
  pageNumber: number = 1;
  statusFilter: string ="";
  locationFilter: string ="";
  respAdmin: any;

  debounceGetListEvent = _.debounce(()=> {
    this.loading = true;
    this.managelistData = [];
    this.getAdminList();
  }, 500)
  adminResetPasswordForm: FormGroup;
  reset_password_model: boolean = false;
  authAdminData: any = "";
  selectedAdminTeam: string = "";


  constructor(
    private authService: AuthService,
    private router: Router,
    private apiService: AdminService,
    private el: ElementRef,
    private ng2ImgMaxService: Ng2ImgMaxService,
    private toastsService: ToastsService
  ) { }

  ngOnInit() {
    this.authAdminData = this.authService.getData();
    this.getAdminList();
    this.createFormVal();
    this.getISDList();
    this.profileForm();
    this.adminResetPasswordFormValidation();
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 100, 0, 1);
    this.maxDate = new Date(currentYear - 18, 0, 1);
  }

  getAdminList() {
    const { page: pageNumber, limit } = this.pageDetails;
    let params = {
      pageNumber,
      limit,
      ...this.filters,
    };
    this.loading = true;
    this.apiService.postApiCommon('/campaign/adminlist', params).subscribe((response: any) => {
      if (ifStatusOkay(response)) {
        this.data = response.body;
        if (this.data.responseCode === 200) {
          this.respAdmin=this.data.result.docs?this.data.result.docs:[];
          if(this.respAdmin.length){
            this.setPageDetails(this.data.result);
            this.setAdminList(this.data.result);
          }
          this.create_mobile_code = '';
        } else {
          this.message = this.data.responseMessage;
        }
        this.loading = false
      }
    }, error => {
      this.loading = false
      this.message = "Something went wrong! Please try again.";
    });
  }

  setAdminList = ({
    docs
  }): void => {
    this.managelistData = [
      ...this.managelistData,
      ...docs
    ];
  };

  setPageDetails = ({
    limit,
    page,
    pages,
  }): void => {
    this.pageDetails = {
      limit,
      page,
      pages,
    };
  };
  // create admin validation
  createFormVal() {
    this.createForm = new FormGroup({
      first_name: new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])),
      last_name: new FormControl('', Validators.compose([Validators.pattern('^[a-zA-Z ]*$')])),
      email: new FormControl('', Validators.compose([Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/)])),
      mobile_number: new FormControl('', Validators.compose([Validators.minLength(4), Validators.maxLength(14), Validators.pattern('[0-9]*')])),
      department: new FormControl('', Validators.compose([Validators.required])),
      isd: new FormControl('+91'),
    });
  }

  // submit create admin
  onSubmitCreateForm(data) {
    if (this.createForm.invalid) {
      return;
    }
    let params = {
      'email': data.email,
      'name': data.first_name.trim().toLowerCase(),
      'last_name': data.last_name.trim().toLowerCase(),
      'mobile': data.mobile_number,
      'isd': data.isd,
      'team': data.department
    }
    this.apiService.postApiCommon('/campaign/admin/create', params)
      .subscribe((response: any) => {
        if (response.status == 200) {
          this.data = response.body;
          if (this.data.responseCode == 200) {
            this.toastsService.success(this.data.responseMessage);
            this.createForm.reset();
            this.closeCreateAdminModal();
            this.refreshAllFilter(true);
          } else if (this.data.responseCode == 409) {
            this.message = this.data.responseMessage;
            this.toastsService.error(this.data.responseMessage);
          } else if (this.data.responseCode == 403) {
            this.message = this.data.responseMessage;
            this.toastsService.error(this.data.responseMessage);
          } else {
            this.message = this.data.responseMessage;
          }
        }
      }, error => {
           this.handleApiError(error);
      }
    );
  };

  handleApiError = (error): void => {
    this.toastsService.error(getErrorMessage(error));
  }

  getISDList() {
    this.apiService.getApi('/country/isd')
      .subscribe((response: any) => {
        if (response.status == 200) {
          this.data = response.body;
          if (this.data.responseCode == 200) {
            this.isdlist = [];
            for (let i = 0; i < this.data.result.length; i++) {
              this.isdlist.push(this.data.result[i]);
            }
          } else {
            this.message = this.data.responseMessage;
          }
        }
      }, error => {
        this.handleApiError(error);
      }
    );
  }

  //cancel items in create form
  cancelCreate() {
  //  this.myCreateForm.reset();
    this.closeCreateAdminModal();
  }

  openCreateAdminModal() {
    this.show_create_admin = true;
  }

  closeCreateAdminModal() {
    this.myCreateForm.resetForm();
    this.show_create_admin = false;
  }

  onFirstThingSelect(event: MatSelectChange) {
    if (event.value == this.mobile_code) {
      return;
    }
    this.mobile_code = event.value;
    this.hidden_code = event.value;
    this.showSelect = false;
  }

  onNoChangeSelect(event: MatSelect) {
    this.mobile_code = this.hidden_code;
    this.showSelect = false;
  }

  showSelectOption() {
    this.showSelect = true;
  }
  openedChange(opened: boolean) {
    if (!opened) {
      this.showSelect = false;
    }
  }

  // Open confirm box
  adminStatus(status, user_id) {
    this.statusActiveInactive = status.target.value;
    this.userId = user_id;
    this.show_active_model = true;
  }

  // Close confirm box
  adminStatusClose() {
    this.show_active_model = false;
  }

  toggleAdminStatus = ({
    user_id,
    status
  }) => {
    const { managelistData: list } = this;
    const { obj, index } = findIndexAndItem(list, "admin_id", user_id);
    if (index !== undefined) {
      list.splice(index, 1, {
        ...obj,
        status
      });
      this.managelistData = [...list];
    }
  }

  // Change status and udpate the list
  adminStatusChange() {
    let params = {
      "user_id": this.userId,
      "status": this.statusActiveInactive
    }
    this.apiService.postApiCommon('/campaign/admin/status_update', params)
      .subscribe((response: any) => {
        if (response.status == 200) {
          this.data = response.body;
          if (this.data.responseCode == 200) {
            this.toggleAdminStatus(params);
            this.show_active_model = false;
            this.toastsService.success(this.data.responseMessage);
          } else {
            this.show_active_model = false;
            this.message = this.data.responseMessage;
          }
        }
      }, error => {
          this.handleApiError(error);
      }
    );
  };

  // Clear cross button
  clearCrosValue() {
    this.searchDataValue = "";
    this.searchSymbolValue = true;
    this.getAdminList();
  }

  // Refresh Button
  refreshAllFilter(callListAgain = false) {
    this.pageNumber = 1;
    this.statusFilter = "";
    this.sortAdmin = "";
    this.filterAdmin = "";
    this.searchDataValue = "";
    this.managelistData = [];
    this.filters = {
      keyword: "",
      filter: "",
      sort: "",
      loc: ""
    };
    this.pageDetails = {
      limit: 10,
      page: 1,
      pages: 1,
    }
    if (callListAgain) {
      this.getAdminList();
    }
  }

  changeStatusList(statusChange: string) {
    this.statusFilter = statusChange;
    this.pageNumber = 1;
    this.managelistData = [];
    this.getAdminList()
  }

  changeLocaton(locationChange: string){
    this.locationFilter = locationChange;
    this.pageNumber = 1;
    this.managelistData = [];
    this.getAdminList()
  }

  // Change Status
  changeSortingData() {
    if (this.sortAdmin == "")
      this.sortAdmin = '1';
    else if (this.sortAdmin == "1")
      this.sortAdmin = '0';
    else
      this.sortAdmin = '0';
    this.getAdminList()
  }

  // Load more function call scroll down
  onScrollDown() {
    if(this.respAdmin.length === 5){
      this.pageNumber=this.pageNumber+1;
      this.getAdminList();
    }
  }

  openeditModal(admin) {
    this.userId = admin.admin_id;
    this.adminProfileForm.get('first_name').setValue(admin.name);
    this.adminProfileForm.get('last_name').setValue(admin.last_name);
    this.adminProfileForm.get('email').setValue(admin.email);
    this.adminProfileForm.get('dob').setValue(admin.dob || "");
    this.adminProfileForm.get('isd_code').setValue(admin.isd || "+91");
    this.adminProfileForm.get('mobile').setValue(admin.mobile);
    this.adminProfileForm.get('phone_ext').setValue(admin.extension || "");
    this.adminProfileForm.get('team').setValue(admin.team || "");
    this.adminProfileForm.get('location').setValue(admin.location || "");
    this.adminProfileForm.get('hidden_user_id').setValue(admin.admin_id);
    this.adminProfileForm.get('status').setValue(admin.status);
    this.adminProfileForm.get('role').setValue(admin.role);
    this.statusShowEdit = admin.status;
    this.selectedAdminTeam = admin.team || "";
    this.adminData = admin;
    if (admin.isd) {
      this.mobile_code = admin.isd;
    }else{
      this.mobile_code = '+91';
    }
    if (admin.profile_image) {
      this.imageurl = admin.profile_image;
    } else {
      this.imageurl = "assets/images/user.jpg";
    }
    this.show_edit_model = true;
  }

  // edit profile validation
  // Profile form
  profileForm() {
    this.adminProfileForm = new FormGroup({
      first_name: new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])),
      last_name: new FormControl('', Validators.compose([Validators.pattern('^[a-zA-Z ]*$')])),
      dob: new FormControl(''),
      hidden_user_id: new FormControl(''),
      email: new FormControl({ value: '', disabled: true }),
      isd_code: new FormControl('', Validators.compose([Validators.required])),
      mobile: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]*')])),
      phone_ext: new FormControl('', Validators.compose([Validators.pattern('[0-9]*')])),
      status: new FormControl(''),
      team: new FormControl({ value: "", disabled: true }),
      location: new FormControl('', Validators.compose([Validators.required])),
      role: new FormControl('')
    });
  }

  adminResetPasswordFormValidation = (): void => {
    this.adminResetPasswordForm = new FormGroup({
      new_password: new FormControl('', Validators.compose([Validators.required,Validators.minLength(8)])),
      re_password: new FormControl('', Validators.compose([Validators.required])),
      admin_id: new FormControl('', Validators.compose([Validators.required])),
    });
  }

  openResetPasswordModal = ({admin_id}): any => {
    this.adminResetPasswordForm.get('admin_id').setValue(admin_id);
    this.reset_password_model = true;
  }

  closeResetPasswordModal = (): void => {
    this.myResetForm.resetForm();
    this.reset_password_model = false;
  }

  onSubmitResetPassword = ({admin_id, new_password , re_password}): void => {
    if(this.adminResetPasswordForm.invalid)return;
    if(new_password !== re_password)return;
    let data = {
      admin_id,
      new_password,
      re_password
    }
    this.apiService.postApiCommon("/campaign/admin/reset-password", data).subscribe((response: any) => {
      if (response.status == 200) {
        let data = response.body;
        if (data.responseCode == 200) {
          this.toastsService.success(data.responseMessage);
          this.closeResetPasswordModal();
        } else {
          this.apiService.error(data.responseMessage);
        }
      }
    }, error => {
      this.toastsService.error("Something went wrong! Please try again.");
    });

  }

  closeeditModal = (): void => {
    this.myEditForm.resetForm();
    this.show_edit_model = false;
    this.selectedAdminTeam = "";
  }

  changeProfileDetails = ({
    name,
    last_name,
    mobile,
    isd: isd_code,
    status,
    location,
    dob,
    extension,
    id,
    role
  }): void => {
    const { managelistData: list } = this;
    const { obj, index } = findIndexAndItem(list, "admin_id", id);
    if (index !== undefined) {
      list.splice(index, 1, {
        ...obj,
        name,
        last_name,
        mobile,
        isd_code,
        status,
        location,
        dob,
        extension,
        role
      });
      this.managelistData = [...list];
    }
  }

  onSubmitProfile(editData) {
    if (this.adminProfileForm.invalid) {
      return;
    }
    let data = this.authService.getData();
    let params = {
      "name": editData.first_name.trim().toLowerCase(),
      "last_name": editData.last_name.trim().toLowerCase(),
      "mobile": editData.mobile,
      "isd": editData.isd_code || this.mobile_code,
      "status": editData.status,
      "location": editData.location,
      "dob": editData.dob,
      "extension": editData.phone_ext,
      "team": this.selectedAdminTeam,
      "id": editData.hidden_user_id,
      "role": editData.role
    }

    this.apiService.postApiCommon("/campaign/admin/update", params)
      .subscribe((response: any) => {
        if (response.status == 200) {
          this.data = response.body;
          if (this.data.responseCode == 200) {
            this.toastsService.success("Profile updated successfully");
            this.closeeditModal();
            this.changeProfileDetails(params);
          } else {
            this.message = this.data.responseMessage;
          }
        }
      }, error => {
          this.handleApiError(error);
      }
    );
  }

  //Show dynamically profile image change and convert , image validation
  onImageChange(event, user_id) {
    if (event.target.files && event.target.files[0]) {
      let image = event.target.files[0];
      this.ng2ImgMaxService.resizeImage(image, 400, 300).subscribe(result => {
        var reader = new FileReader();
        reader.readAsDataURL(result); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.image = new Image();
          this.image.src = event.target.result;
          this.imageurl = this.image.src;
          this.imageurlUpload = this.image.src;
          if (this.imageurlUpload && this.imageurlUpload != 'undefind') {
            let userId = user_id;
            let data = {
              "id": userId,
              "profilePic": this.imageurlUpload
            }
            this.apiService.postApiCommon('/campaign/admin/user_image_upload', data).subscribe((response: any) => {
              if (response.status == 200) {
                this.data = response.body;
                if (this.data.responseCode == 200) {
                  this.refreshAllFilter(true);
                  // this.getAdminList();
                  this.toastsService.success(this.data.responseMessage);
                } else {
                  this.toastsService.error(this.data.responseMessage);
                }
              }
            }, error => {
                this.handleApiError(error);
            })
          }

        }
      }, error => {
        this.toastsService.error(error.reason)
      }
      )
    }
  }

  onFirstCreateThingSelect(event: MatSelectChange) {
    if (event.value == this.create_mobile_code) {
      return;
    }
    this.create_mobile_code = event.value;
    this.hidden_code = event.value;
    this.showCreateSelect = false;
  }

  onNoCreateChangeSelect(event: MatSelect) {
    this.create_mobile_code = this.hidden_code;
    this.showCreateSelect = false;
  }

  showCreateSelectOption() {
    this.showCreateSelect = true;
  }
  CreateopenedChange(opened: boolean) {
    if (!opened) {
      this.showCreateSelect = false;
    }
  }

  onChangeFilter = (
    value: string,
    key: string
  ): void => {
    this.filters = {
      ...this.filters,
      [key]: value
    };
    this.pageDetails = {
      ...this.pageDetails,
      page: 1
    };
    this.debounceGetListEvent();
  };

  onChangeSearchInput = ({ target:
    { value }
  }): void => {
    const { keyword: currentKeyword } = this.filters;
    this.filters = {
      ...this.filters,
      keyword: value
    };
    this.pageDetails = {
      ...this.pageDetails,
      page: 1
    };
    if (value.length > 1) {
      this.debounceGetListEvent();
    }
    if (!value.length && this.searchKeyInput.length) {
      this.pageDetails = {
        ...this.pageDetails,
        page: 1
      };
      this.loading = true;
      this.managelistData = [];
      this.getAdminList();
    }
    this.searchKeyInput = value;
  };

  clearSearchInput = (): void => {
    this.searchDataValue = "";
    this.filters = {
      ...this.filters,
      keyword: ""
    };
    this.pageDetails = {
      ...this.pageDetails,
      page: 1
    };
    this.loading = true;
    this.managelistData = [];
    this.getAdminList();
  }

  // Window Scroll Events
  decideNextPage = ({
    page: currentPage,
    pages: totalPages,
    loading
  }): any => {
    let shouldCallDataAgain = true;
    if (
      !loading &&
      (totalPages > currentPage) &&
      (((window.innerHeight + window.scrollY) + 200)
      >= document.body.offsetHeight)
    ) {
      return {
        shouldCallDataAgain,
        nextPage: ++currentPage
      };
    } else {
      return {
        shouldCallDataAgain: false,
        nextPage: currentPage
      };
    };
  };

  @HostListener('window:scroll', ['$event'])
  onScrollEvent = (): void => {
    const {
      shouldCallDataAgain,
      nextPage,
    } = this.decideNextPage({
      loading: this.loading,
      ...this.pageDetails
    });
    if (shouldCallDataAgain) {
      this.pageDetails = {
        ...this.pageDetails,
        page: nextPage
      };
      this.getAdminList();
    }
  };
  // Window Scroll Events

  ifFiltersActive = (): boolean => {
    const defaultFilters = {
      keyword: "",
      filter: "",
      sort: "",
      loc: ""
    }
    return !_.isEqual(this.filters, defaultFilters);
  };


}
