import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatNativeDateModule } from '@angular/material/core';
import { 
     MatButtonModule, 
     MatCheckboxModule, 
     MatInputModule, 
     MatMenuModule,
     MatIconModule, 
     MatSelectModule, 
     MatRadioModule, 
     MatExpansionModule, 
     MatTabsModule, 
     MatDatepicker, 
     MatDatepickerModule
   } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Ng2ImgMaxModule } from 'ng2-img-max';
import { Ng5SliderModule } from 'ng5-slider';
import { NgSelectModule } from '@ng-select/ng-select';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AdminManagementRoutingModule } from './admin-management-routing.module';
import { AdminListComponent } from './admin-list/admin-list.component';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { CommonFileModule } from '../common-file/common-file.module';

@NgModule({
  declarations: [AdminListComponent, AdminLayoutComponent],
  imports: [
    CommonModule,
    CommonFileModule,
    AdminManagementRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    Ng2ImgMaxModule,
    MatNativeDateModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatSelectModule,
    MatCheckboxModule,
    MatRadioModule,
    Ng5SliderModule,
    MatExpansionModule,
    MatTabsModule,
    Ng5SliderModule,
    NgSelectModule,
    MatDatepickerModule,
    InfiniteScrollModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AdminManagementModule { }
