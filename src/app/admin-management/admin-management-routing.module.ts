import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminListComponent } from './admin-list/admin-list.component';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';

const routes: Routes = [
  {
    path: "", component: AdminLayoutComponent,
    children: [
      { path: "", component: AdminListComponent },
      { path: "list", component: AdminListComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminManagementRoutingModule { }
