import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./auth/login/login.component";
import { ModalComponent } from './_modal/modal/modal.component';
import { UpdatePasswordComponent } from './auth/update-password/update-password.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { AuthGuard } from './_guard/auth.guard';
import { LoginGuard } from './_guard/login.guard';
import { FourZeroFourComponent } from './not-found/four-zero-four/four-zero-four.component';
import { FourZeroThreeComponent } from './not-found/four-zero-three/four-zero-three.component';
import { SkeletonComponent } from './skeleton/skeleton.component';

const routes: Routes = [
  { path: "", component: LoginComponent, canActivate: [LoginGuard] },
  { path: "login", component: LoginComponent, canActivate: [LoginGuard] },
  { path: "organisation-modal", component: ModalComponent, canActivate: [AuthGuard] },
  { path: "update-password", component: UpdatePasswordComponent, canActivate: [AuthGuard] },
  { path: "admin-profile", component: AdminProfileComponent, canActivate: [AuthGuard] },
  { path: "403", component: FourZeroThreeComponent },
  { path: "404", component: FourZeroFourComponent, canActivate: [AuthGuard] },
  { path: "skeleton", component: SkeletonComponent, canActivate: [AuthGuard] },
  {
    path: "advertiser",
    loadChildren: "./advertiser/advertiser.module#AdvertiserModule", canActivate: [AuthGuard]
  },
  {
    path: "admin",
    loadChildren: "./admin-management/admin-management.module#AdminManagementModule", canActivate: [AuthGuard]
  },
  {
    path: "influencer",
    loadChildren: "./influencer-management/influencer-management.module#InfluencerManagementModule", canActivate: [AuthGuard]
  },
  {
    path: "campaign",
    loadChildren: "./campaign/campaign.module#CampaignModule", canActivate: [AuthGuard]
  },
  {
    path: "board-detail",
    loadChildren: "./collaboration-board/collaboration-board.module#CollaborationBoardModule", canActivate: [AuthGuard]
  },
  {
    path: "board",
    loadChildren: "./collaboration/collaboration.module#CollaborationModule", canActivate: [AuthGuard]
  },
  {
    path: "content",
    loadChildren: "./content/content.module#ContentModule", canActivate: [AuthGuard]
  },
  { path: "**", redirectTo: "/", pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
//