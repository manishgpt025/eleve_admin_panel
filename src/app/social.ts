export class Social {
   
    facebook: facebookAccount[]
    instagram: instagramAccount[]
    twitter: twitterAccount[]
}
export class facebookAccount {
    platform: string
    handle: string
    url: string
}
export class instagramAccount {
    platform: string
    handle: string
    url: string
}
export class twitterAccount {
    platform: string
    handle: string
    url: string
}

export class blogAccount {
    platform: string
    handle: string
    url: string
}


export class pinterestAccount {
    platform: string
    handle: string
    url: string
}


export class youtubeAccount {
    platform: string
    handle: string
    url: string
}

export class linkedinAccount {
    platform: string
    handle: string
    url: string
}

export class snapchatAccount {
    platform: string
    handle: string
    url: string
}

