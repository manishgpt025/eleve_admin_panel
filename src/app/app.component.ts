import { Component } from "@angular/core";
import {
  Event,
  NavigationCancel,
  NavigationEnd,
  NavigationError,
  NavigationStart,
  Router
} from "@angular/router";
import { AdminService } from './services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  fullScreenLoader: boolean = true;
  loading: boolean = false;

  constructor (
    private router: Router,
    private _service: AdminService
  ) {
    this.router.events.subscribe((event: Event) => {
      this.setRouteLoader(event);
    });
  }

  ngOnInit() {
    window.addEventListener("offline", this.changeConnectionStatus);
    window.addEventListener("online", this.changeConnectionStatus);
  }

  setRouteLoader = (event: any): void => {
    switch (true) {
      case event instanceof NavigationStart: {
        if (event.url === "/" || event.url === "/login") {
          this.fullScreenLoader = true;
        } else {
          this.loading = true;
        }
        break;
      }
      case event instanceof NavigationEnd:
      case event instanceof NavigationCancel:
      case event instanceof NavigationError: {
        this.loading = false;
        this.fullScreenLoader = false;
        break;
      }
      default: {
        break;
      }
    }
  };
  
  changeConnectionStatus = (event): void => {
    if (event.type === "offline") {
      this.fullScreenLoader = true;
      this._service.error('Your Internet connection has been lost. Please check!');
    } else {
      this.fullScreenLoader = false;
    }
  };

}
