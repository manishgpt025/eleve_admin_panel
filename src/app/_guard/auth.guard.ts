import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from
'@angular/router';
import { Observable } from 'rxjs';
import { Router ,ActivatedRoute,Event, NavigationEnd} from '@angular/router';
import { AuthService } from '../services';
@Injectable()
export class AuthGuard implements CanActivate {
  private arrayUrls = [
    'admin'
  ]
  constructor(
    private auth: AuthService,
    private myRoute: Router
  ){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):
    Observable<boolean> | Promise<boolean> | boolean {
    if(this.auth.isLoggedIn()){
      let data = this.auth.getData();
      let adminPermission= data.role || 'admin';
      let curUrl = state.url.split('/')[1];
      let findUrl= this.arrayUrls.includes(curUrl);
      if(findUrl && adminPermission == 'admin' ){
        this.myRoute.navigate(["/admin-profile"]);
        return false;
      }
      return true;
    }else{
      this.myRoute.navigate(["/"]);
      return false;
    }
  }
}