import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BloggerLayoutComponent } from './blogger-layout/blogger-layout.component';
import { BloggerListComponent } from './blogger-list/blogger-list.component';

const routes: Routes = [
  {
    path: '', component: BloggerLayoutComponent,
    children: [
      { path: '', component: BloggerListComponent },
      { path: 'list', component: BloggerListComponent },
      
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BloggerRoutingModule { }
