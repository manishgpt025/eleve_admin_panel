import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatButtonModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonFileModule } from '../common-file/common-file.module';
import { BloggerRoutingModule } from './blogger-routing.module';
import { BloggerLayoutComponent } from './blogger-layout/blogger-layout.component';
import { BloggerListComponent } from './blogger-list/blogger-list.component';

@NgModule({
  declarations: [BloggerLayoutComponent, BloggerListComponent],
  imports: [
    CommonModule,
    CommonFileModule,
    FormsModule,
    ReactiveFormsModule,
    BloggerRoutingModule,
    MatButtonModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BloggerModule { }
