
export const ifStatusOkay = ({ status, body: {
    responseCode
  } 
}) => (
  status === 200 
  && responseCode === 200
);

export const getErrorMessage = ({ status }) => {
  return status == 404 
    ? "Service not found" 
    : "Something went wrong! Please try again.";
};

export const getResult = ({
  body: {
    result
  },
}) => ({ ...result });
  