
export const ifObject = obj => {
  return obj && typeof obj === "object" && Array.isArray(obj) === false;
};

export const ifObjectEmpty = obj => {
  return ifObject(obj) && !Object.keys(obj).length;
};

export const updateKeyValueInObject = (obj, key, value) => {
  if (ifObject(obj) && key && value) {
    return {
      ...obj,
      key: value
    };
  }
  return obj;
};
