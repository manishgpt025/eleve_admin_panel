// import { assets } from 'src/assets/images';
const CDN_URL = 'https://cdn.eleveglobal.com';

//get file type icon according to the mimetype
export const getFileIcon = (file: string) => {
  let mimeType = getMimeType(file);
  let imgURL = '';
   switch(mimeType) {
     case "pdf" : {
       imgURL = `${CDN_URL}/assets/pdf.svg`
       break;
     }
     case "xlsx":
     case "xls" : {
       imgURL = `${CDN_URL}/assets/xls.svg`
       break;
     }
     case "doc":
     case "docx" : {
       imgURL = `${CDN_URL}/assets/doc.svg`
       break;
     }
     case "pptx":
     case "ppt" : {
       imgURL = `${CDN_URL}/assets/ppt.svg`
       break;
     }
     case "txt" : {
       imgURL = `${CDN_URL}/assets/txt.svg`
       break;
     }
     case "mp3" : {
       imgURL = `${CDN_URL}/assets/mp3.svg`
       break;
     }
     default : {
       imgURL = `${CDN_URL}/${file}`
       break;
     }
   }
  return imgURL
}

// gives your mimetype of a file
export const getMimeType = (file: string) => {
  let parts = file.split('.');
  return parts[parts.length - 1];
}

// gives file name of url
export const getFileName = (file: string) => {
  let parts = file.split('/');
  return parts[parts.length - 1];
}

//get board id
export const getBoardId = () => {
  let route = window.location.href;
  let path = route.split('/');
  return path[path.length - 1];
}

//get initials
export const getInitials = (name = ""): string => {
  if (!name) {
    return "";
  }
  const [firstWord = "", secondWord = ""] = name.split(" ");
  if (!firstWord) {
    return "";
  }
  let initials = firstWord.charAt(0).toUpperCase();
  initials = secondWord
    ? initials.concat(secondWord.charAt(0).toUpperCase())
    : initials;
  return initials;
};
