import { ifArray, ifArrayEmpty } from "./array-handler";
import { ifString, ifStringEmpty } from "./string-handler";
import { ifObject, ifObjectEmpty,  } from "./object-handler";
import { ifNumber, ifNumberEmpty } from "./number-handler";
import { ifFunction } from "./function-handler";

export const ifEmpty = data => {

  if (ifObject(data)) {
    return ifObjectEmpty(data);
  }

  if (ifArray(data)) {
    return ifArrayEmpty(data);
  }

  if (ifString(data)) {
    return ifStringEmpty(data);
  }

  if (ifNumber(data)) {
    return ifNumberEmpty(data);
  }

  if (ifFunction(data)) {
    return ifEmpty(data());
  }

  return true;
};

//Get Upper case name
export const getNameInitials = ({ name }) => {
  if (name) {
    const [ firstName, lastName = "" ] = name.split(" ");
    return (firstName[0].toUpperCase()+(lastName ? lastName[0].toUpperCase() : ""));
  }
  return "";
};
