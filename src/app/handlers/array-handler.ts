
export const ifArray = data => {
  return (data
    && Array.isArray(data)
    && data instanceof Array
  );
};

export const ifArrayEmpty = data => {
  if (ifArray(data)) {
    return !(!!data.length)
  }

  return false;
};

export const findIndexAndItem = (
  data: Array<any> = [],
  key: string = "",
  valueToMatch: any = ""
) => {
  let obj: any = {};
  let matchedIndex;
  if(ifArray(data) && key && valueToMatch) {
    let dateLength = data.length;
    for(let i = 0; i < dateLength; i++) {
      if (data[i][key] === valueToMatch) {
        obj = data[i];
        matchedIndex = i;
        break;
      }
    }
  }
  return {
    obj,
    index: matchedIndex,
  };
}

export const handleSelectedValues = (val, selectedItems) => {
  let items = [...selectedItems];
  let index = items.findIndex(item => item === val);
  if (index === -1) {
    selectedItems = [...items, val];
  } else {
    selectedItems.splice(index, 1);
  }
  return selectedItems;
};
