
export const ifString = data => {
  return (
    data === "" 
    || typeof data === "string"
  );
};

export const ifStringEmpty = data => {
  return data.length === 0
};