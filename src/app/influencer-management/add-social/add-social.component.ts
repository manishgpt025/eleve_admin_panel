import { Component, OnInit, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { addSocialFormErrors } from '../mock-data/error-msgs';
import { AdminService, AuthService } from 'src/app/services';

@Component({
  selector: 'app-add-social',
  templateUrl: './add-social.component.html',
  styleUrls: ['./add-social.component.css']
})

export class AddSocialComponent implements OnInit {
  @Input() values;
  @Input() defaultGenres;
  @Input() social: string;
  @Input() detail;
  @Output() closed = new EventEmitter();
  @Output() saved = new EventEmitter();
  $isdCodes: Observable<any>;
  $genreOptions: Observable<any>;
  genreOptions: Array<any> = [];
  filteredGenres: Array<any> = [];
  genres: Array<any> = [];
  selectedSocial = '';
  errorMessages = {};
  formSubmitted: boolean;
  urlRegex = '';
  checkSocial = {
    valid: false,
    errorMsg: null
  };
  addSocialForm: FormGroup;
  values_valid = false;
  socialLabels = socialLabels;
  userId: string = "";
  genreInput: string = "";


  constructor(
    private adminService: AdminService,
    private formBuilder: FormBuilder,
    private elem: ElementRef,
    private auth: AuthService
  ) { }

  ngOnInit() {
    this.values_valid = this.values && true;
    // set default genres if they are passed to the component
    this.genres = this.defaultGenres || [];
    // get genres list
    this.$genreOptions = this.adminService.postApiCommon('/influencer/genreList',
      { user_id: this.getUserId() }
    ).pipe(map((results) => {
      this.genreOptions = results.body.result.genre_list;
      return results.body.result.genre_list.map(genre => genre.subgenre);
    }));

    // get isdCodes
    // this.$isdCodes = this.adminService.getApi('/country/isd').pipe(map((results) => results.body.result));

    // currency/list
    this.$isdCodes = this.adminService.getApi('/currency/list').pipe(map((results) => results.body.result));

    // add custom errors
    this.errorMessages = {
      ...addSocialFormErrors,
      link: { required: 'URL is mandatory', pattern: `Enter a valid http ${this.social} URL` },
      name: {
        required: `${this.getName()} is mandatory`,
        minlength: `${this.getName()} should be 2 characters long`,
        pattern: 'Spaces not allowed'
      }
    };

    this.urlRegex = `^((http|https):\/\/)(www\.)?(${this.social.toLocaleLowerCase() === 'blog' ? 'blogger'
    : this.social.toLowerCase()}.com)\/.+`;

    // add social form, addidg default values if they are passed to component
    const { values } = this;
    this.addSocialForm = this.formBuilder.group({
      name: this.social === 'Instagram'
        || this.social === 'Twitter'
        || this.social === 'SnapChat'
        || this.social === 'TikTok'
        ? [
            {value: values && values.name || '' , disabled: this.values && this.detail && true},
            [ Validators.required, Validators.minLength(2), Validators.pattern(/^\S*$/)]
          ]
        : [
            {value: values && values.name || '' , disabled: this.values && this.detail && true},
            [Validators.required, Validators.minLength(2)]
          ],
      profile_type: [values ? this.detail ? values.type : values.profile_type
        : '', this.social === 'Facebook' && Validators.required],
      link: (this.social === 'SnapChat' || this.social === 'TikTok') ? [
        {value: values && values.link || '', disabled: this.values && this.detail && true},
        []
      ]: [
          {value: values && values.link || '', disabled: this.values && this.detail && true},
          [ Validators.required,
            Validators.pattern(this.social !== 'Blog'
              ? this.urlRegex
              : '^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$'
              )
          ]
        ],
      followers_count: [values && values.followers_count || '', [Validators.required,
      Validators.pattern('^[0-9]*$')]],
      currency: [values && values.currency || ''],
      cost: [values && values.cost || '', [
        Validators.pattern('^[0-9]*$')]],
      category: [values && values.category || '', Validators.required],
      genre: [values && values.genre || ''],
      bio: [values && values.bio || ''],
    });

    // genre field autocomplete logic
    this.addSocialForm.get('genre').valueChanges.subscribe(
      (val) => this.filteredGenres = this.genreOptions.filter(option => option.subgenre.toLowerCase().startsWith(val.toLowerCase())
      ));
  }

  // close add social modal
  close() {
    this.closed.emit();
  }

  // submitting add/edit social form
  save() {
    this.formSubmitted = true;
    const followers_type = this.getFollowers();
    const { name, profile_type, link, followers_count, currency, cost, category, bio } = this.addSocialForm.value;
    if (this.addSocialForm.invalid || !this.values_valid || !this.genres) {
      if (!this.checkSocial.valid) {
        if(this.checkSocial.errorMsg)
        this.adminService.error(this.checkSocial.errorMsg);
      }
      const FirstInvalid = this.elem.nativeElement.querySelectorAll('.ng-invalid')[1];
      FirstInvalid.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'start' });
      return;

    } else if (!this.addSocialForm.invalid && this.genres.length > 0) {
      this.saved.emit({
        profile_type,
        name: !this.values ? name.split('').filter(char => char !== '@').join('') : this.values.name,
        link, followers_count, currency, cost, category, bio, followers_type, social: this.social,
        genres: this.genres,
        connection_type: 'manual'
      });
    }
  }

  setUserId = () => {
    this.userId = this.auth.getData()._id;
  };

  getUserId = () => {
    if (!this.userId) {
      this.setUserId();
    }
   return this.userId;
  };

  // get name field label
  getName() {
    if (this.social === 'Instagram'
      || this.social === 'Twitter'
      || this.social === 'SnapChat'
      || this.social === 'TikTok'
    ) {
      return 'Handle';
    } else { return 'Name'; }
  }

  // get reach field label
  getFollowers() {
    if (this.social === 'YouTube') {
      return 'Subscribers';
    } else if (this.social === 'Blogger') {
      return 'Traffic';

    } else if (this.social === 'LinkedIn') {
      return 'Connections';
    } else { return 'Reach'; }
  }

  // on add genre (add genre or throw error)
  onGenreSelect(e) {
    if (this.genres.filter(val => val === e.option.value).length === 1) {
      this.adminService.error('Genre is selected');
      this.addSocialForm.controls.genre.setValue('');
    } else if (this.genres.length === 5) {
      this.adminService.error('You can add 5 genres max');
    } else {
      this.genres.push(e.option.value);
      this.addSocialForm.controls.genre.setValue('');
    }
    this.genreInput = "";
  }
  // delete genre from the list
  deleteGenre(index) {
    this.genres.splice(index, 1);
  }

  // check name and url values via API
  checkValues(field) {
    const link = this.addSocialForm.get('link').value;
    let name = this.addSocialForm.get('name').value;
    const body = {
      user_id: this.getUserId(),
      platform: this.social.toLowerCase(),
      name: null,
      link: null
    };
    // delete '@' sign before send values to API
    if((this.social !== 'SnapChat' && this.social !== 'TikTok') && (field === "link" || field === "name")){
      if (name) {
        body.name = name.indexOf('@') === 0 ?
        name.substr(1) : name;
      } else if (link) {
        body.link = link;
      }
      if (this.addSocialForm.get(field).valid) {
        this.adminService.postApiCommon('/influencer/checkHandleUrl', body).
          subscribe(data => {
            switch (data.body.responseCode) {
              case 200:
                this.values_valid = true;
                this.checkSocial.valid = false;
                break;
              case 409:
                this.adminService.error(data.body.responseMessage);
                this.checkSocial.errorMsg = data.body.responseMessage;
                this.checkSocial.valid = false;
                this.values_valid = false;
                break;
              case 500:
                this.adminService.error(data.body.responseMessage);
                this.checkSocial.errorMsg = data.body.responseMessage;
                this.checkSocial.valid = false;
                this.values_valid = false;
                break;
              case 404:
                this.adminService.error(data.body.responseMessage);
                this.checkSocial.errorMsg = data.body.responseMessage;
                this.checkSocial.valid = false;
                this.values_valid = false;
                break;
              case 500:
                this.values_valid = true;
                this.checkSocial.valid = false;
                break;
              default:
                break;
            }
          });
      }
    }
  }

  get f() { return this.addSocialForm.controls; }

}

const socialLabels = {
  facebook: {
    name: {
      label: 'Name',
    },
    type: {
      label: 'Page Type',
    },
    link: {
      label: 'Facebook Link/Url',
    },
    followers: {
      label: 'Friends/Followers',
    },
    cost: {
      label: 'Cost per post',
    },
    category: {
      label: 'Select Category'
    },
    genre: {
      label: 'Select Genre (Max 5)'
    },
    bio: {
      label: 'Bio/About',
    }
  },
  twitter: {
    name: {
      label: 'Twitter Handle',
      placeholder: 'Twitter Handle',
    },
    link: {
      label: 'Twitter Link/Url',
      placeholder: 'Twitter Link/Url',
    },
    followers: {
      label: 'Followers',
    },
    cost: {
      label: 'Cost per twitter post',
    },
    category: {
      label: 'Select Category'
    },
    genre: {
      label: 'Select Genre (Max 5)'
    },
    bio: {
      label: 'Bio/About',
    }
  },
  instagram: {
    name: {
      label: 'Instagram Handle',
      placeholder: 'Instagram Handle',
    },
    link: {
      label: 'Instagram Link/Url',
      placeholder: 'Instagram Link/Url',
    },
    followers: {
      label: 'Followers',
    },
    cost: {
      label: 'Cost per instagram post',
    },
    category: {
      label: 'Select Category'
    },
    genre: {
      label: 'Select Genre (Max 5)'
    },
    bio: {
      label: 'Bio/About',
    }
  },
  linkedin: {
    name: {
      label: 'LinkedIn Name',
      placeholder: 'LinkedIn Name',
    },
    link: {
      label: 'LinkedIn Link/Url',
      placeholder: 'LinkedIn Link/Url',
    },
    followers: {
      label: 'Connections',
    },
    cost: {
      label: 'Cost per LinkedIn post',
    },
    category: {
      label: 'Select Category'
    },
    genre: {
      label: 'Select Genre (Max 5)'
    },
    bio: {
      label: 'Bio/About',
    }
  },
  youtube: {
    name: {
      label: 'Channel Name',
      placeholder: 'Channel Name',
    },
    link: {
      label: 'Youtube Link/Url',
      placeholder: 'Youtube Link/Url',
    },
    followers: {
      label: 'Subscribers',
    },
    cost: {
      label: 'Cost per youtube post',
    },
    category: {
      label: 'Select Category'
    },
    genre: {
      label: 'Select Genre (Max 5)'
    },
    bio: {
      label: 'Bio/About',
    }
  },
  blog: {
    name: {
      label: 'Blog Name',
      placeholder: 'Blog Name',
    },
    link: {
      label: 'Blog Link/Url',
      placeholder: 'Blog Link/Url',
    },
    followers: {
      label: 'Traffic',
    },
    cost: {
      label: 'Cost per blog post',
    },
    category: {
      label: 'Select Category'
    },
    genre: {
      label: 'Select Genre (Max 5)'
    },
    bio: {
      label: 'Bio/About',
    }
  },
  tiktok: {
    name: {
      label: 'TikTok Handle',
      placeholder: 'TikTok Handle',
    },
    link: {
      label: 'TikTok Link/Url',
      placeholder: 'TikTok Link/Url',
    },
    followers: {
      label: 'Followers',
    },
    cost: {
      label: 'Cost per TikTok post',
    },
    category: {
      label: 'Select Category'
    },
    genre: {
      label: 'Select Genre (Max 5)'
    },
    bio: {
      label: 'Bio/About',
    }
  },
  snapchat: {
    name: {
      label: 'Snapchat Handle',
      placeholder: 'Snapchat Handle',
    },
    link: {
      label: 'Snapchat Link/Url',
      placeholder: 'Snapchat Link/Url',
    },
    followers: {
      label: 'Followers',
    },
    cost: {
      label: 'Cost per Snapchat post',
    },
    category: {
      label: 'Select Category'
    },
    genre: {
      label: 'Select Genre (Max 5)'
    },
    bio: {
      label: 'Bio/About',
    }
  },
};
