import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Ng2ImgMaxModule } from "ng2-img-max";
import {
  MatButtonModule, 
  MatCheckboxModule, 
  MatInputModule, 
  MatMenuModule, 
  MatIconModule, 
  MatSelectModule, 
  MatRadioModule, 
  MatExpansionModule, 
  MatTabsModule, 
  MatNativeDateModule, 
  MatDatepicker, 
  MatDatepickerModule, 
  MatAutocompleteModule,
  MatAutocomplete
} from "@angular/material";
import {MatTooltipModule} from "@angular/material/tooltip";
import { Ng5SliderModule } from "ng5-slider";
import { NgSelectModule } from "@ng-select/ng-select";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

// In app imports 
import { CreateInfluencerComponent } from "./create-influencer/create-influencer.component";
import { InfluencerDetailComponent } from "./influencer-detail/influencer-detail.component";
import { AddSocialComponent } from "./add-social/add-social.component";
import { CheckEmailComponent } from "./check-email/check-email.component";
import { ErrorMsgComponent } from "./error-msg/error-msg.component";
import { EditInfluencerModalComponent } from "./influencer-detail/edit-influencer-modal/edit-influencer-modal.component";
import { InfluencerListComponent } from "./list/list.component";
import { InfluencerListItemComponent } from "./list/list-item/list-item.component";
import { CommonFileModule } from "../common-file/common-file.module";
import { InfluencerManagementRoutingModule } from "./influencer-management-routing.module";
import { InfluencerLayoutComponent } from "./influencer-layout/influencer-layout.component";
import { ListFiltersComponent } from './list/list-filters/list-filters.component';
import { CapitalizeFirstLetter } from "../pipes/capitalize-first-letter";



@NgModule({
  declarations: [
    InfluencerLayoutComponent, 
    CreateInfluencerComponent, 
    ErrorMsgComponent, 
    AddSocialComponent,
    InfluencerDetailComponent, 
    CheckEmailComponent, 
    EditInfluencerModalComponent,
    InfluencerListComponent, 
    InfluencerListItemComponent, 
    ListFiltersComponent,
    CapitalizeFirstLetter,
  ],
  imports: [
    CommonModule,
    CommonFileModule,
    InfluencerManagementRoutingModule,
    Ng2ImgMaxModule,
    MatNativeDateModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatSelectModule,
    MatRadioModule,
    Ng5SliderModule,
    MatExpansionModule,
    MatTabsModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatTooltipModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class InfluencerManagementModule { }
