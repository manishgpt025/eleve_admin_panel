import { Component, OnInit } from "@angular/core";

import { AdminService, AuthService } from "src/app/services";

@Component({
  selector: 'app-influencer-layout',
  templateUrl: './influencer-layout.component.html',
})
export class InfluencerLayoutComponent implements OnInit {

  constructor(
    private apiService: AdminService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    // this.getGenres();
  }

  // getGenres = (): void => {
  //   const params = {
  //     user_id: "",
  //   };
  //   this.apiService.postApiCommon("/influencer/genreList", params)
  //     .subscribe(response => {
  //         console.log(response);
  //     }, error => this.apiService.error(error)
  //   );
  // };

}
