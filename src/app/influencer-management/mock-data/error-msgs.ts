export const influencerFormErrors = {
  first_name: {
    required: 'First name is mandatory',
    minlength: 'First name should be 2 to 20 characters long',
    maxlength: 'First name should be 2 to 20 characters long',
    pattern: 'Please avoid special characters',
    spaces: 'First name should be 2 to 20 characters long'
  },
  last_name: {
    required: 'Last name is mandatory',
    minlength: 'Last name should be 2 to 20 characters long',
    maxlength: 'Last name should be 2 to 20 characters long',
    pattern: 'Please avoid special characters & spaces'
  },
  gender: {
    required: 'Gender is mandatory'
  },
  country: {
    required: 'Country is mandatory'
  },
  state: {
    required: 'State is mandatory',
    previousNotSelected : 'Select country first'
  },
  city: {
    required: 'City is mandatory',
    previousNotSelected : 'Select country & state first'
  },
  email_id: {
    required: 'Primary email is mandatory',
    email: 'Please enter a valid email',
    pattern: 'Please enter a valid email',
    registered: 'This email ID is already registered with another influencer. Provide a different email ID'
  },
  alt_email_id: {
    email: 'Please enter a valid email',
    pattern: 'Please enter a valid email',
    mustMatch: 'Alternate email must be different from primary email'
  },
  mobile: {
    minlength: 'Mobile number should be 4 to 14 characters long',
    maxlength: 'Mobile number should be 4 to 14 characters long',
    pattern: 'Please enter a valid mobile number'
  },
  alt_mobile: {
    minlength: 'Mobile number should be 4 to 14 characters long',
    maxlength: 'Mobile number should be 4 to 14 characters long',
    pattern: 'Please enter a valid mobile number',
    mustMatch: 'Alternate mobile must be different from primary mobile'
  },
  dob: {
    age: 'User must be between 15 to 100 years of age'
  }
};

export const addSocialFormErrors = {
  profile_type: {
    required: 'Profile type is mandatory',
  },
  link: {
    required: 'URL is mandatory',
    pattern: 'Please enter a valid URL'
  },
  followers_count: {
    required: 'Reach is mandatory',
    pattern: 'Enter only numbers',
  },
  cost: {
    required: 'Cost is mandatory',
    pattern: 'Enter only numbers',
  },
  category: {
    required: 'Category is mandatory'
  },
  genre: {
    required: 'Please select a genre'
  },
}