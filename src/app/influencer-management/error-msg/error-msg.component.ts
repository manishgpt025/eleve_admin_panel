import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'app-error-msg',
  templateUrl: './error-msg.component.html',
  styleUrls: ['./error-msg.component.css']
})
export class ErrorMsgComponent implements OnChanges {

  @Input() fieldName: string;
  @Input() error;
  @Input() errorMessages;
  message: string;
  constructor() { }


  ngOnChanges(changes: SimpleChanges): void {
    this.message = this.errorMessages[`${Object.keys(this.error)[0]}`];
  }

}
