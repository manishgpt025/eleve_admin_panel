import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-check-email',
  templateUrl: './check-email.component.html',
})
export class CheckEmailComponent implements OnInit {
  @Input() showModal: boolean;
  @Input() email: string;
  @Output() closed = new EventEmitter<boolean>();
  @Output() cancelled = new EventEmitter<boolean>();
  @Output() delinked = new EventEmitter<boolean>();
  constructor() { }

  close() {
    this.closed.emit();
  }
  cancel() {
    this.cancelled.emit();
  }
  delink() {
    this.delinked.emit();
  }

  ngOnInit() {
  }

}
