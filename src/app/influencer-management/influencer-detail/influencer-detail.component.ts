import { Component, OnInit } from "@angular/core";
import { RouterStateSnapshot, Router, ActivatedRoute } from "@angular/router";
import { FormControl } from "@angular/forms";
import { Observable, Subscription } from "rxjs";
import { map } from "rxjs/operators";
import numeral from "numeral";

import { AdminService, ToastsService } from "src/app/services";

@Component({
  selector: "app-influencer-detail",
  templateUrl: "./influencer-detail.component.html",
})
export class InfluencerDetailComponent implements OnInit {
  editActive = false;
  $userDetails: Observable<any>;
  $userDetailsSubscription: Subscription;
  socials = {
    facebook: [],
    twitter: [],
    instagram: [],
    linkedin: [],
    snapchat: [],
    tiktok: [],
    youtube: [],
    blog: []
  };
  userDetails;
  socialsCount: number;
  showAddSocialModal = false;
  addSocialValues: any;
  addSocialGenres: any;
  selectedSocial: string;
  selectedIndex = null;
  subscriptions = new Subscription();
  showStatusModal = false;
  statusSelect = new FormControl(this.userDetails && this.userDetails.status);
  statusToChange = '';
  influencerId: string;
  modalOpen: boolean = false;
  platformToDelete: string = "";
  platformIndexToDelete: number;
  constructor(
    private apiService: AdminService,
    private route: ActivatedRoute,
    private toastsService: ToastsService
  ) {
    this.influencerId = this.route.snapshot.params.id;
  }

  getSolialsCount = (): number => {
    let socialCount = 0;
    for(let key in this.socials) {
      socialCount += this.socials[key].length;
    }
    return socialCount;
  };

  openAddSocial = (): void => {
    this.showAddSocialModal = true;
  };

  onAddSocialClose = (): void => {
    this.showAddSocialModal = false;
    this.selectedIndex = null;
    this.selectedSocial = '';
    this.addSocialValues = undefined;
  };

  onAddSocialSave = (data): void => {
    const { social, profile_type, name, link, category, genres, followers_count, cost, currency, bio } = data;
    const newSocial = {
      type: profile_type, name, link, category, genres, followers_count, cost, currency, bio,Elv_social:name,
      connection_type: 'manual'
    };
    const nameToReturn = this.socials[social.toLowerCase()].filter(soc => soc.name === name).length > 0 ? name : '';
    if (this.selectedIndex !== null) {
      this.socials[social.toLowerCase()][this.selectedIndex] = newSocial;
    } else {
      this.socials[social.toLowerCase()].push(newSocial);
    }
    this.subscriptions.add(this.apiService
      .postApiCommon('/influencer/edit_social_detail',
        {
          influencer_id: this.influencerId,
          platform: social.toLowerCase(),
          name: nameToReturn,
          [social.toLowerCase()]: [{ ...newSocial }]
        }).subscribe(res => {
          if (res.body.responseCode === 200) {
            this.toastsService.success(res.body.responseMessage);
            this.onAddSocialClose();
          } else {
            this.toastsService.error(res.body.responseMessage);
            this.onAddSocialClose();
          }
        }));
  };

  deleteSocial = (): void => {
    this.modalOpen = false;
    const social = this.platformToDelete;
    const index = this.platformIndexToDelete;
    const params = {
      influencer_id: this.influencerId,
      platform: social.toLowerCase(),
      name: this.socials[social][index].name
    };
    this.subscriptions.add(
    this.apiService.postApiCommon('/influencer/delete_social_card', params).subscribe());
    this.platformToDelete = "";
    this.platformIndexToDelete = undefined;
    this.socials[social.toLowerCase()].splice(index, 1);
  };

  onClickDontDelete = (): void => {
    this.modalOpen = false;
    this.platformToDelete = "";
    this.platformIndexToDelete = undefined;
  };

  onClickdeleteSocial = (social, index): void => {
    this.modalOpen = true;
    this.platformToDelete = social;
    this.platformIndexToDelete = index;
  };

  addSocial = (social): void => {
    this.selectedSocial = social;
    this.addSocialGenres = [];
    // this.getDefaultGenres();
    this.openAddSocial();
  };

  editSocial = (social, index): void => {
    this.selectedSocial = social.charAt(0).toUpperCase() + social.slice(1);
    this.selectedIndex = index;
    this.addSocialValues = this.socials[social][index];
    this.getDefaultGenres(social, index);
    this.openAddSocial();
  };

  getDefaultGenres = (social?, index?): void => {
    if (social) {
      this.addSocialGenres = this.socials[social][index].genres ? [...this.socials[social][index].genres] : [];
    } else {
      this.addSocialGenres = [];
    }
  };l

  formatSubscribers = (n): any => {
    return numeral(n).format('0a');
  };

  changeStatus = (): void =>  {
    this.apiService
      .postApiCommon('/influencer/status/update',
        {
          influencer_id: this.influencerId,
          status: this.statusToChange,
        }).subscribe(res => {
          this.$userDetails = this.apiService
            .postApiCommon('/influencer/get_influencer', { influencer_id: this.influencerId }).pipe(map(resp => resp.body.result));
          this.subscriptions.add(this.$userDetailsSubscription = this.$userDetails.subscribe(resp => {
            this.userDetails = {
              ...resp, payment_details:
                [
                  {
                    ...resp.payment_details, ektp: resp.payment_details.ektp ? resp.payment_details.ektp : "",
                    npwp: resp.payment_details.npwp ? resp.payment_details.npwp : "",
                    pan_number: resp.payment_details.pan_number ? resp.payment_details.pan_number : "",
                    gst_number: resp.payment_details.gst_number ? resp.payment_details.gst_number : "",
                    tax_id: resp.payment_details.tax_id ? resp.payment_details.tax_id : "",
                    business_relation: resp.payment_details.business_relation ? resp.payment_details.business_relation : "",
                  }
                ]
            };
            this.toastsService.success(`Influencer has been moved to ${this.statusToChange === '1' ? 'Active' :
              this.statusToChange === '0' ? 'Inactive' : 'Barred'} tab`);
            this.closeStatusModal(this.statusToChange);
          }));
        }
        );
  }

  openStatusModal = (status): void => {
    this.showStatusModal = true;
    this.statusToChange = `${status}`;
  }
  closeStatusModal = (status?): void => {
    this.showStatusModal = false;
    this.statusSelect.patchValue(status ? status : this.userDetails.status);
    this.statusToChange = '';
  }

  ngOnInit() {
    this.$userDetails = this.apiService
      .postApiCommon('/influencer/get_influencer', { influencer_id: this.influencerId }).pipe(map(res => res.body.result));

    this.subscriptions.add(this.$userDetailsSubscription = this.$userDetails.subscribe(res => {
      const { facebook, twitter, instagram, linkedin, snapchat, tiktok, youtube, blog } = res;
      this.socials = {
        facebook, twitter, instagram, linkedin, snapchat, tiktok, youtube, blog
      };
      let { payment_details } = res;
      if (payment_details.length) {
        const {
          ektp = "",
          npwp = "",
          pan_number = "",
          gst_number = "",
          tax_id = "",
          business_relation = "",
          bank_detail = "",
          tax_image = ""
        } = payment_details[0];
        payment_details = [{
          ektp,
          npwp,
          pan_number,
          gst_number,
          tax_id,
          business_relation,
          bank_detail,
          tax_image
        }];
      } else {
        payment_details = [{
          ektp: "",
          npwp: "",
          pan_number: "",
          gst_number: "",
          tax_id: "",
          business_relation: "",
          bank_detail: "",
          tax_image: ""
        }];
      }
      this.userDetails = {
        ...res,
        payment_details
      };
    }));
  }

  toggleModal = (changed): void => {
    this.editActive = !this.editActive;
    if (changed) {
      this.$userDetails = this.apiService
        .postApiCommon('/influencer/get_influencer', { influencer_id: this.influencerId }).pipe(map(res => res.body.result));
      this.subscriptions.add(this.$userDetailsSubscription = this.$userDetails.subscribe(res => {
        const { facebook, twitter, instagram, linkedin, snapchat, tiktok, youtube, blog } = res;
        this.socials = {
          facebook, twitter, instagram, linkedin, snapchat, tiktok, youtube, blog
        };
        this.userDetails = {
          ...res, payment_details:
            [
              {
                ...res.payment_details, ektp: res.payment_details.ektp ? res.payment_details.ektp : "",
                npwp: res.payment_details.npwp ? res.payment_details.npwp : "",
                pan_number: res.payment_details.pan_number ? res.payment_details.pan_number : "",
                gst_number: res.payment_details.gst_number ? res.payment_details.gst_number : "",
                tax_id: res.payment_details.tax_id ? res.payment_details.tax_id : "",
                business_relation: res.payment_details.business_relation ? res.payment_details.business_relation : "",
                bank_detail: res.payment_details.bank_detail ? res.payment_details.bank_detail : "",
              }
            ]
        };
      }));
    }
  }

  getUserImage = (): string => {
    const { profile_pic: userImage } = this.userDetails || { profile_pic: undefined };
    if (!userImage) {
      return "assets/images/user.jpg";
    }
    return `${userImage}?version=${userImage.substr(userImage.length -4)}`;
  };

  getDefault = (attributeKey, verificationKey): any => {
    if (this.userDetails) {
      if (this.userDetails[attributeKey] && this.userDetails[verificationKey]) {
        let ifTrue = this.userDetails[attributeKey] && this.userDetails[verificationKey];
        return {
          icon: ifTrue != '0' ? "check_circle" : "error",
          class: ifTrue != '0' ? "color_green" : "color_red",
        }
      }else{
        return {
          icon: "error",
          class:"color_red",
        }
      }
    }
    return {
      icon: "",
      class: ""
    }
  }

  onClickSocialLink = (url): any => window.open(url, '_blank');

  ifPaymentDetailsProvided = (key): boolean => {
    const { payment_details: paymentDetails = [] } = this.userDetails;
    const [{ [key]: value = "" }] = paymentDetails;
    return value !== "";
  };

  formatCost = (cost = ""): string => {
    if (!cost) {
      return "";
    }
    const formattedAmount = parseInt(cost).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if (formattedAmount === "NaN") {
      return "0";
    }
    return formattedAmount
  };

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}
