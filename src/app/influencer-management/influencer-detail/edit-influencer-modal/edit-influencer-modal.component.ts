import { Component, OnInit, Input, ElementRef, EventEmitter, Output } from '@angular/core';
import { RouterState, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Ng2ImgMaxService } from 'ng2-img-max';

import { AdminService } from 'src/app/services/admin.service';
import { influencerFormErrors } from '../../mock-data/error-msgs';

const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const AgeValidator = (control: AbstractControl): { [key: string]: boolean } | null => {
  const today = new Date();
  const birthDate = new Date(control.value);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age = age - 1;
  }
  if (age < 16 || age > 100) {
    return { age: true };
  }
  return null;
};

const MustMatch = (controlName: string, matchingControlName: string) => {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if ((matchingControl.errors && !matchingControl.errors.mustMatch) || !matchingControl.value) {
      return;
    }

    if (control.value === matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
};

const SpacesValidator = (controlName: string) => {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const value = control.value.replace(/ /g, '');
    if ((control.errors && !control.errors.mustMatch) || !control.value) {
      return;
    }

    if (!value.length) {
      control.setErrors({ spaces: true });
    } else {
      control.setErrors(null);
    }
  };
};

const LocationValidator = (controlName: string, matchingControlName: string) => {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if ((matchingControl.errors && !matchingControl.errors.mustMatch)) {
      return;
    }

    if (!control.value) {
      matchingControl.setErrors({ previousNotSelected: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
};


@Component({
  selector: 'app-edit-influencer-modal',
  templateUrl: './edit-influencer-modal.component.html',
  styleUrls: ['./edit-influencer-modal.component.css']
})
export class EditInfluencerModalComponent implements OnInit {
  @Input() influencer: any;
  @Output() closeIt: EventEmitter<any> = new EventEmitter();
  influencerForm: FormGroup;
  $isdCodes: Observable<Array<any>>;
  $stateList: Observable<Array<any>>;
  subscriptions = new Subscription();
  $citiesList: Observable<Array<any>>;
  errorMessages = influencerFormErrors;
  imagePath: string;
  base64textString: any;
  imgURL: any;
  image: HTMLImageElement;
  imageUpload: string;
  minDate: Date = null;
  maxDate: Date = null;

  constructor(
    private adminService: AdminService,
    private formBuilder: FormBuilder,
    private elem: ElementRef,
    private ng2ImgMaxService: Ng2ImgMaxService
  ) { }

  closeModal(changed?: boolean) {
    this.closeIt.emit(changed);
  }

  preview(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files.length === 0) {
      return;
    }
    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(file);

        //Convert base64
        this.ng2ImgMaxService.resizeImage(file, 400, 300).subscribe(result => {
          let reader = new FileReader();
          reader.readAsDataURL(result); // read file as data url
          reader.onload = (event: any) => { // called once readAsDataURL is completed
            this.image = new Image();
            this.image.src = event.target.result;
            this.imageUpload = this.image.src;
          }
        }, error => {
          this.adminService.error(error.reason)
        })

    reader.onload = this._handleReaderLoaded.bind(this);

  }

  // converting image into base64
  _handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.imgURL = binaryString;
    this.base64textString = binaryString;
  }

  ngOnInit() {
    this.$stateList = this.adminService
    .postApi('/statelist', { country: this.influencer.country }).pipe(
      map((data => data.body.result))
    );
    this.$citiesList = this.adminService
      .postApi('/cities', { state: this.influencer.state }).pipe(
        map((data => data.body.result))
      );
    this.imgURL = this.influencer.profile_pic;
    this.$isdCodes = this.adminService.getApi('/country/isd').pipe(map((results) => results.body.result));
    this.influencerForm = this.formBuilder.group({
      first_name: [this.influencer.first_name, [Validators.required, Validators.maxLength(20),
      Validators.minLength(2), Validators.pattern('^[a-zA-Z0-9^\\s]+$')]],
      last_name: [this.influencer.last_name, [Validators.maxLength(20), Validators.minLength(2),
      Validators.pattern('^[a-zA-Z0-9]+$')]],
      gender: [this.influencer.gender ? this.influencer.gender.toLowerCase():'', Validators.required],
      dob: [this.influencer.dob, AgeValidator],
      country: [this.influencer.country, Validators.required],
      state: [this.influencer.state, Validators.required],
      city: [this.influencer.city, Validators.required],
      address: [this.influencer.address],
      email_id: [{value: this.influencer.email, disabled: true}, [Validators.required,
      Validators.pattern(emailRegex)]],
      alt_email: [this.influencer.alt_email, Validators.pattern(emailRegex)],
      isd_code: [this.influencer.isd_code],
      mobile: [this.influencer.mobile, [Validators.maxLength(14), Validators.minLength(4),
      Validators.pattern('^[0-9]+$')]],
      alt_isd_code: [this.influencer.alt_isd_code],
      alt_mobile: [this.influencer.alt_mobile, [Validators.maxLength(14), Validators.minLength(4),
      Validators.pattern('^[0-9]+$')]],
      note: [this.influencer.note]
    }, {
        validator: [MustMatch('email_id', 'alt_email'), MustMatch('mobile', 'alt_mobile'),
        SpacesValidator('first_name'), LocationValidator('country', 'state'), LocationValidator('state', 'city')]
      });
    this.influencerForm.patchValue({country: this.influencer.country, state: this.influencer.state});
    // loading states according to the country selected
    this.subscriptions.add(this.f.country.valueChanges.subscribe(value => {
      this.influencerForm.patchValue({state: null, city: null});
      this.$stateList = this.adminService
      .postApi('/statelist', { country: `${value}` }).pipe(
        map((data => data.body.result))
      )}));
    // loading cities according to the state selected
    this.subscriptions.add(this.f.state.valueChanges.subscribe(value => {
      this.influencerForm.patchValue({city: null});
      this.$citiesList = this.adminService
      .postApi('/cities', { state: `${value}` }).pipe(
        map((data => data.body.result))
      )}));

    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 100, 0, 1);
    this.maxDate = new Date(currentYear - 18, 0, 1);

  }
  get f() { return this.influencerForm.controls; }

  onSubmit() {
    this.influencerForm.markAllAsTouched();
    if (this.influencerForm.invalid) {
    const FirstInvalid = this.elem.nativeElement.querySelectorAll('.ng-invalid')[1];
    FirstInvalid.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'start' });
    return;
  } else {
    const user = {
      ...this.influencerForm.value,
      email_id: this.influencer.email,
    //  profilePic: this.base64textString,
      profilePic: this.imageUpload,
      influencer_id: this.influencer._id
      };
    this.subscriptions.add(this.adminService.postApiCommon('/influencer/edit_detail', user).subscribe(res => {
      if (res.body.responseCode === 200) {
        this.closeModal(true);
        this.adminService.success('Basic details updated successfully.');
      } else {
        this.adminService.error(res.message);
      }
    }));
  }
}

ngOnDestroy(): void {
  this.subscriptions.unsubscribe();
}

}
