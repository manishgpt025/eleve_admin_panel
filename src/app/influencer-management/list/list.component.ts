import { Component, OnInit, HostListener, ElementRef, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { isEqual, debounce } from "lodash";
import { Options } from "ng5-slider";

import { AdminService, AuthService, ToastsService } from "src/app/services";
import { getErrorMessage, ifStatusOkay, getResult } from "../../handlers";
import { ListFiltersComponent } from "./list-filters/list-filters.component";

import Utils from "./utils";
const utils = new Utils();

@Component({
  selector: "app-influencer-list",
  templateUrl: "./list.component.html",
})
export class InfluencerListComponent implements OnInit {
  loading: boolean = true;
  userId: string = "";
  data: any = utils.getDefaultData();
  filters: any = utils.getDefaultFilters();
  selectedInfluencer: any = utils.getDefaultSelectedInfluencer();
  followersReach: any = utils.getDefaultReach();
  reachOptions: Options = utils.getDefaultReachOptions();
  generes: Array<object> = [];
  currentStatustab = 0;
  searchQuery: string = "";
  confirmationModal: any = {
    show: false,
    message: ""
  };
  currentKeyword: string = "";
  @ViewChild(ListFiltersComponent, { static: true }) listFilters: ListFiltersComponent;

  debounceInfluencersList = debounce(() => {
    this.resetCurrentPageData();
    this.getInfluencersList();
  }, 750)

  constructor (
    private apiService: AdminService,
    private elem: ElementRef,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private toastsService: ToastsService
  ) { }

  ngOnInit() {
    this.getFollowersReach();
    this.getGenres();
  };

  handleChangedKeyword = (): boolean => {
    if (this.loading
      && (
        (this.currentKeyword !== this.filters.keyword)
      )
    ) {
      this.debounceInfluencersList();
      return;
    }
  };

  getInfluencersList = (): void => {
    const status = this.getCurrentStatus();
    const params = {
      status,
      user_id: this.getUserId(),
      pageNumber: this.getCurrentPage(status),
      ...this.filters,
    };
    this.handleChangedKeyword();
    this.currentKeyword = this.filters.keyword;
    this.loading = true;
    this.apiService.postApiCommon("/influencer/get_list", params)
      .subscribe(response => {
        if (ifStatusOkay(response)) {
          const result = getResult(response);
          this.setInfluencersData(result, status);
        }
      }, error => {
        this.toastsService.error(error);
        this.loading = false;
      }
    )
  };

  getGenres = (): void => {
    const params = {
      user_id: this.getUserId(),
    };
    this.apiService.postApiCommon("/influencer/genreList", params)
      .subscribe(response => {
        if (ifStatusOkay(response)) {
          const { genre_list: list } = getResult(response);
          this.generes = list.map(genere => ({
            name: genere.subgenre
          }));
        }
      }, error => this.toastsService.error(error)
    );
  };

  setReachOption = (key): void => {
    const { [key]: value } = this.followersReach;
    this.reachOptions = {
      floor: 0,
      ceil: value
    };
  };

  setFollowersReach = ({
    active_max_reach: active,
    inactive_max_reach: inActive,
    barred_max_reach: barred
  }) => {
    this.followersReach = {
      active,
      inActive,
      barred
    };
    this.setReachOption("active");
  };

  getFollowersReach = (): void => {
    this.apiService.getApiCommon("/influencer/get_max_reach_count")
      .subscribe(response => {
        if (ifStatusOkay(response)) {
          this.setFollowersReach(getResult(response));
          this.getInfluencersList();
        }
      }, error => this.toastsService.error(error)
    );
  };

  ifDataAlreadyExist = (currentPageKey): boolean => {
    const { [currentPageKey]: {
        filters: currentKeyFilters,
        list
      }
    } = this.data;
    return isEqual(currentKeyFilters, this.filters) && list.length;
  };

  onClickConfirmChangeStatus = (): void => {
    const {
      id: influencer_id,
      status
    } = this.selectedInfluencer
    const params = { influencer_id, status };
    this.apiService.postApiCommon("/influencer/status/update", params)
      .subscribe(response => {
        if (ifStatusOkay(response)) {
          this.toastsService.success(
            `Influencer has been moved to
             ${status == '1'
               ? 'Active'
               : status == '0'
               ? 'Inactive'
               : 'Barred'
             } tab`
          );
          this.removeSelectedInfluencer(influencer_id);
        }
      }, error => {
        this.toastsService.error(error);
        this.closeConfirmationModal();
      }
    );
  };

  resetCurrentPageData = (): void => {
    const currentPageKey = this.getCurrentPageKey();
    this.resetDataKey(currentPageKey);
  };

  getCurrentPageData = (key): any => {
    const currentPageKey = this.getCurrentPageKey();
    const {
      [currentPageKey]: {
        [key]: value
      }
    } = this.data;
    return value;
  };

  removeSelectedInfluencer = (id): void => {
    const currentPageKey = this.getCurrentPageKey();
    const currentList = this.getInfluencersData(currentPageKey, "list");
    const influencerIndex = currentList.findIndex(influencer => {
      return influencer._id === id
    });
    if (influencerIndex > -1) {
      currentList.splice(influencerIndex, 1);
      const { [currentPageKey]: currentKeyData } = this.data;
      this.data = {
        ...this.data,
        [currentPageKey]: {
          ...currentKeyData,
          list: [...currentList],
          total: --currentKeyData.total
        }
      };
      this.closeConfirmationModal();
    }
  };

  onChangeTab = (status): void => {
    this.currentStatustab = status;
    const currentPageKey = this.getCurrentPageKey();
    this.setReachOption(currentPageKey);
    if (!this.ifDataAlreadyExist(currentPageKey)) {
      this.resetDataKey(currentPageKey);
      this.loading = true;
      this.getInfluencersList();
    }
  };

  resetDataKey = (key): void => {
    this.data = {
      ...this.data,
      [key]: {
        ...utils.getDefaultSingleKeyData()
      }
    };
  }

  getCurrentStatus = (): number => {
    return this.currentStatustab === 0
      ? 1 : this.currentStatustab === 1
      ? 0 : 2
  };

  getCurrentPageKey = (currentStatus?: number): string => {
    if (!currentStatus) {
      currentStatus = this.getCurrentStatus();
    }
    return utils.getInfluencerStatus().find(influencer => (
      currentStatus === influencer.value
    )).label;
  };

  getCurrentPage = (status): number => {
    const currentInfluencerLabel = this.getCurrentPageKey();
    const {
      [currentInfluencerLabel]: { page: pageNumber }
    } = this.data;
    return pageNumber;
  };

  setInfluencersData = ({
    limit,
    page,
    pages,
    total,
    docs: newList
  }, status): void => {
    const key = this.getCurrentPageKey(status);
    const { [key]: {
        list: currentList,
      },
    } = this.data;
    this.data = {
      ...this.data,
      [key]: {
        list: [...currentList, ...newList],
        filters: {
          ...this.filters,
          gender: [...this.filters.gender],
          genres: [...this.filters.genres],
          social: [...this.filters.social],
          category: [...this.filters.category],
        },
        limit,
        page,
        pages,
        total,
      }
    };
    this.loading = false;
  };

  setUserId = (): void => {
    this.userId = this.authService.getData()._id;
  };

  getUserId = (): string => {
    if (!this.userId) {
      this.setUserId();
    }
   return this.userId;
  };

  closeConfirmationModal = (): void => {
    this.selectedInfluencer = utils.getDefaultSelectedInfluencer();
    this.confirmationModal = {
      show: false,
      message: ""
    };
  };

  updateSearchQuery = ({
    target: { value: query = "" }
  }): void => {
    if (query.length > 1) {
      this.filters.keyword = query;
      this.debounceInfluencersList();
    } else {
      if (this.filters.keyword.length && !query.length) {
        this.filters.keyword = "";
        this.debounceInfluencersList();
      }
    }
  };

  changeInfluencerStatus = ({ status, id }): void => {
    this.selectedInfluencer = { status, id };
    this.confirmationModal = {
      message: "Are you sure you want to "+ (
        status == 1 ? "Activate" : status == "0"
        ? "Deactivate" : "Barred"
      ) + " Influncer?",
      show: true,
    };
  };

  updateFilters = ({
    key, value, filterType
  }: any): void => {
    let tempObj: any = {};
    switch(filterType) {
      case "multiselect": {
        let { [key]: currentValues } = this.filters;
        if (currentValues.includes(value)) {
          currentValues.splice(currentValues.indexOf(value), 1);
        } else {
          currentValues.push(value);
        }
        tempObj = { [key]: [...currentValues] };
        break;
      }
      case "slider": {
        const {
          min_reach: minReach = 0,
          max_reach: maxReach = 0,
        } = value;
        const { outdated_profiles: outdatedProfiles } = this.filters;
        this.filters = {
          ...this.filters,
          ...value,
          outdated_profiles: (!minReach && !maxReach)
            ? outdatedProfiles
            : 0
        };
        break;
      }
      case "dropdown": {
        const { name = "" } = value;
        tempObj = { genres: name ? [name]: [] };
        break;
      }
      case "toggle": {
        if (key === "outdated_profiles") {
          tempObj = { outdated_profiles: value };
          this.filters = utils.getDefaultFilters();
        }
      }
      default: {
        tempObj = { [key] : value };
      }
    }
    const filtersToNeglect = ["toggle", "slider"];
    if (!filtersToNeglect.includes(filterType)) {
      this.filters = {
        ...this.filters,
        ...tempObj,
        outdated_profiles: 0
      }
    } else {
      this.loading = true;
      this.onClickRefreshFilters({ shouldCallListAgain: false });
      this.filters = {
        ...this.filters,
        ...tempObj,
      };
    }
    this.debounceInfluencersList();
  };

  clearSearch = ({ shouldCallListAgain = false }: any): void => {
    this.filters.keyword = "";
    this.loading = true;
    this.searchQuery = "";
    this.currentKeyword = "";
    this.resetCurrentPageData();
    if (shouldCallListAgain) {
      this.getInfluencersList();
    }
  };

  resetCheckboxesUI = (): void => {
    this.elem.nativeElement.querySelectorAll(".filter").forEach(element => {
      element.checked = false;
    });
  };

  onClickRefreshFilters = ({ shouldCallListAgain = false }: any): void => {
    this.filters = utils.getDefaultFilters();
    this.resetCheckboxesUI();
    this.listFilters.resetSliderAndGenreList();
    this.data = utils.getDefaultData();
    const currentDataKey = this.getCurrentPageKey();
    this.setReachOption(currentDataKey);
    if (shouldCallListAgain) {
      this.clearSearch({ shouldCallListAgain: true });
    }
  };

  getInfluencersData = (dataToAccess, keyInData): any => {
    const {
      [dataToAccess]: {
        [keyInData]: value
      }
    } = this.data;
    return value;
  };

  ifFiltersActive = (): boolean => {
    return !isEqual(this.filters, utils.getDefaultFilters());
  };

  @HostListener('window:scroll', ['$event'])
  onScroll = (): void => {
    const currentDataKey = this.getCurrentPageKey();
    let { [currentDataKey]: {
        page: currentPage,
        pages: totalPages,
        list: data
      }
    } = this.data;
    const shouldCallDataAgain =
      totalPages > currentPage
      && data.length
      && (
            window.innerHeight
            + window.scrollY
            + 200 >= document.body.offsetHeight
          )
      && !this.loading;
    if (shouldCallDataAgain) {
      const {
        [currentDataKey]: currentKeyValue
      } = this.data;
      this.data = {
        ...this.data,
        [currentDataKey]: {
          ...currentKeyValue,
          page: ++currentPage
        }
      };
      this.getInfluencersList();
    }
  };
}
