import { Component, OnInit, HostListener, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import * as moment from "moment";
import { map, shareReplay, take, switchMap, tap } from 'rxjs/operators';

import { AdminService } from "src/app/services";
import { numberSIUnits } from "../../../constants";
import { ifString, ifNumber } from "../../../handlers";
import Utils from "../utils";
const utils = new Utils();


@Component({
  selector: "influencers-list-item",
  templateUrl: "./list-item.component.html",
})
export class InfluencerListItemComponent implements OnInit {
  @Input() influencersList;
  @Input() loading;
  @Input() ifFiltersActive;
  @Input() currentPageKey;
  @Output() changeInfluencerStatus = new EventEmitter();
  influencerStatusArray: any = utils.getInfluencerStatus();

  ngOnInit() { }

  formatCost = (cost = ""): string => {
    if (!cost) {
      return "";
    }
    const formattedAmount = parseInt(cost).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if (formattedAmount === "NaN") {
      return "";
    }
    return formattedAmount
  };

  formatFollowersCount = (count: any = ""): any => {
    if (!count) {
      return "";
    }
    if (ifString(count)) {
      count = parseInt(count).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    const regexToCompare = /\.0+$|(\.[0-9]*[1-9])0+$/;
    let currentValueRange = 1;
    let currentSymbol = "";
    for(let i = 0; i < numberSIUnits.length; i++) {
      if (count >= numberSIUnits[i].value) {
        currentValueRange = numberSIUnits[i].value;
        currentSymbol = numberSIUnits[i].symbol;
      }
    }
    count = (count / currentValueRange).toFixed(0).replace(regexToCompare, "$1") + currentSymbol;
    if (count === "NaN") {
      return "";
    }

    return count;
  };

  onChangeInfluencerStatus = ({
    target: {
        value: status
      }
    },{
    _id: id
  }): void => {
    this.changeInfluencerStatus.emit({
      status,
      id
    })
  };

  formatER = (ER: any = 0): number | string => {
    if (!ER) {
      return "0.00"
    }
    ER = parseFloat(ER);
    if (Number.isNaN(ER)) {
      return "0.00";
    }
    ER = ER.toFixed(2);
    if (ER === "NaN") {
      return "0.00";
    }
    return ER;
  };

  hasValue = (value = null): boolean => {
    return (value !== null && value !== "" && value !== undefined);
  };

}