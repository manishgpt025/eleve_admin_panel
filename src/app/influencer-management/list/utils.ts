import { cloneDeep } from "../../handlers";

class Utils {
  getDefaultPageDetails = () => ({
    page: 1,
    pages: 1,
  });

  getDefaultSelectedInfluencer = () => ({
    id: "",
    status: ""
  });

  getDefaultSingleKeyData = () => ({
    list: [],
    total: 0,
    pages: 1,
    page: 1,
    limit: 10,
    filters: {
      ...this.getDefaultFilters()
    },
  });

  getDefaultData = () => ({
    active: {
      ...this.getDefaultSingleKeyData(),
    },
    inActive: {
      ...this.getDefaultSingleKeyData(),
    },
    barred: {
      ...this.getDefaultSingleKeyData(),
    }
  });

  getInfluencerStatus = () => ([
    { value: 0, label: "inActive" },
    { value: 1, label: "active" },
    { value: 2, label: "barred" },
  ]);

  getDefaultReachOptions = () => ({
    floor: 0,
    ceil: 1000000,
    step: 100000,
    noSwitching: true
  });

  getCSVSocialLabel = () => ([
    "name",
    'social_name',
    "followers_count",
    "ER",
    'updated_dt',
    "link",
    "genre",
    "category",
    "type",
    "currency",
    "cost",
    "text",
    "Image",
    "Video",
    "Story"
  ]);

  getCSVPlatforms = () => (
    ["facebook", "twitter", "instagram", "youtube"]
  );

  getDefaultFilters = () => ({
    keyword: "",
    gender: [],
    social: [],
    category: [],
    genres:[],
    min_reach: 0,
    max_reach: 0,
    outdated_profiles: 0,
  });

  getDefaultSelectedCampaign = () => ({
    campaignId: "",
    status: "",
    startDate: "",
  });

  getDefaultConfirmationModalDetails = () => ({
    show: false,
    title: "",
    message: "",
    buttonText: "",
  });

  getDefaultPageFIlters = () => ({
    keyword: "",
    platform: "",
    status: "",
  });

  getDefaultReach = () => ({
    active: 0,
    inActive: 0,
    barred: 0
  });

  getCampaignStatusColour = (status) => {
    switch (status) {
      case "2":
        return "color_yellow";
      case "3":
        return "color_green";
      case "4":
      case "6":
        return "color_red";
      case "8":
        return "color_organe";
      default:
        return "eleve_color"
    }
  };

  getFilterToastMessages = type => {
    switch (type) {
      case "category":
        return "Showing all users lying in atleast one of the selected categories";
      case "gender":
        return "Showing profiles of all selected genders";
      case "social":
        return "Showing users with profiles on all selected platforms";
      default:
        return "";
    }
  };

  handleReachFilters = ({
    min_reach: minReach,
    max_reach: maxReach,
  },
    currentDataKey,
    followersReach
  ): any => {
    let defaultRerurnValue = {
      min_reach: 0,
      max_reach: 0
    };
    const { [currentDataKey]: currentMaxReach } = followersReach;
    if (!currentMaxReach) {
      return defaultRerurnValue
    };
    let maxReachPercentage = (maxReach / currentMaxReach) * 100;
    maxReachPercentage = parseInt(maxReachPercentage.toFixed(2));
    if (maxReachPercentage !== 100 || minReach > 0) {
      return {
        min_reach: minReach,
        max_reach: maxReach
      };
    }
    return defaultRerurnValue;
  };

  handleFilters = ({
    key, 
    value, 
    filterType,
    currentDataKey,
    followersReach,
    filters
  }: any): any => {
    filters = cloneDeep(filters)
    let updatedFilters: any = {}; 
    let tempObj: any = {};
    switch(filterType) {
      case "multiselect": {
        let { [key]: currentValues } = filters;
        if (currentValues.includes(value)) {
          currentValues.splice(currentValues.indexOf(value), 1)
        } else {
          currentValues.push(value);
        }
        tempObj = { [key]: currentValues };
        break;
      }
      case "slider": {
        tempObj = {
          ...this.handleReachFilters(value, currentDataKey, followersReach)
        };
        break;
      }
      case "dropdown": {
        const { name = "" } = value;
        tempObj = { genres: name ? [name]: [] };
        break;
      }
      case "toggle": {
        if (key === "outdated_profiles") {
          tempObj = { outdated_profiles: value };
        }
      }
      default: {
        tempObj = { [key] : value };
      }
    }
    if (
        filterType !== "toggle" 
        && key !== "outdated_profiles"
      ) {
      if (filterType === "slider") {
        const { 
          min_reach: minReach = 0, 
          max_reach: maxReach = 0,
        } = tempObj;
        const { outdated_profiles: outdatedProfiles } = filters;
        updatedFilters = {
          ...filters,
          ...tempObj,
          outdated_profiles: (!minReach && !maxReach) 
            ? outdatedProfiles
            : 0
        };
      } else {
        updatedFilters = {
          ...filters,
          ...tempObj,
          outdated_profiles: 0
        }
      }
    } else {
      updatedFilters = {
        ...cloneDeep(this.getDefaultFilters()),
        ...tempObj,
      };
    }
     return updatedFilters;
  };
}

export default Utils;