import { Component, OnInit, EventEmitter, Input, Output, SimpleChanges } from "@angular/core";

import { AngularCsv } from "angular7-csv";
import { map, shareReplay, take, switchMap, tap } from "rxjs/operators";
import { Subscription } from "rxjs";
import * as moment from "moment";

import { AdminService, AuthService } from "src/app/services";

import Utils from "../utils";
const utils = new Utils();

@Component({
  selector: "list-filters",
  templateUrl: "./list-filters.component.html",
})
export class ListFiltersComponent implements OnInit {
  @Input() loading;
  @Input() influencersLength;
  @Input() filters;
  @Input() generes;
  @Input() status;
  @Input() reachOptions;
  @Input() followersReach;
  @Input() currentPageKey;
  @Output() onChangeFilters = new EventEmitter();
  csvLoading: boolean = false;
  minSliderReach: number = 0;
  maxSliderReach: number = 0;
  selectedGenre: string;

  constructor(
    private apiService: AdminService,
    private authService: AuthService
  ) { }

  ngOnInit() {  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty("reachOptions")) {
      this.maxSliderReach = changes.reachOptions.currentValue.ceil;
    }
  }

  updateFilters = (key, value, filterType): void => {
    this.onChangeFilters.emit({
      key,
      value,
      filterType
    });
  };

  onChangeSlider = (): void => {
    const { 
      max_reach: currentFilterMaxReach,
      min_reach: currentFilterMinReach 
    } = this.filters;
    const { [this.currentPageKey]: currentSliderMaxRange } = this.followersReach;
    const { minSliderReach, maxSliderReach } = this;
    let maxReachPercentage = (maxSliderReach / currentSliderMaxRange) * 100;
    maxReachPercentage = parseInt(maxReachPercentage.toFixed(2));
    if (
      currentFilterMaxReach 
      || currentFilterMinReach
      || (maxReachPercentage !== 100 && currentSliderMaxRange !== 0)
      || minSliderReach
    ) {
      this.onChangeFilters.emit({
        key: "reach",
        value: {
          min_reach: this.minSliderReach,
          max_reach: this.maxSliderReach
        },
        filterType: "slider"
      }); 
    }
  };

  resetSliderAndGenreList = (): void => {
    this.minSliderReach = 0;
    this.maxSliderReach = 0;
    this.selectedGenre = undefined;
  };

  onClickClearGenre = (): void => {
    this.selectedGenre = undefined;
    this.onChangeFilters.emit({
      key: "genres",
      value: { name: "" },
      filterType: "dropdown"
    });
  };

  getInfluencerStatusLabel = (status: number | string): string => {
    return utils.getInfluencerStatus().find(influencer => (
      status == influencer.value
    )).label;
  };

  getCSVName = (): string => {
    const {
      category,
      genres,
    } = this.filters;
    return ("Registered_Influencers"+
      category.toString()+
      genres.toString()+
      moment().format("Do MMM YY").split(" ").join()).replace( /,/g, "");
  };

  getPlatformsHeader = (): Array<string> => {
    let allHEaders = [];
    const PLATFORM_INDEX_COUNT = [1, 2, 3, 4];
    utils.getCSVPlatforms().map(platform => {
      return PLATFORM_INDEX_COUNT.map(count => {
        let labelsArray = utils.getCSVSocialLabel().map(label => {
          return platform+"_"+count+"_"+label;
        });
        allHEaders = [...allHEaders, ...labelsArray];
      });
    });
    return allHEaders;
  };

  getEmptyKeys = (platform, index): any => {
    let data = {};
    utils.getCSVSocialLabel().map(keyToConcat => {
      data = {
        ...data,
        [platform+"_"+index+"_"+keyToConcat]: "",
      }
    })
    return data;
  };

  getDataKeys = (platform, obj, index): any => {
    const {
      name = "",
      Elv_social = "",
      followers_count = "",
      engagement_rate = "",
      engagement_updated = "",
      link = "",
      url = "",
      genres: genre = "",
      category = "",
      type = "",
      currency = "",
      cost = "",
      text = "",
      Image = "",
      Video = "",
      Story = ""
    } =  obj;
    let flattenedGenres = genre ? genre.toString() : genre;
    let data = {};
    for (let key in obj) {
      data = {
        [platform+"_"+index+"_"+"name"]: name,
        [platform+"_"+index+"_"+"social_name"]: Elv_social,
        [platform+"_"+index+"_"+"followers_count"]: followers_count,
        [platform+"_"+index+"_"+"ER"]: engagement_rate,
        [platform+"_"+index+"_"+"updated_dt"]: (engagement_updated) ?moment(engagement_updated).format('DD/MM/YYYY'): "",
        [platform+"_"+index+"_"+"link"]: link || url,
        [platform+"_"+index+"_"+"genre"]: flattenedGenres,
        [platform+"_"+index+"_"+"category"]: category,
        [platform+"_"+index+"_"+"type"]: type,
        [platform+"_"+index+"_"+"currency"]: currency,
        [platform+"_"+index+"_"+"cost"]: cost,
        [platform+"_"+index+"_"+"text"]: text,
        [platform+"_"+index+"_"+"Image"]: Image,
        [platform+"_"+index+"_"+"Video"]: Video,
        [platform+"_"+index+"_"+"Story"]: Story,
      }
    }
    return data;
  };

  getPlatformKeys = (platform, platformData): any => {
    let data = {};
    for(let index = 0; index < 4; index++){
      let currentIndexObject = platformData[index];
      let keyIndex = index + 1;
      if (currentIndexObject !== undefined) {
        data = {
          ...data,
          ...this.getDataKeys(platform, currentIndexObject, keyIndex)
        }
      } else {
        data = {
          ...data,
          ...this.getEmptyKeys(platform, keyIndex),
        }
      }
    }
    return data;
  };

  getSocialKeysData = ({ facebook, twitter, youtube, instagram }): any => {
    let facebookJson = { ...this.getPlatformKeys("facebook", facebook) };
    let twitterJson = { ...this.getPlatformKeys("twitter", twitter) };
    let instagramJson = { ...this.getPlatformKeys("instagram", instagram) };
    let youtubeJson = { ...this.getPlatformKeys("youtube", youtube) };
    let socialJson = {
      ...facebookJson,
      ...twitterJson,
      ...instagramJson,
      ...youtubeJson,
    };
    return socialJson;
  };

  getListData = (params: any): any => {
    params = {
      ...params,
      excel_download: 1,
      status: this.status,
    };
    this.csvLoading = true;
    return this.apiService.postApiCommon("/influencer/get_list", params)
      .pipe(
        map(data => data.body.result),
        shareReplay(1)
      );
  };

  downloadNotUpdatedProfiles = (): void => {
    this.csvLoading = true;
    const params = {
      outdated_profiles: 1,
    };
    const listData = this.getListData(params);
    this.generateCSV(listData); 
  };

  downloadFilteredProfiles = (): void => {
    const params = {
      ...this.filters
    };
    const listData = this.getListData(params);
    this.generateCSV(listData); 
  };

  generateCSV = (listData: any): void => {
    const subscriptions = new Subscription();
    subscriptions.add(listData.subscribe(influencerData => {
      const responseData = [...influencerData];
      let csvData = responseData.map((obj, index) => {
        const {
          first_name,
          last_name,
          gender,
          country,
          state,
          city,
          address,
          email,
          isd_code,
          mobile,
          status,
        } = obj;
        const singleCSVRow = {
          first_name,
          last_name,
          gender: gender,
          country: country,
          state,
          city,
          address,
          email,
          isd_code,
          mobile,
          status: this.getInfluencerStatusLabel(status).toLowerCase(),
          ...this.getSocialKeysData(obj),
        };
        return singleCSVRow;
      });
      let options = {
        showLabels: true,
        headers: [
          "first_name",
          "last_name",
          "gender","country",
          "state","city",
          "address",
          "email_id",
          "isd_code",
          "mobile",
          "status",
          ...this.getPlatformsHeader()
        ],
      };
      new AngularCsv(csvData, this.getCSVName(), options);
      this.csvLoading = false;
    }));
  };

}