import { Component, OnInit, OnDestroy, Renderer, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Ng2ImgMaxService } from 'node_modules/ng2-img-max';
import { NgxSpinnerService } from 'ngx-spinner';
import { influencerFormErrors } from '../mock-data/error-msgs';
import { Observable, ReplaySubject, Subscription } from 'rxjs';
import { map, mergeMap, switchMap } from 'rxjs/operators';
import numeral from 'numeral';

import { AdminService } from 'src/app/services';


const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


const AgeValidator = (control: AbstractControl): { [key: string]: boolean } | null => {
  const today = new Date();
  const birthDate = new Date(control.value);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age = age - 1;
  }
  if (age < 16 || age > 100) {
    return { age: true };
  }
  return null;
};

const MustMatch = (controlName: string, matchingControlName: string) => {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if ((matchingControl.errors && !matchingControl.errors.mustMatch) || !matchingControl.value) {
      return;
    }

    if (control.value === matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
};

const SpacesValidator = (controlName: string) => {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const value = control.value.replace(/ /g, '');
    if ((control.errors && !control.errors.mustMatch) || !control.value) {
      return;
    }

    if (!value.length) {
      control.setErrors({ spaces: true });
    } else {
      control.setErrors(null);
    }
  };
};

const LocationValidator = (controlName: string, matchingControlName: string) => {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if ((matchingControl.errors && !matchingControl.errors.mustMatch)) {
      return;
    }

    if (!control.value) {
      matchingControl.setErrors({ previousNotSelected: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
};





@Component({
  selector: 'app-create-influencer',
  templateUrl: './create-influencer.component.html',
  styleUrls: ['./create-influencer.component.css']
})
export class CreateInfluencerComponent implements OnInit, OnDestroy {
  base64textString = '';
  formSubmitted = false;
  influencerForm: FormGroup;
  $isdCodes: Observable<Array<any>>;
  $stateList: Observable<Array<any>>;
  subscriptions = new Subscription();
  $citiesList: Observable<Array<any>>;
  imagePath: string;
  imgURL: any;
  message: string;
  showCheckEmailModal = false;
  showAddSocialModal = false;
  selectedSocial = '';
  email_valid = 0;
  emailRegistered = false;
  errorMessages = influencerFormErrors;
  addedSocials: Array<any> = [];
  socialDefaultValues;
  socialDefaultGenres;
  socialIndex: any;
  image: HTMLImageElement;
  imageUpload: string;
  minDate: Date = null;
  maxDate: Date = null;


  constructor(private http: HttpClient, private toastr: ToastrService, private router: Router,
              private renderer: Renderer, private elem: ElementRef, private ng2ImgMaxService: Ng2ImgMaxService,
              private spinner: NgxSpinnerService, private formBuilder: FormBuilder, private adminService: AdminService) { }

  // function for uploading and previewing an image
  preview(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files.length === 0) {
      return;
    }
    const mimeType = files[0].type;
    // if (mimeType.match(/image\/*/) == null) {
    //   this.message = 'Only images are supported.';
    //   return;
    // }
    const reader = new FileReader();
    const reader2 = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(file);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    };
    //Convert base64
    this.ng2ImgMaxService.resizeImage(file, 400, 300).subscribe(result => {
      var reader = new FileReader();
      reader.readAsDataURL(result); // read file as data url
      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.image = new Image();
        this.image.src = event.target.result;
        this.imageUpload = this.image.src;
      }
    }, error => {
      this.adminService.error(error.reason)
    })
    reader2.onload = this._handleReaderLoaded.bind(this);
    reader2.readAsBinaryString(file);



  }

  // converting image into base64
  _handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
   // this.base64textString = binaryString;
    this.base64textString = this.imageUpload;
  }


  // check email request & logic
  checkEmail() {
    if (this.f.email_id.invalid && !this.f.email_id.hasError('registered')) {
      return;
    }
    this.adminService.postApiCommon('/influencer/check_email', { email_id: this.f.email_id.value }).
      subscribe(data => {
        switch (data.body.responseCode) {
          case 502:
            this.showCheckEmailModal = true;
            this.emailRegistered = true;
            this.influencerForm.controls.email_id.setErrors(null);
            break;

          case 409:
            this.error(data.body.responseMessage);
            this.influencerForm.controls.email_id.setErrors({ registered: true });
            break;

          case 404:
            this.influencerForm.controls.email_id.setErrors(null);
            break;
          default:
            this.error(data.body.responseMessage);
            this.influencerForm.controls.email_id.setErrors(null);
            break;
        }
      });
  }

  onCheckEmailClose() {
    this.close();
  }
  onCheckEmailCancel() {
    this.close();
    this.success('Kindly use a different email in this form');
  }
  onCheckEmailDelink() {
    this.close();
    this.success('Email can now be used for creating a new profile');
    this.emailRegistered = false;
    this.email_valid = 1;
  }
  close() {
    this.showCheckEmailModal = false;
  }

  // toasts
  success(msg) {
    this.toastr.success(msg);
  }
  error(msg) {
    this.toastr.error(msg);
  }
  // For spinner
  show() {
    this.spinner.show();
  }
  hide() {
    this.spinner.hide();
  }

  // add social modal
  getDefaultGenres(socialName) {
    return this.addedSocials[this.socialIndex] && this.addedSocials[this.socialIndex].genres;
    }

  formatSubscribers(n) {
    return numeral(n).format('0a');
  }
  getDefaultValues(socialName) {
    return this.addedSocials[this.socialIndex] && this.addedSocials[this.socialIndex];
  }
  // function for adding social
  addSocial(social: string, i) {
    this.socialIndex = i !== '' && i;
    this.selectedSocial = social;
    this.showAddSocialModal = true;
  }
  onAddSocialClose() {
    this.closeAddSocial();
  }

  onAddSocialSave(data) {
    this.closeAddSocial();
    if (typeof(this.socialIndex) === 'number') {
      this.addedSocials = this.addedSocials.map((soc, i ) => i === this.socialIndex ? data : soc);
    } else {
      this.addedSocials = [...this.addedSocials, data];
    }
    const defVal = this.getDefaultValues(data.social);
    this.adminService.success(`${data.social} profile has been successfully ${this.addedSocials[this.socialIndex] ? 'updated' : 'added'}`);
    this.socialIndex = '';
  }

  closeAddSocial() {
    this.showAddSocialModal = false;
    this.selectedSocial = '';
  }
  getSelectedSocial() {
    return this.selectedSocial;
  }
  deleteSocial(i) {
    this.addedSocials.splice(i, 1);
  }

  cancelCreateInfluencer(){
    this.router.navigate(['influencer/list']);
  }


  ngOnInit() {
    this.$isdCodes = this.adminService.getApi('/country/isd').pipe(map((results) => results.body.result));
    this.influencerForm = this.formBuilder.group({
      first_name: ['', [Validators.required, Validators.maxLength(20),
      Validators.minLength(2), Validators.pattern('^[a-zA-Z0-9^\\s]+$')]],
      last_name: ['', [Validators.maxLength(20), Validators.minLength(2),
      Validators.pattern('^[a-zA-Z0-9]+$')]],
      gender: ['', Validators.required],
      dob: ['', AgeValidator],
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      address: [''],
      email_id: ['', [Validators.required,
      Validators.pattern(emailRegex)]],
      alt_email: ['', Validators.pattern(emailRegex)],
      isd_code: [''],
      mobile: ['', [Validators.maxLength(14), Validators.minLength(4),
      Validators.pattern('^[0-9]+$')]],
      alt_isd_code: [''],
      alt_mobile: ['', [Validators.maxLength(14), Validators.minLength(4),
      Validators.pattern('^[0-9]+$')]],
      note: ['']
    }, {
        validator: [MustMatch('email_id', 'alt_email'), MustMatch('mobile', 'alt_mobile'),
        SpacesValidator('first_name'), LocationValidator('country', 'state'), LocationValidator('state', 'city')]
      });
    // loading states according to the country selected
    this.subscriptions.add(this.f.country.valueChanges.subscribe(value => this.$stateList = this.adminService
      .postApi('/statelist', { country: `${value}` }).pipe(
        map((data => data.body.result))
      )));
    // loading cities according to the state selected
    this.subscriptions.add(this.f.state.valueChanges.subscribe(value => this.$citiesList = this.adminService
      .postApi('/cities', { state: `${value}` }).pipe(
        map((data => data.body.result))
      )));
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 100, 0, 1);
    this.maxDate = new Date(currentYear - 18, 0, 1);


  }

  get f() { return this.influencerForm.controls; }

  onSubmit() {
    this.influencerForm.markAllAsTouched();
    if (this.influencerForm.invalid) {
      const FirstInvalid = this.elem.nativeElement.querySelectorAll('.ng-invalid')[1];
      FirstInvalid.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'start' });
      if (this.f.email_id.hasError('registered')) {
        this.error('Email Id already exist in registered influencer.');
      }
      return;
    }
    if (this.emailRegistered) {
      this.showCheckEmailModal = true;
      return;
    }
    // organizing added socials in a proper way
    const socials = [...new Set(this.addedSocials.map(s => s.social))].map(s => {
      return {
        [s.toLowerCase()]: this.addedSocials.filter(soc => soc.social === s).map(social => {
          const { name, link, followers_count, currency, cost, category, bio, profile_type, genres,connection_type } = social;
          return s === 'Facebook' ? { name, link, followers_count, currency, cost, category, bio, type: profile_type, genres,Elv_social:name,connection_type } :
            { name, link, followers_count, currency, cost, category, bio, genres,Elv_social:name,connection_type };
        })
      };
    });
    const user = {
      ...this.influencerForm.value,
     // profilePic: this.base64textString,
      profilePic: this.imageUpload,
      email_valid: `${this.email_valid}`
    };
    // maping added socials to the user's data
    socials.forEach(el => {
      user[Object.keys(el)[0]] = el[Object.keys(el)[0]];
    });
    // sending 2 requests with the help of rxjs "switchMap", it allows to make asynchronous requests go one after another
    this.subscriptions.add(this.adminService.postApiCommon('/influencer/create_influencer', user).
      pipe(switchMap(res => this.adminService.postApiCommon('/influencer/status/update', { influencer_id: res.body.result._id, status: `0` })))
      .subscribe(data => {
        if (data.status === 200) {
          this.success('Influencer has been created & invited');
          this.router.navigate(['influencer/list']);
        }
      }));
  }

// unsubscribing from all subscriptions for better performance
  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}
