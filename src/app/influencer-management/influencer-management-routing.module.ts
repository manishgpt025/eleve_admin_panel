import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { InfluencerLayoutComponent } from "./influencer-layout/influencer-layout.component";
import { CreateInfluencerComponent } from "./create-influencer/create-influencer.component";
import { InfluencerListComponent } from "./list/list.component";
import { InfluencerDetailComponent } from "./influencer-detail/influencer-detail.component";

const routes: Routes = [
  {
    path: "", component: InfluencerLayoutComponent,
    children: [
      { path: "", redirectTo: "create" },
      { path: "create", component: CreateInfluencerComponent },
      { path: "list", component: InfluencerListComponent },
      { path: ":id", children: [
        {path: "detail", component: InfluencerDetailComponent}
    ] }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfluencerManagementRoutingModule { }
