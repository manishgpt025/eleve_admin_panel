import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
import * as moment from "moment";
import { map, shareReplay, take, switchMap, tap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Options } from 'ng5-slider';
import { AngularCsv } from 'angular7-csv';

import { AdminService, AuthService } from "src/app/services";

@Component({
  selector: 'app-influencer-list',
  templateUrl: './influencer-list.component.html',
  styleUrls: ['./influencer-list.component.css']
})
export class InfluencerListComponent implements OnInit {
  user_id = '5d31733ae5ac90da0c676a85';
  showStatusModal = false;
  statusToChange = '';
  influencerId = null;
  availablePages = 1;
  isLoading = false;
  currentPage = 1;
  statusChangeIndex = null;
  subscriptions = new Subscription();
  influencers = [];
  filtersObject = {
    user_id: this.user_id,
    keyword: '',
    sort: '1',
    status: '0',
    pageNumber: '1',
    gender: [],
    social: [],
    category: [],
    genres:[],
    min_reach:0,
    max_reach:0
    };
  $influencers;
  activeAmount;
  inactiveAmount;
  barredAmount;
  defaultScrollPosition = 0;
  $downloadData: any;
  responseData: any;
  genreArray: object[] = [];
  PLATFORMS = ["facebook", "twitter", "instagram", "youtube"];
  PLATFORM_INDEX_COUNT = [1, 2, 3, 4];
  SOCIAL_LABELS = ["name", "followers_count", "ER","link","genre","category","type","currency","cost","text","Image","Video","Story"];
  minValue: number = 0;
  maxValue: number = 0;
  options: Options = {
    floor: 0,
    ceil: 10000000
  };
  genreValue = '';
  // influencerStatusArray = [
  //   { value: 0, label: "Inactive"},
  //   { value: 1, label: "Active"},
  //   { value: 2, label: "Barred"},
  // ];

  constructor(private adminService: AdminService, private elem: ElementRef,private auth: AuthService) {}

  @HostListener('window:scroll', ['$event']) // for window scroll events
  onScroll = event =>  {
    if (window.innerHeight + window.scrollY + 200 >= document.body.offsetHeight && this.influencers.length
       && this.currentPage < this.availablePages && !this.isLoading ) {
         this.isLoading = true;
      // tslint:disable-next-line: radix
         const pageNeeded = parseInt(this.filtersObject.pageNumber) + 1;
         this.filtersObject.pageNumber = pageNeeded.toString();
         this.updateFilters('pageNumber', pageNeeded.toString());
    }
  };

  updateAmounts = () => {
    let filtersObject = Object.assign({}, this.filtersObject);
    filtersObject.status = '0';
    this.adminService
    .postApiCommon('/influencer/get_list', filtersObject)
    .pipe(
      map((data) => data.body.result.total),
      tap(value => {filtersObject.status = '1'; this.inactiveAmount = value; }),
      switchMap(() => this.adminService.postApiCommon('/influencer/get_list', filtersObject).
      pipe(
        map((data) => data.body.result.total),
         tap(value => {filtersObject.status = '2'; this.activeAmount = value; }),
         switchMap(() => this.adminService.postApiCommon('/influencer/get_list',filtersObject).
         pipe(
          map((data) => data.body.result.total),
           tap(value => {this.barredAmount = value; }))
    )))).subscribe();
  };

  changeStatus = () => {
    this.adminService
      .postApiCommon('/influencer/status/update',
        {
          influencer_id: this.influencerId,
          status: this.statusToChange,
        }).pipe(
          switchMap((res) => this.adminService.postApiCommon('/influencer/get_list', this.filtersObject).pipe(
            map(data => data.body.result)
          )
          )
        ).subscribe(data => {
          this.availablePages = data.pages;
          this.currentPage = parseInt(data.page);
          this.influencers = data.docs;
          this.adminService.success(
            `Influencer has been moved to
             ${this.statusToChange === '1'
               ? 'Active'
               : this.statusToChange === '0'
               ? 'Inactive'
               : 'Barred'
             } tab`
          );
          this.closeStatusModal(this.statusToChange);
          this.updateAmounts();
       }
     );
  };

  openStatusModal = (status, id) => {
    this.influencerId = id;
    this.showStatusModal = true;
    this.statusToChange = `${status}`;
  };

  closeStatusModal = (status?) => {
    this.showStatusModal = false;
    this.statusToChange = '';
  };

  ngOnInit() {
    console.log(setTimeout(() => {
      console.log(this.influencers);
    }, 4000))
    console.log("ngOnIt")
    this.updateAmounts();
    this.getGenre();
    this.$influencers = this.adminService
      .postApiCommon('/influencer/get_list', this.filtersObject)
      .pipe(
        map(data => data.body.result),
        shareReplay(1)
      );
    this.subscriptions.add(this.$influencers.subscribe(data => {
      this.availablePages = data.pages;
      this.currentPage = parseInt(data.page);
      this.influencers = data.docs;
    }));
  };

  getGenre = async () => {
    let data = this.auth.getData();
    let reqData = {
      "user_id": data._id
    }
    this.adminService.postApiCommon('/influencer/genreList', reqData).subscribe((response: any) => {
      if (response.status == 200) {
        let arr = response.body;
        if (arr.responseCode == 200) {
          if (arr.result.length) {
            let genres = [];
            for (let i = 0; i < arr.result.length; i++) {
              genres.push({name: arr.result[i].subgenre})
            }
            this.genreArray = [...genres];
          }
        }
      }
    });
  };

  showToast = (label: string, values) => {
    if (!values.length) {
      return;
    }
    switch (label) {
      case 'category':
        if (values.length > 1) {
          this.adminService.success(
            'Showing all users lying in atleast one of the selected categories'
          );
        } else {
          this.adminService.success(`Showing results for ${values[0]}`);
        }
        break;
      case 'gender':
        if (values.length > 1) {
          this.adminService.success('Showing profiles of all selected genders');
        } else {
          this.adminService.success(`Showing results for ${values[0]}`);
        }
        break;
      case 'social':
        if (values.length > 1) {
          this.adminService.success(
            'Showing users with profiles on all selected platforms'
          );
        } else {
          this.adminService.success(
            `Showing users having profiles on ${values[0]}`
          );
        }
        break;
      default:
        break;
    }
  }


  updateFilters = (type: string, value) => {
    if (type) {
      switch (type) {
        case 'keyword':
          if (value.length === 1) {
            return;
          }
          this.filtersObject.keyword = `${value}`;
          break;
        case 'sort':
          this.filtersObject.sort = this.filtersObject.sort === '1' ? '0' : '1';
          break;
        case 'status':
          this.filtersObject.status = `${value}`;
          break;
        case 'pageNumber':
          this.filtersObject.pageNumber = `${value}`;
          break;
        case 'reachs':
            this.filtersObject.min_reach = this.minValue;
            this.filtersObject.max_reach = this.maxValue;
            break;
        case 'genres':
          this.filtersObject[`${type}`] = [];
          this.filtersObject[`${type}`].push(value.name);
          break;
        default:
          if (this.filtersObject[`${type}`].includes(value)) {
            this.filtersObject[`${type}`].splice(
            this.filtersObject[`${type}`].indexOf(value),
              1
            );
          } else {
            this.filtersObject[`${type}`].push(value);
          }
          this.showToast(type, this.filtersObject[`${type}`]);
          break;
      }
    }
    if (type !== 'pageNumber' ) {
      this.influencers = [];
      this.filtersObject.pageNumber = '1';
    }
    this.updateAmounts();
    this.adminService
      .postApiCommon('/influencer/get_list', this.filtersObject)
      .pipe(map(data => data.body.result)).subscribe(data => {
    // tslint:disable-next-line: radix
    this.isLoading = false;
    this.availablePages = data.pages;
    this.currentPage = parseInt(data.page);
    if (parseInt(this.filtersObject.pageNumber) > 1) {
      this.influencers.push(...data.docs);
    } else {
      this.influencers = data.docs;
    }
    });
  }

  clearSearch = () => {
    this.filtersObject.keyword = '';
    this.updateAmounts();
    this.adminService
      .postApiCommon('/influencer/get_list', this.filtersObject)
      .pipe(map(data => data.body.result)).subscribe(data => {
    // tslint:disable-next-line: radix
    this.isLoading = false;
    this.availablePages = data.pages;
    this.currentPage = parseInt(data.page);
    this.influencers = data.docs;
    });
  };

  refresh = () => {
    this.filtersObject.category = [];
    this.filtersObject.keyword = '';
    this.filtersObject.sort = '1';
    this.filtersObject.gender = [];
    this.filtersObject.social = [];
    this.filtersObject.genres = [];
    this.genreValue = '';
    this.filtersObject.max_reach = 0;
    this.filtersObject.min_reach = 0;
    this.minValue = 0;
    this.maxValue = 0;
    this.elem.nativeElement.querySelectorAll('.filter').forEach(element => {
      element.checked = false;
    });
    this.updateAmounts();
    this.adminService
      .postApiCommon('/influencer/get_list', this.filtersObject)
      .pipe(map(data => data.body.result)).subscribe(data => {
    // tslint:disable-next-line: radix
    this.isLoading = false;
    this.availablePages = data.pages;
    this.currentPage = parseInt(data.page);
    this.influencers = data.docs;
    });
  }

  /// CSV Generator Functions
  getCSVName = () => {
    const {
      category,
      genres,
    } = this.filtersObject;
    return ("Registered_Influencers"+category.toString()+genres.toString()+moment().format("Do MMM YY").split(" ").join()).replace( /,/g, "");
  }

  getPlatformsHeader = () => {
    let allHEaders = [];
    this.PLATFORMS.map(platform => {
      return this.PLATFORM_INDEX_COUNT.map(count => {
        let labelsArray = this.SOCIAL_LABELS.map(label => {
          return platform+"_"+count+"_"+label;
        });
        allHEaders = [...allHEaders, ...labelsArray];
      });
    });
    return allHEaders;
  };

  getEmptyKeys = (platform, index) => {
    let data = {};
    this.SOCIAL_LABELS.map(keyToConcat => {
      data = {
        ...data,
        [platform+"_"+index+"_"+keyToConcat]: "",
      }
    })
    return data;
  }

  getDataKeys = (platform, obj, index) => {
    const {
      name = "",
      followers_count = "",
      engagement_rate = "",
      link = "",
      genres: genre = "",
      category = "",
      type = "",
      currency = "",
      cost = "",
      text = "",
      Image = "",
      Video = "",
      Story = ""
    } =  obj;
    let flattenedGenres = genre ? genre.toString() : genre;
    let data = {};
    for (let key in obj) {
      data = {
        [platform+"_"+index+"_"+"name"]: name,
        [platform+"_"+index+"_"+"followers_count"]: followers_count,
        [platform+"_"+index+"_"+"ER"]: engagement_rate,
        [platform+"_"+index+"_"+"link"]: link,
        [platform+"_"+index+"_"+"genre"]: flattenedGenres,
        [platform+"_"+index+"_"+"category"]: category,
        [platform+"_"+index+"_"+"type"]: type,
        [platform+"_"+index+"_"+"currency"]: currency,
        [platform+"_"+index+"_"+"cost"]: cost,
        [platform+"_"+index+"_"+"text"]: text,
        [platform+"_"+index+"_"+"Image"]: Image,
        [platform+"_"+index+"_"+"Video"]: Video,
        [platform+"_"+index+"_"+"Story"]: Story,
      }
    }
    return data;
  }

  getPlatformKeys = (platform, platformData) => {
    let data = {};
    for(let index = 0; index < 4; index++){
      let currentIndexObject = platformData[index];
      let keyIndex = index + 1;
      if (currentIndexObject !== undefined) {
        data = {
          ...data,
          ...this.getDataKeys(platform, currentIndexObject, keyIndex)
        }
      } else {
        data = {
          ...data,
          ...this.getEmptyKeys(platform, keyIndex),
        }
      }
    }
    return data;
  }

  getSocialKeysData = ({ facebook, twitter, youtube, instagram }) => {
    let facebookJson = { ...this.getPlatformKeys("facebook", facebook) };
    let twitterJson = { ...this.getPlatformKeys("twitter", twitter) };
    let instagramJson = { ...this.getPlatformKeys("instagram", instagram) };
    let youtubeJson = { ...this.getPlatformKeys("youtube", youtube) };
    let socialJson = {
      ...facebookJson,
      ...twitterJson,
      ...instagramJson,
      ...youtubeJson,
    };
    return socialJson;
  }

  downloadCsv = () => {
    this.$downloadData = this.adminService
    .postApiCommon('/influencer/get_download_list', this.filtersObject)
    .pipe(
      map(data => data.body.result),
      shareReplay(1)
    );
    this.subscriptions.add(this.$downloadData.subscribe(influencerData => {
      const responseData = [...influencerData];
      let csvData = responseData.map((obj, index) => {
        const {
          _id: influencer_ID,
          first_name,
          last_name,
          gender,
          country,
          state,
          city,
          address,
          email,
          isd_code,
          mobile,
          facebook,
          twitter,
          instagram,
          youtube,
        } = obj;
        const singleCSVRow = {
          influencer_ID,
          first_name,
          last_name,
          gender: gender,
          country: country,
          state,
          city,
          address,
          email,
          isd_code,
          mobile,
          ...this.getSocialKeysData(obj),
        };
        return singleCSVRow;
      });
      let options = {
        showLabels: true,
        headers: [
          "influencer_ID",
          "first_name",
          "last_name",
          "gender",
          "country",
          "state",
          "city",
          "address",
          "email_id",
          "isd_code",
          "mobile",
          ...this.getPlatformsHeader()
        ]
      };
      new AngularCsv(csvData, this.getCSVName(), options);
    }));
  }

  formatCost = (cost = ""): string => {
    if (!cost) {
      return "0";
    }
    const formattedAmount = parseInt(cost).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    if (formattedAmount === "NaN") {
      return "0";
    }
    return formattedAmount
  };
}