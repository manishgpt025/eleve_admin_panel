// import { Component, OnInit, HostListener, ElementRef } from '@angular/core';
// import * as moment from "moment";

// import { AdminService, AuthService } from "src/app/services";
// import { map, shareReplay, take, switchMap, tap } from 'rxjs/operators';
// import { Subscription } from 'rxjs';
// import { Options } from 'ng5-slider';
// import { AngularCsv } from 'angular7-csv';
// @Component({
//   selector: 'app-influencer-list',
//   templateUrl: './template.html',
//   styleUrls: ['./influencer-list.component.css']
// })
// export class InfluencerListComponent implements OnInit {
//   userId: string = "";
//   filtersObject = {
//     keyword: "",
//     sort: '1',
//     status: '0',
//     pageNumber: '1',
//     gender: [],
//     social: [],
//     category: [],
//     genres:[],
//     min_reach:0,
//     max_reach:0
//   };

//   influencers: object = {
//     inActiveTotal: 0,
//     activeTotal: 0,
//     barredTotal: 0,
//     active: [],
//     inActive: [],
//     barred: [],
//     inActivePage: "",
//     activePage: "",
//     barredPage: "",
//     inActivePages: "",
//     activePages: "",
//     barredPages: "",
//     inActiveLimit: "",
//     activeLimit: "",
//     barredLimit: "",
//   };
//   defaultFilters: any = {
//     keyword: '',
//     sort: '1',
//     gender: [],
//     social: [],
//     category: [],
//     genres:[],
//     min_reach:0,
//     max_reach:0,
//   };
//   filters = { ...this.defaultFilters };
//   statusArray = [
//     { action: 0, label: "inActive" },
//     { action: 1, label: "active" },
//     { action: 2, label: "barred" },
//   ];
//   loading: boolean = true;
//   currentStatustab = 0;

//   showStatusModal = false;
//   statusToChange = '';
//   influencerId = null;
//   availablePages = 1;
//   isLoading = false;
//   currentPage = 1;
//   statusChangeIndex = null;
//   subscriptions = new Subscription();
  
//   $downloadData: any;
//   responseData: any;
//   genreArray: object[] = [];
//   PLATFORMS = ["facebook", "twitter", "instagram", "youtube"];
//   PLATFORM_INDEX_COUNT = [1, 2, 3, 4];
//   SOCIAL_LABELS = ["name", "followers_count", "ER","link", "genre", "category", "type", "currency", "cost", "text", "Image", "Video", "Story"];
//   minValue: number = 0;
//   maxValue: number = 0;
//   options: Options = {
//     floor: 0,
//     ceil: 10000000
//   };
//   genreValue = '';

//   constructor(private adminService: AdminService, private elem: ElementRef, private auth: AuthService) {}

//   @HostListener('window:scroll', ['$event']) // for window scroll events
//   onScroll: any = this.onScroll;

//   ngOnInit() {
//     (() => {
//       this.statusArray.map(status => {
//         this.getInfluencersList(status.action, 1);
//       });
//       this.getGenres();
//     })();
//   };

//   ifApiStatusOkay = ({ status, body: {
//       responseCode
//     } 
//   }) => (status === 200 && responseCode === 200);

//   handleApiError = ({ status }) => 
//     this.adminService.error(status == 404 
//       ? "Service not found" 
//       : "Something went wrong! Please try again."
//     )

//   setInfluencersData = ({ 
//     limit, 
//     page, 
//     pages, 
//     docs, 
//     total 
//   }, status) => {
//     let key = status == 0 
//       ? "inActive" 
//       : status == 1 
//       ? "active"
//       : "barred";
//     const { [(key)]: currentDocs } = this.influencers
//     this.influencers = {
//       ...this.influencers,
//       [(key)]: [...currentDocs, ...docs],
//       [(key)+"Total"]: total,
//       [(key)+"Page"]: page,
//       [(key)+"Pages"]: pages,
//       [(key)+"Limit"]: limit,
//     };
//     this.loading = false;
//   };

//   setUserId = () => {
//     this.userId = this.auth.getData()._id;
//   };

//   getUserId = () => {
//     if (this.userId) {
//       this.setUserId();
//     }
//    return this.userId;
//   };

//   getInfluencersList = (status, pageNumber) => {
//     const params = {
//       user_id: this.getUserId(),
//       status,
//       pageNumber,
//       ...this.filters
//     };

//     let filters = { ...this.filters, status };
//     this.adminService
//       .postApiCommon("/influencer/get_list", params )
//       .subscribe(response => {
//         if (this.ifApiStatusOkay(response)) {
//           const { body: { 
//             result,
//             },
//           } = response;
//           this.setInfluencersData(result, status);
//         }
//       }, error => this.handleApiError(error)
//     )
//   }

//   getGenres = () => {
//     const params = {
//       user_id: this.getUserId(),
//     };
//     this.adminService
//       .postApiCommon("/influencer/genreList", params )
//       .subscribe(response => {
//         if (this.ifApiStatusOkay(response)) {
//           const { body: { 
//             result: generes,
//             },
//           } = response;
//           this.genreArray = generes.map(genere => ({
//             name: genere.subgenre
//           }));
//         }
//       }, error => this.handleApiError(error)
//     );
//   };

//   getInfluencersData = (key = "") => {
//     console.log(key);
//     if (key === "") {
//       return this.influencers
//     }
//     const { 
//       [(key)+"Total"]: total, 
//       [(key)+"Page"]: page, 
//       [(key)+"Pages"]: pages, 
//       [(key)+"Limit"]: limit,
//       [key]: data
//     } = this.influencers;
//     return {
//       [key]: data,
//       total,
//       page,
//       pages,
//       limit,
//       length: data.length
//     };
//   };

//   onChangeTab = status => this.currentStatustab = status;

//   changeStatus = () => {
//     const params = {
//       influencer_id: this.influencerId,
//       status: this.statusToChange,
//     };

//     // this.adminService
//     //   .postApiCommon('/influencer/status/update', params).pipe(
//     //       switchMap((res) => this.adminService.postApiCommon('/influencer/get_list', this.filtersObject).pipe(
//     //         map(data => data.body.result)
//     //       )
//     //       )
//     //     ).subscribe(data => {
//     //       this.availablePages = data.pages;
//     //       this.currentPage = parseInt(data.page);
//     //       this.influencers = data.docs;
//     //       this.adminService.success(
//     //         `Influencer has been moved to
//     //          ${this.statusToChange === '1'
//     //            ? 'Active'
//     //            : this.statusToChange === '0'
//     //            ? 'Inactive'
//     //            : 'Barred'
//     //          } tab`
//     //       );
//     //       this.closeStatusModal(this.statusToChange);
//     //    }
//     //  );
//   };

//   openStatusModal = (status, id) => {
//     this.influencerId = id;
//     this.showStatusModal = true;
//     this.statusToChange = `${status}`;
//   };

//   closeStatusModal = (status?) => {
//     this.showStatusModal = false;
//     this.statusToChange = '';
//   };

//   showToast = (label: string, values) => {
//     if (!values.length) {
//       return;
//     }
//     switch (label) {
//       case 'category':
//         if (values.length > 1) {
//           this.adminService.success(
//             'Showing all users lying in atleast one of the selected categories'
//           );
//         } else {
//           this.adminService.success(`Showing results for ${values[0]}`);
//         }
//         break;
//       case 'gender':
//         if (values.length > 1) {
//           this.adminService.success('Showing profiles of all selected genders');
//         } else {
//           this.adminService.success(`Showing results for ${values[0]}`);
//         }
//         break;
//       case 'social':
//         if (values.length > 1) {
//           this.adminService.success(
//             'Showing users with profiles on all selected platforms'
//           );
//         } else {
//           this.adminService.success(
//             `Showing users having profiles on ${values[0]}`
//           );
//         }
//         break;
//       default:
//         break;
//     }
//   }

//   updateFilters = (type: string, value) => {
//     console.log(type, value);
//     let filters = this.filters;
//     if (type) {
//       switch (type) {
//         case "keyword" :
//           if (value.length === 1) {
//             return;
//           }
//           filters.keyword = `${value}`;
//           break;
//         case "sort":
//           filters.sort = this.filtersObject.sort === "1" ? "0" : "1";
//           break;
//         case "status":
//           filters.status = `${value}`;
//           break;
//         case "pageNumber":
//           filters.pageNumber = `${value}`;
//           break;
//         case "reachs":
//             filters.min_reach = this.minValue;
//             filters.max_reach = this.maxValue;
//             break;
//         case "genres":
//           filters[`${type}`] = [];
//           filters[`${type}`].push(value.name);
//           break;
//         default:
//           if (filters[`${type}`].includes(value)) {
//             filters[`${type}`].splice(
//             filters[`${type}`].indexOf(value),
//               1
//             );
//           } else {
//             filters[`${type}`].push(value);
//           }
//           this.showToast(type, filters[`${type}`]);
//           break;
//       }
//     }
//     if (type !== "pageNumber") {
//       this.influencers = [];
//       filters.pageNumber = "1";
//     }
//     this.adminService
//       .postApiCommon("/influencer/get_list", filters)
//       .pipe(map(data => data.body.result)).subscribe(data => {
//     // tslint:disable-next-line: radix
//     this.isLoading = false;
//     this.availablePages = data.pages;
//     this.currentPage = parseInt(data.page);
//     if (parseInt(this.filtersObject.pageNumber) > 1) {
//       // this.influencers.push(...data.docs);
//     } else {
//       // this.influencers = data.docs;
//     }
//     });
//   }

//   clearSearch = () => {
//     this.filtersObject.keyword = '';
//     this.adminService
//       .postApiCommon('/influencer/get_list', this.filtersObject)
//       .pipe(map(data => data.body.result)).subscribe(data => {
//     // tslint:disable-next-line: radix
//     this.isLoading = false;
//     this.availablePages = data.pages;
//     this.currentPage = parseInt(data.page);
//     this.influencers = data.docs;
//     });
//   };

//   refresh = () => {
//     this.filtersObject.category = [];
//     this.filtersObject.keyword = '';
//     this.filtersObject.sort = '1';
//     this.filtersObject.gender = [];
//     this.filtersObject.social = [];
//     this.filtersObject.genres = [];
//     this.genreValue = '';
//     this.filtersObject.max_reach = 0;
//     this.filtersObject.min_reach = 0;
//     this.minValue = 0;
//     this.maxValue = 0;
//     this.elem.nativeElement.querySelectorAll('.filter').forEach(element => {
//       element.checked = false;
//     });
//     this.adminService
//       .postApiCommon('/influencer/get_list', this.filtersObject)
//       .pipe(map(data => data.body.result)).subscribe(data => {
//     // tslint:disable-next-line: radix
//     this.isLoading = false;
//     this.availablePages = data.pages;
//     this.currentPage = parseInt(data.page);
//     this.influencers = data.docs;
//     });
//   }

//   /// CSV Generator Functions
//   getCSVName = () => {
//     const {
//       category,
//       genres,
//     } = this.filtersObject;
//     return ("Registered_Influencers"+
//       category.toString()+
//       genres.toString()+
//       moment().format("Do MMM YY").split(" ").join()).replace( /,/g, "");
//   }

//   getPlatformsHeader = () => {
//     let allHEaders = [];
//     this.PLATFORMS.map(platform => {
//       return this.PLATFORM_INDEX_COUNT.map(count => {
//         let labelsArray = this.SOCIAL_LABELS.map(label => {
//           return platform+"_"+count+"_"+label;
//         });
//         allHEaders = [...allHEaders, ...labelsArray];
//       });
//     });
//     return allHEaders;
//   };

//   getEmptyKeys = (platform, index) => {
//     let data = {};
//     this.SOCIAL_LABELS.map(keyToConcat => {
//       data = {
//         ...data,
//         [platform+"_"+index+"_"+keyToConcat]: "",
//       }
//     })
//     return data;
//   }

//   getDataKeys = (platform, obj, index) => {
//     const {
//       name = "",
//       followers_count = "",
//       engagement_rate = "",
//       link = "",
//       genres: genre = "",
//       category = "",
//       type = "",
//       currency = "",
//       cost = "",
//       text = "",
//       Image = "",
//       Video = "",
//       Story = ""
//     } =  obj;
//     let flattenedGenres = genre ? genre.toString() : genre;
//     let data = {};
//     for (let key in obj) {
//       data = {
//         [platform+"_"+index+"_"+"name"]: name,
//         [platform+"_"+index+"_"+"followers_count"]: followers_count,
//         [platform+"_"+index+"_"+"ER"]: engagement_rate,
//         [platform+"_"+index+"_"+"link"]: link,
//         [platform+"_"+index+"_"+"genre"]: flattenedGenres,
//         [platform+"_"+index+"_"+"category"]: category,
//         [platform+"_"+index+"_"+"type"]: type,
//         [platform+"_"+index+"_"+"currency"]: currency,
//         [platform+"_"+index+"_"+"cost"]: cost,
//         [platform+"_"+index+"_"+"text"]: text,
//         [platform+"_"+index+"_"+"Image"]: Image,
//         [platform+"_"+index+"_"+"Video"]: Video,
//         [platform+"_"+index+"_"+"Story"]: Story,
//       }
//     }
//     return data;
//   }

//   getPlatformKeys = (platform, platformData) => {
//     let data = {};
//     for(let index = 0; index < 4; index++){
//       let currentIndexObject = platformData[index];
//       let keyIndex = index + 1;
//       if (currentIndexObject !== undefined) {
//         data = {
//           ...data,
//           ...this.getDataKeys(platform, currentIndexObject, keyIndex)
//         }
//       } else {
//         data = {
//           ...data,
//           ...this.getEmptyKeys(platform, keyIndex),
//         }
//       }
//     }
//     return data;
//   };

//   getSocialKeysData = ({ facebook, twitter, youtube, instagram }) => {
//     let facebookJson = { ...this.getPlatformKeys("facebook", facebook) };
//     let twitterJson = { ...this.getPlatformKeys("twitter", twitter) };
//     let instagramJson = { ...this.getPlatformKeys("instagram", instagram) };
//     let youtubeJson = { ...this.getPlatformKeys("youtube", youtube) };
//     let socialJson = {
//       ...facebookJson,
//       ...twitterJson,
//       ...instagramJson,
//       ...youtubeJson,
//     };
//     return socialJson;
//   };

//   downloadCsv = () => {
//     this.$downloadData = this.adminService
//     .postApiCommon('/influencer/get_download_list', this.filtersObject)
//     .pipe(
//       map(data => data.body.result),
//       shareReplay(1)
//     );
//     this.subscriptions.add(this.$downloadData.subscribe(influencerData => {
//       const responseData = [...influencerData];
//       let csvData = responseData.map((obj, index) => {
//         const {
//           _id: influencer_ID,
//           first_name,
//           last_name,
//           gender,
//           country,
//           state,
//           city,
//           address,
//           email,
//           isd_code,
//           mobile,
//           facebook,
//           twitter,
//           instagram,
//           youtube,
//         } = obj;
//         const singleCSVRow = {
//           influencer_ID,
//           first_name,
//           last_name,
//           gender: gender,
//           country: country,
//           state,
//           city,
//           address,
//           email,
//           isd_code,
//           mobile,
//           ...this.getSocialKeysData(obj),
//         };
//         return singleCSVRow;
//       });
//       let options = {
//         showLabels: true,
//         headers: [
//           "influencer_ID", 
//           "first_name",
//           "last_name", 
//           "gender","country", 
//           "state","city", 
//           "address",
//           "email_id", 
//           "isd_code",
//           "mobile", 
//           ...this.getPlatformsHeader()
//         ],
//       };
//       new AngularCsv(csvData, this.getCSVName(), options);
//     }));
//   };

//   formatCost = (cost = 0) => cost.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") || 0;

//   onScroll = () => {
//     const currentTab = this.currentStatustab;
//     const currentDataKey = this.statusArray.find(
//       obj => obj.action === currentTab
//     ).label;
//     const { 
//       [(currentDataKey)+"Page"]: currentPage,
//       [(currentDataKey)+"Pages"]: totalPages, 
//       [(currentDataKey)]: data,
//     } = this.influencers;
//     const shouldCallDataAgain = 
//       totalPages > currentPage 
//       && data.length 
//       && window.innerHeight + window.scrollY + 200 >= document.body.offsetHeight
//       && !this.loading;
//     if (shouldCallDataAgain) {
//       this.getInfluencersList(currentTab, currentPage+1);
//     }
//   };
// }