import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateCampaignStepOneComponent } from './create-campaign-step-one/create-campaign-step-one.component';
import { CampaignLayoutComponent } from './campaign-layout/campaign-layout.component';
import { StepTwoSuccessComponent } from './step-two-success/step-two-success.component';
import { MicroComponent } from './micro/micro.component';
import { PremiumComponent } from './premium/premium.component';
import { CreateCampaignStepThreeComponent } from './create-campaign-step-three/create-campaign-step-three.component';
import { ManageKolComponent } from './manage-kol/manage-kol.component';
import { CampaignListComponent } from './list/list.component';


const routes: Routes = [
  {
    path: "", component: CampaignLayoutComponent,
    children: [
      { path: "", component: CampaignListComponent },
      { path: "create", component: CreateCampaignStepOneComponent },
      { path: "create/:id", component: CreateCampaignStepOneComponent },
      { path: "step-two/:id", component: StepTwoSuccessComponent },
      { path: "micro/:platform/:id", component: MicroComponent },
      { path: "premium/:platform/:id", component: PremiumComponent },
      { path: "step-one", component: CreateCampaignStepOneComponent },
      { path: "step-three/:id", component: CreateCampaignStepThreeComponent },
      { path: "step-two-success", component: StepTwoSuccessComponent },
      { path: "kol/:id", component: ManageKolComponent },
      { path: "list/:action", component: CampaignListComponent }
    ],
    runGuardsAndResolvers: "always"
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignRoutingModule { }
