import { Component, OnInit, ElementRef, Inject } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Validators, FormControl, FormGroup, FormArray, FormBuilder, AbstractControl } from "@angular/forms";

import { AdminService, AuthService } from "src/app/services";

const hashtagValidator = (control: AbstractControl): { [key: string]: boolean } | null => {
  let value = control.value;
  if (!value) {
    return null;
  }
  let valueLength = value.length;
  const HASHTAG_PATTERN = /^([a-zA-Z0-9_]+)$/;
  if (valueLength === 201 || valueLength === 0) {
    return null;
  }
  if (value.charAt(0) === "#") {
    value = value.substr(1);
  }
  if (!HASHTAG_PATTERN.test(value)) {
    return { "hashTagPettern": true };
  }
  return null;
};

@Component({
  selector: "app-create-campaign-step-one",
  templateUrl: './create-campaign-step-one.component.html',
  styleUrls: ['./create-campaign-step-one.component.css']
})

export class CreateCampaignStepOneComponent implements OnInit {
  campaignFormStepOne: FormGroup;
  agencylist: object[] = [];
  advertiserlist: object[] = [];
  data: any;
  agencyIdSelect: any;
  advertiserIdSelect: any;
  brandlist: any[];
  dBrandlist:object[] = [];
  dBrandIdSelect: any;
  managerlist: any[];
  listNameManager: any[];
  ManagerValueAndNameList: any;
  exts=['.png','.gif','.jpeg','.jpg','.mkv'];
  SelectPhoto: File = null;
  validPhoto: boolean;
  url: any = "";
  show_div_objective_change:boolean = true;
  show_div_rt_url:boolean = true;
  imageUploadSuccess: boolean;
  ImageData: FormArray;
  agencyListData: any;
  advertiserDataList: any;
  brandDataList: any;
  directBrandDataList: any;
  SelectVideo: File = null;
  videoUploadSuccess: boolean;
  videoData: string = "";
  disabledAdvertiserAgencyField: boolean= true;
  disabledAdvertiserBrandField: boolean= true;
  campaignId: any = "";
  campaignData: any = "";
  campaignDetailsData: any;
  campaignD: any;
  selectedManagerName: string = "";
  selectVideoLink: any = "";
  adminData: any =[];

  constructor(
    private route: ActivatedRoute,
    private auth: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private apiService: AdminService,
    private el: ElementRef,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
     let dataId = params.id;
      if(dataId != undefined || dataId != ""){
        this.campaignId = params.id;
      }else{
        this.campaignId = "";
      }
    });
    this.campaignDetails();
    this.camaignCreateOneFormVal();
    this.getAgencyList();
    this.getDirectBrancdList();
    this.getManagerList();
  }

  getAdminData = ():any => {
    if(!this.adminData.length)
      this.adminData = this.auth.getData();
    return this.adminData._id;
  }
    // Get the campaign detail
  campaignDetails = ():void => {
      if (this.campaignId) {
        let reqData =
        {
          "admin_id": this.getAdminData(),
          "campaign_id": this.campaignId
        }
        this.apiService.postApiCommon('/campaign/details', reqData)
        .subscribe((response: any) => {
          if (response.status === 200) {
            this.campaignData = response.body;
            if(this.campaignData.responseCode == 200){
              this.campaignDetailsData = this.campaignData.result.campaign;
              this.bindCampaignData(this.campaignDetailsData);
              if(
              this.campaignDetailsData
                &&
              this.campaignDetailsData.status == '2')
              {
                this.campaignStatusChange();
              }
            }else{
              this.apiService.error("Campaign not found.");
            }
          }
        }, error => {
          this.apiService.error("Something went wrong! Please try again.");
        });
      }
  };

    // Validation
  camaignCreateOneFormVal = (): void => {
      this.campaignFormStepOne = new FormGroup({
        campaign_title: new FormControl('', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(60)])),
        objective_type: new FormControl('', Validators.compose([Validators.required])),
        primary_hashtag:new FormControl('', Validators.compose([ Validators.minLength(1), Validators.maxLength(200), hashtagValidator ])),
        primary_hashtag_share: new FormControl(''),
        url: new FormControl('',
        Validators.compose([
          Validators.pattern(/^(?:https?:\/\/(?:www\.)?|www\.)[a-z0-9]+(?:[-.][a-z0-9]+)*\.[a-z]{2,5}(?::[0-9]{1,5})?(?:\/\S*)?$/)
        ])),
        url_share: new FormControl(''),
        secondary_hashtag: new FormControl('',Validators.compose([Validators.minLength(1), Validators.maxLength(200),hashtagValidator])),
        secondary_hashtag_list:this.fb.array([]),
        secondary_hashtag_track: new FormControl(''),
        keywords: new FormControl('',Validators.compose([Validators.minLength(2),Validators.pattern(/^[^#]+$/)])),
        keywords_list:this.fb.array([]),
        keywords_share : new FormControl('',),
        keywords_track : new FormControl(''),
        retweet_url: new FormControl('',
        Validators.compose([
          Validators.pattern(/^(?:https:\/\/)?(?:www\.)?twitter\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-]*)?(?:\/\S*)?$/)
        ])),
        rtUrl_list:this.fb.array([]),
        image: new FormControl(''),
        video: new FormControl(''),
        media_share: new FormControl(''),
        advertiser_type: new FormControl('', Validators.compose([Validators.required])),
        agency_id: new FormControl({value: '', disabled: true}),
        advertiser_id: new FormControl(''),
        brand_id: new FormControl(''),
        brand_id_brand: new FormControl({value: '', disabled: true}),
        advertiser_id_brand: new FormControl(''),
        ro_number: new FormControl('',Validators.compose([Validators.maxLength(20),Validators.pattern(/^\S*$/)])),
        eo_number: new FormControl('',Validators.compose([Validators.maxLength(20),Validators.pattern(/^\S*$/)])),
        manager_id: new FormControl('', Validators.compose([Validators.required])),
        colleagues: new FormControl(''),
        colleagues_list:this.fb.array([]),
        colleagues_list_name:this.fb.array([]),
        image_list:this.fb.array([]),
        image_list_value:this.fb.array([])
      });
  }

    //Bind campaign Data
  bindCampaignData = (campaignDetail):void => {
      this.campaignD = campaignDetail;
      this.campaignFormStepOne.get('campaign_title').setValue(this.campaignD.title);
      this.campaignFormStepOne.get('objective_type').setValue(this.campaignD.type);
      if(this.campaignD.type)
        this.campaignObjectiveChange({ target:{value:this.campaignD.type}});
      if(
        this.campaignD.type == 'post'
        ||
        this.campaignD.type == 'trending'
      ){
        if(this.campaignD.hashtag.length)
        {
          this.campaignFormStepOne.get('primary_hashtag').setValue('#'+this.campaignD.hashtag);
        }
        this.campaignFormStepOne.get('primary_hashtag_share').setValue(this.campaignD.hashtag_share);
        this.campaignFormStepOne.get('url').setValue(this.campaignD.url);
        this.campaignFormStepOne.get('url_share').setValue(this.campaignD.url_share);
        this.campaignFormStepOne.get('media_share').setValue(this.campaignD.media_share);

        if(
          this.campaignD.secondary_hashtag
            &&
          this.campaignD.secondary_hashtag.length > 0
        ){
          let secondryArray = this.campaignD.secondary_hashtag[0].split(",");
          const sTagData = <FormArray>this.campaignFormStepOne.controls['secondary_hashtag_list'];
          secondryArray.forEach(function (svalue) {
            sTagData.push(new FormControl(svalue));
          });
          this.campaignFormStepOne.get('secondary_hashtag_track')
          .setValue(this.campaignD.track_secondary_hashtag);
        }
        if(
          this.campaignD.keywords
          &&
          this.campaignD.keywords.length > 0
        ){
          let keywordsArray = this.campaignD.keywords[0].split(",");
          const kTagData = <FormArray>this.campaignFormStepOne.controls['keywords_list'];
          keywordsArray.forEach(function (kvalue) {
            kTagData.push(new FormControl(kvalue));
          });
          this.campaignFormStepOne.get('keywords_share').setValue(this.campaignD.keywords_share);
          this.campaignFormStepOne.get('keywords_track').setValue(this.campaignD.track_keywords);
        }
      }
      if(this.campaignD.type == 'amplification'){
        if(
          this.campaignD.retweet_url
          &&
          this.campaignD.retweet_url.length > 0
        ){
          let rArray = this.campaignD.retweet_url[0].split(",");
          const rUrl = <FormArray>this.campaignFormStepOne.controls['rtUrl_list'];
          rArray.forEach(function (kvalue) {
            rUrl.push(new FormControl(kvalue));
          });
        }
      }
      if(this.campaignD.advertiser_type == 'agency'){
        this.campaignFormStepOne.get('agency_id').setValue(this.campaignD.organization_id);
        this.campaignFormStepOne.get('advertiser_id').setValue(this.campaignD.advertiser_id);
        this.campaignFormStepOne.get('brand_id').setValue(this.campaignD.advertiser_brand_id);
      }
      if(this.campaignD.advertiser_type == 'brand'){
        this.campaignFormStepOne.get('brand_id_brand').setValue(this.campaignD.organization_id);
        this.campaignFormStepOne.get('advertiser_id_brand').setValue(this.campaignD.advertiser_id);
      }
      this.campaignFormStepOne.get('advertiser_type').setValue(this.campaignD.advertiser_type);
      this.campaignFormStepOne.get('ro_number').setValue(this.campaignD.ro_number);
      this.campaignFormStepOne.get('eo_number').setValue(this.campaignD.eo_number);
      this.campaignFormStepOne.get('manager_id').setValue(this.campaignD.manager_id);
        if(
          this.campaignD.colleagues
          &&
          this.campaignD.colleagues.length > 0
        ){
          let cArray = this.campaignD.colleagues[0].split(",");
          const colleagues = <FormArray>this.campaignFormStepOne.controls['colleagues_list'];
          const credsName = <FormArray>this.campaignFormStepOne.controls['colleagues_list_name'];
          cArray.forEach(function (kvalue) {
            colleagues.push(new FormControl(kvalue));
            credsName.push(new FormControl(kvalue));
          });
        }
        if(
          this.campaignD.images
          &&
          this.campaignD.images.length > 0
        ){
          const { images } = this.campaignD;
          images.map(async (image) => {
            const IData = <FormArray>this.campaignFormStepOne.controls['image_list'];
            this.ImageData = <FormArray>this.campaignFormStepOne.controls['image_list_value'];
            await this.URLtoImage(image.url, dataUrl => {
              IData.push(
                this.fb.group({name:new FormControl(dataUrl),imageId:new FormControl(image._id)})
              );
            })
          })
        }
  }

    //Campaign Status change
  campaignStatusChange = (): any => {
      let status = '8';
      let reqData =
      {
        "admin_id": this.getAdminData(),
        "campaign_id": this.campaignId,
        "status": status
      }
      this.apiService.postApiCommon('/campaign/edit/status', reqData)
      .subscribe((response: any) =>
      {
        if (response.status === 200) {
          return true;
        }
      });
      return true;
  };

  URLtoImage = async(url, callback) => {
      var xhr = new XMLHttpRequest();
      xhr.onload = function() {
        var reader = new FileReader();
        reader.onloadend = function() {
          callback(reader.result);
        }
        reader.readAsDataURL(xhr.response);
      };
      xhr.open('GET', url);
      xhr.responseType = 'blob';
      xhr.send();
  }

  //Check Primary hashTag
  checkPrimaryHashTag = event => {
    if(
      event.target.value.startsWith("#")
       ||
      event.target.value.startsWith("_")
    ){
     var eventValue = event.target.value.substring(1);
    }
    const sDuplicate = this.campaignFormStepOne.get('secondary_hashtag_list').value;
    if(sDuplicate.filter(x => x == eventValue) != ""){
      this.apiService.error('Primary & secondary hashtags cannot be same');
      this.campaignFormStepOne.get('primary_hashtag').reset();
    }
  };

  //Add Secondry tag in form array data
  addSecondaryTagFormArray = ():any => {
    let secData = this.campaignFormStepOne.get('secondary_hashtag').value;
    let preData = this.campaignFormStepOne.get('primary_hashtag').value;
    if(
      secData.startsWith("#")
       ||
      secData.startsWith("_")
    ){
      secData = secData.substring(1);
    }
    if(
    preData.startsWith("#")
     ||
    preData.startsWith("_")
    ){
      preData = preData.substring(1);
    }
    if(secData != preData){
      const sTagData = <FormArray>this.campaignFormStepOne.controls['secondary_hashtag_list'];
      const sDuplicate = this.campaignFormStepOne.get('secondary_hashtag_list').value;
      if(sDuplicate.filter(x => x == secData) == ""){
        if(sDuplicate.length < 5){
          if(this.campaignFormStepOne.get('secondary_hashtag').valid){
            sTagData.push(new FormControl(secData));
            this.campaignFormStepOne.get('secondary_hashtag').reset();
          }else{
            this.apiService.error('Please enter valid tag.');
            this.campaignFormStepOne.get('secondary_hashtag').reset();
          }
        }else{
          this.apiService.error('Maximum allowed five tags.');
          this.campaignFormStepOne.get('secondary_hashtag').reset();
        }
      } else {
        this.apiService.error('Cannot enter same hashtag twice');
        this.campaignFormStepOne.get('secondary_hashtag').reset();
      }
    }else{
      this.apiService.error('Primary & secondary hashtags cannot be same');
      this.campaignFormStepOne.get('secondary_hashtag').reset();
    }
  }

  //Remove Secondry Aarry in form data
  removeSecondaryTagFormArray = index => {
    const sTagData = <FormArray>this.campaignFormStepOne.controls['secondary_hashtag_list'];
    sTagData.removeAt(index);
  };

  //Add Keywords tag in form array data
  addKeywordsFormArray = event => {
    const kTagData = <FormArray>this.campaignFormStepOne.controls['keywords_list'];
    const sDuplicate = this.campaignFormStepOne.get('keywords_list').value;
    if (event.target.value.trim().length > 1) {
      if(sDuplicate.filter(x => x == event.target.value) == ""){
        if(sDuplicate.length < 5){
          if(this.campaignFormStepOne.get('keywords').valid){
            kTagData.push(new FormControl(this.campaignFormStepOne.get('keywords').value));
            this.campaignFormStepOne.get('keywords').reset();
          }else{
            this.apiService.error('Please enter valid keyword.');
            this.campaignFormStepOne.get('keywords').reset();
          }
        }else{
          this.apiService.error('Maximum allowed five keywords.');
          this.campaignFormStepOne.get('keywords').reset();
        }
      }
      else {
        this.apiService.error('Cannot enter same keyword twice');
        this.campaignFormStepOne.get('keywords').reset();
      }
    } else {
      this.apiService.error('keywords should contain atleast 2 characters except empty spaces');
      this.campaignFormStepOne.get('keywords').reset();
    }
  }

  //Remove Keywords Aarry in form data
  removeKeywordsFormArray = index => {
    const kTagData = <FormArray>this.campaignFormStepOne.controls['keywords_list'];
    kTagData.removeAt(index);
  };

  //Add RtUrl tag in form array data
  addRtUrlFormArray = event => {
    const kTagData = <FormArray>this.campaignFormStepOne.controls['rtUrl_list'];
    const sDuplicate = this.campaignFormStepOne.get('rtUrl_list').value;
    if(sDuplicate.length < 10){
      if(this.campaignFormStepOne.get('retweet_url').valid){
        if(sDuplicate.filter(x => x == event.target.value) == ""){
          kTagData.push(new FormControl(this.campaignFormStepOne.get('retweet_url').value));
          this.campaignFormStepOne.get('retweet_url').reset();
        }else{
          this.apiService.error('You have already provided this URL.');
          this.campaignFormStepOne.get('retweet_url').reset();
        }
      }else{
        this.apiService.error('Please enter a valid Twitter URL.');
        this.campaignFormStepOne.get('retweet_url').reset();
      }
    }else{
      this.apiService.error('Only 10 RT URLs can be added.');
      this.campaignFormStepOne.get('retweet_url').reset();
    }
  };

  //Remove RtUrl Aarry in form data
  removeRtUrlFormArray = index => {
    const kTagData = <FormArray>this.campaignFormStepOne.controls['rtUrl_list'];
    kTagData.removeAt(index);
  };

  // get advertiser list behalf agency select
  agencySelectChange = selectAgencyId => {
    if(selectAgencyId){
      this.campaignFormStepOne.get('advertiser_id').setValue('');
      this.agencyIdSelect = selectAgencyId;
      this.advertiserlist = [];
      this.getAdvertiserList();
    }
  }
  // Check advertiser
  checkAdvertiser = (event):void => {
    if(event == 'agency'){
      this.campaignFormStepOne.controls['agency_id'].enable();
      this.campaignFormStepOne.controls['advertiser_id'].enable();
      this.campaignFormStepOne.controls['brand_id'].enable();
      this.campaignFormStepOne.controls['brand_id_brand'].disable();
      this.campaignFormStepOne.get('brand_id_brand').setValue('');
      this.campaignFormStepOne.controls['advertiser_id_brand'].disable();
      this.campaignFormStepOne.get('advertiser_id_brand').setValue('');
    }
    if(event == 'brand'){
      this.campaignFormStepOne.controls['brand_id_brand'].enable();
      this.campaignFormStepOne.controls['advertiser_id_brand'].enable();
      this.campaignFormStepOne.controls['agency_id'].disable();
      this.campaignFormStepOne.get('agency_id').setValue('');
      this.campaignFormStepOne.controls['advertiser_id'].disable();
      this.campaignFormStepOne.get('advertiser_id').setValue('');
      this.campaignFormStepOne.controls['brand_id'].disable();
      this.campaignFormStepOne.get('brand_id').setValue('');
    }
    if(event == 'test'){
      this.campaignFormStepOne.controls['agency_id'].disable();
      this.campaignFormStepOne.controls['brand_id_brand'].disable();
      this.campaignFormStepOne.controls['advertiser_id_brand'].disable();
      this.campaignFormStepOne.controls['advertiser_id'].disable();
      this.campaignFormStepOne.controls['brand_id'].disable();
      this.campaignFormStepOne.get('brand_id_brand').setValue('');
      this.campaignFormStepOne.get('advertiser_id_brand').setValue('');
      this.campaignFormStepOne.get('agency_id').setValue('');
      this.campaignFormStepOne.get('advertiser_id').setValue('');
      this.campaignFormStepOne.get('brand_id').setValue('');
    }
  }

  // get advertiser list behalf agency select
  advertiserSelectChange = selectAdvertiserId => {
    if(selectAdvertiserId){
      this.campaignFormStepOne.get('brand_id').setValue('');
      this.advertiserIdSelect = selectAdvertiserId;
      if(this.advertiserDataList){
        let brandName = this.advertiserDataList.filter(list => list._id == selectAdvertiserId);
        this.advertiserIdSelect =  brandName[0].associated_brands;
      }
      this.brandlist = [];
      this.getBrandList();
    }
  }

  // get advertiser list behalf agency select
  dBrandSelectChange = selectDBrandId => {
    if(selectDBrandId){
      this.campaignFormStepOne.get('advertiser_id').setValue('');
      this.agencyIdSelect = selectDBrandId;
      this.advertiserlist = [];
      this.getAdvertiserList();
    }
  }

  //Add Keywords tag in form array data
  addcolleaguesFormArray = (event):any => {
    if(this.ManagerValueAndNameList){
      this.listNameManager = this.ManagerValueAndNameList.filter(list => list._id == event);
    }
    const mData = <FormArray>this.campaignFormStepOne.controls['colleagues_list'];
    const credsName = <FormArray>this.campaignFormStepOne.controls['colleagues_list_name'];
    const sDuplicate = this.campaignFormStepOne.get('colleagues_list').value;
    if(sDuplicate.filter(x => x == event) == ""){
      if(sDuplicate.length < 5){
        if(this.campaignFormStepOne.get('colleagues').valid){
          mData.push(new FormControl(event));
          credsName.push(new FormControl(this.listNameManager[0].name));
        }else{
          this.apiService.error('Please select valid colleagues.');
          this.campaignFormStepOne.get('colleagues').reset();
          return;
        }
      }else{
        this.apiService.error('Only five colleagues allowed.');
        this.campaignFormStepOne.get('colleagues').reset();
        return;
      }
    }else{
      this.apiService.error('this colleagues Already added.');
      this.campaignFormStepOne.get('colleagues').reset();
      return;
    }
  }

  //Remove Keywords Aarry in form data
  removeColleaguesFormArray = index => {
      const mData = <FormArray>this.campaignFormStepOne.controls['colleagues_list'];
      const credsName = <FormArray>this.campaignFormStepOne.controls['colleagues_list_name'];
      const sDuplicate = this.campaignFormStepOne.get('colleagues_list').value;
      mData.removeAt(index);
      credsName.removeAt(index);
      if(sDuplicate.length == 0){
        mData.removeAt(0);
        credsName.removeAt(0);
      }
      this.campaignFormStepOne.get('colleagues').reset();
  }

  // Agency List Api
  getAgencyList = ():void => {
    this.agencylist = [];
      let reqData = {
        "type":'agency'
      }
      this.apiService.postApiCommon('/campaign/searchagencylist',reqData)
      .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        this.agencyListData = this.data.result;
        if (this.data.responseCode == 200) {
          this.agencylist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.agencylist.push({
                id: this.data.result[i]._id, name: this.data.result[i].organisation_name
            })
          }
        } else {
          this.agencylist = [];
        }
      }
    }, error => {
      this.apiService.error('Something went wrong! Please try again.');
    });
  }

  // Agency List Api
  getAdvertiserList = ():void => {
    this.advertiserlist = [];
      let reqData = {
        "organisation_id": this.agencyIdSelect
      }
      this.apiService.postApiCommon('/campaign/searchadvertiserlist',reqData)
      .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.advertiserlist = [];
          this.advertiserDataList = this.data.result;
          for (let i = 0; i < this.data.result.length; i++) {
            this.advertiserlist.push({
              id: this.data.result[i]._id, name: this.data.result[i].name
            })
          }
        } else {
          this.advertiserlist = [];
        }
      }
    }, error => {
      this.apiService.error('Something went wrong! Please try again.');
    });
  }

  // Brand List Api
  getBrandList = ():void => {
    this.brandlist = [];
      let reqData = {
        "associated_brands": this.advertiserIdSelect
      }
      this.apiService.postApiCommon('/campaign/searchbrandlist',reqData)
      .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.brandlist = [];
          this.brandDataList = this.data.result;
          for (let i = 0; i < this.data.result.length; i++) {
            this.brandlist.push({
              id: this.data.result[i]._id, name: this.data.result[i].organisation_name
            })
          }
        } else {
          this.brandlist = [];
        }
      }
    }, error => {
      this.apiService.error('Something went wrong! Please try again.');
    });
  }

  // Direct brand List Api
  getDirectBrancdList = ():void => {
    this.dBrandlist = [];
      let reqData = {
        "type":'brand'
      }
      this.apiService.postApiCommon('/campaign/searchagencylist',reqData)
      .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.dBrandlist = [];
          this.directBrandDataList = this.data.result;
          for (let i = 0; i < this.data.result.length; i++) {
            this.dBrandlist.push({
              id: this.data.result[i]._id, name: this.data.result[i].organisation_name
            })
          }
        } else {
          this.dBrandlist = [];
        }
      }
    }, error => {
      this.apiService.error('Something went wrong! Please try again.');
    });
  }

  // Manager List Api
  getManagerList = ():void =>{
    this.managerlist = [];
    let dataAdmin = this.auth.getData();
      let reqData = {
        "admin_id":dataAdmin._id
      }
      this.apiService.postApiCommon('/campaign/getmanagerlist',reqData)
      .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.managerlist = [];
          this.ManagerValueAndNameList = this.data.result;
          const { result } = this.data;
           this.managerlist = result.map(obj => ({
             id: obj._id || "",
             name: obj.name || "",
             email: obj.email || "",
             src: obj.profile_image || ""
           }));
        } else {
          this.managerlist = [];
        }
      }
    }, error => {
      this.apiService.error('Something went wrong! Please try again.');
    });
  }
  //Drag image or video functionality
  dragOverHandler = ev => {
    ev.currentTarget.setAttribute(
      "style", "border: 2px dashed #2196f3"
    );
    ev.preventDefault();
  }
  //drop image or video functionality
  dropHandler = ev => {
    ev.currentTarget.setAttribute(
      "style", "border: 1px solid #ddd"
    );
    ev.preventDefault();
    if (ev.dataTransfer.items) {
      for (let i = 0; i < ev.dataTransfer.items.length; i++) {
        if (ev.dataTransfer.items[i].kind === 'file') {
          let file = ev.dataTransfer.items[i].getAsFile();
          let { name } = file;
          if(name.match(/[^/]+(jpg|png|gif|PNG|JPG|jpeg|GIF)$/)){
            // process image
            this.uploadImages(file);
          } else {
            if(name.match(/[^/]+(mp4|3gp|MP4|mkv|MKV|avi)$/)){
              //process video
              this.uploadVideo(file);
            }
          }
        }
      }
    } else {
      // Use DataTransfer interface to access the file(s)
      for (var i = 0; i < ev.dataTransfer.files.length; i++) {
        console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
      }
    }
  }

  //upload Image from input
  uploadImageFromInput = $event => {
    let file = $event.target.files[0];
    if(file.name.match(/[^/]+(jpg|png|gif|PNG|JPG|jpeg|GIF)$/)){
      this.uploadImages(file);
    }else{
      this.apiService.error("Upload only image formats (PNG, JPG, JPEG, GIF).");
      this.campaignFormStepOne.get('image').reset();
      return;
    }
  }

  //upload Video from input
  uploadVideoFromInput = $event => {
    let file = $event.target.files[0];
    if(file.name.match(/[^/]+(mp4|3gp|MP4|mkv|MKV|avi)$/)){
      this.uploadVideo(file);
    }else{
      this.apiService.error("Upload only video formats (MP4 3GP MKV AVI).");
      this.campaignFormStepOne.get('video').reset();
      return;
    }
  }

  //Upload image function
  uploadImages = file => {
    let SelectPhoto = <File>file;
    let name = SelectPhoto.name;
    let size = SelectPhoto.size/ 1024 / 1024;
    if(size > 15){
      this.apiService.error("File is too large. Upload file less than 15 MB.");
      this.campaignFormStepOne.get('image').reset();
      return;
    }
    if(this.videoData){
      this.clearAllMedia();
    }
    this.validPhoto = (
      new RegExp('(' +this.exts.join('|').replace(/\./g, '\\.') + ')$')).test(SelectPhoto.name
    );
    if(!this.validPhoto){
      this.apiService.error("Upload only image formats (PNG, JPG, JPEG, GIF).");
      this.campaignFormStepOne.get('image').reset();
      return;
    }else{
      this.url = "";
      const reader = new FileReader();
      reader.readAsDataURL(file); // read file as data url
      reader.onload = (event:any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
        if(this.url){
          const IData = <FormArray>this.campaignFormStepOne.controls['image_list'];
          this.ImageData = <FormArray>this.campaignFormStepOne.controls['image_list_value'];
          const sDuplicate = this.campaignFormStepOne.get('image_list').value;
          const DimageData = this.campaignFormStepOne.get('image_list_value').value;
          if(DimageData.filter(x => x.name == name) == ""){
            if(sDuplicate.length <= 50){
              if(this.campaignFormStepOne.get('image').valid){
                IData.push(this.fb.group({name:new FormControl(this.url)}));
                //Image upload function
                const formImage = new FormData();
                formImage.append('imageObject', SelectPhoto, SelectPhoto.name);
                formImage.append('status', '1',);
                this.apiService.postFormApi('/campaign/uploadImage',formImage)
                .subscribe((responseLink: any) => {
                  if(responseLink.status === 200){
                    this.ImageData.push(new FormControl(responseLink.body.imageLink));
                    this.campaignFormStepOne.get('image').reset();
                  }
                 });
              }else{
                this.apiService.error('Please upload valid image file.');
                this.campaignFormStepOne.get('image').reset();return;
              }
            }else{
              this.apiService.error('Maximum of 50 images can be uploaded');
              this.campaignFormStepOne.get('image').reset();return;
            }
          } else{
            this.apiService.error('This image already uploaded.');
            this.campaignFormStepOne.get('image').reset();return;
          }
        }
      }
      this.imageUploadSuccess = true;
    }
  }

  //Upload video function
   uploadVideo = async (file) => {
    if(this.videoData){
      this.apiService.error('Only one video can be uploaded.');
      this.campaignFormStepOne.get('video').reset();
      return;
    }
    const IData = <FormArray>this.campaignFormStepOne.controls['image_list'];
    if(IData.length > 0){
      this.clearAllMedia();
    }
    this.SelectVideo = <File>file;
    let name = this.SelectVideo.name;
    let size = this.SelectVideo.size/ 1024 / 1024;

    if(size > 20){
      this.apiService.error("File is too large. Upload file less than 20 MB.");
      this.campaignFormStepOne.get('video').reset();
      return;
    }
    const videoImage = new FormData();
    videoImage.append('videoObject', this.SelectVideo, this.SelectVideo.name);
    videoImage.append('status', '2',);
    await this.apiService.postFormApi('/campaign/uploadImage',videoImage)
      .subscribe(async (responseLink: any) => {
        if(responseLink.status === 200){
          this.selectVideoLink = responseLink.body.videoLink;
        }
      }
    );
    this.videoData = await this.apiService.generateThumbnail(this.SelectVideo);
    this.videoUploadSuccess = true;
    this.campaignFormStepOne.get('video').reset();
  }

  //Remove Image Aarry in form data
  removeImageFormArray = (index, imageId = null):void => {
    if(imageId){
      this.deleteImageEditList(imageId);
    }
    const IData = <FormArray>this.campaignFormStepOne.controls['image_list'];
    const ImageData = <FormArray>this.campaignFormStepOne.controls['image_list_value'];
    IData.removeAt(index);
    ImageData.removeAt(index);
    this.campaignFormStepOne.get('image').reset();
    this.apiService.success('Attachment removed');
  }

  //Remove Video
  removeVideo = ():void => {
    this.videoData = "";
    this.SelectVideo = null;
    this.apiService.success('Attachment removed');
  }

  // Clear all media data
  clearAllMedia = ():void => {
    const IData = <FormArray>this.campaignFormStepOne.controls['image_list'];
    const ImageData = <FormArray>this.campaignFormStepOne.controls['image_list_value'];
    if(this.videoData || IData.length !==0)
      this.apiService.success('Attachments removed');
    while (IData.length !== 0) {
      IData.removeAt(0)
    }
    while (ImageData.length !== 0) {
      ImageData.removeAt(0)
    }
    this.videoData = "";
    this.SelectVideo = null;
  }

  //Change campaign objective selection
  campaignObjectiveChange = event => {
    if(event){
      //this.apiService.success('Form elements are now shown as per the objective selected');
      if(event.target.value == 'amplification'){
        this.show_div_rt_url = true;
        this.show_div_objective_change = false;
      }else{
        this.show_div_rt_url = false;
        this.show_div_objective_change = true;
      }
    }
  }

  //Submit function for campaign
  campaignOneFormSubmit = (reqData , statusType) => {
    if(statusType == '1'){
      this.campaignFormStepOne.get('objective_type').setValidators(null);
      this.campaignFormStepOne.get('objective_type').updateValueAndValidity();
      this.campaignFormStepOne.get('advertiser_type').setValidators(null);
      this.campaignFormStepOne.get('advertiser_type').updateValueAndValidity();
      this.campaignFormStepOne.get('manager_id').setValidators(null);
      this.campaignFormStepOne.get('manager_id').updateValueAndValidity();
    }else{
      this.campaignFormStepOne.get('objective_type').setValidators([Validators.required]);
      this.campaignFormStepOne.get('objective_type').updateValueAndValidity();
      this.campaignFormStepOne.get('advertiser_type').setValidators([Validators.required]);
      this.campaignFormStepOne.get('advertiser_type').updateValueAndValidity();
      this.campaignFormStepOne.get('manager_id').setValidators([Validators.required]);
      this.campaignFormStepOne.get('manager_id').updateValueAndValidity();
    }
    if(this.campaignFormStepOne.invalid){
      const FirstInvalid = this.el.nativeElement.querySelectorAll('.ng-invalid')[1];
      FirstInvalid.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'start' });
      return;
    }
      const form = new FormData();
      form.append('campaign_title', reqData.campaign_title);
      form.append('objective_type', reqData.objective_type);
    if(reqData.objective_type == 'amplification'){
      form.append('primary_hashtag', "");
      form.append('primary_hashtag_share',"");
      form.append('url', "");
      form.append('url_share', "");
      form.append('secondary_hashtag', "");
      form.append('secondary_hashtag_track', "");
      form.append('keywords', "");
      form.append('keywords_share', "");
      form.append('keywords_track', "");
      form.append('image', "");
      form.append('video', "");
      form.append('media_share', "");
      form.append('retweet_url', reqData.rtUrl_list);
    }else{
      if(
        reqData.primary_hashtag.startsWith("#")
         ||
        reqData.primary_hashtag.startsWith("_")
      ){
        reqData.primary_hashtag = reqData.primary_hashtag.substring(1);
      }
      form.append('primary_hashtag',reqData.primary_hashtag);
      form.append('primary_hashtag_share', reqData.primary_hashtag ? reqData.primary_hashtag_share: false);
      form.append('url', reqData.url);
      form.append('url_share', reqData.url ? reqData.url_share : false);
      form.append('secondary_hashtag', reqData.secondary_hashtag_list);
      form.append('secondary_hashtag_track', reqData.secondary_hashtag_list.length ? reqData.secondary_hashtag_track:false);
      form.append('keywords', reqData.keywords_list);
      form.append('keywords_share', reqData.keywords_list.length ? reqData.keywords_share:false);
      form.append('keywords_track', reqData.keywords_list.length ? reqData.keywords_track:false);
      form.append('retweet_url', reqData.rtUrl_list);
      if(this.imageUploadSuccess == true){
        let ImageData = this.ImageData.value;
        for(let i=0; i < ImageData.length; i++){
          form.append('image', ImageData[i]);
        }
      }
      if(this.videoUploadSuccess == true){
        form.append('video', this.selectVideoLink);
      }
      form.append('media_share', (this.imageUploadSuccess || this.videoUploadSuccess ) ? reqData.media_share : false);
    }
    form.append('advertiser_type', reqData.advertiser_type);
    if(
      reqData.advertiser_type
       &&
      reqData.advertiser_type == 'agency'
    ){
    if(!reqData.agency_id || !reqData.advertiser_id )
      return this.apiService.error('Please fill Agency details.');
    form.append('agency_id', reqData.agency_id);
    form.append('brand_id', reqData.brand_id);
    form.append('advertiser_id', reqData.advertiser_id);

    if(this.agencyListData){
      let agencyName = this.agencyListData.filter(list => list._id == reqData.agency_id);
      form.append('agency_name', agencyName[0].organisation_name);
    }

    if(this.advertiserDataList){
      let advertiserName = this.advertiserDataList.filter(list => list._id == reqData.advertiser_id);
      form.append('advertiser_name', advertiserName[0].name);
    }
    if(this.brandDataList && reqData.brand_id){
      let brandName = this.brandDataList.filter(list => list._id == reqData.brand_id);
      form.append('brand_name', brandName[0].organisation_name);
    }else{
      form.append('brand_name',"");
    }

    }else if(reqData.advertiser_type && reqData.advertiser_type == 'brand'){
      if(!reqData.brand_id_brand || !reqData.advertiser_id_brand )
        return this.apiService.error('Please fill Brand details.');
      form.append('agency_id', reqData.brand_id_brand);
      form.append('advertiser_id', reqData.advertiser_id_brand);
      form.append('brand_id', "");

      if(this.advertiserDataList){
        let advertiserName = this.advertiserDataList.filter(list => list._id == reqData.advertiser_id_brand);
        form.append('advertiser_name', advertiserName[0].name);
      }

      if(this.directBrandDataList){
      let brandName = this.directBrandDataList.filter(list => list._id == reqData.brand_id_brand);
        form.append('agency_name', brandName[0].organisation_name);
      }
    }else{
      form.append('agency_id', "");
      form.append('advertiser_id', "");
      form.append('brand_id', "");
    }
    form.append('ro_number', reqData.ro_number);
    form.append('eo_number', reqData.eo_number);
    form.append('manager_id', reqData.manager_id);
    form.append('colleagues', reqData.colleagues_list_name);
    form.append('status', statusType);
    if(
      this.campaignId
       &&
      this.campaignId != ""
       &&
      this.campaignId != undefined
    ){
      form.append('campaign_id', this.campaignId);
    }else{
      form.append('campaign_id', "");
    }
    this.apiService.postFormApiWithHeader('/campaign/create_campaign',form)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          let campaignId = this.data.result._id;
          if(this.data.result.status === '0'){
            this.apiService.success(this.data.responseMessage);
            this.router.navigate(["campaign/list/drafts"]);
          }else{
            this.router.navigate(['campaign/step-two/',campaignId]);
          }
        } else {
          this.apiService.error(this.data.responseMessage);
        }
      }
    }, error => {
      this.apiService.error('Something went wrong! Please try again.');
    });

  }
  //Get Upper case name
  getNameInitials = ({ name }) => {
    if (name) {
      const [ firstName, lastName = "" ] = name.split(" ");
      return (
        firstName[0].toUpperCase()
        +
        (lastName ? lastName[0].toUpperCase() : "")
      );
    }
    return "";
  };

  //Delete campaigns
  discardSubmit = ():void => {
    if(this.campaignId) {
      let reqData =
      {
        "admin_id": this.getAdminData(),
        "campaign_id": this.campaignId
      }
      this.apiService.postApiCommon('/campaign/discard', reqData)
      .subscribe((response: any) => {
        if (response.status == 200) {
          this.data = response.body;
          if (this.data.responseCode = 200) {
            this.apiService.success(this.data.responseMessage);
            this.router.navigate(["campaign/list/active"]);
          } else {
            this.apiService.error(this.data.responseMessage);
          }
        }
      }, error => {
        this.apiService.error("Something went wrong! Please try again.");
      });
    } else {
      this.apiService.success('Campaign Discarded!');
      this.router.navigate(["campaign/list/active"]);
    }
  }

  //Delete image on list
  deleteImageEditList = imageId => {
     let reqData = {
       "admin_id": this.getAdminData(),
        "campaign_id": this.campaignId,
        "image_id": imageId
      }
      this.apiService.postApiCommon('/campaign/image/delete',reqData)
      .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          return true;
        } else {
          this.apiService.error('Image not found.');
        }
      }
    }, error => {
      this.apiService.error('Something went wrong! Please try again.');
    });
  }
}
