import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatMenuModule,
  MatIconModule,
  MatSelectModule,
  MatRadioModule,
  MatExpansionModule,
  MatTabsModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MatNativeDateModule } from "@angular/material/core";
import { AngularEditorModule } from "@kolkov/angular-editor";
import { Ng2SearchPipeModule } from "ng2-search-filter";
import { NgxFileDropModule } from "ngx-file-drop";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { Ng2ImgMaxModule } from "ng2-img-max";
import { Ng5SliderModule } from "ng5-slider";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgxDaterangepickerMd } from "ngx-daterangepicker-material";

import { CommonFileModule } from "../common-file/common-file.module";
import { CampaignRoutingModule } from "./campaign-routing.module";
import { CreateCampaignStepOneComponent } from "./create-campaign-step-one/create-campaign-step-one.component";
import { CampaignLayoutComponent } from "./campaign-layout/campaign-layout.component";
import { StepTwoSuccessComponent } from "./step-two-success/step-two-success.component";
import { MicroComponent } from "./micro/micro.component";
import { PremiumComponent } from "./premium/premium.component";
import { CreateCampaignStepThreeComponent } from "./create-campaign-step-three/create-campaign-step-three.component";
import { ManageKolComponent } from "./manage-kol/manage-kol.component";
import { KolListItemComponent } from "./manage-kol/kol-list-item/kol-list-item.component";
import { CampaignListComponent } from "./list/list.component";
import { ListItemComponent } from "./list/list-item/list-item.component";
import { SkeletonComponent } from './step-two-success/skeleton/skeleton.component';
// import { CapitalizeFirstLetter } from "../pipes/capitalize-first-letter";

@NgModule({
  declarations: [
    CreateCampaignStepOneComponent,
    CampaignLayoutComponent,
    StepTwoSuccessComponent,
    MicroComponent,
    PremiumComponent,
    CreateCampaignStepThreeComponent,
    ManageKolComponent,
    KolListItemComponent,
    CampaignListComponent,
    ListItemComponent,
    SkeletonComponent,
    // CapitalizeFirstLetter
  ],
  imports: [
    CommonModule,
    CommonFileModule,
    ReactiveFormsModule,
    FormsModule,
    CampaignRoutingModule,
    Ng2ImgMaxModule,
    MatNativeDateModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatSelectModule,
    MatRadioModule,
    Ng5SliderModule,
    MatExpansionModule,
    MatTabsModule,
    NgSelectModule,
    NgxDaterangepickerMd.forRoot(),
    InfiniteScrollModule,
    Ng2SearchPipeModule,
    AngularEditorModule,
    NgxFileDropModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CampaignModule { }
