import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { AdminService, AuthService } from 'src/app/services';

@Component({
  selector: "app-step-two-success",
  templateUrl: "./step-two-success.component.html",
})
export class StepTwoSuccessComponent implements OnInit {
  message: any;
  campaignId: any;
  data: any;
  campaignDeatilData: any = [];
  campaignPlatformDeatilData: any;
  show_div_selected_platform: boolean = false;
  show_div_platform: boolean = true;
  show_confirmation_model: boolean = false;
  twitterMicro: any = [];
  twitterPremimum: any = [];
  instagramPremimum: any = [];
  instagramMicro: any = [];
  youtubePremimum: any = [];
  youtubeMicro: any = [];
  facebookPremimum: any = [];
  facebookMicro: any = [];
  platformIdData: any;
  platformData: any;
  platformTypeData: any;
  showDemoDataOnLoad: boolean = true;
  adminData: any = [];

  constructor(
    private router: Router,
    private auth: AuthService,
    private _service: AdminService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.campaignId = params.id;
    });
    this.campaignDetails();
  }

  getAdminData = ():any => {
    if(!this.adminData.length)
      this.adminData = this.auth.getData();
    return this.adminData._id;
  }
  //Get the campaign step 2 details data
  campaignDetails = () => {
    let reqData =
    {
      "campaign_id": this.campaignId,
      "admin_id": this.getAdminData()
    }
    this.twitterMicro = [];
    this.twitterPremimum = [];
    this.instagramPremimum = [];
    this.instagramMicro = [];
    this.youtubePremimum = [];
    this.youtubeMicro = [];
    this.facebookPremimum = [];
    this.facebookMicro = [];
    this.showDemoDataOnLoad = !this.showDemoDataOnLoad;
    this._service.postApiCommon('/campaign/details', reqData)
    .subscribe((response: any) => {
      if (response.status == 200) {
       this.showDemoDataOnLoad = !this.showDemoDataOnLoad;
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.campaignDeatilData = this.data.result.campaign;
          this.campaignPlatformDeatilData = this.campaignDeatilData.platforms;
          if (
            this.campaignPlatformDeatilData
             &&
            this.campaignPlatformDeatilData.length > 0
          )this.show_div_selected_platform = true;
          if (
            this.campaignDeatilData.type == 'trending'
             ||
            this.campaignDeatilData.type == 'amplification'
            )this.show_div_platform = false;
          else
            this.show_div_platform = true;
          if (
            this.campaignPlatformDeatilData
             &&
            this.campaignPlatformDeatilData.length > 0
          ){
            for (let i = 0; i < this.campaignPlatformDeatilData.length; i++) {
              if (this.campaignPlatformDeatilData[i].name == 'facebook') {
                if (this.campaignPlatformDeatilData[i].type == 'micro')
                  this.facebookMicro = this.campaignPlatformDeatilData[i];
                else
                  this.facebookPremimum = this.campaignPlatformDeatilData[i];
              }
              if (this.campaignPlatformDeatilData[i].name == 'twitter') {
                if (this.campaignPlatformDeatilData[i].type == 'micro')
                  this.twitterMicro = this.campaignPlatformDeatilData[i];
                else
                  this.twitterPremimum = this.campaignPlatformDeatilData[i];
              }
              if (this.campaignPlatformDeatilData[i].name == 'youtube') {
                if (this.campaignPlatformDeatilData[i].type == 'micro')
                  this.youtubeMicro = this.campaignPlatformDeatilData[i];
                else
                  this.youtubePremimum = this.campaignPlatformDeatilData[i];
              }
              if (this.campaignPlatformDeatilData[i].name == 'instagram') {
                if (this.campaignPlatformDeatilData[i].type == 'micro')
                  this.instagramMicro = this.campaignPlatformDeatilData[i];
                else
                  this.instagramPremimum = this.campaignPlatformDeatilData[i];
              }
            }
          }
        }
      }
    }, error => {
      this.showDemoDataOnLoad = !this.showDemoDataOnLoad;
      this._service.error("Something went wrong! Please try again.");
    });
  }

  //Delete campaign all data
  discardSubmit = () => {
    let reqData =
    {
      "admin_id": this.getAdminData(),
      "campaign_id": this.campaignId
    }
    this._service.postApiCommon('/campaign/discard', reqData)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode = 200) {
          this._service.success(this.data.responseMessage);
          this.router.navigate(["campaign/list/active"]);
        } else {
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  // Close modal function
  confirmationClose = ():void => {
    this.show_confirmation_model = false;
  }
  //Confirmation modal open on platform delete
  platformDelete = (platform_id, platform, type):void => {
    this.platformIdData = platform_id;
    this.platformData = platform;
    this.platformTypeData = type;
    this.show_confirmation_model = true;
  }

  //Delete selected platform
  platformDeleteConfirmation = () => {
    let reqData =
    {
      "admin_id": this.getAdminData(),
      "campaign_id": this.campaignId,
      "platform_id": this.platformIdData,
      "platform": this.platformData,
      "type": this.platformTypeData
    }
    this._service.postApiCommon('/campaign/platform/delete', reqData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this._service.success(this.data.responseMessage);
          this.show_confirmation_model = false;
          this.campaignDetails();
        } else {
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this.message = "Something went wrong! Please try again.";
    });
  }

  //Save draft change only status
  saveDraft = () => {
    let status = '0';
    let reqData =
    {
      "admin_id": this.getAdminData(),
      "campaign_id": this.campaignId,
      "status": status
    }
    this._service.postApiCommon('/campaign/edit/status', reqData)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this._service.success(this.data.responseMessage);
          this.router.navigate(["campaign/list/drafts"]);
        } else {
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  saveSubmit = () => {
    if (this.campaignPlatformDeatilData && this.campaignPlatformDeatilData.length > 0) {
      this.router.navigate(['campaign/step-three/',this.campaignId]);
    }else{
      this._service.error('Please select platform first.');
      return;
    }
  }
 //Save live change only
  saveLiveSubmit = ():void => {
    this._service.success('Changes save successfully.');
    this.router.navigate(["campaign/list/active"]);
  }

}
