import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-skeleton',
  templateUrl: './skeleton.component.html',
})
export class SkeletonComponent implements OnInit {
  @Input() skeletonLoader;
  constructor() { }

  ngOnInit() {
  }

}
