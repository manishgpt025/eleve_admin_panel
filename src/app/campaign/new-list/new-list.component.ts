// import { Component, OnInit, ElementRef } from '@angular/core';
// import { ActivatedRoute, Router } from '@angular/router';
// import { FormGroup, Validators, FormControl, FormGroupDirective } from '@angular/forms';
// import { MatSelect, MatSelectChange } from '@angular/material';
// import { Ng2ImgMaxService } from "node_modules/ng2-img-max";
// import * as moment from "moment";
// import _ from "lodash";

// import { AdminService, AuthService } from 'src/app/services';
// import { ifStatusOkay, getErrorMessage, getResult } from "../../handlers";


// @Component({
//   selector: 'app-new-list',
//   templateUrl: './new-list.component.html',
// })
// export class NewListComponent implements OnInit {
//   pageNumber: number=1;
//   searchData: any;
//   statusFilter: any;
//   sortAdmin: any;
//   locationFilter: any;
//   respAdmin: any;
//   managelistData: any;
//   create_mobile_code: string;
//   message: any;
//   campaignlistData: any = [];
//   campaign_id: any;
//   show_active_model: boolean;
//   showEndDateModal: boolean;
//   showStopCampaignModal: boolean;
//   campaignStartDate: string;
//   searchDataValue: string;
//   endDateValue: any;
//   searchSymbolValue: boolean = true;
//   throttle = 50;
//   scrollDistance = 2;
//   scrollUpDistance = 2;

//   loading: boolean = true;
//   searchKeywordValue: string = "";
//   campaignStatus: string = "";
//   pageTitle: string = "";
//   routes: Array<any> = [
//     { label: "active", value: "1"},
//     { label: "drafts", value: "0"},
//     { label: "completed", value: "6"},
//   ];
//   filters: any = {
//     keyword: "",
//     platform: "",
//   };
//   pageDetails: any = {
//     page: 1,
//     pages: 1
//   };
//   data: any = {
//     list: []
//   };
//   debounceGetListEvent = _.debounce(() => {
//     this.getCampaignList();
//   }, 500)


//   constructor(
//     private authService: AuthService,
//     private router: Router,
//     private apiService: AdminService,
//     private el: ElementRef,
//     private ng2ImgMaxService: Ng2ImgMaxService,
//     private activatedRoute: ActivatedRoute
//   ) { }

//   ngOnInit() {
//     const currentRoute = this.getRoute() || "";
//     const existingRouteObject = this.routes.find(
//       ele => ele.label === currentRoute
//     );
//     if (existingRouteObject !== undefined) {
//       this.pageTitle = currentRoute.slice(0, 1).toUpperCase() + currentRoute.slice(1);
//       this.campaignStatus = existingRouteObject.value;
//       this.getCampaignList();
//     } else this.router.navigate([""]);
//   }

//   getRoute = (): string => {
//     let action = "";
//     this.activatedRoute.params
//       .subscribe(params => {
//         action = params.action;
//       }
//     );
//     return action;
//   };

//   getAdminId = (): string => {
//     return this.authService.getData()._id || "";
//   };

//   getCampaignList = (): void => {
//     const { platform, keyword,} = this.filters;
//     const { page: pageNumber } = this.pageDetails;
//     let params = {
//       admin_id: this.getAdminId(),
//       status: this.campaignStatus,
//       keyword,
//       platform,
//       pageNumber
//     };
//     this.apiService.postApiCommon('/campaign/allcampaignlist', params)
//       .subscribe((response: any) => {
//         if (ifStatusOkay(response)) {
//           const { docs: list, ...rest } = getResult(response);
//           this.setCampaignList(list);
//           this.setPageDetails(rest);
//         }
//       }, error => {
//         this.message = "Something went wrong! Please try again.";
//       }
//     );
//   };

//   setCampaignList = (
//     list: Array<any> = []
//   ): void => {
//     this.data = {
//       ...this.data,
//       list: [...this.data.list, ...list],
//     };
//     console.log("this.data", this.data);
//   };

//   setPageDetails = ({
//     page,
//     pages
//   }): void => {
//     this.pageDetails = {
//       page,
//       pages,
//     };
//     console.log("this.pageDetails", this.pageDetails);
//   };

//   onClickRefresh = (): void => {
//     this.filters = {
//       keyword: "",
//       platform: "",
//     };
//     this.pageDetails = {
//       page: 1,
//       pages: 1
//     };
//     this.data = {
//       list: []
//     };
//     this.getCampaignList();
//   };

//   onChangeSearchInput = ({ target: 
//     { value } 
//   }): void => {
//     const { keyword: currentKeyword } = this.filters;
//     if (currentKeyword !== value) {
//       this.filters = {
//         ...this.filters,
//         keyword: value
//       };
//       this.pageDetails = {
//         ...this.pageDetails,
//         page: 1
//       };
//       this.loading = true;
//       this.data = {
//         ...this.data,
//         list: []
//       };
//       this.debounceGetListEvent(); 
//     }
//   };

//   clearSearchInput = (): void => {
//     this.searchKeywordValue = "";
//     this.filters = {
//        ...this.filters,
//       keyword: ""
//     };
//     this.pageDetails = {
//       ...this.pageDetails,
//       page: 1
//     };
//     this.loading = true;
//     this.data = {
//       ...this.data,
//       list: []
//     };
//     this.getCampaignList();
//   };

//   // stopCampaign(campaign_id){
//   //   let data = this.auth.getData();
//   //   let params = {
//   //     campaign_id,
//   //     end_date: moment(Date.now()).format('MM/DD/YYYY HH:mm:ss'),
//   //     status: this.campaignStatus,
//   //     admin_id: data._id,
//   //   };
//   //   this._service.postApiWithHeader('/campaign/campaignstop', params, '3').subscribe((response: any) => {
//   //     if (response.status == 200) {
//   //       if(response.body.responseCode==200){
//   //         this._service.success('Campaign has Stopped!');
//   //         this.showStopCampaignModal = false;
//   //         this.getcampaignlist();
//   //       }
//   //     }
//   //   }, error => {
//   //     this._service.hide();
//   //     this._service.error('Something went wrong! Please try again.')
//   //   });
//   // }

//   // updateCampaignEndDate(campaign_id){
//   //   let { endDate, startDate } = this.endDateValue;
//   //   let params = {
//   //     campaign_id,
//   //     end_date: moment(endDate).format('MM/DD/YYYY HH:mm:ss'),
//   //     status: this.campaignStatus
//   //   };
//   //   if(this.validateCampaignDate(endDate)){
//   //     this._service.postApiWithHeader('/campaign/create_campaign', params, '3').subscribe((response: any) => {
//   //       if (response.status == 200) {
//   //         if(response.body.responseCode==200){
//   //           this._service.success('Campaign End Date Updated');
//   //           this.showEndDateModal = false;
//   //           this.getcampaignlist();
//   //         }
//   //       }
//   //     }, error => {
//   //       this._service.hide();
//   //       this._service.error('Something went wrong! Please try again.')
//   //     });
//   //   }
//   // }

//   // validateCampaignDate(end_date){
//   //   let startDate = this.campaignStartDate;
//   //   let endDate = moment(end_date).format('MM/DD/YYYY HH:mm:ss');
//   //   if(endDate < startDate ){
//   //     this._service.error('End Date Can not be less Start Date');
//   //     return false;
//   //   }
//   //   return true;
//   // }

//   // // Open confirm box
//   // campaignDiscard(campaign_id) {
//   //   this.campaign_id = campaign_id;
//   //   this.show_active_model = true;
//   // }

//   // // Open edit end date box
//   // editCampaignEndDate(campaign_id) {
//   //   this.campaign_id = campaign_id;
//   //   const campaign = this.campaignlistData.find(campaign => campaign._id === campaign_id);
//   //   this.campaignStartDate = campaign.start_date;
//   //   this.campaignStatus = campaign.status;
//   //   this.showEndDateModal = true;
//   // }

//   // openStopCampaignModal(campaign_id){
//   //   this.campaign_id = campaign_id;
//   //   const campaign = this.campaignlistData.find(campaign => campaign._id === campaign_id);
//   //   this.campaignStatus = campaign.status;
//   //   this.showStopCampaignModal = true;
//   // }

//   // // close edit end date modal
//   // discardEditCampaignModal() {
//   //   this.showEndDateModal = false;
//   // }

//   // // close stop campaign modal
//   // closeStopCampaignModal(){
//   //   this.showStopCampaignModal = false;
//   // }

//   // // Close confirm box
//   // campaignDiscardClose() {
//   //   // this.getDraftCampaignlist();
//   //   this.show_active_model = false;
//   // }

//   // // Delete campaign all data
//   // discardSubmit() {
//   //   let data = this.auth.getData();
//   //   let reqData =
//   //   {
//   //     "admin_id": data._id,
//   //     "campaign_id": this.campaign_id
//   //   }
//   //   this._service.postApiCommon('/campaign/discard', reqData).subscribe((response: any) => {
//   //     if (response.status == 200) {
//   //       this.data = response.body;
//   //       if (this.data.responseCode == 200) {
//   //         this._service.success(this.data.responseMessage);
//   //         this.show_active_model = false;
//   //         this.getcampaignlist();
//   //       } else {
//   //         this._service.error(this.data.responseMessage);
//   //         this.show_active_model = false;
//   //       }
//   //     }
//   //   }, error => {
//   //     this.message = "Something went wrong! Please try again.";
//   //   });
//   // }

//   // // Refresh list and reset data for list
//   // refreshAllFilter() {
//   //   this.searchData = "";
//   //   this.searchDataValue = "";
//   //   this.pageNumber = 1;
//   //   this.statusFilter = "";
//   //   this.getcampaignlist();

//   // }

//   // // Clear search
//   // clearCrosValue() {
//   //   this.searchData = "";
//   //   this.searchDataValue = "";
//   //   this.searchSymbolValue = true;
//   //   this.getcampaignlist();
//   // }

//   // // Search Functionality for advertiser
//   // searchDataCampaign(event) {
//   //   this.searchData = event.target.value;
//   //   this.searchDataValue = event.target.value;
//   //   if (this.searchData.length > 1) {
//   //     this.searchSymbolValue = false;
//   //     this.pageNumber = 1;
//   //     this.campaignlistData = [];
//   //     this.getcampaignlist();
//   //   }
//   // }

//   // // Load more function call scroll down
//   // onScrollDown() {
//   //   if (this.respAdmin.length === 1) {
//   //     this.pageNumber = this.pageNumber + 1;
//   //     this.getcampaignlist();
//   //   }
//   // }

// }

