
class Utils {
  getData = () => ({
    influencersList: [],
    microInfluencersList: []
  });

  getFilters = () => ({
    page_number: 1,
    limit: 0,
    search: "",
    gender: [],
    status: "",
    type: "",
    social: "",
  });

  getMicroInfluencersPageDetails = () => ({
    page: 1,
    pages: 1,
  });

  getCampaignDetails = () => ({
    id: "",
    invitedPlatforms: [],
    title: "",
  });

  getPageDetails = () => ({
    limit: 10,
    page: 1,
    pages: 1,
  });

  getDnfluencerDefaultTypes = () => ([
    "All", 
    "Micro", 
    "Premium"
  ]); 

  getAllPlatforms = () => ([
    "Facebook", 
    "Linkedin", 
    "Pinterest", 
    "Blog", 
    "Tiktok", 
    "Snapchat", 
    "Youtube", 
    "Twitter"
  ]);

  getInfluencerDefaultStatus = () => ([
    "Invited", 
    "Accepted", 
    "Engaged", 
    "Completed", 
    "Declined"
  ]);

  ifObjectEmpty = obj => (Object.keys(obj).length === 0);

  getPlatformColor = platform => platform+"_color";

  getPlatformSpecificClasses = platform => {
     let classes = "";
     switch(platform) {
       case "facebook" : {
         classes = "fab fa-facebook-f";
         break;
       }
       case "linkedin" : {
         classes = "fab fa-linkedin-in";
         break;
       }
       case "pinterest" : {
         classes = "fab fa-pinterest-p";
         break;
       }
       case "blog" : {
         classes = "fab fa-blogger-b";
         break;
       }
       case "tiktok" : {
         classes = "material-icons";
         break;
       }
       case "snapchat" : {
         classes = "fab fa-snapchat-ghost";
         break;
       }
       default : {
         classes = `fab fa-${platform}`
       }
     }
    return `${classes} ${this.getPlatformColor(platform)}`;
  };

  getCampaignStatusColorClass = action => {
    let className = "";
    switch (action) {
      case "":
        className = "color_000";
        break;
      case "accepted":
        className = "color_eleve";
        break;
      case "engaged":
        className = "color_Engaged";
        break;
      case "completed":
        className = "color_green";
        break;
      case "declined":
        className = "color_red";
        break;
      default:
        className = "color_eleve"      
    };
    return className;
  };

  ifIncldesInArray = (value, list) => {
    if (value) {
     return list.includes(value);
    }
    return false;  
  };

  toggleValueInArray = (value, list) => {
    if (value) {
      const newList = [...list]
      const idIndex = newList.indexOf(value);
      if (idIndex > -1) {
        newList.splice(idIndex, 1);
      } else {
        newList.push(value);
      }
      return [...newList];
    }
    return list;
  };

  getConfirmationModalEnities = (type, influencersCount) => {
    let modalAlertMessage = "";
    let modalConfirmActionName = "";
    switch (type) {
      case "remove_influencer":
        modalAlertMessage = `There are ${influencersCount} non-engaged social profiles of selected users who have accepted the campaign. Are you sure you want to remove these social profiles?`;
        modalConfirmActionName = "onClickConfirmRemoveInfluencer";
        break;
      case "remove_handle":   
      default:
        modalAlertMessage = "Remove the relevant profile (whole user will be removed if that user had only one profile)"
        modalConfirmActionName = "onClickConfirmRemoveHandle"
        break;
    }
    return { modalAlertMessage, modalConfirmActionName };
  };

  hasKeyAndValue = (obj, key) => {
    if (!Object.keys(obj).length || !key) {
      return false;
    }
    if (obj.hasOwnProperty(key)) {
      const value = obj[(key)] || null;
      if (!value) {
        return false;
      }
      if (Array.isArray(value)) {
        return value.length ? true : false;
      }
      if ((typeof value === "object") && (Array.isArray(value) === false)) {
        return Object.keys(value).length === 0;;
      }
      return !!value;
    }
    return false;
  };

  hasInArray = (value, list) => {
    return list.includes(value);
  };

  updateKeyInObject = (obj, key, value) => {
    if (!this.ifObjectEmpty(obj)) {
      console.log("obj has not key");
    }
  };

}

export default Utils;