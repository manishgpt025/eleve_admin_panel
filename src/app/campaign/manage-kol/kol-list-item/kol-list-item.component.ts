import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { AdminService, AuthService } from 'src/app/services';
import { ManageKolComponent } from '../manage-kol.component';
import Utils from "../utils";

const utils = new Utils();

@Component({
  selector: 'app-kol-list-item',
  templateUrl: './kol-list-item.component.html',
})
export class KolListItemComponent implements OnInit {
  @Input() loading;
  @Input() influencersList;
  @Input() selectedInfluencersList;
  @Output() onClickRemoveInfluencer = new EventEmitter();
  @Output() onClickRemoveHandle = new EventEmitter();
  @Output() onClickToggleSelectInfluencer = new EventEmitter();
  emptyLoadingArray: Array<number> = [1, 2, 3, 4, 5, 6, 7, 8];
  ifSuperAdmin: boolean = false;

  constructor(
    private router: Router, 
    private activeRoute: ActivatedRoute,
    private auth: AuthService,
    private service: AdminService
  ) {}

  ngOnInit() {
    const { role } = this.auth.getData();
    this.ifSuperAdmin = (role === "superadmin");
  }

  getInfluencerDetails = ({
    influencerData: [ data ]
  }, key, capitalize = false): any => {
    const { [key]: value = "" } = data;
    if (capitalize && value) {
      value.charAt(0).toUpperCase() + value.slice(1)
    }
    return value;
  };

  getInfluencersProfiles = ({ 
    profiles
  }): Array<any> => profiles;

  getPlatformClasses = (platform): string => {
    return utils.getPlatformSpecificClasses(platform);
  };

  getPlatformDetails = (key, profile): any => {
    const { [key]: value = "" } = profile;
    return value;
  };

  getPlatformFollowers = ({
    platform, 
    Elv_social
  },{
    influencerData: [data],
  }): string => {
    const { [platform]: platformArray = [] } = data;
    if (platformArray.length) {
      const requiredPlatform = platformArray.find(platform => 
        platform.Elv_social === Elv_social
      );
      if (requiredPlatform) {
        return requiredPlatform.followers_count;
      }
    }
    return "";
  };

  onClickDeleteProfile = ({
    _id: id,
  }): void => {
    this.onClickRemoveHandle.emit(id);
  };

  getActionClass = ({ action }): string => {
    return utils.getCampaignStatusColorClass(action);
  };

  onCLickSelectInfluencer = ({ 
    influencer_id: influencerId 
  }): void => {
    this.onClickToggleSelectInfluencer.emit({
      influencerId, 
    });
  };

  onRemoveInfluencer = ({
    influencer_id: influencerId 
  }): void => {
    this.onClickToggleSelectInfluencer.emit({
      influencerId, 
    });
    this.onClickRemoveInfluencer.emit();
  };

  ifInfluencerSelected = ({
    influencer_id:  influencerId 
  }): void => utils.ifIncldesInArray(
    influencerId, this.selectedInfluencersList
  );

  ifUserImageIxist = ({
    influencerData: [data]
  }): boolean => (
    data.hasOwnProperty("profile_pic") 
    && data.profile_pic !== ""
  );

  getUserNameInitials = ({
    influencerData: [data]
  }): void => {
    const { 
      first_name: firstName = "",
      last_name: lastName = ""
    } = data;
    return (
      firstName 
      && firstName.charAt(0).toUpperCase()
      ) + (
      lastName && lastName.charAt(0).toUpperCase()
    );
  };

  hasKey = ({
    influencerData: [data]
  }, key): boolean => utils.hasKeyAndValue(data, key);

  showTrashOnProfile = (profile): boolean => {
    return (
      !utils.hasKeyAndValue(profile, "post_done")
      && this.ifSuperAdmin
    );
  };

}