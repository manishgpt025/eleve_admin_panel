import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import _ from "lodash";

import Utils from "./utils";
import { AdminService, AuthService } from 'src/app/services';
import { ifStatusOkay, getErrorMessage } from "../../handlers";

const utils = new Utils();

@Component({
  selector: "app-manage-kol",
  templateUrl: "./manage-kol.component.html",
})
export class ManageKolComponent implements OnInit {
  data: any = utils.getData();  // Default data type
  campaignDetails: any = utils.getCampaignDetails(); // Default campaign details type 
  pageDetails: any = utils.getPageDetails(); // Default page details
  filters: any = utils.getFilters(); // Default filters
  microInfluencersPageDetails: any = utils.getMicroInfluencersPageDetails(); // Page details to maintain pagination for micro influencers 
  influencerDefaultTypes: Array<string> = utils.getDnfluencerDefaultTypes(); // All Influencers types  
  allPlatforms: Array<string> = utils.getAllPlatforms(); // All social platforms 
  allStatuses: Array<string> = utils.getInfluencerDefaultStatus(); // All statues associated with influencers on behalf of campaign
  loading: boolean = true; // Influencers list loading
  selectedInfluencersList: Array<string> = []; // Array of ids of selected influencers 
  handleId: string = ""; // Handle id to remove handle  
  searchInfluencerKeyword: string = ""; // Input keyword to search for influencers 
  modalAlertMessage: string = ""; // Message on modal 
  modalConfirmAction: Function = () => {}; // Method to execute on modal confirmation button
  showConfirmModal: boolean = false; // Decide to show or hide modal 
  searchMicroKeyword: string = ""; // Input keyword to search for micro influencers 
  inputMicroKeyword: string = "";
  microInfluencerLoading: boolean = false;

  
  debounceMicroInfluencerSearch = _.debounce(()=> { 
    this.microInfluencersPageDetails = utils.getMicroInfluencersPageDetails();
    this.resetKeyInData("microInfluencersList");
    this.microInfluencerLoading = true;
    this.getMicroInfluencersList();
  }, 500)

  debounceInfluencerSearch = _.debounce(()=> { 
    this.resetKeyInData("influencersList");
    this.loading = true;
    this.getInfluencersList();
  }, 500)

  constructor(
    private router: Router, 
    private activeRoute: ActivatedRoute,
    private auth: AuthService,
    private service: AdminService
  ) { }

  ngOnInit() {
    const campaignId = this.getCampaignId();
    if (campaignId) {
      this.getInfluencersList();
    } else this.router.navigate(["campaign/list"]);
  }

  // API Calls
  getInfluencersList = (): void => {
    const { _id: admin_id } = this.auth.getData();
    const params = {
      admin_id,
      campaign_id: this.getCampaignId(),
      ...this.filters,
    }
    this.service.postApiCommon("/campaign/kols_list", params)
      .subscribe((response: any) => {
        if (ifStatusOkay(response)) {
          this.service.hide();
          const { body: { 
              result,
            },
          } = response;
          this.setPageDetails(result);
          this.setInfluencersList(result);
          this.setCampaignDetails(result);
          this.loading = false;
        }
      }, error => {
        this.loading = false;
        this.service.error(
          getErrorMessage(error)
        )
      }
    );
  };

  onClickConfirmRemoveInfluencer = (): void => {
    const { _id: admin_id } = this.auth.getData();
    const params = {
      admin_id,
      campaign_id: this.getCampaignId(),
      influencers: this.selectedInfluencersList
    };

    this.service.postApiCommon("/campaign/influencer/remove", params)
      .subscribe((response: any) => {
        if (ifStatusOkay(response)) {
          this.service.success(
            `${this.selectedInfluencersList.length} non-engaged profiles removed`
          );
          let { influencersList } = this.data;
          influencersList = influencersList.filter(influencer => (
            !this.selectedInfluencersList.includes(influencer.influencer_id)
          ));
          this.toggleSelectInfluencerModal();
          if (!influencersList.length) {
             this.getInfluencersList();
          } else {
            this.data = {
              ...this.data,
              influencersList,
            };
          }
        }
      }, error => {
        this.service.error(
          getErrorMessage(error)
        );
        this.toggleSelectInfluencerModal();
      }
    );
  };

  onClickConfirmRemoveHandle = (): void => {
    const { _id: admin_id } = this.auth.getData();
    const params = {
      admin_id,
      handle_id: this.handleId,
    };
    this.service.postApiCommon("/campaign/influencer/handle/remove", params)
      .subscribe((response: any) => {
        if (ifStatusOkay(response)) {
          this.getInfluencersList();
          this.service.success("Social Handle Removed");
          this.toggleSelectInfluencerModal();
        }
      }, error => {
        this.service.error(
          getErrorMessage(error)
        );
        this.toggleSelectInfluencerModal();
      }
    );
  };

  getMicroInfluencersList = (): void => {
    const { _id: admin_id } = this.auth.getData();
    const { page: page_number } = this.microInfluencersPageDetails;
    const params = {
      admin_id,
      page_number,
      keyword: this.inputMicroKeyword,
      campaign_id: this.getCampaignId(),
    };
    this.service.postApiCommon("/campaign/kol/searchmicro", params)
      .subscribe((response: any) => {
        if (ifStatusOkay(response)) {
          const { body: { 
              result,
            },
          } = response;
          this.setMicroInfluencers(result);
          this.setMicroInfluencerListDetails(result);
          this.microInfluencerLoading = false;
        }
      }, error => {
        this.service.error(
          getErrorMessage(error)
        );
        this.microInfluencerLoading = false;
      }
    );
  };

  onClickNotifyAll = (): void => {
    this.service.success("Under Development");
  };

  onClickAddMicroInfluencer = (influencer): void => {
    const {
      platform,
      Elv_social: elv_social,
      cost,
      category,
      type = "micro",
    } = influencer;
    const params = {
      campaign_id: this.getCampaignId(),
      platform,
      elv_social,
      cost,
      category,
      type
    };
    this.service.postApiCommon("/campaign/kol/addinfluencer", params)
      .subscribe((response: any) => {
        if (ifStatusOkay(response)) {
          const { body: { 
              result,
            },
          } = response;
          const {
            microInfluencersList
          } = this.data;
          const influencerIndex = microInfluencersList.findIndex(influencer => (
            influencer.Elv_social === elv_social 
            && influencer.platform === platform
          ));
          microInfluencersList.splice(influencerIndex, 1, {
            ...influencer, 
            campaign_details: { ...result }
          });
          this.data = {
            ...this.data,
            microInfluencersList: [...microInfluencersList]
          };
          this.service.success(elv_social+" added successfully"); 
        }
      }, error => {
        this.service.error(
          getErrorMessage(error)
        );
      }
    );
  };
  // API Calls

  // Setters
  setCampaignDetails = ({
    campaignDetail: {
      invitedPlatforms,
      title,
    },
  }): void => {
    this.campaignDetails = {
      invitedPlatforms,
      title,
    };
  };  

  setMicroInfluencerListDetails = ({
    page,
    pages
  }): void => {
    this.microInfluencersPageDetails = {
      page,
      pages
    };
  };

  setPageDetails = ({
    limit,
    page,
    pages,
  }): void => {
    this.pageDetails = {
      limit,
      page,
      pages,
    };
  };  

  setInfluencersList = ({
    influencerInfo: influencersList,
    limit,
    page,
    pages,
  }): void => {
    const { page: currentPage } = this.pageDetails;
    if (page > currentPage) {
       this.data = {
        ...this.data,
        influencersList: [
          ...this.data.influencersList, 
          ...influencersList
        ],
      }; 
    } else {
      this.data = {
        ...this.data,
        influencersList: [...influencersList]
      };
    }
  }; 

  setMicroInfluencers = ({
    page,
    pages,
    list
  }): void => {
    const { microInfluencersList } = this.data;
    this.data = {
      ...this.data,
      microInfluencersList: [
        ...microInfluencersList,
        ...list
      ]
    };
  };
  // Setters 

  getCampaignId = (): string => {
    let { id } = this.campaignDetails;
    if (!id) {
      this.activeRoute.params
        .subscribe(params => {
          id = params.id;
          this.campaignDetails.id = id;
        }
      );
    }
    return id;
  };

  onClickRemoveHandle = (handleId): void => {
    const { 
      modalAlertMessage, 
      modalConfirmActionName 
    } = utils.getConfirmationModalEnities("remove_handle", 0);
    this.modalAlertMessage = modalAlertMessage;
    this.modalConfirmAction = this[modalConfirmActionName];
    this.handleId = handleId;
    this.showConfirmModal = true;
  };

  onClickToggleSelectInfluencer = ({ 
    influencerId, 
  }) => {
    this.selectedInfluencersList = utils.toggleValueInArray(
      influencerId, this.selectedInfluencersList
    );
  };

  toggleSelectInfluencerModal = (): void => {
    this.showConfirmModal = !this.showConfirmModal;
    if (!this.showConfirmModal) {
      this.selectedInfluencersList = [];
      this.handleId = "";
    }
  };

  onClickSelectAllInfluencers = (): void => {
    const { influencersList } = this.data;
    if (influencersList.length ===  this.selectedInfluencersList.length) {
      this.selectedInfluencersList = this.selectedInfluencersList = [];
    } else {
      let newSelectedList = influencersList.map(influencer => (
        influencer.influencer_id
      ));
      this.selectedInfluencersList = newSelectedList;
    }
  };

  onClickRemoveInfluencer = (): void => {
    const { 
      modalAlertMessage, 
      modalConfirmActionName 
    } = utils.getConfirmationModalEnities(
      "remove_influencer", 
      this.selectedInfluencersList.length
    );
    this.modalAlertMessage = modalAlertMessage;
    this.modalConfirmAction = this[modalConfirmActionName];
    this.showConfirmModal = true;
  };

  onChangeMicroSearchInput = ({
    target: { value }
  }): void => {
    if (value.length > 1) {
      this.debounceMicroInfluencerSearch();
    }
    if (!value.length && this.searchMicroKeyword.length) {
      this.microInfluencersPageDetails = utils.getMicroInfluencersPageDetails();
      this.resetKeyInData("microInfluencersList");
      this.getMicroInfluencersList();
    }
    this.searchMicroKeyword = value;
  };

  onClickPremium = (platform: string): void => {
    this.router.navigate(
      [
        "/campaign/premium/"
        +platform
        +"/"
        +this.getCampaignId()
      ]
    );
  };

  onClickRefresh = (): void => {
    this.pageDetails = utils.getPageDetails();
    this.filters = utils.getFilters();
    this.data = utils.getData();
    this.selectedInfluencersList = [];
    this.showConfirmModal = false;
    this.handleId = "";
    this.modalConfirmAction = () => {};
    this.modalAlertMessage = "";
    this.searchMicroKeyword = "";
    this.searchInfluencerKeyword = "";
    this.inputMicroKeyword = "";
    this.loading = true;
    this.getInfluencersList();
  };

  onChangeInfluencerInput = ({
    target: { value: search }
  }): void => {
    this.filters = {
      ...this.filters,
      search,
      page_number: 1
    }
    if (search.length > 1) {
      this.debounceInfluencerSearch();
    }
    if (!search.length && this.searchInfluencerKeyword.length) {
      this.resetKeyInData("influencersList"); 
      this.loading = true;
      this.getInfluencersList();
    }
    this.searchInfluencerKeyword = search;
  };

  resetKeyInData = (key): void => {
    this.data = {
      ...this.data,
      [key]: [],
    };
  }

  onChangeFilter = (key, value): void => {
    if (key === "gender") {
      this.filters = {
        ...this.filters,
        [key]: [...utils.toggleValueInArray(
          value, this.filters[key]
        )]
      };
    } else {
      this.filters = {
        ...this.filters,
        [(key)]: value
      };
    }
    this.loading = true;
    this.resetKeyInData("influencersList");
    this.getInfluencersList();
  };


  // Utility Methods 
  getPlatformClasses = (platform): string => {
    return utils.getPlatformSpecificClasses(platform);
  };

  // getPlatformBGClass = (platform): string => {
  //   return utils.getPlatformBGClass(platform)
  // };

  getActionClass = (status: string): string => {
    return utils.getCampaignStatusColorClass(status);
  };

  getPlatformColor = (platform): string => {
    return utils.getPlatformColor(platform);
  }

  hasInArray = (value, list): boolean => {
    return utils.hasInArray(value, list);
  };
  // Utility Methods 

  // Window Scroll Events
  decideNextPage = ({ 
    page: currentPage,
    pages: totalPages,
    loading
  }): any => {
    let shouldCallDataAgain = true;
    if (
      !loading &&
      (totalPages > currentPage) && 
      (((window.innerHeight + window.scrollY) + 200) 
      >= document.body.offsetHeight)
    ) {
      return {
        shouldCallDataAgain,
        nextPage: ++currentPage
      };
    } else {
      return {
        shouldCallDataAgain: false,
        nextPage: currentPage
      };
    };
  };

  @HostListener('window:scroll', ['$event']) 
  onScrollEvent = (): void => {
    const {
      shouldCallDataAgain,
      nextPage,
    } = this.decideNextPage({
      loading: this.loading,
      ...this.pageDetails
    });
    if (shouldCallDataAgain) {
      this.filters = {
        ...this.filters,
        page_number: nextPage
      } 
      this.getInfluencersList();
    }
  };
  // Window Scroll Events

  // Micro Influencer
  onScrollMicroInfluencers = (): void => {
    let { 
      page, 
      pages 
    } = this.microInfluencersPageDetails;
    const { microInfluencersList } = this.data;
    if (pages > page) {
      this.microInfluencersPageDetails = {
        pages,
        page: ++page
      };
      this.getMicroInfluencersList();
    }
  };

  onClickClearInfluencerSearch = (): void => {
    this.resetKeyInData("influencersList");
    this.filters = {
      ...this.filters,
      search: "",
      page_number: 1
    };
    this.searchInfluencerKeyword = "";
    this.loading = true;
    this.getInfluencersList();
  };

  onClickClearMicroSearch = (): void => {
    this.resetKeyInData("microInfluencersList");
    this.searchMicroKeyword = "";
    this.inputMicroKeyword = "";
    this.filters = utils.getMicroInfluencersPageDetails();
    this.getMicroInfluencersList();
  };
  // Micro Influencer

  ifFiltersApplied = (): boolean => {
    return !(_.isEqual(this.filters, utils.getFilters()));
  };

}
