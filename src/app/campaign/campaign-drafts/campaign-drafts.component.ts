import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSelect, MatSelectChange } from '@angular/material';
import { FormGroup, Validators, FormControl, FormGroupDirective } from '@angular/forms';
import { Ng2ImgMaxService } from 'node_modules/ng2-img-max';

import { AdminService, AuthService } from 'src/app/services';

@Component({
  selector: 'app-campaign-drafts',
  templateUrl: './campaign-drafts.component.html',
})
export class CampaignDraftsComponent implements OnInit {

  pageNumber: number=1;
  searchData: any;
  statusFilter: any;
  sortAdmin: any;
  locationFilter: any;
  data: any;
  respAdmin: any;
  managelistData: any;
  create_mobile_code: string;
  message: any;
  campaignlistData: any = [];
  show_active_model: boolean = false;
  campaign_id: any;
  searchDataValue: string;
  searchSymbolValue: boolean = true;

  throttle = 50;
  scrollDistance = 2;
  scrollUpDistance = 2;

  constructor(private auth: AuthService, private router: Router, private _service: AdminService, private el: ElementRef, private ng2ImgMaxService: Ng2ImgMaxService) { }

  ngOnInit() {
    this.getDraftCampaignlist();
  }

  // Get draft list
  getDraftCampaignlist() {
    let data = this.auth.getData();
    let reqData =
    {
      "keyword": this.searchData,
      "status": '0',
      "platform": "",
      "pageNumber":this.pageNumber,
    }
    this._service.postApiCommon('/campaign/allcampaignlist', reqData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        // console.log(this.data.result);
        if (this.data.responseCode == 200) {
          this.respAdmin = this.data.result.docs || [];
          this.campaignlistData = [...this.campaignlistData, ...this.respAdmin];
          // this.managelistData = this.data.result.docs;
        } else {
          this.message = this.data.responseMessage;
        }
      }
    }, error => {
      this.message = "Something went wrong! Please try again.";
    });
  }

  // Open confirm box
  campaignDiscard(campaign_id) {
    this.campaign_id = campaign_id;
    this.show_active_model = true;
  }

  // Close confirm box
  campaignDiscardClose() {
    // this.getDraftCampaignlist();
    this.show_active_model = false;
  }

  // Delete campaign all data
  discardSubmit() {
    let data = this.auth.getData();
    let reqData =
    {
      "admin_id": data._id,
      "campaign_id": this.campaign_id
    }
    this._service.postApiCommon('/campaign/discard', reqData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.result.responseCode = 200) {
          this._service.success(this.data.responseMessage);
          this.show_active_model = false;
          this.getDraftCampaignlist();
        } else {
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this.message = "Something went wrong! Please try again.";
    });
  }

  // Refresh list and reset data for list
  refreshAllFilter() {
    this.searchData = "";
    this.searchDataValue = "";
    this.pageNumber = 1;
    this.statusFilter = "";
    this.getDraftCampaignlist();

  }

  // Clear search
  clearCrosValue() {
    this.searchData = "";
    this.searchDataValue = "";
    this.searchSymbolValue = true;
    this.getDraftCampaignlist();
  }

  // Search Functionality for advertiser
  searchDataCampaign(event) {
    this.searchData = event.target.value;
    this.searchDataValue = event.target.value;
    if (this.searchData.length > 1) {
      this.searchSymbolValue = false;
      this.pageNumber = 1;
      this.campaignlistData = [];
      this.getDraftCampaignlist();
    }
  }

  // Load more function call scroll down
  onScrollDown() {
    console.log('scrolled down!!');
    if (this.respAdmin.length === 10) {
      this.pageNumber = this.pageNumber + 1;
      this.getDraftCampaignlist();
    }
  }

}
