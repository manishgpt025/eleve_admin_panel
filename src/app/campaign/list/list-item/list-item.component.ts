import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

import Utils from "../utils"; 

import { findIndexAndItem  } from "../../../handlers";
import { PLATFORMS_SEQUENCE  } from "../../../constants";

const utils = new Utils();

@Component({
  selector: "app-list-item",
  templateUrl: "./list-item.component.html",
})
export class ListItemComponent implements OnInit {
  @Input() loading; 
  @Input() list;
  @Input() ifFiltersActive;
  @Output() onDiscardCampaign = new EventEmitter();
  @Output() onStopCampaign = new EventEmitter();
  @Output() onEditCampaignEndDate = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  onClickStopCampaign = ({
    _id: campaignId,
    status
  }): void => {
    this.onStopCampaign.emit({
      campaignId,
      status
    });
  };

  onClickDiscardCampaign = ({
    _id: campaignId,
    status
  }): void => {
    this.onDiscardCampaign.emit({
      campaignId,
      status
    });
  };

  onClickEditEndDate = ({
    _id: campaignId,
    start_date: startDate,
  }): void => {
    this.onEditCampaignEndDate.emit({
      campaignId,
      startDate,
    });
  };

  getCampaignStatusLabel = (status): string => {
    return utils.getCampaignStatusLabel(status, "status", "label");
  };

  getCampaignStatusColour = (status): string => {
    return utils.getCampaignStatusColour(status);
  };

  getSortedPlatforms = ({ platforms: allPlatforms = [] }): Array<any> => {
    let sortedPlatforms = [];
    allPlatforms.forEach(platform => {
      const { obj, index} = findIndexAndItem(sortedPlatforms, "name", platform.name);
      if (index !== undefined) {
        const influencersCount = parseInt(obj.number_influencer)+parseInt(platform.number_influencer);
        sortedPlatforms.splice(index, 1, { ...obj, number_influencer: influencersCount });
      } else {
        sortedPlatforms.push(platform);
      }
    });
    return sortedPlatforms.sort((first, second) => {
      const firstPlatformIndex = PLATFORMS_SEQUENCE.indexOf(first.name);
      const secondlatformIndex = PLATFORMS_SEQUENCE.indexOf(second.name);
      return (firstPlatformIndex > secondlatformIndex) ? 1
        : (firstPlatformIndex === secondlatformIndex) ? 0
        : -1;
    });
  };

  trackWithId = (index, item): any => {
    if (item && item._id) {
      return item._id;
    }
    return index;
  };

  getBrandInitials = ({ advertiser_brand_name: brandName = "" }): string => {
    if (!brandName) {
      return "";
    }
    const [firstWord = "", secondWord = ""] = brandName.split(" ");
    if (!firstWord) {
      return "";
    }
    let initials = firstWord.charAt(0).toUpperCase();
    initials = secondWord 
      ? initials.concat(secondWord.charAt(0).toUpperCase()) 
      : initials;
    return initials;
  };

}
