import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute, Router, NavigationEnd } from "@angular/router";
import { MatSelect, MatSelectChange } from "@angular/material";
import * as moment from "moment";
import _ from "lodash";

import { AdminService, AuthService } from "src/app/services";
import {
  ifStatusOkay,
  getErrorMessage,
  getResult,
  findIndexAndItem,
} from "../../handlers";

import Utils from "./utils";
const utils = new Utils();

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
})
export class CampaignListComponent implements OnInit {
  routes: Array<any> = utils.getRoutes();
  selectedCampaign: any = utils.getDefaultSelectedCampaign();
  pageDetails: any;
  data: any;
  confirmationModalDetails: any;
  filters: any;
  loading: boolean = true;
  getListApiStatus: boolean = false;
  searchKeywordValue: string = "";
  inputKeywordValue: string = "";
  campaignStatus: string = "";
  pageTitle: string = "";
  selectedCampaignEndDate: any;
  editEndDateModal: boolean = false;
  navigationSubscription: any;
  currentKeywordSearch: string = "";
  validateEndDate: boolean = false;

  debounceGetListEvent = _.debounce(() => {
    this.data = {
      ...this.data,
      list: []
    };
    this.getCampaignList();
  }, 750)

  constructor(
    private authService: AuthService,
    private router: Router,
    private apiService: AdminService,
    private activatedRoute: ActivatedRoute
  ) {
    this.navigationSubscription = this.router.events.subscribe((event: any) => {
     if (event instanceof NavigationEnd) {
        this.initRouteCall();
     }
   });
  }

  ngOnInit() {
    if (!this.getListApiStatus && !this.data.list.length) {
      this.initRouteCall();
    }
  }

  initRouteCall = (): void => {
    const currentRoute = this.getRoute() || "";
    const existingRouteObject = this.routes.find(
      ele => ele.label === currentRoute
    );
    if (existingRouteObject !== undefined) {
      this.campaignStatus = existingRouteObject.value;
      this.pageTitle = currentRoute.slice(0, 1).toUpperCase() + currentRoute.slice(1);
      this.data = { ...utils.getDefaultData() };
      this.filters = { ...utils.getDefaultPageFilters() };
      this.pageDetails = { ...utils.getDefaultPageDetails() };
      this.confirmationModalDetails = { ...utils.getDefaultConfirmationModalDetails() };
      this.getCampaignList();
    } else this.router.navigate([""]);
  };

  getRoute = (): string => {
    let action = "";
    this.activatedRoute.params
      .subscribe(params => {
        action = params.action;
      }
    );
    return action;
  };

  getAdminId = (): string => {
    return this.authService.getData()._id || "";
  };

  getCampaignList = (): void => {
    const { platform, keyword, status } = this.filters;
    const { page: pageNumber } = this.pageDetails;
    let params = {
      keyword,
      platform,
      pageNumber,
      admin_id: this.getAdminId(),
      status: status || this.campaignStatus,
    };
    if ((this.currentKeywordSearch !== keyword) && this.loading) {
      this.debounceGetListEvent();
      return;
    }
    this.currentKeywordSearch = keyword;
    this.setLoaders(true);
    this.apiService.postApiCommon("/campaign/allcampaignlist", params)
      .subscribe((response: any) => {
        if (ifStatusOkay(response)) {
          const { docs: list, ...rest } = getResult(response);
          this.setCampaignList(list);
          this.setPageDetails(rest);
          this.setLoaders(false);
        }
      }, error => {
        this.apiService.error(getErrorMessage(error));
        this.setLoaders(false);
      }
    );
  };

  setLoaders = (value: boolean): void => {
    this.loading = value;
    this.getListApiStatus = value
  };

  onConfirmStopCampaign = (): void => {
    const {
      status,
      campaignId: campaign_id
    } = this.selectedCampaign;
    const params = {
      status,
      campaign_id,
      admin_id: this.getAdminId(),
      end_date: moment(Date.now()).format("MM/DD/YYYY HH:mm:ss")
    };
    this.apiService.postApiWithHeader("/campaign/campaignstop", params, "3")
      .subscribe((response: any) => {
        if (ifStatusOkay(response)) {
          this.apiService.success("Campaign has Ended!");
          this.setEndDate(campaign_id);
          this.resetConfirmationModal();
          this.resetSelectedCampaign();
        } else {
          this.apiService.error("Something went wrong! Please try again.");
        }
      }, error => {
        this.apiService.error(getErrorMessage(error));
      }
    );
  };

  onConfirmDiscardCampaign = (): void => {
    const {
      campaignId: campaign_id
    } = this.selectedCampaign;
    let params = {
      campaign_id,
      admin_id: this.getAdminId(),
    };
    this.apiService.postApiCommon("/campaign/discard", params)
      .subscribe((response: any) => {
        if (ifStatusOkay(response)) {
          this.deleteDiscardedCampaign(campaign_id);
          this.apiService.success("Campaign has Ended!");
          this.resetConfirmationModal();
          this.resetSelectedCampaign();
        }
    }, error => {
      this.apiService.error(getErrorMessage(error));
    });
  };

  setEndDate = (campaignId: string): void => {
    const { list } = this.data;
    const { obj, index } = findIndexAndItem(list, "_id", campaignId);
    if (index !== undefined) {
      list.splice(index, 1, {
        ...obj,
        end_date: moment(new Date()).format("MM/DD/YYYY HH:mm:ss"),
        status: "4"
      });
    }
  };

  deleteDiscardedCampaign = (campaignId: string): void => {
    const { list } = this.data;
    const currentCampaignIndex = list.findIndex(
      campaign => campaign._id === campaignId
    );
    list.splice(currentCampaignIndex, 1);
    this.data = {
      ...this.data,
      list
    };
  }

  resetSelectedCampaign = (): void => {
    this.selectedCampaign = utils.getDefaultSelectedCampaign();
  };

  resetConfirmationModal = (): void => {
    this.confirmationModalDetails = utils.getDefaultConfirmationModalDetails();
  };

  resetCampaignList = (): void => {
    this.data = utils.getDefaultData();
  };

  setCampaignList = (
    list: Array<any> = []
  ): void => {
    this.data = {
      ...this.data,
      list: [...this.data.list, ...list],
    };
  };

  setPageDetails = ({
    page,
    pages
  }): void => {
    this.pageDetails = {
      page,
      pages,
    };
  };

  onClickClearSearch = (): void => {
    this.filters = {
      ...this.filters,
      keyword: ""
    };
    this.searchKeywordValue = ""
    this.getCampaignList();
  };

  onClickRefresh = (): void => {
    this.filters = utils.getDefaultPageFilters();
    this.pageDetails = utils.getDefaultPageDetails();
    this.data = utils.getDefaultData();
    this.searchKeywordValue = "";
    this.getCampaignList();
  };

  onChangeSearchInput = ({ target:
    { value }
  }): void => {
    const query = value.trim();
    if (query.length > 1) {
      this.filters = {
        ...this.filters,
        keyword: query
      };
      this.pageDetails = {
        ...this.pageDetails,
        page: 1
      };
      this.debounceGetListEvent();
    } else {
      if (!query.length && this.inputKeywordValue.length) {
        this.filters = {
          ...this.filters,
          keyword: ""
        };
        this.data = {
          ...this.data,
          list: []
        };
        this.getCampaignList();
      }
    }
    this.inputKeywordValue = query;
  };

  decideNextPage = ({
    page: currentPage,
    pages: totalPages,
    loading
  }): any => {
    let shouldCallDataAgain = true;
    if (
      !loading &&
      (totalPages > currentPage) &&
      (((window.innerHeight + window.scrollY) + 200)
      >= document.body.offsetHeight)
    ) {
      return {
        shouldCallDataAgain,
        nextPage: ++currentPage
      };
    }
    return {
      shouldCallDataAgain: false,
      nextPage: currentPage
    };
  };

  @HostListener('window:scroll', ['$event'])
  onScrollEvent = (): void => {
    const {
      shouldCallDataAgain,
      nextPage,
    } = this.decideNextPage({
      loading: this.loading,
      ...this.pageDetails
    });
    if (shouldCallDataAgain) {
      this.pageDetails = {
        ...this.pageDetails,
        page: nextPage
      }
      this.getCampaignList();
    }
  };

  onStopCampaign = ({
    status,
    campaignId
  }): void => {
    this.selectedCampaign = {
      campaignId,
      status
    };
    this.confirmationModalDetails = {
      show: true,
      title: "Stopping the campaign will:",
      message: "End the Current campaign.",
      buttonText: "Stop",
      confirmAction: this["onConfirmStopCampaign"],
    };
  };

  onDiscardCampaign = ({
    status,
    campaignId
  }): void => {
    this.selectedCampaign = {
      status,
      campaignId
    };
    this.confirmationModalDetails = {
      show: true,
      title: "Discarding the campaign will:",
      message: "Trash the drafted campaign.",
      buttonText: "Discard",
      confirmAction: this["onConfirmDiscardCampaign"]
    };
  };

  closeConfirmationModal = (): void => {
    this.confirmationModalDetails = utils.getDefaultConfirmationModalDetails();
  };

  onEditCampaignEndDate = ({
    startDate,
    campaignId,
  }): void => {
    this.selectedCampaign = {
      campaignId,
      startDate,
    };
    this.editEndDateModal = true;
  };

  validateCampaignDates = (startDate, event) => {
    //dates from selectiong date picker
    let end_date = moment(event.endDate).format('MM/DD/YYYY HH:mm:ss')
    let newStartDate = moment(startDate).add(30, 'minutes').format('MM/DD/YYYY HH:mm:ss');
    if(end_date < newStartDate){
      this.apiService.error('End date must be greater than the start date & time');
      this.validateEndDate = false;
      return false
    }else{
      this.validateEndDate = true;
    }
  }

  closeEditEndDateModal = (): void => {
    this.editEndDateModal = false;
  };

  setNewEndDate = (campaignId: string, endDate: string): void => {
    const { list } = this.data;
    const { obj, index } = findIndexAndItem(list, "_id", campaignId);
    if (index !== undefined) {
      list.splice(index, 1, { ...obj, end_date: endDate});
    }
  };

  onConfirmCampaignEndDate = (): void => {
    if(!this.validateEndDate)
      return this.apiService.error('End date must be greater than the start date & time');
    const {
      campaignId: campaign_id,
    } = this.selectedCampaign;
    const { endDate } = this.selectedCampaignEndDate;
    const endDateStringValue = moment(endDate).format("MM/DD/YYYY HH:mm:ss");
    const params = {
      campaign_id,
      end_date: endDateStringValue,
    };
    if(this.validateCampaignDate()){
      this.apiService.postApiWithHeader("/campaign/create_campaign", params, "3")
        .subscribe((response: any) => {
          if (ifStatusOkay(response)) {
            this.editEndDateModal = false;
            this.apiService.success("End date has been updated");
            this.setNewEndDate(campaign_id, endDateStringValue);
            this.resetSelectedCampaign();
          }
        }, error => {
          this.apiService.error(getErrorMessage(error));
        }
      );
    }
  };

  validateCampaignDate = (): boolean => {
    let { startDate, endDate } = this.selectedCampaign;
    endDate = moment(endDate).format("MM/DD/YYYY HH:mm:ss");
    if(endDate < startDate ){
      this.apiService.error("End Date Can not be less than Start Date");
      return false;
    }
    return true;
  };

  getSocialFilters = (): Array<any> => {
    return utils.getSocialFilters();
  };

  getAllCampaignStatuses = (): Array<any> => {
    return utils.getDefaultCampaignStatues();
  };

  trackWithId = (index, item): any => {
    if (item && item.id) {
      return item.id;
    }
    return index;
  };

  getCampaignStatusLabel = (status): string => {
    return utils.getCampaignStatusLabel(status, "status", "label");
  };

  onChangeFilter = (value, key): void => {
    this.filters = {
      ...this.filters,
      [key]: value
    };
    this.data = utils.getDefaultData();
    this.pageDetails = utils.getDefaultPageDetails();
    this.getCampaignList();
  };

  ifFiltersActive = (): boolean => {
    return !_.isEqual(this.filters, utils.getDefaultPageFilters());
  };

  getCampaignStatusColour = (status): string => {
    return utils.getCampaignStatusColour(status);
  };

  capitalizeFirstLetter = (val: string): string => {
    if (val) {
       return val.slice(0, 1).toUpperCase() + val.slice(1);
     }
     return "";
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

}