
class Utils {
  getDefaultPageDetails = () => ({
    page: 1,
    pages: 1,
  });

  getDefaultData = () => ({
    list: [],
  });

  getDefaultSelectedCampaign = () => ({
    campaignId: "",
    status: "",
    startDate: "",
  });

  getDefaultConfirmationModalDetails = () => ({
    show: false,
    title: "",
    message: "",
    buttonText: "",
  });

  getDefaultPageFilters = () => ({
    keyword: "",
    platform: "",
    status: "",
  });

  getRoutes = () => ([
    { label: "active", value: "1"},
    { label: "drafts", value: "0"},
    { label: "completed", value: "6"}
  ]);

  getDefaultCampaignStatues = () => ([
    { label: "Paused", status: "8", classes: "color_organe", id: "3456" },
    { label: "Ended", status: "4", classes: "color_red", id: "3453" },
    { label: "Live", status: "3", classes: "color_green", id: "3446" },
    { label: "Scheduled", status: "2", classes: "color_yellow", id: "3476" },
    { label: "Completed", status: "6", classes: "color_organe", id: "3406" },
  ]);

  getCampaignStatusLabel = (value, keyToMatch, keyToGet) => {
    let allStatuses = this.getDefaultCampaignStatues();
    const selectedObj = allStatuses.find(
      currentStatus => (currentStatus[keyToMatch] == value)
    );
    if (selectedObj) {
      return selectedObj[keyToGet];
    }
    return "";
  };

  getCampaignStatusColour = (status) => {
    switch (status) {
      case "2":
        return "color_yellow";
      case "3":
        return "color_green";
      case "4":
      case "6":
        return "color_red";
      case "8":
        return "color_organe";
      default:
        return "eleve_color"
    }
  };

  getSocialFilters = () => ([
    {label: "Facebook", classes: "fa-facebook-f facebook_color", value: "facebook", id: "1234"},
    {label: "Twitter", classes: "fa-twitter twitter_color", value: "twitter", id: "1236"},
    {label: "Instagram", classes: "fa-instagram instagram_color", value: "instagram", id: "1238"},
    {label: "Youtube", classes: "fa-youtube youtube_color", value: "youtube", id: "1240"},
  ]);
  
}

export default Utils;