import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import * as moment from 'moment';

import { AdminService, AuthService } from 'src/app/services';

@Component({
  selector: 'app-create-campaign-step-three',
  templateUrl: './create-campaign-step-three.component.html',
})
export class CreateCampaignStepThreeComponent implements OnInit {
  step: any;
  setStep: any;
  panelOpenState: any;
  campaignId: any = "";
  message: string;
  data: any;
  campaignDetailData: any = [];
  campaignData: any = [];
  campaignFormCreateThree: FormGroup;
  typeData: any = [];
  microType: string = '';
  premiumType: string = '';
  show_div_invitation_change: boolean = false;
  campaignDates: Object = {};
  adminData: any = [];

  // constructor for dependencies injection
  constructor(
      private router: Router,
      private el: ElementRef,
      private auth: AuthService,
      private fb: FormBuilder,
      private _service: AdminService,
      private route: ActivatedRoute
    ) {}

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.campaignId = params.id || "";
    });
    this.campaignDetails();
    this.camaignCreateThreeFormVal();
  }

  //Get admin data from local storage
  getAdminData = ():any => {
    if(!this.adminData.length)
      this.adminData = this.auth.getData();
    return this.adminData._id;
  }

  // Get the campaign data
  campaignDetails = ():void => {
    let reqData =
    {
      "admin_id": this.getAdminData(),
      "campaign_id": this.campaignId
    }
    this._service.postApiCommon('/campaign/details', reqData)
    .subscribe((response: any) => {
      if (response.status === 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.campaignData = this.data.result;
          this.campaignDetailData = this.campaignData.campaign;
          this.setInitialValues(this.campaignDetailData);
        } else {
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  //Bind Initial campaign data
  setInitialValues = (initialValues) => {
    //validate dates of campaign
    this.setInitialCampaignDates(initialValues);
    // setting invitation status
    this.campaignFormCreateThree.get('InvitationStatus').setValue(initialValues.is_invited ? "1" : "0");
    this.show_div_invitation_change = initialValues.is_invited;
    // setting sms status
    this.campaignFormCreateThree.get('smsStatus').setValue(initialValues.send_sms == "1" ? true : false);
    // setting email status
    this.campaignFormCreateThree.get('emailStatus').setValue(initialValues.send_email == "1" ? true : false);
    // setting reply to
    if(initialValues.reply_to instanceof Array && initialValues.reply_to.length){
      //this.campaignFormCreateThree.get('replyToName').setValue(initialValues.reply_to || "");
      const replyToData = <FormArray>this.campaignFormCreateThree.controls['replyTo_list'];
      initialValues.reply_to.forEach(function (rvalue) {
        replyToData.push(new FormControl(rvalue));
      });
    }else{
      const replyToData = <FormArray>this.campaignFormCreateThree.controls['replyTo_list'];
        replyToData.push(new FormControl('ops@eleve.co.in'));
    }
  }

  //Validate range date time picker
  validateCampaignDates = (event, type = "") => {
    //dates from selectiong range date picker
    let campaignDates = {
      start_date: moment(event.startDate).format('MM/DD/YYYY HH:mm:ss'),
      end_date: moment(event.endDate).format('MM/DD/YYYY HH:mm:ss')
    }
    let newStartDate = moment(event.startDate).add(30, 'minutes').format('MM/DD/YYYY HH:mm:ss');
    let current_date = moment(new Date()).format('MM/DD/YYYY HH:mm:ss');
    if(Date.parse(campaignDates.start_date) < Date.parse(current_date) && type == ""){
      this._service.error('Start date cannot be historical');
      this.campaignFormCreateThree.get('scheduleTime').reset(this.campaignDates);
      return false
    }
    if(campaignDates.end_date < newStartDate){
      this._service.error('End date must be greater than the start date & time');
      this.campaignFormCreateThree.get('scheduleTime').reset(this.campaignDates);
      return false
    }else{
      return true;
    }
  }

//Bind date in range picker input
  setInitialCampaignDates = (initialValues) => {
    // setting cmpaign start and end dates
    if(
      initialValues.start_date
        &&
      initialValues.end_date
    ){
      let campaignDates = {
        start: initialValues.start_date,
        end: initialValues.end_date,
      }
      this.campaignDates = campaignDates;
      this.campaignFormCreateThree.get('scheduleTime').reset(campaignDates);
    } else {
      let campaignDates = {
        start: moment().format('MM/DD/YYYY HH:mm'),
        end: moment().format('MM/DD/YYYY HH:mm'),
      }
      this.campaignDates = campaignDates;
      this.campaignFormCreateThree.get('scheduleTime').reset(campaignDates);
    }
  }

  //form validation
  camaignCreateThreeFormVal = () => {
    this.campaignFormCreateThree = new FormGroup({
      scheduleTime: new FormControl('', Validators.compose([Validators.required])),
      InvitationStatus: new FormControl('', Validators.compose([Validators.required])),
      type: this.fb.array([]),
      senderName: new FormControl('noreply@eleve.co'),
      replyToName: new FormControl('',
      Validators.compose([
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/)
      ])),
      replyTo_list:this.fb.array([]),
      mailSubject: new FormControl('New Campaign Invitation', Validators.compose([Validators.required])),
      mailtext: new FormControl('Hi &lt;influencer_name&gt;<influencer_name>,<br><br>Eleve invites you to participate in a new campaign.<br><br>Visit the link to know more about the campaign and to join - &lt;link&gt; <link><br><br>Please login to your account at user.eleveglobal.com and join the campaign. For any query, you can mail to us at operations@eleve.co.in<br><br><b>All the best!</b><br><b>Team Eleve</b>', Validators.compose([Validators.required])),
      emailStatus: new FormControl(''),
      smsStatus: new FormControl(''),
      smsText: new FormControl('ELEVE INFLUENCER CAMPAIGN INVITE.You are invited to a new campaign. Visit <link> Login at dev.eleveglobal.com to participate.', Validators.compose([Validators.required]))
    });
  }

  //Type store in array
  onChangeType = (event) => {
    if (event.checked) {
      this.typeData.push(event.source.value);
      if (event.source.value == 'micro')
        this.microType = '1';
      if (event.source.value == 'premium')
        this.premiumType = '1';
    } else {
      if (event.source.value == 'micro')
        this.microType = '0';
      if (event.source.value == 'premium')
        this.premiumType = '0';
    }
  }

  //InvitationStatus change
  InvitationStatusChange = (event) => {
    if (event.value == '1')
      this.show_div_invitation_change = true;
     else{
      this.show_div_invitation_change = false;
      this.campaignFormCreateThree.get('mailSubject').setValidators(null);
      this.campaignFormCreateThree.get('mailSubject').updateValueAndValidity();
      this.campaignFormCreateThree.get('mailtext').setValidators(null);
      this.campaignFormCreateThree.get('mailtext').updateValueAndValidity();
      this.campaignFormCreateThree.get('smsText').setValidators(null);
      this.campaignFormCreateThree.get('smsText').updateValueAndValidity();
      this.campaignFormCreateThree.get('replyToName').setValidators(null);
      this.campaignFormCreateThree.get('replyToName').updateValueAndValidity();
    }
  }

  sendEmailOrSms = (sendType) => {
    if (sendType.source.value == 'email') {
      if (sendType.checked) {
        this.campaignFormCreateThree.get('replyToName').setValidators([
          Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/)
        ]);
        this.campaignFormCreateThree.get('replyToName').updateValueAndValidity();
        this.campaignFormCreateThree.get('mailSubject').setValidators([Validators.required]);
        this.campaignFormCreateThree.get('mailSubject').updateValueAndValidity();
        this.campaignFormCreateThree.get('mailtext').setValidators([Validators.required]);
        this.campaignFormCreateThree.get('mailtext').updateValueAndValidity();
      } else {
        this.campaignFormCreateThree.get('replyToName').setValidators(null);
        this.campaignFormCreateThree.get('replyToName').updateValueAndValidity();
        this.campaignFormCreateThree.get('mailSubject').setValidators(null);
        this.campaignFormCreateThree.get('mailSubject').updateValueAndValidity();
        this.campaignFormCreateThree.get('mailtext').setValidators(null);
        this.campaignFormCreateThree.get('mailtext').updateValueAndValidity();
        this._service.error('Email will not be sent to influencers.');
      }
    } else {
      if (sendType.checked) {
        this.campaignFormCreateThree.get('smsText').setValidators([Validators.required]);
        this.campaignFormCreateThree.get('smsText').updateValueAndValidity();
      } else {
        this.campaignFormCreateThree.get('smsText').setValidators(null);
        this.campaignFormCreateThree.get('smsText').updateValueAndValidity();
        this._service.error('SMS will not be sent to influencers.');
      }
    }

  }

  //Add Keywords tag in form array data
  addReplyToFormArray = event => {
    const replyToData = <FormArray>this.campaignFormCreateThree.controls['replyTo_list'];
    const sDuplicate = this.campaignFormCreateThree.get('replyTo_list').value;
      if(sDuplicate.filter(x => x == event.target.value) == ""){
          if(this.campaignFormCreateThree.get('replyToName').valid){
            replyToData.push(new FormControl(this.campaignFormCreateThree.get('replyToName').value));
            this.campaignFormCreateThree.get('replyToName').reset();
          }else{
            this._service.error('Please enter valid Email.');
            this.campaignFormCreateThree.get('replyToName').reset();
          }
      }else {
        this._service.error('Cannot enter same email twice');
        this.campaignFormCreateThree.get('replyToName').reset();
      }
  }

  //Remove Keywords Aarry in form data
  removereplyToFormArray = index => {
    const replyToData = <FormArray>this.campaignFormCreateThree.controls['replyTo_list'];
    replyToData.removeAt(index);
  };

  // submit the data
  campaignThreeFormSubmit = (resData, type: string):any => {
    if (this.campaignFormCreateThree.invalid) {
      const FirstInvalid = this.el.nativeElement.querySelectorAll('.ng-invalid')[1];
      FirstInvalid.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'start' });
      return;
    }
    let scheduleDateTime = {
      startDate :resData.scheduleTime.start,
      endDate : resData.scheduleTime.end
    }
    if(!this.validateCampaignDates(scheduleDateTime, 'submit'))return;
    if(
      resData.InvitationStatus == '1'
        &&
      this.campaignData.micro_influencers
        &&
      this.microType === ''
    )this.microType = '1';
    if(
      resData.InvitationStatus == '1'
        &&
      this.campaignData.premium_influencers
        &&
      this.premiumType === ''
    )this.premiumType = '1';
    if(resData.InvitationStatus == '1'){
      if (
          !(this.premiumType == '1')
           &&
          !(this.microType == '1')
      )return this._service.error('Mandatory to select a recipient.');
    }

    if (this.show_div_invitation_change) {
      if(
        !resData.emailStatus
          &&
        !resData.smsStatus
      )return this._service.error('Mandatory to select either Email or SMS');
    }
    if(
      this.show_div_invitation_change
        &&
      resData.emailStatus
    ){
      if (!resData.mailtext.includes('&lt;link&gt;'))
        return this._service.error('Body must have <link> mentioned for placement of Campaign URL.');
    }
    let data = this.auth.getData();
    let reqData = {
        "campaign_id": this.campaignId,
        "start_date": moment(resData.scheduleTime.start).format('MM/DD/YYYY HH:mm:ss'),
        "end_date": moment(resData.scheduleTime.end).format('MM/DD/YYYY HH:mm:ss'),
        "invitation_status": resData.InvitationStatus,
        "send_micro": this.microType,
        "send_premium": this.premiumType,
        "send_email": resData.emailStatus == true ? '1' : '0',
        "email_sender": resData.senderName,
        "email_subject": resData.mailSubject,
        "reply_to":  resData.replyTo_list,
        "email_body": resData.mailtext,
        "send_sms": resData.smsStatus ? '1' : '0',
        "sms_body": resData.smsText,
        "is_draft": type,
        "created_by":data.name,
        "created_id":data._id
    }
    this._service.postApiWithHeader('/campaign/create_campaign', reqData, '3')
    .subscribe((response: any) => {
      if (response.status === 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          if (type == '1') {
            this._service.success('Campaign has been saved as draft');
            this.router.navigate(["campaign/list/drafts"]);
          } else {
            this._service.success('Campaign is now scheduled to go live on start date');
            this.router.navigate(["campaign/list/active"]);
          }
        } else {
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error('Something went wrong! Please try again.')
    });
  }

  //editor config options
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'no',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    //uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
  };
}
