import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
      FormBuilder,
      FormGroup,
      FormControl,
      FormArray, Validators,
      AbstractControl
} from '@angular/forms';
import { AngularCsv } from 'angular7-csv';
import { Options } from 'ng5-slider';
import { AdminService, AuthService } from 'src/app/services';
import { AngularEditorConfig } from '@kolkov/angular-editor';

const validateMentionHashtag = (control: AbstractControl): { [key: string]: boolean } | null => {
  let value = control.value;
  if (!value) {
    return null;
  }
  let valueLength = value.length;
  const HASHTAG_PATTERN = /^([a-zA-Z0-9_]+)$/;
  if(value.length < 2){
    return { "mentionHashtagMinLength": true };
  }
  if(value.length > 200){
    return { "mentionHashtagMaxLength": true };
  }
  if (value.charAt(0) === "@") {
    value = value.substr(1);
  }
  if (!HASHTAG_PATTERN.test(value)) {
    return { "mentionHashtagPattern": true };
  }
  return null;
};

@Component({
  selector: 'app-micro',
  templateUrl: './micro.component.html',
})

export class MicroComponent implements OnInit {
  @ViewChild('myInputFile',{ static: true }) myInputFileVariable: ElementRef;
  step: any;
  setStep: any;
  panelOpenState: true;
  animateSubmit: boolean = true;
  minValue: number = 0;
  maxValue: number = 0;
  options: Options = {
    floor: 0,
    ceil: 1000000
  };
  disableData: boolean = false;
  message: any;
  campaignId: any;
  document_csv: File;
  uploadSuccessCSV: boolean;
  error_msg_upload: boolean;
  error_message: string;
  validCSV: boolean;
  exts = ['.csv'];
  data: any;
  campaignDetaillData: any = [];
  campaignPlatformDetailData: any;
  campaignFormMicroStepTwo: FormGroup;
  platformName: any;
  csvNameMicro: string = "";
  microInfluencerData: any = "";
  filterInfluencerList: any = [];
  genderData: any = [];
  categoryData: any = [];
  ageData: any = [];
  country: any = "";
  statelist: any[];
  citylist: any[];
  state: any = "";
  countrylist: any[];
  city: any = "";
  countryName: any = "";
  cityName: any = "";
  stateName: any = "";
  campaignlist: any[];
  includeCampaign: any= [];
  excludeCampaign: any= [];
  camData: any;
  includeCampaignName: any= [];
  excludeCampaignName: any= [];
  catData: any = [];
  show_div_description_change:boolean = false;
  show_div_confirmation_uncheck_change: boolean = false;
  copyPlatform: boolean = false;
  show_div_confirmation_check_change: boolean = false;
  checkedInfluencerData: any = [];
  finalInfluencerData: any = [];
  influencerSelectedCountData: any = 0;
  influencerUnselectedCountData: any = 0;
  selectedPlatforms: any = [];
  campaignInfluencerDetailData: any = [];
  selectedInfluencer: any =[];
  platformID: any = "";
  searchText:any = "";
  breif_section_tab: boolean = false;
  breif_section: boolean = true;
  selectedBriefPlatforms: any = [];
  selectedBriefMaleDescription: any = [];
  selectedBriefMaleSample: any = [];
  selectedBriefFemaleDescription: any = [];
  selectedBriefFemaleSample: any = [];
  selectedBriefOtherDescription: any = [];
  selectedBriefOtherSample: any = [];
  selectedBriefDescription: any = [];
  selectedBriefSample: any = [];
  filteranimateSubmit: boolean = true;
  continueAnimate: boolean = true;
  adminData: any = [];
  upload_title:string = ""
  // constructor for dependencies injection
  constructor(
      private router: Router,
      private auth: AuthService,
      private fb: FormBuilder,
      private _service: AdminService,
      private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.campaignId = params.id;
      this.platformName = params.platform;
    });
    this.campaignDetails();
    this.camaignCreateMicroTwoFormVal();
    this.getCountryList();
    this.getCampaignList();
  }

  getAdminData = ():any => {
    if(!this.adminData.length)
      this.adminData = this.auth.getData();
    return this.adminData._id;
  }
  // Get the campaign data
  campaignDetails = () => {
    let reqData =
    {
      "admin_id": this.getAdminData(),
      "campaign_id": this.campaignId
    }
    this._service.postApiCommon('/campaign/details', reqData)
    .subscribe((response: any) => {
      if (response.status === 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.campaignDetaillData = this.data.result.campaign;
          this.bindMicroPlatformData(this.campaignDetaillData);
        } else {
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }
  // bind DB data
  bindMicroPlatformData = (platformAllData) => {
    this.campaignPlatformDetailData = platformAllData.platforms;
    let campaignMentionDetailData = platformAllData.mentions;
    let campaignPlatformSpecificDetailData = platformAllData.platform_specific;
    let campaignPlatformDecriptionDetailData = platformAllData.description;
    let campaignPlatformSampleDetailData = platformAllData.sample;

    let campaignPlatformMaleSampleDetailData = platformAllData.male_sample;
    let campaignPlatformFemaleSampleDetailData = platformAllData.female_sample;
    let campaignPlatformOtherSampleDetailData = platformAllData.other_sample;
    let campaignPlatformMaleDescriptionData = platformAllData.male_description;
    let campaignPlatformFemaleDescriptionData = platformAllData.female_description;
    let campaignPlatformOtherDescriptionData = platformAllData.other_description;

    this.campaignInfluencerDetailData = this.data.result.campaign_influencers;
    if(platformAllData.type && platformAllData.type == 'amplification'){
      this.campaignFormMicroStepTwo.get('post_per_kol').setValue('1');
      this.campaignFormMicroStepTwo.controls['post_per_kol'].disable();
    }
    // Set copy brief data for platfom condition
    if(
      platformAllData.micro_common_brief
      &&
      platformAllData.micro_common_brief != ""
    ){
        if(this.platformName == platformAllData.micro_common_brief){
          this.campaignFormMicroStepTwo.get('copyBriefAllPlatform').setValue(1);
          this.campaignFormMicroStepTwo.get('genderSpecific').enable();
          this.breif_section = true;
        }else{
          this.breif_section = false;
          this.editorConfig.editable = false;
          this.editorConfig.enableToolbar = false;
          this.editorConfig.showToolbar = false;
          this.campaignFormMicroStepTwo.get('genderSpecific').disable();
        }
    }else{
          this.campaignFormMicroStepTwo.get('genderSpecific').enable();
          this.breif_section = true;
    }
    //Selected platform
    if(
      this.campaignPlatformDetailData
      &&
      this.campaignPlatformDetailData.length
    ){
    this.selectedPlatforms = this.campaignPlatformDetailData.find(platform=>
      platform.type === 'micro' && platform.name == this.platformName);
    if(
      this.selectedPlatforms
      &&
      this.selectedPlatforms != undefined
    ){
      this.campaignFormMicroStepTwo.get('max_influencer').setValue(this.selectedPlatforms.number_influencer);
      this.campaignFormMicroStepTwo.get('post_per_kol').setValue(this.selectedPlatforms.number_post);
      this.campaignFormMicroStepTwo.get('conversations').setValue(this.selectedPlatforms.number_conversation);
      //Selected Influencer
      this.selectedInfluencer = this.campaignInfluencerDetailData.filter(platformData=>
        platformData.type === 'micro' && platformData.platform == this.platformName)
      this.filterInfluencerList = this.selectedInfluencer;
      if(this.filterInfluencerList.length > 0){
        this.influencerSelectedCountData = this.filterInfluencerList.length;
      }
      //Selected mentions
      let selectedPlatformsMention = campaignMentionDetailData.find(mention=>
      mention.platform == this.platformName);
      if(
        selectedPlatformsMention
        &&
        Object.keys(selectedPlatformsMention).length > 0
      ){
        const mTagData = <FormArray>this.campaignFormMicroStepTwo.controls['mention_hashtag_list'];
        selectedPlatformsMention.name.forEach(function (mvalue) {
        if(mvalue != "")
          mTagData.push(new FormControl(mvalue));
        });
      }
      //select gender specific description
      if(campaignPlatformSpecificDetailData){
        this.breif_section_tab = true;
        if(
          platformAllData.micro_common_brief
          &&
          platformAllData.micro_common_brief != ""
          &&
          this.platformName != platformAllData.micro_common_brief
        ){
          let selectedSpecificMention = campaignPlatformSpecificDetailData.find(specific=>
            specific.platform ==platformAllData.micro_common_brief)
          if(
            selectedSpecificMention
            &&
            selectedSpecificMention.is_gender === "1"
          ) {
            let selectedMaleDescription = campaignPlatformMaleDescriptionData.find(description=>
              description.platform ==platformAllData.micro_common_brief);
            let selectedMaleSample = campaignPlatformMaleSampleDetailData.find(sample=>
              sample.platform ==platformAllData.micro_common_brief);
            let selectedFemaleDescription = campaignPlatformFemaleDescriptionData.find(description=>
              description.platform ==platformAllData.micro_common_brief);
            let selectedFemaleSample = campaignPlatformFemaleSampleDetailData.find(sample=>
              sample.platform ==platformAllData.micro_common_brief);
            let selectedOtherDescription = campaignPlatformOtherDescriptionData.find(description=>
              description.platform ==platformAllData.micro_common_brief);
            let selectedOtherSample = campaignPlatformOtherSampleDetailData.find(sample=>
              sample.platform ==platformAllData.micro_common_brief);
            this.campaignFormMicroStepTwo.get('common_male').setValue(selectedMaleDescription.description);
            this.campaignFormMicroStepTwo.get('common_female').setValue(selectedFemaleDescription.description);
            this.campaignFormMicroStepTwo.get('common_other').setValue(selectedOtherDescription.description);
            this.campaignFormMicroStepTwo.get('sample_male').setValue(selectedMaleSample.sample);
            this.campaignFormMicroStepTwo.get('sample_female').setValue(selectedFemaleSample.sample);
            this.campaignFormMicroStepTwo.get('sample_other').setValue(selectedOtherSample.sample);
          }else{
            let selectedDescription = campaignPlatformDecriptionDetailData.find(description=>
              description.platform ==platformAllData.micro_common_brief);
            let selectedSample = campaignPlatformSampleDetailData.find(sample=>
              sample.platform ==platformAllData.micro_common_brief);

            this.campaignFormMicroStepTwo.get('common_description').setValue(selectedDescription.description);
            this.campaignFormMicroStepTwo.get('common_sample').setValue(selectedSample.sample);
          }
          if(selectedSpecificMention.is_gender === "1"){
            this.campaignFormMicroStepTwo.get('genderSpecific').setValue(true);
            this.show_div_description_change = true;
          }else{
            this.campaignFormMicroStepTwo.get('genderSpecific').setValue(false);
            this.show_div_description_change = false;
          }
        }else{
          let selectedSpecificMention = campaignPlatformSpecificDetailData.find(specific=>
            specific.platform == this.platformName)
          if(selectedSpecificMention && selectedSpecificMention.is_gender === "1") {
            let selectedMaleDescription = campaignPlatformMaleDescriptionData.find(description=>
              description.platform == this.platformName);
            let selectedMaleSample = campaignPlatformMaleSampleDetailData.find(sample=>
              sample.platform == this.platformName);
            let selectedFemaleDescription = campaignPlatformFemaleDescriptionData.find(description=>
              description.platform == this.platformName);
            let selectedFemaleSample = campaignPlatformFemaleSampleDetailData.find(sample=>
              sample.platform == this.platformName);
            let selectedOtherDescription = campaignPlatformOtherDescriptionData.find(description=>
              description.platform == this.platformName);
            let selectedOtherSample = campaignPlatformOtherSampleDetailData.find(sample=>
              sample.platform == this.platformName);
            this.campaignFormMicroStepTwo.get('common_male').setValue(selectedMaleDescription.description);
            this.campaignFormMicroStepTwo.get('common_female').setValue(selectedFemaleDescription.description);
            this.campaignFormMicroStepTwo.get('common_other').setValue(selectedOtherDescription.description);
            this.campaignFormMicroStepTwo.get('sample_male').setValue(selectedMaleSample.sample);
            this.campaignFormMicroStepTwo.get('sample_female').setValue(selectedFemaleSample.sample);
            this.campaignFormMicroStepTwo.get('sample_other').setValue(selectedOtherSample.sample);
          }else{
            let selectedDescription = campaignPlatformDecriptionDetailData.find(description=>
              description.platform == this.platformName);
            let selectedSample = campaignPlatformSampleDetailData.find(sample=>
              sample.platform == this.platformName);

            this.campaignFormMicroStepTwo.get('common_description').setValue(selectedDescription.description);
            this.campaignFormMicroStepTwo.get('common_sample').setValue(selectedSample.sample);
          }
          if(selectedSpecificMention.is_gender === "1"){
            this.campaignFormMicroStepTwo.get('genderSpecific').setValue(true);
            this.show_div_description_change = true;
          }else{
            this.campaignFormMicroStepTwo.get('genderSpecific').setValue(false);
            this.show_div_description_change = false;
          }
        }
      }
    }else{
      this.selectedInfluencer = [];
      if(
        platformAllData.micro_common_brief
        &&
        platformAllData.micro_common_brief != ""
        &&
        this.platformName != platformAllData.micro_common_brief
      ){
        this.breif_section_tab = true;
        let selectedSpecificMention = campaignPlatformSpecificDetailData.find(specific=>
          specific.platform ==platformAllData.micro_common_brief)
        if(selectedSpecificMention && selectedSpecificMention.is_gender === "1") {
          let selectedMaleDescription = campaignPlatformMaleDescriptionData.find(description=>
            description.platform ==platformAllData.micro_common_brief);
          let selectedMaleSample = campaignPlatformMaleSampleDetailData.find(sample=>
            sample.platform ==platformAllData.micro_common_brief);
          let selectedFemaleDescription = campaignPlatformFemaleDescriptionData.find(description=>
            description.platform ==platformAllData.micro_common_brief);
          let selectedFemaleSample = campaignPlatformFemaleSampleDetailData.find(sample=>
            sample.platform ==platformAllData.micro_common_brief);
          let selectedOtherDescription = campaignPlatformOtherDescriptionData.find(description=>
            description.platform ==platformAllData.micro_common_brief);
          let selectedOtherSample = campaignPlatformOtherSampleDetailData.find(sample=>
            sample.platform ==platformAllData.micro_common_brief);
          this.campaignFormMicroStepTwo.get('common_male').setValue(selectedMaleDescription.description);
          this.campaignFormMicroStepTwo.get('common_female').setValue(selectedFemaleDescription.description);
          this.campaignFormMicroStepTwo.get('common_other').setValue(selectedOtherDescription.description);
          this.campaignFormMicroStepTwo.get('sample_male').setValue(selectedMaleSample.sample);
          this.campaignFormMicroStepTwo.get('sample_female').setValue(selectedFemaleSample.sample);
          this.campaignFormMicroStepTwo.get('sample_other').setValue(selectedOtherSample.sample);
        }else{
          let selectedDescription = campaignPlatformDecriptionDetailData.find(description=>
            description.platform ==platformAllData.micro_common_brief);
          let selectedSample = campaignPlatformSampleDetailData.find(sample=>
            sample.platform ==platformAllData.micro_common_brief);

          this.campaignFormMicroStepTwo.get('common_description').setValue(selectedDescription.description);
          this.campaignFormMicroStepTwo.get('common_sample').setValue(selectedSample.sample);
        }
        if(selectedSpecificMention.is_gender === "1"){
          this.campaignFormMicroStepTwo.get('genderSpecific').setValue(true);
          this.show_div_description_change = true;
        }else{
          this.campaignFormMicroStepTwo.get('genderSpecific').setValue(false);
          this.show_div_description_change = false;
        }
      }
    }
    }else{
      this.selectedPlatforms = [];
    }
  }

  // Mention visible
  mention_visible = () => {
    return!(
      (this.platformName == 'youtube')
        ||
      (this.platformName == 'twitter' && this.campaignDetaillData.type == 'amplification')
    )
  }
  //form validation
  camaignCreateMicroTwoFormVal = () => {
    this.campaignFormMicroStepTwo = new FormGroup({
      max_influencer: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]*')])),
      post_per_kol: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]*')])),
      conversations:new FormControl({ value: '', disabled: true }),
      gender: this.fb.array([]),
      category: this.fb.array([]),
      age: this.fb.array([]),
      country: new FormControl('', Validators.compose([Validators.required])),
      state: new FormControl(''),
      city: new FormControl(''),
      campaign: new FormControl(''),
      campaignStatus:new FormControl(''),
      genderSpecific:new FormControl(''),
      mention_hashtag: new FormControl('',Validators.compose([validateMentionHashtag ])),
      mention_hashtag_list:this.fb.array([]),
      common_description:new FormControl('',Validators.compose([Validators.required])),
      common_sample:new FormControl(''),
      common_male:new FormControl('',Validators.compose([Validators.required])),
      sample_male:new FormControl(''),
      common_female:new FormControl('',Validators.compose([Validators.required])),
      sample_female:new FormControl(''),
      common_other:new FormControl('',Validators.compose([Validators.required])),
      sample_other:new FormControl(''),
      copyBriefAllPlatform:new FormControl(''),
      checkedInfluencer: this.fb.array([])
    });
  }

  //Calculate data conversation
  calculateConversation = (type:String) => {
    let max = this.campaignFormMicroStepTwo.get('max_influencer').value;
    let post = this.campaignFormMicroStepTwo.get('post_per_kol').value;

    if(this.campaignDetaillData.status === "3"){
      if(this.selectedPlatforms && type === "2" && this.selectedPlatforms.number_post > post){
        this._service.error('Post count can only be decreased.');
        this.campaignFormMicroStepTwo.get('post_per_kol').setValue(this.selectedPlatforms.number_post);
        return;
      }
      if(
        this.selectedInfluencer
         &&
        this.selectedInfluencer.length > 0 && type === "1"
      ){
        let selectedAcceptedInfluencer = this.campaignInfluencerDetailData.filter(platformData=>
          platformData.type === 'micro' && platformData.platform == this.platformName && platformData.action == 'accepted');
          if((
            max < selectedAcceptedInfluencer.length)
              &&
            this.campaignFormMicroStepTwo.get('max_influencer').valid
          ){
            this._service.error(
              'Max. influencer count is lesser than the influencers who have already accepted the campaign. Increase the number over & above'+ selectedAcceptedInfluencer.length
            );
            this.campaignFormMicroStepTwo.get('max_influencer').setValue(this.selectedPlatforms.number_influencer);
            return;
          }
      }
    }
    if(
      max
       &&
      this.campaignFormMicroStepTwo.get('max_influencer').valid
    ){
      if(
        post
          &&
        this.campaignFormMicroStepTwo.get('post_per_kol').valid
      ){
        let calData = max*post;
        this.campaignFormMicroStepTwo.get('conversations').setValue(calData);
      }
      if(
        post
         &&
        this.campaignDetaillData.type == 'amplification'
      ){
        let calData = max*post;
        this.campaignFormMicroStepTwo.get('conversations').setValue(calData);
      }

    }
  }
  // CSV upload
  uploadCSV = $event => {
    if($event.target.files.length == 0)return;
    this.document_csv = <File>$event.target.files[0];
    let name = this.document_csv.name;
    let type = this.document_csv.type;
    let size = this.document_csv.size;
    if (size > 10242880) {
      this._service.error("File is too large. Upload file less than 10 MB");
      this.document_csv = null;
      this.csvNameMicro = "";
      return;
    }
    this.validCSV = (new RegExp('(' + this.exts.join('|').replace(/\./g, '\\.') + ')$')).test(name);
    if (!this.validCSV) {
      this._service.error("Upload only CSV format.");
      this.document_csv = null;
      this.csvNameMicro = "";
      return;
    } else {
      this.uploadSuccessCSV = true;
      this.csvNameMicro = name;
    }
  }

  // CSV upload submit
  microFileUploadSubmit = () => {
    if(this.document_csv == null)
      return this._service.error("Must upload a CSV first.");
    const form = new FormData();
    form.append('admin_id', this.getAdminData());
    form.append('campaign_id', this.campaignId);
    form.append('platform', this.platformName);
    form.append('file', this.document_csv, this.document_csv.name);
    this.filterInfluencerList = [];
    this.animateSubmit = false;
    this._service.postFormApi('/campaign/upload/csv', form)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.animateSubmit = true
        this.data = response.body;
        if (this.data.responseCode == 200) {
          //this.document_csv = null;
          //this.csvNameMicro = "";
          this.microInfluencerData = this.data.result;
          this.filterInfluencerList = this.microInfluencerData.influencers;
          if(this.filterInfluencerList.length > 0){
            this.influencerSelectedCountData = this.filterInfluencerList.length;
            this.upload_title = "Excel Uploads";
          }else this.upload_title = "";
        } else {
          this._service.error(this.data.responseMessage);
        }

      }
    }, error => {
      this.animateSubmit = true
      this._service.error("Something went wrong! Please try again.");
    });
  }
//Clean All filter data
  clearAllFilterData = ():void =>{
      this.document_csv = null;
      this.csvNameMicro = "";
      this.myInputFileVariable.nativeElement.value = "";
      this.microInfluencerData=[];
  }

// Download Sample csv for micro influencer
  downloadSample = ():void => {
    let data = [
      {
        name: "Shiwani Dhiman",
        handle: "ShiwaniDhiman5"
      },
      {
        name: "M Gawande",
        handle: "murlidhargawan2"
      },
      {
        name: "Ahva",
        handle: "ahvadsouzaa"
      },
    ];
    let options = {
      showLabels: true,
      headers: ["name", "handle"]
    };
    new AngularCsv(data, 'MicroSample',options);
  }

  // =======================================================*****=======================================================//

  //gender store in array
  onChangeGender = event => {
    if (event.checked) {
     this.genderData.push(event.source.value);
    } else {
      const i = this.genderData.indexOf(event.source.value);
      this.genderData.splice(i, 1);
    }
  }
  //category store in array
  onChangeCategory = event => {
    if (event.checked) {
      this.categoryData.push(event.source.value);
    } else {
      const i = this.categoryData.indexOf(event.source.value);
      this.categoryData.splice(i, 1);
    }
  }
  //age store in array
  onChangeAge = event => {
    if (event.checked) {
      this.ageData.push(event.source.value);
    } else {
      const i = this.ageData.indexOf(event.source.value);
      this.ageData.splice(i, 1);
    }
  }
  // change state list behalf country select
  countrySelectChange = selectCountryName => {
    if (selectCountryName) {
      this.campaignFormMicroStepTwo.get('state').setValue('');
      this.campaignFormMicroStepTwo.get('city').setValue('');
      this.country = selectCountryName;
      this.statelist = [];
      this.citylist = [];
      this.getStateList();
    }
  }
  // state change behalf country
  stateSelectChange = selectStateName => {
    if (selectStateName) {
        this.campaignFormMicroStepTwo.get('city').setValue('');
        this.state = selectStateName;
        this.citylist = [];
        this.getCityList();
    }
  }
    // City change behalf country
  citySelectChange = selectCityName => {
    if (selectCityName) {
        this.city = selectCityName;
    }
  }
  // Get country list from database
  getCountryList = ():void => {
    this.countrylist = [];
    this._service.getApi('/countries')
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.countrylist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.countrylist.push(this.data.result[i].name);
          }
        } else {
          this.countrylist = [];
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });

  }
  // Get state list from database
  getStateList = ():void => {
    let data = {
      'country': this.country
    }
    this.statelist = [];
    this._service.postApi('/statelist', data)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.statelist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.statelist.push(this.data.result[i].name);
          }
        } else {
          this.statelist = [];
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });

  }
  // Get city List from database
  getCityList = ():void => {
    let data = {
      'state': this.state
    }
    this.citylist = [];
    this._service.postApi('/cities', data)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.citylist = [];
          for (let i = 0; i < this.data.result.length; i++) {
            this.citylist.push(this.data.result[i].name);
          }
        } else {
          this.citylist = [];
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });

  }
  //Location show
  locationSelect = ():void => {
    if(
      this.country == ""
       &&
      this.state == "" && this.city == ""
    ){
      this._service.error('Please select location.');return;
    } else {
    if(
      this.countryName
         &&
      (this.country !== this.countryName)
    ){
        this.countryName = this.country;
        this.stateName = this.state = '';
        this.cityName = this.city = '';
        return;
      }
      this.countryName = this.country;
      this.stateName = this.state;
      this.cityName = this.city;
      this.campaignFormMicroStepTwo.get('country').reset();
      this.campaignFormMicroStepTwo.get('state').reset();
      this.campaignFormMicroStepTwo.get('city').reset();
    }
  }
  // Get campaign List from database
  getCampaignList = ():void => {
    let adminData = this.auth.getData();
    let data = {
      'admin_id': adminData._id
    }
    this.campaignlist = [];
    this._service.postApiCommon('/campaign/completed', data)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.camData = response.body.result;
           this.campaignlist = [];
          for (let i = 0; i < this.data.result.length; i++) {
             this.campaignlist.push({id: this.data.result[i]._id, name: this.data.result[i].title})
          }
        } else {
           this.campaignlist = [];
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });

  }
  //select campaign status wise
  campaignSelectChange = ():any => {
    let campaignId = this.campaignFormMicroStepTwo.get('campaign').value;
    let campaignStatus = this.campaignFormMicroStepTwo.get('campaignStatus').value;
    if (campaignId == '')
      return this._service.error('Please select previous campaign.');
    if (campaignStatus == '')
      return this._service.error('Please choose action type.');
    if(
      this.includeCampaign.filter(x => x == campaignId) != ""
       ||
      this.excludeCampaign.filter(x => x === campaignId) != ""
    ){
      this.campaignFormMicroStepTwo.get('campaign').reset();
      this._service.error('Campaign Already Added');
      return;
    }
    if (campaignStatus == '1') {
      this.includeCampaign.push(campaignId);
      let campName = this.camData.filter(list => list._id == campaignId);
      this.includeCampaignName.push(campName[0].title);
      this.campaignFormMicroStepTwo.get('campaign').reset();
    } else {
      this.excludeCampaign.push(campaignId);
      let campName = this.camData.filter(list => list._id == campaignId);
      this.excludeCampaignName.push(campName[0].title);
      this.campaignFormMicroStepTwo.get('campaign').reset();
    }
  }
// delete selected campaign in the list
  deleteSelectCampaign = (index:any, type:any) => {
    if(type == '1'){
      this.includeCampaign.splice(index, 1);
      this.includeCampaignName.splice(index, 1);
    }else{
      this.excludeCampaign.splice(index, 1);
      this.excludeCampaignName.splice(index, 1);
    }
    this.campaignFormMicroStepTwo.get('campaign').reset();
  }
// clear manual filter selected
  clearFilterData = ():void => {
      this.categoryData = [];
      this.minValue = 0;
      this.maxValue= 0;
      this.genderData= [];
      this.ageData= [];
      this.clearLocationItems('country');
      this.includeCampaign= [];
      this.excludeCampaign = [];
      this.microInfluencerData=[];
  }

  //micro manual filter submit
  microfilterSubmit = ():any => {
    this.campaignFormMicroStepTwo.get('max_influencer').setValidators(null);
    this.campaignFormMicroStepTwo.get('max_influencer').updateValueAndValidity();
    this.campaignFormMicroStepTwo.get('post_per_kol').setValidators(null);
    this.campaignFormMicroStepTwo.get('post_per_kol').updateValueAndValidity();
   // this.campaignFormMicroStepTwo.get('conversations').setValidators(null);
    //this.campaignFormMicroStepTwo.get('conversations').updateValueAndValidity();
    this.clearAllFilterData();
    if(
      this.categoryData.length == 0
       &&
      this.genderData.length == 0
       &&
      this.ageData.length == 0
        &&
      this.country == ""
       &&
      this.includeCampaign.length == 0
        &&
      this.maxValue == 0
    )return this._service.error('Please select any filter option.');
    if(this.includeCampaign.length > 0){
      this.campaignFormMicroStepTwo.get('country').setValidators(null);
      this.campaignFormMicroStepTwo.get('country').updateValueAndValidity();
    }
    if(
      (this.categoryData.length > 0
        ||
      this.genderData.length > 0
        ||
      this.ageData.length > 0
        ||
      this.minValue >= 0
        ||
      this.maxValue >= 0)
    ){
      if(this.country == ""){
        this.campaignFormMicroStepTwo.get('country').setValidators([Validators.required]);
        this.campaignFormMicroStepTwo.get('country').updateValueAndValidity();
        this._service.error('select location filter option.');
        return;
      }
    }
    if(this.categoryData.length == 0){
      this.catData = ['C','D'];
    }else{
      this.catData = this.categoryData;
    }
    let reqData = {
      platform:this.platformName,
      campaign_id:this.campaignId,
      type:'micro',
      category:this.catData ,
      followers_less_than:this.minValue,
      followers_more_than:this.maxValue,
      gender: this.genderData,
      age: this.ageData,
      country:this.country,
      state:this.state,
      city:this.city,
      include_campaign:this.includeCampaign,
      exclude_campaign: this.excludeCampaign
    }
    this.influencerUnselectedCountData = 0;
    this.influencerSelectedCountData = 0;
    this.filteranimateSubmit = false;
    this._service.postApiCommon('/campaign/searchmicroinfluencer', reqData)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.filteranimateSubmit = true;
        this.data = response.body;
        this.filterInfluencerList = [];
        if (this.data.responseCode == 200) {
          this.filterInfluencerList = this.data.result;
          if(this.filterInfluencerList.length > 0){
            this.influencerSelectedCountData = this.filterInfluencerList.length;
            this.upload_title = "Filtered Results";
          }else{
            this.filterInfluencerList = [];
            this.upload_title = "";
            this._service.error(this.data.responseMessage);
          }
        } else {
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this.filteranimateSubmit = true;
      this._service.error("Something went wrong! Please try again.");
    });
  }
//====================================================***********====================================================//
  //Add Mention tag in form array data
  addMentionTagFormArray = event => {
      const mTagData = <FormArray>this.campaignFormMicroStepTwo.controls['mention_hashtag_list'];
      const mDuplicate = this.campaignFormMicroStepTwo.get('mention_hashtag_list').value;
      let mention_data = event.target.value;
      if (event.target.value.charAt(0) === "@")
        mention_data = event.target.value.substr(1);
      if(this.campaignFormMicroStepTwo.get('mention_hashtag').valid){
        if(mDuplicate.length < 5){
          if(!mDuplicate.some(x => x === mention_data)){
            if (event.target.value.charAt(0) === "@") {
              mTagData.push(new FormControl(mention_data));
            } else {
              mTagData.push(new FormControl(mention_data));
            }
            this.campaignFormMicroStepTwo.get('mention_hashtag').reset();
          } else {
            return this._service.error('This handle is already mentioned');
          }
        }else{
          return this._service.error('Maximum 5 handles can be saved');
        }
      }else{
        return this._service.error('Please enter valid handle');
      }

  }
  //Remove Secondry Aarry in form data
  removeSecondaryTagFormArray = index => {
      const mTagData = <FormArray>this.campaignFormMicroStepTwo.controls['mention_hashtag_list'];
      mTagData.removeAt(index);
  }
  // hide and show text fild behalf
  genderSpecificChange = event => {
    if(event.checked == true)
      this.show_div_description_change = true;
    else
      this.show_div_description_change = false;
  }

  // Copy brief content for all platform
  clickCopyBriefAllPlatform = event => {
    if(event.checked == true)
      this.show_div_confirmation_check_change = true;
    else
      this.show_div_confirmation_uncheck_change = true;
  }
  // Close modal
  modalClose = ():void => {
    if (this.copyPlatform == true){
      this.copyPlatform = false;
      this.show_div_confirmation_check_change = false;
    }else{
      this.copyPlatform = true;
      this.show_div_confirmation_uncheck_change = false;
    }
  }
  //Confirm select for brief for copy
  modalConfirm = ():void => {
    this.show_div_confirmation_check_change = false;
    this.show_div_confirmation_uncheck_change = false;
  }
  // Unselected influencer from filter influencer list
  onChangecheckedInfluencer = (event, index) => {
    if (!event.checked) {
      this.checkedInfluencerData.push(index);
      this.influencerUnselectedCountData ++;
      this.influencerSelectedCountData --;
    } else {
      this.checkedInfluencerData.splice(this.checkedInfluencerData.indexOf(index), 1);
      this.influencerUnselectedCountData --;
      this.influencerSelectedCountData ++;
    }
  }

  //Submit Step two micro form final
  campaignTwoMicroFormSubmit = (reqData, type):any => {
    this.campaignFormMicroStepTwo.get('country').setValidators(null);
    this.campaignFormMicroStepTwo.get('country').updateValueAndValidity();
    if(
      this.campaignDetaillData
        &&
      this.campaignDetaillData.micro_common_brief
    ){
      if(this.campaignDetaillData.micro_common_brief != this.platformName){
        this.platformDifferentValidation();
        this.briefCommonData(this.campaignDetaillData.micro_common_brief);
      }else{
        this.briefValidationAdd(reqData);
      }
    }else{
      this.briefValidationAdd(reqData);
    }
    if (this.filterInfluencerList.length > 0) {
      this.finalInfluencerData =[];
      //this.finalInfluencerData = [...this.filterInfluencerList];
      const influencer_length = this.filterInfluencerList.length;
      for (var i = 0; i < influencer_length; i++) {
        if(!this.checkedInfluencerData.includes(i))
          this.finalInfluencerData.push(this.filterInfluencerList[i]);
      }
      if (this.finalInfluencerData.length <= 0)
        return this._service.error('Please select influencers.');
      else{
          if(reqData.max_influencer < this.finalInfluencerData.length)
            return this._service.error('Selected influecner not maximum than max influencer count.');
      }
    }else
      return this._service.error('Please filter influencers.');
    if(
      !reqData.common_description
       &&
      !reqData.common_male
       &&
      this.breif_section
    ){
      this.breif_section_tab = true;
      this._service.error('Provide brief for influencers');
      return ;
    }

    if (this.campaignFormMicroStepTwo.invalid)return;
    let conversationValue = this.campaignFormMicroStepTwo.getRawValue().conversations;
    let data = {
      "campaign_id":this.campaignId,
      "platform": this.platformName,
      "platform_id":(this.selectedPlatforms &&  this.selectedPlatforms != undefined)? this.selectedPlatforms._id:"",
      "type":"micro",
      "max_influencer":reqData.max_influencer,
      "post_per_kol":(this.campaignDetaillData.type == 'amplification') ? '1': reqData.post_per_kol,
      "genderSpecific": reqData.genderSpecific ? "1":"0",
      "conversations":conversationValue || 0,
      "mentions":reqData.mention_hashtag_list.length ? reqData.mention_hashtag_list : '',
      "common_description":Object.keys(this.selectedBriefDescription).length ? this.selectedBriefDescription.description : reqData.common_description,
      "male_description":Object.keys(this.selectedBriefMaleDescription).length ? this.selectedBriefMaleDescription.description : reqData.common_male,
      "female_description": Object.keys(this.selectedBriefFemaleDescription).length ?  this.selectedBriefFemaleDescription.description : reqData.common_female,
      "other_description":Object.keys(this.selectedBriefOtherDescription).length ? this.selectedBriefOtherDescription.description : reqData.common_other,
      "common_sample":Object.keys(this.selectedBriefSample).length ? this.selectedBriefSample.sample : reqData.common_sample,
      "male_sample": Object.keys(this.selectedBriefMaleSample).length ?  this.selectedBriefMaleSample.sample : reqData.sample_male,
      "female_sample":Object.keys(this.selectedBriefFemaleSample).length ? this.selectedBriefFemaleSample.sample : reqData.sample_female,
      "other_sample":Object.keys(this.selectedBriefOtherSample).length ?  this.selectedBriefOtherSample.sample : reqData.sample_other,
      "copy_brief_description":reqData.copyBriefAllPlatform ? this.platformName : "",
      "status":type,
      "influencers":this.finalInfluencerData
    }
    if(type !== '1')
      this.continueAnimate = false;
    this._service.postApiWithHeader('/campaign/create_campaign',data, '2B')
      .subscribe((response: any) => {
        if (response.status == 200) {
          this.data = response.body;
          if (this.data.responseCode == 200) {
            this.continueAnimate = true;
            if(type == '1'){
              this._service.success(this.data.responseMessage);
              this.router.navigate(['campaign/list/drafts']);
            }else{
              this._service.success(this.data.responseMessage);
              this.router.navigate(['campaign/step-two/',this.campaignId]);
            }
          } else {
            this.continueAnimate = true;
            this._service.error(this.data.responseMessage)
          }
        }
      }, error => {
        this.continueAnimate = true;
        this._service.error('Something went wrong! Please try again.')
      });
  }
//Brief section validation
  briefValidationAdd = ({genderSpecific = '0'}) => {
      if(
        this.campaignDetaillData.type
          &&
        this.campaignDetaillData.type == 'amplification'
      ) {
        this.campaignFormMicroStepTwo.get('common_description').setValidators(null);
        this.campaignFormMicroStepTwo.get('common_description').updateValueAndValidity();
        this.campaignFormMicroStepTwo.get('common_male').setValidators(null);
        this.campaignFormMicroStepTwo.get('common_male').updateValueAndValidity();
        this.campaignFormMicroStepTwo.get('common_female').setValidators(null);
        this.campaignFormMicroStepTwo.get('common_female').updateValueAndValidity();
        this.campaignFormMicroStepTwo.get('common_other').setValidators(null);
        this.campaignFormMicroStepTwo.get('common_other').updateValueAndValidity();
      }else if(genderSpecific == '1') {
        this.campaignFormMicroStepTwo.get('common_description').setValidators(null);
        this.campaignFormMicroStepTwo.get('common_description').updateValueAndValidity();
        this.campaignFormMicroStepTwo.get('common_male').setValidators([Validators.required]);
        this.campaignFormMicroStepTwo.get('common_male').updateValueAndValidity();
        this.campaignFormMicroStepTwo.get('common_female').setValidators([Validators.required]);
        this.campaignFormMicroStepTwo.get('common_female').updateValueAndValidity();
        this.campaignFormMicroStepTwo.get('common_other').setValidators([Validators.required]);
        this.campaignFormMicroStepTwo.get('common_other').updateValueAndValidity();
      } else {
        this.campaignFormMicroStepTwo.get('common_description').setValidators([Validators.required]);
        this.campaignFormMicroStepTwo.get('common_description').updateValueAndValidity();
        this.campaignFormMicroStepTwo.get('common_male').setValidators(null);
        this.campaignFormMicroStepTwo.get('common_male').updateValueAndValidity();
        this.campaignFormMicroStepTwo.get('common_female').setValidators(null);
        this.campaignFormMicroStepTwo.get('common_female').updateValueAndValidity();
        this.campaignFormMicroStepTwo.get('common_other').setValidators(null);
        this.campaignFormMicroStepTwo.get('common_other').updateValueAndValidity();
      }
  }
  //Breif validation
  platformDifferentValidation = ():any =>{
    this.campaignFormMicroStepTwo.get('common_description').setValidators(null);
    this.campaignFormMicroStepTwo.get('common_description').updateValueAndValidity();
    this.campaignFormMicroStepTwo.get('common_male').setValidators(null);
    this.campaignFormMicroStepTwo.get('common_male').updateValueAndValidity();
    this.campaignFormMicroStepTwo.get('common_female').setValidators(null);
    this.campaignFormMicroStepTwo.get('common_female').updateValueAndValidity();
    this.campaignFormMicroStepTwo.get('common_other').setValidators(null);
    this.campaignFormMicroStepTwo.get('common_other').updateValueAndValidity();
  }
// Get brief commom Data
  briefCommonData = (commonPlatformName):void => {
    let campaignPlatformSpecificBriefDetailData = this.campaignDetaillData.platform_specific;
    let campaignPlatformDecriptionBriefDetailData = this.campaignDetaillData.description;
    let campaignPlatformSampleBriefDetailData = this.campaignDetaillData.sample;
    let campaignPlatformMaleSampleBriefDetailData = this.campaignDetaillData.male_sample;
    let campaignPlatformFemaleSampleBriefDetailData = this.campaignDetaillData.female_sample;
    let campaignPlatformOtherSampleBriefDetailData = this.campaignDetaillData.other_sample;
    let campaignPlatformMaleBriefDescriptionData = this.campaignDetaillData.male_description;
    let campaignPlatformFemaleBriefDescriptionData = this.campaignDetaillData.female_description;
    let campaignPlatformOtherBriefDescriptionData = this.campaignDetaillData.other_description;
    //select gender specific description
    if(campaignPlatformSpecificBriefDetailData){
      this.breif_section_tab = true;
      let selectedSpecificData = campaignPlatformSpecificBriefDetailData.find(specific=>
        specific.platform == commonPlatformName)
      if(selectedSpecificData && selectedSpecificData.is_gender === "1") {
        this.selectedBriefMaleDescription = campaignPlatformMaleBriefDescriptionData.find(description=>
          description.platform == commonPlatformName);
        this.selectedBriefMaleSample = campaignPlatformMaleSampleBriefDetailData.find(sample=>
          sample.platform == commonPlatformName);
        this.selectedBriefFemaleDescription = campaignPlatformFemaleBriefDescriptionData.find(description=>
          description.platform == commonPlatformName);
        this.selectedBriefFemaleSample = campaignPlatformFemaleSampleBriefDetailData.find(sample=>
          sample.platform == commonPlatformName);
        this.selectedBriefOtherDescription = campaignPlatformOtherBriefDescriptionData.find(description=>
          description.platform == commonPlatformName);
        this.selectedBriefOtherSample = campaignPlatformOtherSampleBriefDetailData.find(sample=>
          sample.platform == commonPlatformName);
      }else{
        this.selectedBriefDescription = campaignPlatformDecriptionBriefDetailData.find(description=>
          description.platform == commonPlatformName);
        this.selectedBriefSample = campaignPlatformSampleBriefDetailData.find(sample=>
          sample.platform == commonPlatformName);
      }
    }
  }

// Location filter clean
  clearLocationItems = (key=''):void => {
    if(key === 'country'){
      this.campaignFormMicroStepTwo.get('country').reset();
      this.campaignFormMicroStepTwo.get('state').reset();
      this.campaignFormMicroStepTwo.get('city').reset();
      this.countryName = '';
      this.stateName = '';
      this.cityName = '';
      this.country = '';
      this.state = '';
      this.city = '';
    }
    if(key === 'state'){
      this.campaignFormMicroStepTwo.get('state').reset();
      this.campaignFormMicroStepTwo.get('city').reset();
      this.stateName = '';
      this.cityName = '';
      this.state = '';
      this.city = '';
    }
    if(key === 'city'){
      this.campaignFormMicroStepTwo.get('city').reset();
      this.cityName = '';
      this.city = '';
    }
  }
  //editor config options
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'no',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    //uploadUrl: '',
    sanitize: true,
    toolbarPosition: 'top',
  };

  //Clear search text
  clearSearchText = ():void =>{
    this.searchText = '';
  }
}
