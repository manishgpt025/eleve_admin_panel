import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { AngularCsv } from 'angular7-csv';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { AdminService, AuthService } from 'src/app/services';

@Component({
  selector: 'app-premium',
  templateUrl: './premium.component.html',
})
export class PremiumComponent implements OnInit {
  @ViewChild('f', { static: true }) premiumForm;
  @ViewChild('myInputFile',{ static: true }) myInputFileVariable: ElementRef;
  campaignId: any;
  message: string;
  document_csv: File;
  uploadSuccessCSV: boolean;
  error_msg_upload: boolean;
  error_message: string;
  validCSV: boolean;
  exts = ['.csv'];
  platformName: any;
  animateSubmit: boolean = true;
  csvNamePremium: string;
  premiumInfluencerData: any = [];
  filterInfluencerList: any = [];
  data: any;
  campaignFormPremiumStepTwo: FormGroup;
  influencerListData: object[] = [];
  premiumData: any;
  premiumreqData:  object[] = [];
  $currencyCode: Observable<Array<any>>;
  influencerDataList: any;
  influencerDataFilter: any;
  campaignDetaillData: any = [];
  campaignInfluencerDetailData: any;
  selectedPlatforms: any;
  selectedInfluencer: any = [];
  duplicateInfluencer: string = '';
  indexOld: any;
  breifValidation: any;
  breif_selector: String = '0';
  validationData: any;
  breifCheck: boolean = false;
  adminData: any = [];
  history = [];
  previousRoute: string;
  previousRouteUrl: boolean = false;
  campaignFormArrayPremium: any;
  // constructor for dependencies injection
  constructor(
    private router: Router,
    private el: ElementRef,
    private auth: AuthService,
    private _FB: FormBuilder,
    private _service: AdminService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.campaignId = params.id;
      this.platformName = params.platform;
    });
    this.campaignDetails();
    this.$currencyCode = this._service.getApi('/currency/list').pipe(map((results) => results.body.result));
    this.camaignCreatePremiumTwoFormVal();
    this.influencerList();
    this.getPreviousUrlData();
  }
  getPreviousUrlData = ():any =>{
    let url = this.auth.getPreviousUrl();
    if(url.includes('campaign/kol'))
      this.previousRouteUrl = true;
    else
      this.previousRouteUrl = false;
  }

  getAdminData = ():any => {
    if(!this.adminData.length)
      this.adminData = this.auth.getData();
    return this.adminData._id;
  }
  // Get the campaign detail
  campaignDetails() {
    let reqData =
    {
      "admin_id": this.getAdminData(),
      "campaign_id": this.campaignId
    }
    this._service.postApiCommon('/campaign/details', reqData)
    .subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.campaignDetaillData = this.data.result.campaign;
          if(
            this.campaignDetaillData.type
             &&
            this.campaignDetaillData.type == 'amplification'
          )this.validationData = Validators.compose([Validators.minLength(50)]);
          else
            this.validationData =  Validators.compose([Validators.required,Validators.minLength(50)]);
          this.breifValidation = this.validationData;
          let campaignPlatformDetailData = this.campaignDetaillData.platforms;
          let commomPremiumBreifData = this.campaignDetaillData.premium_common_brief;
          this.campaignInfluencerDetailData = this.data.result.campaign_influencers;
          if(
            campaignPlatformDetailData
             &&
            campaignPlatformDetailData.length
          ){
            this.selectedPlatforms = campaignPlatformDetailData.find(platform=>
              platform.type === 'premium' && platform.name == this.platformName)
            if(this.selectedPlatforms){
              //Selected Influencer
            this.selectedInfluencer = this.campaignInfluencerDetailData.filter(platformData=>
              platformData.type === 'premium' && platformData.platform == this.platformName)
            this.filterInfluencerList = this.selectedInfluencer;
                 //Common Breif for all
            let commomBreifForAll = commomPremiumBreifData.find(premiumData=> premiumData.platform == this.platformName);
            if(
              commomBreifForAll
               &&
              commomBreifForAll.status == '1'
            ){
                this.validationData = null;
                this.breif_selector = '1';
                this.breifCheck = true;
              }
              if(
                this.filterInfluencerList
                  &&
                this.filterInfluencerList.length > 0
              ){
                if(this.influencerRows.length > 0)
                  this.removeAllRows();
                for(let i = 0; i < this.filterInfluencerList.length; i++ ){
                  if(i > 0)
                      this.influencerRows.push(this.influencerFields());
                  <FormArray>this.campaignFormPremiumStepTwo['controls'].campaignFormArrayPremium['controls'][i]
                  .patchValue({
                      influencerId: this.filterInfluencerList[i].Elv_social,
                      currency_code:this.filterInfluencerList[i].currency_code,
                      cost:
                      this.filterInfluencerList[i].cost
                        ?
                      this.filterInfluencerList[i].cost
                        :
                      this.filterInfluencerList[i].package_cost,
                      postPerCount: this.filterInfluencerList[i].post_needed,
                      perCost: this.filterInfluencerList[i].cost ? "1" : "2",
                      brief: this.filterInfluencerList[i].premium_brief,
                      sample: this.filterInfluencerList[i].premium_sample ? this.filterInfluencerList[i].premium_sample : ""
                  });
                  if(this.campaignDetaillData.status === '3')
                    <FormArray>this.campaignFormPremiumStepTwo['controls'].campaignFormArrayPremium['controls'][i].get('influencerId').disable();
                }
              }
              }else
                this.selectedPlatforms = [];
          }
        }else{
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  //form validation
  camaignCreatePremiumTwoFormVal() {
    this.campaignFormPremiumStepTwo = new FormGroup({
      file: new FormControl(''),
      campaignFormArrayPremium: this._FB.array([
        this.influencerFields()
      ])
    });
  }

  influencerFields(): FormGroup {
    return this._FB.group({
      influencerId: new FormControl('', [Validators.required]),
      currency_code:new FormControl('', [Validators.required]),
      cost: new FormControl('',   Validators.compose([Validators.required, ,Validators.pattern(/^[1-9]\d*$/)])),
      postPerCount:new FormControl('',  Validators.compose([Validators.required, ,Validators.pattern(/^[1-9]\d*$/)])),
      perCost: new FormControl('', [Validators.required]),
      brief: new FormControl('',this.validationData),
      sample: new FormControl(''),
    });
  }

  //Download Sample csv for micro influencer
  downloadSample() {
    let data = [
      {
        name: "Shiwani Dhiman",
        handle: "ShiwaniDhiman5",
        Post: "1",
        PerPostCost: "1",
        PackageCost: "234",
        Brief: "Test"
      },
      {
        name: "M Gawande",
        handle: "murlidhargawan2",
        Post: "1",
        PerPostCost: "1",
        PackageCost: "234",
        Brief: "Test"
      },
      {
        name: "Ahva",
        handle: "ahvadsouzaa",
        Post: "1",
        PerPostCost: "1",
        PackageCost: "234",
        Brief: "Test"
      },
    ];
    let options = {
      showLabels: true,
      headers: ["name", "handle", "Post", "PerPostCost", "PackageCost", "Brief"]
    };
    new AngularCsv(data, 'PremiumSample', options);
  }

  // CSV upload
  uploadCSV($event) {
    this.csvNamePremium = "";
    this.document_csv = <File>$event.target.files[0];
    let name = this.document_csv.name;
    let type = this.document_csv.type;
    let size = this.document_csv.size;
    if (size > 5242880) {
      this.uploadSuccessCSV = false;
      this.error_msg_upload = true;
      this.error_message = "File is too large. Upload file less than 5 MB";
      this.csvNamePremium = "";
      return;
    }
    this.validCSV = (new RegExp('(' + this.exts.join('|').replace(/\./g, '\\.') + ')$')).test(name);
    if (!this.validCSV) {
      this.uploadSuccessCSV = false;
      this.error_msg_upload = true;
      this.error_message = "Upload only CSV formats";
      this.csvNamePremium = "";
    } else {
      this.csvNamePremium = name;
      this.uploadSuccessCSV = true;
    }
  }

  //Clear csv upload
  clearCSV() {
    this.document_csv = null;
    this.csvNamePremium = "";
    this.uploadSuccessCSV = false;
    this.myInputFileVariable.nativeElement.value = "";
  }

  // CSV upload submit
  premiumFileUploadSubmit(platform) {
    if (this.document_csv == null) {
      this._service.error("Must upload a CSV first.");
      return;
    }
    let data = this.auth.getData();
    const form = new FormData();
    form.append('admin_id', data._id);
    form.append('campaign_id', this.campaignId);
    form.append('platform', this.platformName);
    form.append('file', this.document_csv, this.document_csv.name);
    this.animateSubmit = false
    this._service.postFormApi('/campaign/premium/csv/upload', form).subscribe((response: any) => {
      if (response.status == 200) {
        this.data = response.body;
        if (this.data.responseCode == 200) {
          this.animateSubmit = true;
          this.document_csv = null;
          this.csvNamePremium = "";
          this.uploadSuccessCSV = false;
          this.myInputFileVariable.nativeElement.value = "";
          this.premiumInfluencerData = this.data.result;
          this.filterInfluencerList = this.premiumInfluencerData.influencers;
          if( this.influencerRows.length > 0){
            this.removeAllRows();
          }
          for(let i = 0; i < this.filterInfluencerList.length; i++){
            if(i > 0){
              this.influencerRows.push(this.influencerFields());
            }
            <FormArray>this.campaignFormPremiumStepTwo['controls'].campaignFormArrayPremium['controls'][i].patchValue({
              influencerId: this.filterInfluencerList[i].Elv_social,
              cost: this.filterInfluencerList[i].cost,
              postPerCount: "",
              perCost: "",
              brief: "",
              sample:""
            });
          }
        } else {
          this.animateSubmit = true;
          this._service.error(this.data.responseMessage);
        }
      }
    }, error => {
      this.animateSubmit = true;
      this.message = "Something went wrong! Please try again.";
    });
  }

  // filter premium influencer
  influencerList() {
    let data = this.auth.getData();
    let reqData =
    {
      "admin_id": data._id,
      "platform": this.platformName
    }
    this.influencerListData = [];
    this.influencerDataFilter = [];
    this._service.postApiCommon('/campaign/influencers', reqData).subscribe((response: any) => {
      if (response.status == 200) {
        this.data =response. body;
        if (this.data.responseCode == 200) {
          this.influencerListData = [];
          this.influencerDataFilter =  this.data.result;
          for (let i = 0; i < this.data.result.length; i++) {
            this.influencerListData.push({ id: this.data.result[i], name: this.data.result[i].Elv_social })
          }
        } else {
          this.influencerListData = [];
        }
      }
    }, error => {
      this.message = "Something went wrong! Please try again.";
    });
  }

  // influencer dynamic Rows
  get influencerRows() {
    return this.campaignFormPremiumStepTwo.get('campaignFormArrayPremium') as FormArray;
  }
  //Add More rows
  addInfluencerRows() {
    this.influencerRows.push(this.influencerFields());
  }
  //Remove All new rows
  removeAllRows() {
    while (this.influencerRows.length !== 1) {
      this.influencerRows.removeAt(0);
    }
  }
//Remove one new row
  removeOneRow(influIndex){
    this.influencerRows.removeAt(influIndex);
  }

// Change on breif Check box
  change_breif_to_all = event => {
    if(event.checked == true){
      this._service.success('All breif are common.');
      this.validationData = null;
      //this.campaignFormArrayPremium.at(0).controls.breif.clearValidators();
      this.breif_selector = '1';
      return;
    }
    this.validationData = this.breifValidation;
    this.breif_selector = '0';
  }

//Apend all data form selection influencer
  influencerSelectChange = (event,index) => {
    let influencerData = this.influencerDataFilter.filter(list => list.Elv_social == event);
    let influencerRowData  = this.campaignFormPremiumStepTwo.get('campaignFormArrayPremium').value;
    let influencerDevData = influencerRowData.filter(list => list.influencerId == event);
    if(influencerDevData.length > 1){
      this._service.error('You have already added this profile.');
      this.duplicateInfluencer = "1";return;
    }else{
      this.duplicateInfluencer = "";
    }
    if(influencerData){
      <FormArray>this.campaignFormPremiumStepTwo['controls'].campaignFormArrayPremium['controls'][index].patchValue({
        cost: influencerData[0] ? influencerData[0].cost : '',
        postPerCount: '',
        perCost: '',
        brief: '',
        sample:'s'
      })
    }

  }
  // form submit premium influencer
  submitPremiumInfluencerFilter = (reqData, type):any => {
    if(
      this.campaignDetaillData.type
        &&
      this.campaignDetaillData.type != 'amplification'
        &&
      !reqData.campaignFormArrayPremium[0].brief
    )return this._service.error('Description is Mandatory.');

    // if(
    //   reqData.campaignFormArrayPremium[0].brief
    //     &&
    //   reqData.campaignFormArrayPremium[0].brief.length < 50
    // )return this._service.error('Description must be at least 50 characters long..');

    if(this.campaignFormPremiumStepTwo.invalid){
      const FirstInvalid = this.el.nativeElement.querySelectorAll('.ng-invalid')[1];
      FirstInvalid.scrollIntoView({ behavior: 'smooth', block: 'center', inline: 'start' });
      return;
    }
    if(this.duplicateInfluencer)return this._service.error('Influencer profile not duplicate.');
    if(type === '1'){
      if(reqData.campaignFormArrayPremium.length === 0){
        if(this.previousRouteUrl)
          this.router.navigate(['campaign/kol/',this.campaignId]);
        else
          this.router.navigate(['campaign/step-two/',this.campaignId]);
      }
    }else{
      if(reqData.campaignFormArrayPremium.length === 0)
        return this._service.error('Please select atlest one influencer.');
    }
    let premiumData = this.campaignFormPremiumStepTwo.getRawValue().campaignFormArrayPremium;
    this.premiumreqData = [];
    for (let j = 0; j < premiumData.length; j++) {
      let influencerData = this.influencerDataFilter.filter(
        list => list.Elv_social == premiumData[j].influencerId
      );
      this.premiumreqData.push({
        influencer_id: influencerData[0].influencer_id,
        Elv_social: influencerData[0].Elv_social,
        category: influencerData[0].category,
        profile_image_url: influencerData[0].profile_image_url ? influencerData[0].profile_image_url : "",
        post_count: premiumData[j].postPerCount,
        brief: (this.breif_selector === '1') ? premiumData[0].brief : premiumData[j].brief,
        sample: (this.breif_selector === '1') ? premiumData[0].sample : premiumData[j].sample,
        currency_code: premiumData[j].currency_code,
        cost: premiumData[j].perCost == '1'? premiumData[j].cost:"",
        package_cost: premiumData[j].perCost == '2'? premiumData[j].cost:""
      });
    }
    let resData = {
      "admin_id":this.getAdminData(),
      "campaign_id":this.campaignId,
      "platform":this.platformName,
      "platform_id":this.selectedPlatforms ? this.selectedPlatforms._id : "",
      "type":"premium",
      "influencer_count":reqData.campaignFormArrayPremium.length,
      "post_count":"",
      "status":type,
      "influencers":this.premiumreqData,
      "breif_to_all":this.breif_selector
    }
      this._service.postApiWithHeader('/campaign/create_campaign',resData, '2C')
      .subscribe((response: any) => {
        if (response.status == 200) {
          this.data = response.body;
          if (this.data.responseCode == 200) {
            if(type == '1'){
              this._service.success(this.data.responseMessage);
              this.router.navigate(['campaign/list/drafts']);
            }else{
              this._service.success(this.data.responseMessage);
              if(this.previousRouteUrl)
                this.router.navigate(['campaign/kol/',this.campaignId]);
              else
                this.router.navigate(['campaign/step-two/',this.campaignId]);
            }
          } else {
            this._service.error(this.data.responseMessage)
          }
        }
      }, error => {
        this._service.error('Something went wrong! Please try again.')
      });

  }

  postPerCountChange = influIndex => {
    if(
      this.campaignDetaillData
        &&
      this.campaignDetaillData.status === "3"
    ){
      let influencerData = this.influencerRows.at(influIndex);
     }
  }

  //editor config options
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '0',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'no',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    //uploadUrl: '',
    sanitize: true,
    toolbarPosition: 'top',
  };
}
