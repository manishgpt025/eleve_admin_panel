import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonFileModule } from './common-file/common-file.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { Ng5SliderModule } from 'ng5-slider';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatSelectModule,
    MatRadioModule,
    MatDatepickerModule,
    MatExpansionModule
} from '@angular/material';
import {MatNativeDateModule} from '@angular/material/core';
import { ModalComponent } from './_modal/modal/modal.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { NgSelectModule } from '@ng-select/ng-select';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';

import { UpdatePasswordComponent } from './auth/update-password/update-password.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { AuthService } from './services';
import { AuthGuard } from './_guard/auth.guard';
import { LoginGuard } from './_guard/login.guard';
import { ErrorInterceptor } from './_helper/error.interceptor';
import { FourZeroFourComponent } from './not-found/four-zero-four/four-zero-four.component';
import { FourZeroThreeComponent } from './not-found/four-zero-three/four-zero-three.component';
import { SkeletonComponent } from './skeleton/skeleton.component';
import { CapitalizeFirstLetter } from "./pipes/capitalize-first-letter";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ModalComponent,
    UpdatePasswordComponent,
    AdminProfileComponent,
    FourZeroFourComponent,
    FourZeroThreeComponent,
    SkeletonComponent,
    // CapitalizeFirstLetter
  ],
  imports: [
    BrowserModule,
    NgxMatSelectSearchModule,
    CommonFileModule,
    AppRoutingModule,
    HttpClientModule,
    Ng2ImgMaxModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    MatNativeDateModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatNativeDateModule,
    MatSelectModule,
    MatCheckboxModule,
    MatRadioModule,
    Ng5SliderModule,
    MatDatepickerModule,
    MatExpansionModule,
    ToastrModule.forRoot(), // ToastrModule added
    NgxSpinnerModule,
    NgxDaterangepickerMd.forRoot()
  ],
  providers: [AuthService,
    AuthGuard,
    LoginGuard,
   { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
