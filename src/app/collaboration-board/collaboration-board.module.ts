import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonFileModule } from '../common-file/common-file.module';
import { CollaborationBoardRoutingModule } from './collaboration-board-routing.module';
import { CollaborationLayoutComponent } from './collaboration-layout/collaboration-layout.component';
import { BoardComponent } from './board/board.component';
import { MatMenuModule, MatNativeDateModule, MatDatepickerModule, MatButtonModule, MatCheckboxModule, MatInputModule, MatExpansionModule, MatSelectModule, MatTabsModule, MatProgressBarModule, MatRadioModule } from '@angular/material';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [CollaborationLayoutComponent, BoardComponent],
  imports: [
    CommonModule,
    CommonFileModule,
    CollaborationBoardRoutingModule,
    MatTabsModule,
    MatMenuModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatExpansionModule,
    MatSelectModule,
    MatProgressBarModule,
    MatRadioModule,
    AngularEditorModule,
    HttpClientModule
    
  ]
})
export class CollaborationBoardModule { }
