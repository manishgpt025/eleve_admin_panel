import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CollaborationLayoutComponent } from './collaboration-layout/collaboration-layout.component';
import { BoardComponent } from './board/board.component';
const routes: Routes = [
  {
    path: '', component: CollaborationLayoutComponent,
    children: [
      { path: '', component: BoardComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollaborationBoardRoutingModule { }
