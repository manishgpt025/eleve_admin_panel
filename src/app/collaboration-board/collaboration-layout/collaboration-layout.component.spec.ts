import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborationLayoutComponent } from './collaboration-layout.component';

describe('CollaborationLayoutComponent', () => {
  let component: CollaborationLayoutComponent;
  let fixture: ComponentFixture<CollaborationLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaborationLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborationLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
