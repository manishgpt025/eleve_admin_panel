import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';


@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit {
  divAppendOnReport: string="";
  divAppendOnSubmission: string="";
  divAppendOnEcontract: string="";
  divAppendOnProfilePlatfroms: string="";
  divAppendOnSingleInviteModal: string="";
  divAppendOnBriefSidebar: string="";
  show_modal_all_invite:boolean=false;

  constructor() { }

  ngOnInit() {
  }
  
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '0',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
};

  report_btn_click(type:String){
    if(type == "1")
      this.divAppendOnReport="report_modal_show";
    else
      this.divAppendOnReport="";
  }

  submission_btn_click(type:String){
    if(type == "1")
      this.divAppendOnSubmission="report_modal_show";
    else
      this.divAppendOnSubmission="";
  }

  Econtract_btn_click(type:String){
    if(type == "1")
      this.divAppendOnEcontract="report_modal_show";
    else
      this.divAppendOnEcontract="";
  }

  ProfilePlatfroms_btn_click(type:String){
    if(type == "1")
      this.divAppendOnProfilePlatfroms="report_modal_show";
    else
      this.divAppendOnProfilePlatfroms="";
  }

  BriefSidebar_btn_click(type:String){
    if(type == "1")
      this.divAppendOnBriefSidebar="brief_sidebar_show";
    else
      this.divAppendOnBriefSidebar="";
  }

  SingleInviteModal_btn_click(type:String){
    if(type == "1")
      this.divAppendOnSingleInviteModal="report_modal_show";
    else
      this.divAppendOnSingleInviteModal="";
  }

  openAllInviteModal = (type:any):void => {
    if(type === 1)
    this.show_modal_all_invite = true;
    else
    this.show_modal_all_invite = false;
  }

  

}
