import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CollaborationCardLayoutComponent } from './collaboration-card-layout/collaboration-card-layout.component';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
const routes: Routes = [
  {
    path: '', component: CollaborationCardLayoutComponent,
    children: [
      { path: '', redirectTo: 'list' },
      { path: 'list', component: ListComponent },
      { path: 'create', component: CreateComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollaborationRoutingModule { }
