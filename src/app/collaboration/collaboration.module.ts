import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonFileModule } from '../common-file/common-file.module';
import { CollaborationRoutingModule } from './collaboration-routing.module';
import { ListComponent } from './list/list.component';
import { CreateComponent } from './create/create.component';
import {
  MatMenuModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatExpansionModule,
  MatSelectModule,
  MatTabsModule,
  MatProgressBarModule,
  MatRadioModule,
  MatAutocompleteModule
} from '@angular/material';
import { CollaborationCardLayoutComponent } from './collaboration-card-layout/collaboration-card-layout.component';
@NgModule({
  declarations: [ListComponent, CreateComponent, CollaborationCardLayoutComponent],
  imports: [
    CommonModule,
    CommonFileModule,
    CollaborationRoutingModule,
    MatTabsModule,
    MatMenuModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatExpansionModule,
    MatSelectModule,
    MatProgressBarModule,
    MatRadioModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class CollaborationModule { }
