import { Component, OnInit, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { AuthService } from 'src/app/services/auth.service';
import { AdminService } from 'src/app/services/admin.service';
import {
  handleSelectedValues,
  getMimeType,
  getFileName,
  getBoardId,
  getFileIcon
} from "src/app/handlers";

import isdCodes from 'src/app/handlers/isdCodes.json';
import boardUtils from 'src/app/handlers/boardUtils.json';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  host: {
    '(document:click)': 'setGenreListDropdown($event)',
  }
})

export class CreateComponent implements OnInit {
  board: FormGroup;
  editBoardId: string = "";
  utils: any = boardUtils;
  allServices: any = boardUtils.services;
  influencersCategories: any = boardUtils.influencer_categories;
  allActivities: any = boardUtils.activities;
  selectedInfluencersCategories: any = [];
  selectedServices: any = [];
  selectedActivities: any = [];
  selectedCollegues: any = [];
  isdCodes: object = isdCodes;
  uploadedContent: any = [];
  brands: any = [];
  collegues: any = [];
  allCollegues: any = [];
  selectedCurrency: string = "INR";
  selectedPlatforms: any = [];
  selectedHashtags: any = [];
  selectedGenres: any = [];
  selectedDeliverables: any = {};
  genreList: any = [];
  allGenres: any = [];
  genreSearchToggle: boolean = false;
  loaders = {
    isFileUploaded: false,
    dataLoader: false
  }

  constructor (
    private auth: AuthService,
    private _service: AdminService,
    private _router: Router,
    private el: ElementRef,
    private route: ActivatedRoute
  ) {}

  ngOnInit(){
    this.route.params.subscribe( params => {
      this.editBoardId = params.id;
    });
    this.initBoard();
    this.searchBrands();
    this.getCollegues();
    this.getInfluencerGenres();
    if(this.editBoardId !== undefined || this.editBoardId ){
      this.getBoard(this.editBoardId);
    }
  }

  initBoard = () => {
    this.board = new FormGroup({
      title: new FormControl('', [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200)
        ]
      ),
      brand_name: new FormControl('', Validators.required),
      budget: new FormGroup({
        code: new FormControl(this.selectedCurrency),
        cost: new FormControl('', [Validators.pattern(/^[1-9]\d*(\.\d+)?$/)]),
      }),
      campaign_live_date: new FormControl(''),
      focus: new FormControl(''),
      objective: new FormControl(''),
      problem_statment: new FormControl(''),
      key_points: new FormControl(''),
      target_audience: new FormControl(''),
      output_expected: new FormControl(''),
      brief_overview: new FormControl(''),
      influencers_count: new FormControl(''),
      post_per_inf: new FormControl(''),
    });
  }

  handleServices = (service: String) => {
    this.selectedServices = handleSelectedValues(
      service, this.selectedServices
    );
  }

  handleCategoryChange = (category: String) => {
    this.selectedInfluencersCategories = handleSelectedValues(
      category, this.selectedInfluencersCategories
    )
  }

  handleActivityChange = (activity: String) => {
    this.selectedActivities = handleSelectedValues(
      activity, this.selectedActivities
    );
  }

  handleAddCollegue = (id: String) => {
    this.selectedCollegues = handleSelectedValues(
      id, this.selectedCollegues
    );
  };

  createBoard = () => {
    if(this.board.invalid){
      const FirstInvalid = this.el.nativeElement
      .querySelectorAll('.ng-invalid')[1];
      FirstInvalid.scrollIntoView(
        {
          behavior: 'smooth',
          block: 'center',
          inline: 'start'
        }
      );
      return;
    }
    let isEditing = false;
    let params = {...this.board.value};
    params = {
      ...params,
      colleagues: this.selectedCollegues,
      services: this.selectedServices,
      activity_types: this.selectedActivities,
      file_attached: this.uploadedContent,
      influencer_type: this.selectedInfluencersCategories,
      hashtags: this.selectedHashtags,
      platform_deliverables: this.selectedDeliverables,
      genre: this.selectedGenres,
      platforms: this.selectedPlatforms,
    };
    if (this.editBoardId) {
      params = {
        ...params,
        board_id: this.editBoardId
      }
      isEditing = true;
    }
    let route = isEditing ? `collaboration-board/${this.editBoardId}` : `collaboration/list`;
    if (this.board.valid) {
      this.loaders.dataLoader = true;
      this._service.postApiCommon('/boards/create', params)
        .subscribe((response: any) => {
          if (response.body.responseCode === 200) {
            this._service.success(
              isEditing ?  'Board Updated!' : 'Board Created!'
            );
            this._router.navigate([route]);
            isEditing = false;
            this.loaders.dataLoader = false;
          }else this.loaders.dataLoader = false;
        }, error => {
          this._service.error("Something went wrong! Please try again.");
        });
    }
  }

  getBoard = boardId => {
    this.loaders.dataLoader = true;
    this._service.getApiCommon('/boards/' + boardId)
      .subscribe((response: any) => {
      if (response.body.responseCode == 200) {
        let board = response.body.result;
        Object.keys(this.board.controls).forEach(field => {
            this.board.get(field).setValue(board[field]);
        });
        this.selectedServices = board.services;
        this.selectedActivities = board.activity_types;
        this.uploadedContent = board.file_attached;
        this.selectedInfluencersCategories = board.influencer_type;
        this.selectedHashtags = board.hashtags;
        this.selectedPlatforms = (
          Object.keys(this.selectedDeliverables).length > 0 &&
          Object.keys(this.selectedDeliverables[0])
        )
        this.selectedDeliverables = board.platform_deliverables[0];
        this.selectedGenres = board.genre;
        this.selectedCollegues = board.colleagues.map(
          colleague => (colleague.cid)
        );
        this.selectedPlatforms = board.platforms;
        this.loaders.dataLoader = false;
      }
    }, error => {
      this._service.hide();
      this._service.error("Something went wrong! Please try again.");
    });
  }

  //gets currency value
  handleCurrencyChange = event => {
    // get change event here
  }

  searchBrands = () => {
    this._service.postApiCommon('/boards/search/brand', {})
      .subscribe((response: any) => {
        if (response.body.responseCode === 200) {
          this.brands = [...response.body.result];
        }
        if (response.body.responseCode === 404) {
          this._service.error("No Brands Found");
        }
      }, error => {
        this._service.error("Something went wrong! Please try again.");
      });
  }

  getCollegues = () => {
    const {organisation_id } =  this.auth.getData();
    const params = {
    	organisation_id
    };
    this._service.postApiCommon('/boards/boardcolleague', params)
      .subscribe((response: any) => {
        if (response.body.responseCode === 200) {
          this.collegues = this.allCollegues = [...response.body.result];
        }
        if (response.body.responseCode === 404) {
          this._service.error("No colleagues Found");
        }
      }, error => {
        this._service.error("Something went wrong! Please try again.");
      });
  }

  // getBrandColleagues = (organisation_id: string) => {
  //   if (organisation_id) {
  //     this._service.postApiCommon(routes.getBrandColleagues, { organisation_id })
  //       .subscribe((response: any) => {
  //         if (response.body.responseCode === 200) {
  //           this.collegues = this.allCollegues = [
  //             ...this.collegues, ...response.body.result
  //           ];
  //         }
  //       }, error => {
  //         this._service.error("Something went wrong! Please try again.");
  //       });
  //   }
  // }

  handleGenreSearch = event => {
    if (event.target.value.length > 0) {
      let genres = [...this.allGenres];
      let tempGenres = genres.filter(genre => {
        let { subgenre = '' } = genre;
        if (subgenre && subgenre.toLowerCase().indexOf(
          event.target.value.toLowerCase()
        ) > -1) {
          return genre
        }
      });
      return this.genreList = tempGenres;
    } else {
      return this.genreList = [...this.allGenres];
    }
  }

  selectGenre = (genre: String, event) => {
    event.stopPropagation();
    this.selectedGenres = handleSelectedValues(
      genre, this.selectedGenres
    );
  }

  handleGenreList = event => {
    event.stopPropagation();
    this.genreSearchToggle = true;
  }

  setGenreListDropdown = event => {
    event.stopPropagation();
    this.genreSearchToggle = false;
  }

  getInfluencerGenres = () => {
    const { _id } =  this.auth.getData();
    const params = {
      user_id: _id,
    };
    this._service.postApiCommon('/influencer/genreList', params)
      .subscribe((response: any) => {
        if (response.body.responseCode === 200) {
          this.genreList = response.body.result.genre_list;
          this.allGenres = response.body.result.genre_list;
        }
      }, error => {
        this._service.error("Something went wrong! Please try again.");
      });
  }

  searchCollegue = event => {
    if (event.target.value.length > 0) {
      let collegues = [...this.allCollegues];
      let tempCollegues = collegues.filter(collegue => {
        let { name = '' } = collegue;
        if (name && name.toLowerCase().indexOf(
          event.target.value.toLowerCase()
        ) > -1) {
          return collegue
        }
      });
      return this.collegues = tempCollegues;
    } else {
      return this.collegues = [...this.allCollegues];
    }
  }

  handlePreferredHashtags = event => {
    let pattern = /(?:\S|^)#[A-Za-z0-9\_]+($)/;
    if(event.target.value.length > 0 && event.keyCode === 13) {
      if (pattern.test(event.target.value)) {
        this.selectedHashtags = this.handleHashtags(
          event, this.selectedHashtags, 5
        );
      } else {
        this._service.error(`Hashtag not valid`);
      }
    }
  }

  addPlatformDeliverables = (platform, deliverable) => {
    let tempArr = [];
    const { [(platform)]: selected = [] } = this.selectedDeliverables;
    let index = selected.findIndex(item => item === deliverable);
    if (index === -1) {
      tempArr = [...selected, deliverable];
    } else {
      tempArr.splice(index, 1);
    }
    this.selectedDeliverables = {
      ...this.selectedDeliverables,
      [(platform)]: [...tempArr]
    };
  }

  handleHashtags = (event, selectedItems, hashtagsAllowed) => {
    let allHashtags = [...selectedItems];
    let index = allHashtags.findIndex(badge => badge === event.target.value);
    if (allHashtags.length === hashtagsAllowed) {
      this._service.error(`Max ${hashtagsAllowed} hashtags can be entered`);
      return selectedItems;
    }
    if (index != -1) {
      this._service.error("Cannot add the same hashtag twice");
      return selectedItems;
    }
    selectedItems = [...allHashtags, event.target.value];
    event.target.value = '';
    return selectedItems;
  }

  removeHashtags = tag => {
    let allHashtags = [...this.selectedHashtags];
    let index = allHashtags.findIndex(badge => badge === tag);
    if (index != -1) {
      this.selectedHashtags.splice(index, 1);
    }
  }

  dragOverHandler(ev) {
    ev.currentTarget.setAttribute(
      "style", "border: 2px dashed #2196f3"
    );
    ev.preventDefault();
  }

  dropHandler(ev) {
    let files = [];
    ev.currentTarget.setAttribute(
      "style", "border: 1px dashed #ddd"
    );
    ev.preventDefault();
    if (ev.dataTransfer.items) {
      for (var i = 0; i < ev.dataTransfer.items.length; i++) {
        if (ev.dataTransfer.items[i].kind === 'file') {
          let file = ev.dataTransfer.items[i].getAsFile();
          files.push(file);
        }
      }
      this.onFileUpload(files);
    } else {
      for (var i = 0; i < ev.dataTransfer.files.length; i++) {
        // console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
      }
    }
  }

  //upload Video from input
  uploadVideoFromInput = (event) => {
    let files = event.target.files;
    this.onFileUpload(files);
  }

  onFileUpload = (files: any) => {
    let formData = new FormData();
    formData.append("flag", "3");
    for (var i = 0; i < files.length; i++) {
      formData.append("file", files[i], files[i].name);
    }
    this.loaders.isFileUploaded = true;
    this._service.postFormApi('/boards/upload', formData)
      .subscribe((response: any) => {
        if (response.body.responseCode === 200) {
          if (response.body.result.length > 0) {
            response.body.result.map(file =>
              this.uploadedContent.push({
                image: file.key
              })
            );
          }
          this.loaders.isFileUploaded = false;
          this._service.success("File Uploaded Successfully");
        }
      }, error => {
        this._service.error("Something went wrong! Please try again.");
      });
  }

  handleFileIcons = (file: string) => {
   return getFileIcon(file);
  }

  setPlatformOptions = (platform: String) => {
    this.selectedPlatforms = handleSelectedValues(
      platform, this.selectedPlatforms
    )
  }

  getFilename = (file:string) => {
    return getFileName(file)
  }

  removeUploadFile = (file: string) => {
    let uploadedFiles = [...this.uploadedContent];
    let index = uploadedFiles.findIndex(item => item.image === file);
    if (index != -1) {
      this.uploadedContent.splice(index, 1);
      this._service.success("File Removed");
    }
  }

  discardBoard = () => {
    this._router.navigate(['collaboration/list']);
  }

}
