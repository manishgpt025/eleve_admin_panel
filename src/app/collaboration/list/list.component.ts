import { Component, OnInit, HostListener } from '@angular/core';
import { AdminService } from 'src/app/services/admin.service';
import { ActivatedRoute, Router } from '@angular/router';
import { getNameInitials } from "src/app/handlers";
import * as moment from "moment";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  moment: any = moment;
  boards: any = [];
  activeBoards: Array<object> = [];
  closedBoards: Array<object> = [];
  loaders = {
    initLoader: false
  };
  activePageDetails = {
    page: 1,
    pages: 1,
    limit: 20
  }
  closedPageDetails = {
    page: 1,
    pages: 1,
    limit: 20
  }
  boardStatus = 1;
  sortIndex = 2;
  sortLabel = 'Updated';

  constructor(private _service: AdminService, private _router: Router) {}

  ngOnInit(){
    this.getBoards();
  }

  getBoardByStatus = (index) => {
    if (index === 0) {
      this.boardStatus = 1;
    } else {
      this.boardStatus = 0;
    }
    const key = this.boardStatus === 0 ? 'closedBoards' : 'activeBoards';
    if (!this[key].length) {
      this.getBoards();
    }
  }

  setData = (data) => {
    const key = this.boardStatus === 0 ? 'closedBoards' : 'activeBoards';
    this[key] = [...this[key], ...data];
  }

  // get all boards
  getBoards = () => {
    const boardStatus = this.boardStatus;
    const key = boardStatus === 0 ? 'closed' : 'active';
    this.loaders.initLoader = true;
    this._service.getApiCommon(
      `/boards/list?sort=${this.sortIndex}&page_number=${this[key+`PageDetails`].page}&limit=20&status=${this.boardStatus}`
    ).subscribe((response: any) => {
      if (response.body.responseCode === 200) {
        this.setData(response.body.result.boards);
        this.loaders.initLoader = false;
        this.setPageDetails(response.body.result, boardStatus);
      }
    }, error => {
      this._service.error("Something went wrong! Please try again.");
    });
  }

  sortList = (sortVal: number, label: string) => {
    this.sortIndex = sortVal;
    this.sortLabel = label;
    this.boards = []; this.activeBoards = []; this.closedBoards = [];
    this.getBoards();
  }

  getToBoard = (boardId: string, name: string) => {
    this._router.navigate(
      [`/collaboration-board/${boardId}`],
      {
        state:
          {
            data: {
              boardId,
              name
            }
          }
        }
      );
  }

  renderInitials = (name: any) => {
    return getNameInitials({ name })
  }

  createBoard = () => {
    this._router.navigate(['/board/create']);
  }

  getDefaultPageDetails = () => ({
    page: 1,
    pages: 1,
    limit: 20
  });

  setPageDetails = ({ page, pages, limit }, status): void => {
    const key = status === 0 ? 'closed' : 'active';
    this[key+`PageDetails`] = {
      page,
      pages,
      limit
    };
  };

  decideNextPage = ({ page: currentPage, pages: totalPages, loading }): any =>
  {
    let shouldCallDataAgain = true;
    if (
      !loading &&
      (totalPages > currentPage) &&
      (((window.innerHeight + window.scrollY) + 200)
      >= document.body.offsetHeight)
    ) {
      return {
        shouldCallDataAgain,
        nextPage: ++currentPage
      };
    }
    return {
      shouldCallDataAgain: false,
      nextPage: currentPage
    };
  };

  @HostListener('window:scroll', ['$event'])
  onScrollEvent = (): void => {
    const key = this.boardStatus === 0 ? 'closed' : 'active';
    const {
      shouldCallDataAgain,
      nextPage,
    } = this.decideNextPage({
      loading: this.loaders.initLoader,
      ...this[key+`PageDetails`]
    });
    if (shouldCallDataAgain) {
      this[key+`PageDetails`] = {
        ...this[key+`PageDetails`],
        page: nextPage
      }
      this.getBoards();
    }
  };


}
