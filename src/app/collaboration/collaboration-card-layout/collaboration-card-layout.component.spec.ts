import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollaborationCardLayoutComponent } from './collaboration-card-layout.component';

describe('CollaborationCardLayoutComponent', () => {
  let component: CollaborationCardLayoutComponent;
  let fixture: ComponentFixture<CollaborationCardLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollaborationCardLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborationCardLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
