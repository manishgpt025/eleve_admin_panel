import { Pipe, PipeTransform } from "@angular/core";

@Pipe ({
   name : 'capitalizeFirstLetter'
})
export class CapitalizeFirstLetter implements PipeTransform {
   transform(val : string) : string {
     if (val) {
       return val.slice(0, 1).toUpperCase() + val.slice(1);
     }
     return "";
   };
}